FROM openjdk:11.0.13-jdk-slim

WORKDIR /opt/app

RUN apt update -y && apt install -y maven

COPY . .
ARG BUILD_ENV=production

RUN cp src/main/resources/application-${BUILD_ENV}.properties src/main/resources/application.properties
RUN mvn package -Dmaven.test.skip=true

RUN mkdir -p /usr/local/pmrms
RUN cp target/pmrms-awp-activity-1.0.0.jar /usr/local/pmrms/pmrms-awp-activity.jar
WORKDIR /usr/local/pmrms
CMD ["java", "-Xmx1024M", "-jar", "pmrms-awp-activity.jar", "-Djava.awt.headless=true"]
