package id.go.kemenag.madrasah.pmrms.awp.activity

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import java.util.*
import javax.annotation.PostConstruct

@SpringBootApplication
class PmrmsAwpActivityApplication {

    @PostConstruct
    fun started() {
        // set JVM timezone as UTC
        TimeZone.setDefault(TimeZone.getTimeZone("GMT+7"))
    }
}

fun main(args: Array<String>) {
    runApplication<PmrmsAwpActivityApplication>(*args)
}
