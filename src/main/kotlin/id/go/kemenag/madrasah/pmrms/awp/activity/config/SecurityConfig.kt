package id.go.kemenag.madrasah.pmrms.awp.activity.config

import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.builders.WebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter


@Configuration
class SecurityConfig : WebSecurityConfigurerAdapter() {
    override fun configure(http: HttpSecurity) {
        http.httpBasic()?.disable()
        http.cors()?.and()?.csrf()?.disable()
    }

    override fun configure(web: WebSecurity?) {
        web?.ignoring()?.antMatchers("/css/**")
        web?.ignoring()?.antMatchers("/scripts/**")
        web?.ignoring()?.antMatchers("/images/**")
    }
}
