package id.go.kemenag.madrasah.pmrms.awp.activity.constant

const val APP_FIND_COORDINATOR_LIMIT = 20

const val APP_LSP_ALLOW_CREATE_REPORT_DAYS_LIMIT = 14
