package id.go.kemenag.madrasah.pmrms.awp.activity.constant

const val APPROVAL_STATUS_WAITING = 0

const val APPROVAL_STATUS_APPROVE = 1

const val APPROVAL_STATUS_REJECT = 2
