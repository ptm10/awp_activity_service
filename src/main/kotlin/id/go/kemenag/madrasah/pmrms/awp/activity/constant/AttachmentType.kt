package id.go.kemenag.madrasah.pmrms.awp.activity.constant

const val ATTACHMENT_TYPE_LAPORAN = 1

const val ATTACHMENT_TYPE_PELAKSANAAN = 2

const val ATTACHMENT_TYPE_FINANSIAL = 3

const val ATTACHMENT_TYPE_FOTO_KEGIATAN = 4
