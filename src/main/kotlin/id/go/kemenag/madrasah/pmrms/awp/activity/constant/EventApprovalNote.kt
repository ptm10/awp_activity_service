package id.go.kemenag.madrasah.pmrms.awp.activity.constant

const val EVENT_APPROVAL_MESSAGE_FROM_COORDINATOR = 1

const val EVENT_APPROVAL_MESSAGE_FROM_PMU_TREASURER = 2

const val EVENT_APPROVAL_MESSAGE_FROM_PMU_HEAD = 3

