package id.go.kemenag.madrasah.pmrms.awp.activity.constant

const val EVENT_DOCUMENT_TYPE_ID_AGENDA = "01877c70-f826-11ec-8471-d7a15ddca888"

const val EVENT_DOCUMENT_TYPE_NOTULENSI = "1063726c-f826-11ec-8472-ab8c193aa06a"

const val EVENT_DOCUMENT_PROFILE_NARASUMBER = "1b4b79fe-f826-11ec-8473-cb599c5e97e5"

const val EVENT_DOCUMENT_BAHAN_NARASUMBER = "23525370-f826-11ec-8474-7305f18b1458"

const val EVENT_DOCUMENT_TYPE_FOTO = "2b6fe022-f826-11ec-8475-7b0b5ab006af"

const val EVENT_DOCUMENT_TYPE_JADWAL_ACARA = "6b0968d6-f833-11ec-8ad9-c721cf21388c"

val EVENT_DOCUMENT_TYPE_PROJECT_REPORT_NAMES = mapOf(
    EVENT_DOCUMENT_TYPE_ID_AGENDA to "Lampiran 1: Concept Note",
    EVENT_DOCUMENT_TYPE_JADWAL_ACARA to "Lampiran 2: Realisasi Pelaksanaan Event",
    EVENT_DOCUMENT_PROFILE_NARASUMBER to "Lampiran 3: Narasumber/Pembahas ",
    EVENT_DOCUMENT_TYPE_NOTULENSI to "Lampiran 4: Notulasi Event",
    EVENT_DOCUMENT_TYPE_FOTO to "Lampiran 5: Foto-foto Event",
    EVENT_DOCUMENT_BAHAN_NARASUMBER to "Lampiran 6: Materi Pembahasan / Materi Narasumber"
)
