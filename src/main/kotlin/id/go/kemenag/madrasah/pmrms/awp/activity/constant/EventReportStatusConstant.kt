package id.go.kemenag.madrasah.pmrms.awp.activity.constant

const val EVENT_REPORT_STATUS_DRAFT = 0

const val EVENT_REPORT_STATUS_REVISION = 1

const val EVENT_REPORT_STATUS_APPROVED = 2
