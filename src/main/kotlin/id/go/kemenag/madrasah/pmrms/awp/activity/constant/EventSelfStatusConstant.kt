package id.go.kemenag.madrasah.pmrms.awp.activity.constant

const val EVENT_SELF_STATUS_DONE = 1

const val EVENT_SELF_ON_GOING = 2

const val EVENT_SELF_PLANNING = 3

const val EVENT_SELF_SEARCH_FIELD = "eventSelfStatus"

