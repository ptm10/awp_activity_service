package id.go.kemenag.madrasah.pmrms.awp.activity.constant

val MONEV_QUESTION_MAPPING_BY_TYPE_TO_QUESTION = mapOf(
    "afedb8dc-d1ee-11ec-88cb-0ba398ed0298" to listOf(
        "513138f4-e554-11ec-8472-97ee0d91482d",
        "5b74792a-e554-11ec-8473-cb222a8171db",
        "6725111c-e554-11ec-8474-5bca22e1027c",
        "7d51f586-e554-11ec-8475-bfe27a44d370"
    ), "bb34f0da-d1ed-11ec-88c8-432a3415376c" to listOf(
        "06e6097c-e569-11ec-adf9-9f5c9bd195ba",
        "16034d7a-e569-11ec-adfa-eb6f950e5989",
        "22d8b260-e569-11ec-adfb-1fe0a4a0104f",
        "3b7203cc-e563-11ec-adf3-bfb4ad6850f9"
    )
)

val MONEV_QUESTION_MAPPING_BY_MONEV_TO_QUESTION = mapOf(
    "79f46752-d1f4-11ec-8417-9b8a3e0d058e" to listOf(
        "23dc524e-e563-11ec-adf0-d7c35826b9f4",
        "2f6263e2-e563-11ec-adf1-0fd8ee64a694",
        "3691b4a6-e563-11ec-adf2-7b4c4c054f92",
        "3b7203cc-e563-11ec-adf3-bfb4ad6850f9"
    ), "79f4eae3-d1f4-11ec-8419-d7384900226a" to listOf(
        "70a6d9dc-e563-11ec-adf4-171e3505957d",
        "7dc15a7a-e563-11ec-adf5-239d63718683",
        "8326e23c-e563-11ec-adf6-cfc482c7eba1",
        "3b7203cc-e563-11ec-adf3-bfb4ad6850f9"
    ), "c5bd8f0e-d26e-11ec-87b4-8b4189889f06" to listOf(
        "5d7649fe-e0d8-11ec-8b4d-c7117d8bacd5",
        "66f1f3de-e0d8-11ec-8b4e-b79e65209af9",
        "6d900686-e0d8-11ec-8b4f-0b1cb785abb8"
    ), "c5be2b62-d26e-11ec-87b5-63419c877203" to listOf(
        "771a5b20-e0d8-11ec-8b50-b322ae10dd11",
        "66f1f3de-e0d8-11ec-8b4e-b79e65209af9",
        "6d900686-e0d8-11ec-8b4f-0b1cb785abb8"
    ), "c5be7978-d26e-11ec-87b8-3fc130af2bf9" to listOf(
        "845abfa0-e0d8-11ec-8b53-9719f1822b2e",
        "66f1f3de-e0d8-11ec-8b4e-b79e65209af9",
        "6d900686-e0d8-11ec-8b4f-0b1cb785abb8"
    ), "c5bec798-d26e-11ec-87ba-ebb2b8362161" to listOf(
        "9ad794f6-e0d8-11ec-8b56-3b78b8765b20",
        "66f1f3de-e0d8-11ec-8b4e-b79e65209af9",
        "6d900686-e0d8-11ec-8b4f-0b1cb785abb8"
    ), "c5beeea8-d26e-11ec-87bc-370457927c25" to listOf(
        "aa5f0698-e0d8-11ec-8b59-fbac6a1fc6ce",
        "66f1f3de-e0d8-11ec-8b4e-b79e65209af9",
        "6d900686-e0d8-11ec-8b4f-0b1cb785abb8"
    ), "c5beeea9-d26e-11ec-87bd-3bf95ccfc9bb" to listOf(
        "b76f2a8e-e0d8-11ec-8b5c-c35229cab5d0",
        "66f1f3de-e0d8-11ec-8b4e-b79e65209af9",
        "6d900686-e0d8-11ec-8b4f-0b1cb785abb8"
    ), "c5bf15b9-d26e-11ec-87bf-3b8e3d73573e" to listOf(
        "c393d7ba-e0d8-11ec-8b5f-4b51a8f07ea0",
        "66f1f3de-e0d8-11ec-8b4e-b79e65209af9",
        "6d900686-e0d8-11ec-8b4f-0b1cb785abb8"
    ), "c5bf3cbe-d26e-11ec-87c1-a361e46cd482" to listOf(
        "d08a4396-e0d8-11ec-8b62-efae2ebaf03e",
        "66f1f3de-e0d8-11ec-8b4e-b79e65209af9",
        "6d900686-e0d8-11ec-8b4f-0b1cb785abb8"
    ), "c5bf3cbf-d26e-11ec-87c2-c3a41e3bb45c" to listOf(
        "e695d8bc-e0d8-11ec-8b65-e3bb33a80f93",
        "66f1f3de-e0d8-11ec-8b4e-b79e65209af9",
        "6d900686-e0d8-11ec-8b4f-0b1cb785abb8"
    ), "c5bf63d9-d26e-11ec-87c4-5f95b823ca78" to listOf(
        "f54022f0-e0d8-11ec-8b68-67bb2030a2b8",
        "66f1f3de-e0d8-11ec-8b4e-b79e65209af9",
        "6d900686-e0d8-11ec-8b4f-0b1cb785abb8"
    )
)
