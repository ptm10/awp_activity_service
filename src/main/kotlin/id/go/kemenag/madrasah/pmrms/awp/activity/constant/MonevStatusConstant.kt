package id.go.kemenag.madrasah.pmrms.awp.activity.constant

const val MONEV_STATUS_NOT_APPLICABLE = 1

const val MONEV_STATUS_OFF_TRACK = 2

const val MONEV_STATUS_PATRIALLY_ON_TRACK = 3

const val MONEV_STATUS_ON_TRACK = 4

const val MONEV_STATUS_ACHIEVED = 5
