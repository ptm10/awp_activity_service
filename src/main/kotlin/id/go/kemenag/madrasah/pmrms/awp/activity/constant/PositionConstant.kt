package id.go.kemenag.madrasah.pmrms.awp.activity.constant

const val POSITION_HEAD_PMU = "PMU Head"

const val POSITION_SECRETARY_PMU = "Secretary PMU"

const val POSITION_TREASURER_PMU = "Treasurer PMU"

const val POSITION_COORDINATOR = "Coordinator"

const val POSITION_PROJECT_MANAGEMENT_SPECIALIST = "Project Management Specialist"

