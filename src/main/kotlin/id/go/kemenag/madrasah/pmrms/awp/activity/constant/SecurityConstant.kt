package id.go.kemenag.madrasah.pmrms.awp.activity.constant

const val TOKEN_PREFIX = "Bearer "
const val HEADER_STRING = "Authorization"

val USER_ADMIN_ALLOWED_PATH =
    listOf(
        "/component/*",
        "/awp/*",
        "/awp-implementation/*",
        "/awp-implementation-report/*",
        "/event-type/*",
        "/event-management-type/*",
        "/province/*",
        "/regency/*",
        "/quality-at-entry-question/*",
        "/event/*",
        "/event-progress-report/*",
        "/event-report/*",
        "/monev/*",
        "/awp-data/*",
        "/participant-target-position/*",
        "/monev-result-chain/*",
        "/monev-success-indicator/*",
        "/monev-question/*",
        "/event-form/*",
        "/event-question/*",
        "/event-document-type/*",
        "/event-question-answer/*"
    )

val AUDIENCE_FILTER_PATH = mapOf(
    "user-admin" to USER_ADMIN_ALLOWED_PATH
)
