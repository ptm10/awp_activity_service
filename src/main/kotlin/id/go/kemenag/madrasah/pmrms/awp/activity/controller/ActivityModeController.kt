package id.go.kemenag.madrasah.pmrms.awp.activity.controller

import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.awp.activity.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.awp.activity.service.ActivityModeService
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@Api(tags = ["Activity Mode"], description = "Activity Mode API")
@RestController
@RequestMapping(path = ["activity-mode"])
class ActivityModeController {

    @Autowired
    private lateinit var service: ActivityModeService

    @PostMapping(value = ["datatable"], produces = ["application/json"])
    fun datatable(@RequestBody req: Pagination2Request): ResponseEntity<ReturnData> {
        return service.datatable(req)
    }
}
