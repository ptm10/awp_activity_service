package id.go.kemenag.madrasah.pmrms.awp.activity.controller

import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.AwpRequest
import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.awp.activity.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.awp.activity.service.AwpService
import id.go.kemenag.madrasah.pmrms.awp.activity.service.documents.AwpDocumentsService
import id.go.kemenag.madrasah.pmrms.awp.activity.service.documents.AwpGoogleSheetService
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@Api(tags = ["Awp"], description = "Awp API")
@RestController
@RequestMapping(path = ["awp"])
class AwpController {

    @Autowired
    private lateinit var service: AwpService

    @Autowired
    private lateinit var serviceGoogleSheet: AwpGoogleSheetService

    @Autowired
    private lateinit var serviceDocuments: AwpDocumentsService

    @PostMapping(value = ["datatable"], produces = ["application/json"])
    fun datatable(@RequestBody req: Pagination2Request): ResponseEntity<ReturnData> {
        return service.datatable(req)
    }

    @GetMapping(value = ["{id}"], produces = ["application/json"])
    fun getDetail(@PathVariable id: String): ResponseEntity<ReturnData> {
        return service.getDetail(id)
    }

    @GetMapping(value = ["list-year"], produces = ["application/json"])
    fun listYear(): ResponseEntity<ReturnData> {
        return service.listYear()
    }

    @GetMapping("download-pdf")
    fun downloadPdf(@RequestParam("id") id: String?) {
        return serviceDocuments.downloadPdf(id)
    }

    @PostMapping(value = [""], produces = ["application/json"])
    fun save(@Valid @RequestBody request: AwpRequest): ResponseEntity<ReturnData> {
        return service.saveData(request)
    }

    @PutMapping(value = ["{id}"], produces = ["application/json"])
    fun update(@PathVariable id: String, @Valid @RequestBody request: AwpRequest): ResponseEntity<ReturnData> {
        return service.updateData(id, request)
    }

    @DeleteMapping(value = ["{id}"], produces = ["application/json"])
    fun delete(@PathVariable id: String): ResponseEntity<ReturnData> {
        return service.delete(id)
    }

    @PostMapping("download-excel")
    fun downloadExcel(@RequestBody ids: List<String>): ResponseEntity<ReturnData> {
        return serviceDocuments.downloadExcel(ids)
    }

    @GetMapping("test-google-sheet")
    fun testGoogleSheet(): ResponseEntity<ReturnData> {
        return serviceGoogleSheet.testGoogleSheet()
    }

    @GetMapping("download-word")
    fun downloadWord(@RequestParam("year") year: Int?) {
        return serviceDocuments.downloadWord(year)
    }

    @PostMapping("component-subcomponent-fixing")
    fun componentSubccomponentFixing(): ResponseEntity<ReturnData> {
        return service.componentSubccomponentFixing()
    }
}
