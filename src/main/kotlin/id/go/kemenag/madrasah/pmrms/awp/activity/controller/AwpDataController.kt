package id.go.kemenag.madrasah.pmrms.awp.activity.controller

import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.AwpDataRequest
import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.awp.activity.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.awp.activity.service.AwpDataService
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@Suppress("MVCPathVariableInspection")
@Api(tags = ["Awp Data"], description = "Awp Data")
@RestController
@RequestMapping(path = ["awp-data"])
class AwpDataController {

    @Autowired
    private lateinit var service: AwpDataService

    @GetMapping(value = ["by-year/{year}"], produces = ["application/json"])
    fun getByYear(@PathVariable year: Int): ResponseEntity<ReturnData> {
        return service.getByYear(year)
    }

    @PostMapping(value = ["datatable"], produces = ["application/json"])
    fun datatable(@RequestBody req: Pagination2Request): ResponseEntity<ReturnData> {
        return service.datatable(req)
    }

    @PostMapping(produces = ["application/json"])
    fun save(
        @Valid @RequestBody request: AwpDataRequest
    ): ResponseEntity<ReturnData> {
        return service.saveData(request)
    }

    @PutMapping(value = ["{year}"], produces = ["application/json"])
    fun update(
        @PathVariable("year") year: Int,
        @Valid @RequestBody request: AwpDataRequest
    ): ResponseEntity<ReturnData> {
        return service.updateData(year, request)
    }
}
