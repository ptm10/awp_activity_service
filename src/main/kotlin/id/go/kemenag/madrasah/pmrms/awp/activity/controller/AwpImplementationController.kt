package id.go.kemenag.madrasah.pmrms.awp.activity.controller

import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.*
import id.go.kemenag.madrasah.pmrms.awp.activity.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.awp.activity.service.AwpImplementationService
import id.go.kemenag.madrasah.pmrms.awp.activity.service.documents.AwpImplementationDocumentService
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@Api(tags = ["Awp Implementation"], description = "Awp Implementation API")
@RestController
@RequestMapping(path = ["awp-implementation"])
class AwpImplementationController {

    @Autowired
    private lateinit var service: AwpImplementationService

    @Autowired
    private lateinit var serviceDocuments: AwpImplementationDocumentService

    @PostMapping(value = ["datatable-all"], produces = ["application/json"])
    fun datatableAll(@RequestBody req: Pagination2Request): ResponseEntity<ReturnData> {
        return service.datatableAll(req)
    }

    @PostMapping(value = ["datatable"], produces = ["application/json"])
    fun datatable(@RequestBody req: Pagination2Request): ResponseEntity<ReturnData> {
        return service.datatable(req)
    }

    @PostMapping(value = ["datatable-concept-note"], produces = ["application/json"])
    fun datatableConceptNote(@RequestBody req: Pagination2Request): ResponseEntity<ReturnData> {
        return service.datatableConceptNote(req)
    }

    @PostMapping(value = ["datatable-lsp-choice"], produces = ["application/json"])
    fun datatableLspChoice(@RequestBody req: Pagination2Request): ResponseEntity<ReturnData> {
        return service.datatableLspChoice(req)
    }

    @GetMapping(value = ["{id}"], produces = ["application/json"])
    fun getDetail(@PathVariable id: String): ResponseEntity<ReturnData> {
        return service.getDetail(id)
    }

    @GetMapping("download-pdf")
    fun downloadPdf(@RequestParam("id") id: String?) {
        return service.downloadPdf(id)
    }

    @PutMapping(value = ["planning/{id}"], produces = ["application/json"])
    fun planning(
        @PathVariable id: String,
        @Valid @RequestBody request: AwpImplementationRequest
    ): ResponseEntity<ReturnData> {
        return service.updateData(id, request)
    }

    @PutMapping(value = ["approve-by-coordinator"], produces = ["application/json"])
    fun approveByCoordinator(
        @Valid @RequestBody request: AwpImplementationEvaluationRequest
    ): ResponseEntity<ReturnData> {
        return service.approveByCoordinator(request)
    }

    @PutMapping(value = ["approve-by-pmu"], produces = ["application/json"])
    fun approveByPmu(
        @Valid @RequestBody request: ApproveRequest
    ): ResponseEntity<ReturnData> {
        return service.approveByPmu(request)
    }

    @PostMapping(value = ["schedule"], produces = ["application/json"])
    fun schedule(@RequestBody req: AwpImplementationScheduleRequest): ResponseEntity<ReturnData> {
        return service.schedule(req)
    }

    @GetMapping("download-word")
    fun downloadWord(@RequestParam("id") id: String?) {
        return serviceDocuments.downloadWord(id)
    }
}
