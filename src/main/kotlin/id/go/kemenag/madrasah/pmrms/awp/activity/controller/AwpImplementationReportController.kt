package id.go.kemenag.madrasah.pmrms.awp.activity.controller

import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.AwpImplementationReportApproveRequest
import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.AwpImplementationReportRequest
import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.awp.activity.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.awp.activity.service.AwpImplementationReportService
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@Api(tags = ["Awp Implementation Report"], description = "Awp Implementation Report API")
@RestController
@RequestMapping(path = ["awp-implementation-report"])
class AwpImplementationReportController {

    @Autowired
    private lateinit var service: AwpImplementationReportService

    @PostMapping(value = ["datatable"], produces = ["application/json"])
    fun datatable(@RequestBody req: Pagination2Request): ResponseEntity<ReturnData> {
        return service.datatable(req)
    }

    @GetMapping(value = ["{id}"], produces = ["application/json"])
    fun getDetail(@PathVariable id: String): ResponseEntity<ReturnData> {
        return service.getDetail(id)
    }

    @PostMapping(value = ["datatable-all"], produces = ["application/json"])
    fun datatableAll(@RequestBody req: Pagination2Request): ResponseEntity<ReturnData> {
        return service.datatableAll(req)
    }

    @PostMapping(produces = ["application/json"])
    fun save(
        @Valid @RequestBody request: AwpImplementationReportRequest
    ): ResponseEntity<ReturnData> {
        return service.saveData(request)
    }

    @PutMapping(value = ["{id}"], produces = ["application/json"])
    fun update(
        @PathVariable("id") id: String,
        @Valid @RequestBody request: AwpImplementationReportRequest
    ): ResponseEntity<ReturnData> {
        return service.updateData(id, request)
    }

    @PutMapping(value = ["approve"], produces = ["application/json"])
    fun approve(
        @Valid @RequestBody request: AwpImplementationReportApproveRequest
    ): ResponseEntity<ReturnData> {
        return service.approve(request)
    }
}
