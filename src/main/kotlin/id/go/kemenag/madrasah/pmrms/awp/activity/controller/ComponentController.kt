package id.go.kemenag.madrasah.pmrms.awp.activity.controller

import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.awp.activity.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.Component
import id.go.kemenag.madrasah.pmrms.awp.activity.service.ComponentService
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@Api(tags = ["Component"], description = "Component API")
@RestController
@RequestMapping(path = ["component"])
class ComponentController {

    @Autowired
    private lateinit var service: ComponentService

    @GetMapping(value = ["group-list"], produces = ["application/json"])
    fun groupList(@RequestParam("year") year: Int?): ResponseEntity<ReturnData> {
        return service.groupList(year)
    }

    @PostMapping(value = ["datatable"], produces = ["application/json"])
    fun datatable(@RequestBody req: Pagination2Request): ResponseEntity<ReturnData> {
        return service.datatable(req)
    }

    @GetMapping(value = ["get-sub-component"], produces = ["application/json"])
    fun getSubComponent(@RequestParam("id") id: String?, @RequestParam("year") year: Int?): ResponseEntity<ReturnData> {
        return service.getSubComponent(id, year)
    }

    @PostMapping(value = ["datatable-parrent"], produces = ["application/json"])
    fun datatableParrent(@RequestBody req: Pagination2Request): ResponseEntity<ReturnData> {
        return service.datatableParrent(req)
    }

    @PostMapping(value = [""], produces = ["application/json"])
    fun saveData(@Valid @RequestBody request: Component): ResponseEntity<ReturnData> {
        return service.saveData(request)
    }

    @PutMapping(value = ["{id}"], produces = ["application/json"])
    fun update(@PathVariable("id") id: String, @Valid @RequestBody request: Component): ResponseEntity<ReturnData> {
        return service.updateData(id, request)
    }
}
