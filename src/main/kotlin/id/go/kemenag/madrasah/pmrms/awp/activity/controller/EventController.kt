package id.go.kemenag.madrasah.pmrms.awp.activity.controller

import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.*
import id.go.kemenag.madrasah.pmrms.awp.activity.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.awp.activity.service.EventService
import id.go.kemenag.madrasah.pmrms.awp.activity.service.documents.EventDocumentService
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@Api(tags = ["Event"], description = "Event API")
@RestController
@RequestMapping(path = ["event"])
class EventController {

    @Autowired
    private lateinit var service: EventService

    @Autowired
    private lateinit var documentService: EventDocumentService

    @PostMapping(value = ["datatable-all"], produces = ["application/json"])
    fun datatableAll(@RequestBody req: Pagination2Request): ResponseEntity<ReturnData> {
        return service.datatableAll(req)
    }

    @PostMapping(value = ["datatable"], produces = ["application/json"])
    fun datatable(@RequestBody req: Pagination2Request): ResponseEntity<ReturnData> {
        return service.datatable(req)
    }

    @PostMapping(value = ["datatable-event-lsp"], produces = ["application/json"])
    fun datatableEventLsp(@RequestBody req: Pagination2Request): ResponseEntity<ReturnData> {
        return service.datatableEventLsp(req)
    }

    @PostMapping(value = ["schedule"], produces = ["application/json"])
    fun schedule(@RequestBody req: EventScheduleRequest): ResponseEntity<ReturnData> {
        return service.schedule(req)
    }

    @GetMapping(value = ["self-event-by-status"], produces = ["application/json"])
    fun selfEventByStatus(): ResponseEntity<ReturnData> {
        return service.selfEventByStatus()
    }

    @PostMapping(value = ["self-event-by-status-datatable"], produces = ["application/json"])
    fun selfEventByStatusDatatable(@RequestBody req: Pagination2Request): ResponseEntity<ReturnData> {
        return service.selfEventByStatusDatatable(req)
    }

    @GetMapping(value = ["{id}"], produces = ["application/json"])
    fun getDetail(@PathVariable id: String): ResponseEntity<ReturnData> {
        return service.getDetail(id)
    }

    @PostMapping(produces = ["application/json"])
    fun save(
        @Valid @RequestBody request: EventRequest
    ): ResponseEntity<ReturnData> {
        return service.saveData(request)
    }

    @PutMapping(value = ["{id}"], produces = ["application/json"])
    fun update(
        @PathVariable("id") id: String,
        @Valid @RequestBody request: EventRequest
    ): ResponseEntity<ReturnData> {
        return service.updateData(id, request)
    }

    @PutMapping(value = ["approve-by-coordinator"], produces = ["application/json"])
    fun approveByCoordinator(
        @Valid @RequestBody request: EventEvaluationRequest
    ): ResponseEntity<ReturnData> {
        return service.approveByCoordinator(request)
    }

    @PutMapping(value = ["approve-by-treasurer-pmu"], produces = ["application/json"])
    fun approveByTreasurerPmu(
        @Valid @RequestBody request: EventEvaluationRequest
    ): ResponseEntity<ReturnData> {
        return service.approveByTreasurerPmu(request)
    }

    @PutMapping(value = ["approve-by-head-pmu"], produces = ["application/json"])
    fun approveByHeadPmu(
        @Valid @RequestBody request: EventEvaluationRequest
    ): ResponseEntity<ReturnData> {
        return service.approveByHeadPmu(request)
    }

    @GetMapping("download-pdf")
    fun downloadPdf(@RequestParam("id") id: String?) {
        return documentService.downloadPdf(id)
    }

    @GetMapping("download-word")
    fun downloadWord(@RequestParam("id") id: String?) {
        return documentService.downloadWord(id)
    }

    @GetMapping("download-template-form-user")
    fun downloadTemplateFormUser() : ByteArray? {
        return documentService.downloadTemplateFormUser()
    }

    @PostMapping("upload-form-user", produces = ["application/json"])
    fun uploadFormUser(
        @Valid @RequestBody request: UploadFormUserRequest
    ): ResponseEntity<ReturnData> {
        return documentService.uploadFormUser(request)
    }
}
