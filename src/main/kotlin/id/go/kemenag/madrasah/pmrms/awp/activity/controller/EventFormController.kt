package id.go.kemenag.madrasah.pmrms.awp.activity.controller

import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.EventFormRequest
import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.awp.activity.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.awp.activity.service.EventFormService
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@Api(tags = ["Event Form"], description = "Event Form API")
@RestController
@RequestMapping(path = ["event-form"])
class EventFormController {

    @Autowired
    private lateinit var service: EventFormService

    @GetMapping(value = ["{id}"], produces = ["application/json"])
    fun getDetail(@PathVariable id: String): ResponseEntity<ReturnData> {
        return service.getDetail(id)
    }

    @PostMapping(value = ["datatable"], produces = ["application/json"])
    fun datatable(@RequestBody req: Pagination2Request): ResponseEntity<ReturnData> {
        return service.datatable(req)
    }

    @PostMapping(produces = ["application/json"])
    fun save(
        @Valid @RequestBody request: EventFormRequest
    ): ResponseEntity<ReturnData> {
        return service.saveData(request)
    }

    @PutMapping(value = ["{id}"], produces = ["application/json"])
    fun update(
        @PathVariable("id") id: String,
        @Valid @RequestBody request: EventFormRequest
    ): ResponseEntity<ReturnData> {
        return service.updateData(id, request)
    }

    @DeleteMapping(value = ["{id}"], produces = ["application/json"])
    fun delete(@PathVariable id: String): ResponseEntity<ReturnData> {
        return service.delete(id)
    }
}
