package id.go.kemenag.madrasah.pmrms.awp.activity.controller

import id.go.kemenag.madrasah.pmrms.awp.activity.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.awp.activity.service.EventFormExcelFileService
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@Api(tags = ["Excel From Excel File"], description = "Excel From Excel File")
@RestController
@RequestMapping(path = ["event-form-excel-file"])
class EventFormExcelFileController {

    @Autowired
    private lateinit var service: EventFormExcelFileService

    @GetMapping(value = ["result"], produces = ["application/json"])
    fun result(@RequestParam eventId: String?, @RequestParam createdFor: Int?): ResponseEntity<ReturnData> {
        return service.result(eventId, createdFor)
    }
}
