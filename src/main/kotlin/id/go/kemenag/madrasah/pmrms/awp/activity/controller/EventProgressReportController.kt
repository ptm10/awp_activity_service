package id.go.kemenag.madrasah.pmrms.awp.activity.controller

import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.ApproveRequest
import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.EventProgressReportRequest
import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.awp.activity.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.awp.activity.service.EventProgressReportService
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@Api(tags = ["Event Progress Report"], description = "Event Progress Report API")
@RestController
@RequestMapping(path = ["event-progress-report"])
class EventProgressReportController {

    @Autowired
    private lateinit var service: EventProgressReportService

    @GetMapping(value = ["{id}"], produces = ["application/json"])
    fun getDetail(@PathVariable id: String): ResponseEntity<ReturnData> {
        return service.getDetail(id)
    }

    @GetMapping(value = ["last-report"], produces = ["application/json"])
    fun lastReport(@RequestParam eventId: String?): ResponseEntity<ReturnData> {
        return service.lastReport(eventId)
    }

    @PostMapping(value = ["datatable-all"], produces = ["application/json"])
    fun datatableAll(@RequestBody req: Pagination2Request): ResponseEntity<ReturnData> {
        return service.datatableAll(req)
    }

    @PostMapping(value = ["datatable"], produces = ["application/json"])
    fun datatable(@RequestBody req: Pagination2Request): ResponseEntity<ReturnData> {
        return service.datatable(req)
    }

    @PostMapping(produces = ["application/json"])
    fun save(
        @Valid @RequestBody request: EventProgressReportRequest
    ): ResponseEntity<ReturnData> {
        return service.saveData(request)
    }

    @PutMapping(value = ["{id}"], produces = ["application/json"])
    fun update(
        @PathVariable("id") id: String,
        @Valid @RequestBody request: EventProgressReportRequest
    ): ResponseEntity<ReturnData> {
        return service.updateData(id, request)
    }

    @PutMapping(value = ["approve-by-consultan"], produces = ["application/json"])
    fun approveByConsultan(
        @Valid @RequestBody request: ApproveRequest
    ): ResponseEntity<ReturnData> {
        return service.approveByConsultan(request)
    }
}
