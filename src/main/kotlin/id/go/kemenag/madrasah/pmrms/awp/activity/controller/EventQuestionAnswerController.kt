package id.go.kemenag.madrasah.pmrms.awp.activity.controller

import id.go.kemenag.madrasah.pmrms.awp.activity.constant.EVENT_QUESTION_CREATED_FOR_EVALUATOR
import id.go.kemenag.madrasah.pmrms.awp.activity.constant.EVENT_QUESTION_CREATED_FOR_PARTICIPANT
import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.awp.activity.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.awp.activity.service.EventQuestionAnswerService
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@Api(tags = ["Event Question Answer"], description = "Event Question Answer API")
@RestController
@RequestMapping(path = ["event-question-answer"])
class EventQuestionAnswerController {

    @Autowired
    private lateinit var service: EventQuestionAnswerService

    @PostMapping(value = ["datatable"], produces = ["application/json"])
    fun datatable(@RequestBody req: Pagination2Request): ResponseEntity<ReturnData> {
        return service.datatable(req)
    }

    @GetMapping(value = ["result-participant"], produces = ["application/json"])
    fun resultParticipant(
        @RequestParam("eventId") eventId: String?
    ): ResponseEntity<ReturnData> {
        return service.result(eventId, EVENT_QUESTION_CREATED_FOR_PARTICIPANT)
    }

    @GetMapping(value = ["result-evaluator"], produces = ["application/json"])
    fun resultEvaluator(
        @RequestParam("eventId") eventId: String?
    ): ResponseEntity<ReturnData> {
        return service.result(eventId, EVENT_QUESTION_CREATED_FOR_EVALUATOR)
    }
}
