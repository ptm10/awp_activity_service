package id.go.kemenag.madrasah.pmrms.awp.activity.controller

import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.awp.activity.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.awp.activity.service.EventQuestionService
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@Api(tags = ["Event Question"], description = "Event Question API")
@RestController
@RequestMapping(path = ["event-question"])
class EventQuestionController {

    @Autowired
    private lateinit var service: EventQuestionService

    @GetMapping(value = ["existing-questions"], produces = ["application/json"])
    fun datatable(): ResponseEntity<ReturnData> {
        return service.existingQuestions()
    }

    @PostMapping(value = ["datatable"], produces = ["application/json"])
    fun datatable(@RequestBody req: Pagination2Request): ResponseEntity<ReturnData> {
        return service.datatable(req)
    }
}
