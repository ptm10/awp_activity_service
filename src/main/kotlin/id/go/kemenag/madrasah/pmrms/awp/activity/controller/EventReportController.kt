package id.go.kemenag.madrasah.pmrms.awp.activity.controller

import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.ApproveRequest
import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.EventReportRequest
import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.ProjectReportDocumentRequest
import id.go.kemenag.madrasah.pmrms.awp.activity.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.awp.activity.service.EventReportService
import id.go.kemenag.madrasah.pmrms.awp.activity.service.documents.EventReportDocumentService
import id.go.kemenag.madrasah.pmrms.awp.activity.service.documents.ProjectReportDocumentService
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@Api(tags = ["Event Report"], description = "Event Report API")
@RestController
@RequestMapping(path = ["event-report"])
class EventReportController {

    @Autowired
    private lateinit var service: EventReportService

    @Autowired
    private lateinit var serviceDocuments: EventReportDocumentService

    @Autowired
    private lateinit var servicProjectReporteDocuments: ProjectReportDocumentService

    @GetMapping(value = ["{id}"], produces = ["application/json"])
    fun getDetail(@PathVariable id: String): ResponseEntity<ReturnData> {
        return service.getDetail(id)
    }

    @PostMapping(value = ["datatable"], produces = ["application/json"])
    fun datatable(@RequestBody req: Pagination2Request): ResponseEntity<ReturnData> {
        return service.datatable(req)
    }

    @PostMapping(value = ["datatable-question"], produces = ["application/json"])
    fun datatableQuestion(@RequestBody req: Pagination2Request): ResponseEntity<ReturnData> {
        return service.datatableQuestion(req)
    }

    @PostMapping(produces = ["application/json"])
    fun save(
        @Valid @RequestBody request: EventReportRequest
    ): ResponseEntity<ReturnData> {
        return service.saveData(request)
    }

    @PutMapping(value = ["{id}"], produces = ["application/json"])
    fun update(
        @PathVariable("id") id: String,
        @Valid @RequestBody request: EventReportRequest
    ): ResponseEntity<ReturnData> {
        return service.updateData(id, request)
    }

    @PutMapping(value = ["approve-by-consultan"], produces = ["application/json"])
    fun approveByConsultan(
        @Valid @RequestBody request: ApproveRequest
    ): ResponseEntity<ReturnData> {
        return service.approveByConsultan(request)
    }

    @GetMapping("download-word")
    fun downloadWord(@RequestParam("id") id: String?) {
        return serviceDocuments.downloadWord(id)
    }

    @PostMapping("project-report")
    fun projectReport(
        @Valid @RequestBody request: ProjectReportDocumentRequest
    ) {
        return servicProjectReporteDocuments.downloadWord(request)
    }
}
