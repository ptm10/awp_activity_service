package id.go.kemenag.madrasah.pmrms.awp.activity.controller

import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.MonevResultChainRequest
import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.MonevResultChainUpdateResultRequest
import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.awp.activity.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.awp.activity.service.MonevResultChainService
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@Api(tags = ["Monev Result Chain"], description = "Monev Result Chain Data")
@RestController
@RequestMapping(path = ["monev-result-chain"])
class MonevResultChainController {

    @Autowired
    private lateinit var service: MonevResultChainService

    @PostMapping(value = ["datatable"], produces = ["application/json"])
    fun datatable(@RequestBody req: Pagination2Request): ResponseEntity<ReturnData> {
        return service.datatable(req)
    }

    @PostMapping(value = [""], produces = ["application/json"])
    fun save(@Valid @RequestBody request: MonevResultChainRequest): ResponseEntity<ReturnData> {
        return service.saveData(request)
    }

    @PutMapping(value = ["{id}"], produces = ["application/json"])
    fun update(
        @PathVariable("id") id: String,
        @Valid @RequestBody request: MonevResultChainRequest
    ): ResponseEntity<ReturnData> {
        return service.updateData(id, request)
    }

    @DeleteMapping(value = ["{id}"], produces = ["application/json"])
    fun delete(@PathVariable id: String): ResponseEntity<ReturnData> {
        return service.delete(id)
    }

    @PutMapping(value = ["update-result/{id}"], produces = ["application/json"])
    fun updateResult(
        @PathVariable("id") id: String,
        @Valid @RequestBody request: MonevResultChainUpdateResultRequest
    ): ResponseEntity<ReturnData> {
        return service.updateResult(id, request)
    }

    @GetMapping(value = ["{id}"], produces = ["application/json"])
    fun getDetail(@PathVariable id: String): ResponseEntity<ReturnData> {
        return service.getDetail(id)
    }

    @GetMapping(value = ["group-list"], produces = ["application/json"])
    fun groupList(@RequestParam year : Int?): ResponseEntity<ReturnData> {
        return service.groupList(year)
    }
}
