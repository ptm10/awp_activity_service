package id.go.kemenag.madrasah.pmrms.awp.activity.controller

import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.EventQuestionAnswerRequest
import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.awp.activity.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.awp.activity.service.EventQuestionAnswerService
import id.go.kemenag.madrasah.pmrms.awp.activity.service.EventQuestionService
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@Api(tags = ["Public"], description = "Public")
@RestController
@RequestMapping(path = ["public"])
class PublicController {

    @Autowired
    private lateinit var service: EventQuestionService

    @Autowired
    private lateinit var serviceEventQuestionAnswer: EventQuestionAnswerService

    @PostMapping(value = ["event-question/{id}/{identityId}"], produces = ["application/json"])
    fun eventQuestion(
        @PathVariable("id") id: String?,
        @PathVariable("identityId") identityId: String?,
        @RequestBody req: Pagination2Request
    ): ResponseEntity<ReturnData> {
        return service.eventQuestionByEventFilledInBy(id, identityId, req)
    }

    @PostMapping(value = ["event-question-asnwer"], produces = ["application/json"])
    fun save(
        @Valid @RequestBody request: EventQuestionAnswerRequest
    ): ResponseEntity<ReturnData> {
        return serviceEventQuestionAnswer.saveAnswer(request)
    }
}
