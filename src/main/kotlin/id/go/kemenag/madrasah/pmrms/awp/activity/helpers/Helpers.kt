@file:Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS", "UNCHECKED_CAST")

package id.go.kemenag.madrasah.pmrms.awp.activity.helpers

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import id.go.kemenag.madrasah.pmrms.awp.activity.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.awp.activity.model.users.Users
import id.go.kemenag.madrasah.pmrms.awp.activity.model.users.UsersRole
import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.Resources
import org.apache.poi.xwpf.usermodel.*
import org.openxmlformats.schemas.officeDocument.x2006.sharedTypes.STOnOff1
import org.openxmlformats.schemas.wordprocessingml.x2006.main.*
import org.springframework.core.env.Environment
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.web.context.request.RequestContextHolder
import org.springframework.web.context.request.ServletRequestAttributes
import org.springframework.web.context.support.WebApplicationContextUtils
import org.springframework.web.multipart.MultipartFile
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.math.BigInteger
import java.net.MalformedURLException
import java.net.URL
import java.text.DateFormat
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.temporal.ChronoUnit
import java.util.*
import java.util.concurrent.TimeUnit
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import kotlin.math.pow
import kotlin.math.roundToInt


fun getUserLogin(): Users? {
    return try {
        val principal = SecurityContextHolder.getContext().authentication.principal as Any
        val objectMapper = ObjectMapper()
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        objectMapper.readValue(principal.toString(), Users::class.java)
    } catch (e: Exception) {
        null
    }
}

@Throws(MalformedURLException::class)
fun getBaseUrl(request: HttpServletRequest): String {
    try {
        val requestURL = URL(request.requestURL.toString())
        val port = if (requestURL.port == -1) "" else ":" + requestURL.port
        return requestURL.protocol + "://" + requestURL.host + port + request.contextPath + "/"
    } catch (e: Exception) {
        throw e
    }
}

fun getFullUrl(request: HttpServletRequest): String {
    val requestURL = StringBuilder(request.requestURL.toString())
    val queryString = request.queryString
    return if (queryString == null) {
        requestURL.toString()
    } else {
        requestURL.append('?').append(queryString).toString()
    }
}

fun getUserComponetCodes(): List<String> {
    val rData: MutableList<String> = emptyList<String>().toMutableList()
    if (getUserLogin()?.component?.code != null) {
        if (getUserLogin()?.component?.code?.contains("4.") == false) {
            rData.add(getUserLogin()?.component?.code?.substringBefore(".") ?: "")
        } else {
            rData.add("4")
        }
    }

    return rData
}

fun getUserRoles(): MutableSet<UsersRole> {
    var roles: MutableSet<UsersRole> = mutableSetOf()
    try {
        getUserLogin()?.let {
            if (!it.roles.isNullOrEmpty()) {
                roles = getUserLogin()!!.roles!!
            }
        }
    } catch (e: Exception) {
        e.printStackTrace()
    }

    return roles
}

fun userHasRoles(roles: List<String>): Boolean {
    var allowed = false
    try {
        roles.forEach {
            val findRole: UsersRole? = getUserRoles().find { f ->
                f.roleId == it
            }
            if (findRole != null) {
                allowed = true
                return@forEach
            }
        }
    } catch (e: Exception) {
        e.printStackTrace()
    }
    return allowed
}

fun userHasAllRoles(roles: List<String>): Boolean {
    var allowed = false
    try {
        val userRole = mutableListOf<String?>()
        getUserRoles().forEach {
            userRole.add(it.roleId)
        }
        allowed = userRole.containsAll(roles)
    } catch (e: Exception) {
        e.printStackTrace()
    }
    return allowed
}

fun resourcesHasAllRoles(roles: List<String>, resources: Resources?): Boolean {
    var allowed = false
    try {
        val userRole = mutableListOf<String?>()
        resources?.user?.roles?.forEach {
            userRole.add(it.roleId)
        }
        allowed = userRole.containsAll(roles)
    } catch (e: Exception) {
        e.printStackTrace()
    }

    return allowed
}

fun getEnvFromHttpServletRequest(httpServletRequest: HttpServletRequest, key: String): String {
    var rData = ""
    try {
        val servletContext = httpServletRequest.session.servletContext
        val webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext)
        val env: Environment = webApplicationContext!!.getBean(Environment::class.java)
        rData = env.getProperty(key, "")
        println(rData)
    } catch (_: Exception) {

    }
    return rData
}

@Throws(IOException::class)
fun convertMultiPartToFile(file: MultipartFile): File {
    val convFile = File(file.originalFilename)
    val fos = FileOutputStream(convFile)
    fos.write(file.bytes)
    fos.close()
    return convFile
}

fun getDaysBetween(startDate: Date, endDate: Date, dform: DateFormat): Int {
    return ChronoUnit.DAYS.between(
        LocalDate.parse(dform.format(startDate)), LocalDate.parse(dform.format(endDate))
    ).toInt() + 1
}

fun indonesianNumber(number: Int): String {
    val mapNumber = mutableMapOf<Int, String>()
    mapNumber[1] = "satu"
    mapNumber[2] = "dua"
    mapNumber[3] = "tiga"
    mapNumber[4] = "empat"
    mapNumber[5] = "lima"
    mapNumber[6] = "enam"
    mapNumber[7] = "tujuh"
    mapNumber[8] = "delapan"
    mapNumber[9] = "sembilan"
    mapNumber[10] = "sepuluh"

    if (mapNumber.containsKey(number)) {
        return mapNumber[number] ?: ""
    }
    return ""
}

fun excelPoiAddBreak(r: XWPFRun, number: Int) {
    var bc = 0
    while (bc < number) {
        r.addBreak()
        bc++
    }
}

fun excelPoiBulletNumId(doc: XWPFDocument): BigInteger {
    val cTAbstractNum = CTAbstractNum.Factory.newInstance()
    cTAbstractNum.abstractNumId = BigInteger.valueOf(0)
    val cTLvl = cTAbstractNum.addNewLvl()
    cTLvl.addNewNumFmt().setVal(STNumberFormat.BULLET)
    cTLvl.addNewLvlText().setVal("•")
    val abstractNum = XWPFAbstractNum(cTAbstractNum)
    val numbering: XWPFNumbering = doc.createNumbering()
    val abstractNumID = numbering.addAbstractNum(abstractNum)
    return numbering.addNum(abstractNumID)
}

private fun excelPoiAddCustomHeadingStyle(docxDocument: XWPFDocument, strStyleId: String, headingLevel: Int) {
    val ctStyle = CTStyle.Factory.newInstance()
    ctStyle.styleId = strStyleId
    val styleName = CTString.Factory.newInstance()
    styleName.setVal(strStyleId)
    ctStyle.name = styleName
    val indentNumber = CTDecimalNumber.Factory.newInstance()
    indentNumber.setVal(BigInteger.valueOf(headingLevel.toLong()))

    // lower number > style is more prominent in the formats bar
    ctStyle.uiPriority = indentNumber
    val onoffnull = CTOnOff.Factory.newInstance()
    ctStyle.unhideWhenUsed = onoffnull

    // style shows up in the formats bar
    ctStyle.qFormat = onoffnull

    // style defines a heading of the given level
    val ppr = CTPPrGeneral.Factory.newInstance()
    ppr.outlineLvl = indentNumber
    ctStyle.pPr = ppr
    val style = XWPFStyle(ctStyle)

    // is a null op if already defined
    val styles = docxDocument.createStyles()
    style.type = STStyleType.PARAGRAPH
    styles.addStyle(style)
}

fun getCharForNumber(i: Int): String? {
    return if (i in 1..26) (i + 64).toChar().toString() else null
}

fun formatRupiah(number: Double?, showRp: Boolean = true): String {
    var paramNumber = number
    if (paramNumber == null) {
        paramNumber = 0.0
    }

    val localeID = Locale("in", "ID")
    val formatRupiah: NumberFormat = NumberFormat.getCurrencyInstance(localeID)

    return if (showRp) {
        formatRupiah.format(paramNumber).replace("Rp", "Rp. ")
    } else {
        formatRupiah.format(paramNumber).replace("Rp", "").replace(",00", "")
    }
}

fun dateFormatIndonesia(date: Date?, withDays: Boolean = false): String {
    if (date != null) {
        val fromDate = SimpleDateFormat("yyyy-MM-dd")
        val formatDay = SimpleDateFormat("dd")
        val formatMonth = SimpleDateFormat("MM")
        val formatYear = SimpleDateFormat("yyyy")
        val fromDateFormat = fromDate.parse(SimpleDateFormat("yyyy-MM-dd").format(date))

        val listMonth = mapOf(
            "1" to "Januari",
            "2" to "Februari",
            "3" to "Maret",
            "4" to "April",
            "5" to "Mei",
            "6" to "Juni",
            "7" to "Juli",
            "8" to "Agustus",
            "9" to "September",
            "10" to "Oktober",
            "11" to "November",
            "12" to "Desember"
        )

        return buildString {
            if (withDays) {
                val fDays = SimpleDateFormat("EEEE")
                val days = fDays.format(date)
                val listDays = mapOf(
                    "Monday" to "Senin",
                    "Tuesday" to "Selasa",
                    "Wednesday" to "Rabu",
                    "Thursday" to "Kamis",
                    "Friday" to "Jumat",
                    "Saturday" to "Sabtu",
                    "Sunday" to "Minggu"
                )

                append(listDays[days])
                append(", ")
            }

            append(formatDay.format(fromDateFormat).replaceFirst("0", ""))
            append(" ")
            append(
                listMonth[formatMonth.format(fromDateFormat).replaceFirst("0", "")]
            )
            append(" ")
            append(formatYear.format(fromDateFormat))
        }
    }

    return "-"
}

fun startEndDateIndonesia(startDate: Date?, endDate: Date?): String {
    var rData = ""
    if (startDate != null && endDate != null) {
        val fromDate = SimpleDateFormat("yyyy-MM-dd")
        val formatDay = SimpleDateFormat("dd")
        val formatMonth = SimpleDateFormat("MM")
        val formatYear = SimpleDateFormat("yyyy")

        val startDateFormat = fromDate.parse(SimpleDateFormat("yyyy-MM-dd").format(startDate))
        val endtDateFormat = fromDate.parse(SimpleDateFormat("yyyy-MM-dd").format(endDate))

        val listMonth = mapOf(
            "1" to "Januari",
            "2" to "Februari",
            "3" to "Maret",
            "4" to "April",
            "5" to "Mei",
            "6" to "Juni",
            "7" to "Juli",
            "8" to "Agustus",
            "9" to "September",
            "10" to "Oktober",
            "11" to "November",
            "12" to "Desember"
        )

        rData += "${formatDay.format(startDateFormat).replaceFirst("0", "")} - ${
            formatDay.format(endtDateFormat).replaceFirst("0", "")
        }"
        rData += " ${listMonth[formatMonth.format(endtDateFormat).replaceFirst("0", "")]}"
        rData += " ${formatYear.format(endtDateFormat)}"
    }



    if (rData == "") {
        rData = "-"
    }

    return rData
}

fun daysBetweenDates(date1: Date, date2: Date): Long {
    try {
        val diff = date1.time - date2.time
        return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)
    } catch (e: Exception) {
        e.printStackTrace()
    }

    return 0
}

fun excelPoiCreateToc(doc: XWPFDocument, p: XWPFParagraph) {
    val ctP = p.ctp
    val toc = ctP.addNewFldSimple()
    toc.instr = "TOC \\h"
    toc.dirty = STOnOff1.Enum.forString("on")
//    try {
//        val cursor = p.ctp!!.newCursor()
//        val uri = CTSdtBlock.type.name.namespaceURI
//        val localPart = "sdt"
//        cursor.beginElement(localPart, uri)
//        cursor.toParent()
//        val block: CTSdtBlock = cursor.getObject() as CTSdtBlock
//        val toc = TOC(block)
//
//        val var3: Iterator<*> = doc.paragraphs.iterator()
//        while (var3.hasNext()) {
//            val par = var3.next() as XWPFParagraph
//
//            val parStyle = par.style
//            if (parStyle != null && parStyle.startsWith("Heading")) {
//                try {
//                    val level = parStyle.substring("Heading".length).toInt()
//                    toc.addRow(level, par.text, 1, "112723803")
//                } catch (e: Exception) {
//                    throw e
//                }
//            }
//        }
//    } catch (e: Exception) {
//        throw e
//    }
}

fun compareDataChange(oldData: Any?, newData: Any?): Boolean {
    var changeData = false
    try {
        if (oldData != null && newData != null) {
            val objectMapper = ObjectMapper()
            val setOldDataStr =
                objectMapper.writeValueAsString(setCompareData(objectMapper.writeValueAsString(oldData)))
            val setNewDataStr =
                objectMapper.writeValueAsString(setCompareData(objectMapper.writeValueAsString(newData)))

            if (setOldDataStr != setNewDataStr) {
                changeData = true
            }
        }
    } catch (e: Exception) {
        throw e
    }
    return changeData
}


fun encryptWithoutStr(param: String, ignore: List<String>): String {
    val encrypt = BCryptPasswordEncoder().encode(param)

    var constrain = false
    ignore.forEach {
        if (encrypt.contains(it)) {
            constrain = true
        }
    }

    if (constrain) {
        return encryptWithoutStr(param, ignore)
    }

    return encrypt
}

fun setCompareData(strData: String): Map<String, Any?> {
    val setData = mutableMapOf<String, Any?>()
    val objectMapper = ObjectMapper()

    try {
        val map: Map<String, Any?> = objectMapper.readValue(strData, Map::class.java) as Map<String, Any?>

        val ignoreField = listOf("createdAt", "updatedAt", "createdBy", "createdByUser", "updatedBy", "updatedByUser")

        map.forEach {
            if (!ignoreField.contains(it.key)) {
                when (it.value) {
                    is String -> {
                        setData[it.key] = it.value.toString().trim().lowercase()
                    }
                    is Int -> {
                        setData[it.key] = it.value
                    }
                    is Long -> {
                        setData[it.key] = it.value
                    }
                    is Double -> {
                        setData[it.key] = it.value
                    }
                    is Date -> {
                        setData[it.key] = it.value
                    }
                    is Boolean -> {
                        setData[it.key] = it.value
                    }

                    else -> {
                        try {
                            val parse = objectMapper.writeValueAsString(it.value)
                            if (parse[0] == '[') {
                                try {
                                    val mapValue = it.value as List<Map<String, Any?>>
                                    val mapDatas = mutableListOf<Map<String, Any?>>()
                                    mapValue.forEach { mv ->
                                        mapDatas.add(
                                            setCompareData(
                                                objectMapper.writeValueAsString(mv)
                                            )
                                        )
                                    }
                                    setData[it.key] = mapDatas
                                } catch (_: Exception) {

                                }
                            }
                        } catch (_: Exception) {

                        }
                    }
                }
            }
        }
    } catch (e: Exception) {
        e.printStackTrace()
    }
    return setData
}


fun round(value: Double?, places: Int): String {
    return try {
        var paramValue = value
        if (paramValue == null) {
            paramValue = 0.0
        }

        require(places >= 0)
        val factor = 10.0.pow(places.toDouble()).toLong()
        paramValue *= factor
        val tmp = paramValue.roundToInt()
        String.format("%.2f", tmp.toDouble() / factor)
    } catch (e: Exception) {
        ""
    }
}

fun responseNotFound(response: HttpServletResponse, message: String = "not found") {
    val json = ObjectMapper().writeValueAsString(
        ReturnData(
            message = message
        )
    )

    response.contentType = "application/json"
    response.status = HttpServletResponse.SC_NOT_FOUND
    response.writer.write(json)
    response.writer.flush()
    response.writer.close()
}

fun getApplicationProp(applicationProp: String): String? {
    val request = (RequestContextHolder.getRequestAttributes() as ServletRequestAttributes?)?.request
    val servletContext = request?.session?.servletContext
    val webApplicationContext = servletContext?.let { WebApplicationContextUtils.getWebApplicationContext(it) }
    val env: Environment = webApplicationContext!!.getBean(Environment::class.java)

    return env.getProperty(applicationProp)
}

fun gender(gender: Char?): String? {
    return if (gender == 'm') {
        "Laki - Laki"
    } else if (gender == 'f') {
        "Perempuan"
    } else {
        null
    }
}

fun mergeTableWordCellHorizontally(table: XWPFTable, row: Int, fromCol: Int, toCol: Int) {
    val cell = table.getRow(row).getCell(fromCol)
    // Try getting the TcPr. Not simply setting an new one every time.
    var tcPr = cell.ctTc.tcPr
    if (tcPr == null) tcPr = cell.ctTc.addNewTcPr()
    // The first merged cell has grid span property set
    if (tcPr!!.isSetGridSpan) {
        tcPr.gridSpan.setVal((toCol - fromCol + 1).toBigInteger())
    } else {
        tcPr.addNewGridSpan().setVal((toCol - fromCol + 1).toBigInteger())
    }
    // Cells which join (merge) the first one, must be removed
    for (colIndex in toCol downTo fromCol + 1) {
        table.getRow(row).ctRow.removeTc(colIndex)
        table.getRow(row).removeCell(colIndex)
    }
}

fun strStarAnswer(num: Int?): String {
    var rdData = ""
    if (num != null) {
        repeat(num) {
            rdData += "*"
        }
    }
    return rdData
}

fun strYesOrnoAnswer(answer: Boolean?): String {
    var rdData = ""
    if (answer != null) {
        val answerData = mapOf(true to "Ya", false to "Tidak")
        rdData = answerData[answer].toString()
    }
    return rdData
}

fun replaceUnusedSpaceCkEditor(str: String): String {
    return str.replace("<p><br>&nbsp;</p>", "")
}

