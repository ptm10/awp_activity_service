package id.go.kemenag.madrasah.pmrms.awp.activity.model

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.Files

data class ReturnDataFiles(
    var success: Boolean? = false,
    var data: Files? = null,
    var message: String? = null
)
