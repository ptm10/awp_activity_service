package id.go.kemenag.madrasah.pmrms.awp.activity.model.notif

data class EmailRequest(

    var to: String? = null,

    var subject: String? = null,

    var message: String? = null
)

