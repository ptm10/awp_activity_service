package id.go.kemenag.madrasah.pmrms.awp.activity.model.request

import id.go.kemenag.madrasah.pmrms.awp.activity.constant.VALIDATOR_MSG_REQUIRED
import javax.validation.constraints.NotEmpty

data class ActivityRequest(

    @field:NotEmpty(message = "Id Awp $VALIDATOR_MSG_REQUIRED")
    var awpId: String? = null,

    @field:NotEmpty(message = "File Ids $VALIDATOR_MSG_REQUIRED")
    var fileIds: List<String>? = null
)
