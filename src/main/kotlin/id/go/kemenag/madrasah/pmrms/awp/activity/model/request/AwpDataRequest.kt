package id.go.kemenag.madrasah.pmrms.awp.activity.model.request

import id.go.kemenag.madrasah.pmrms.awp.activity.constant.VALIDATOR_MSG_REQUIRED
import javax.validation.constraints.NotNull

data class AwpDataRequest(
    @field:NotNull(message = "Tahun $VALIDATOR_MSG_REQUIRED")
    var year: Int? = null,

    var keteranganKontribusiKomponent: String? = null,

    var keteranganKemajuanProgram: String? = null,

    var keteranganKendala: String? = null,

    var keteranganPembelajaran: String? = null,

    var keteranganRencanaKerja: String? = null,

    var keteranganUsulanKegiatan: String? = null,

    var dipaAwalKomponen1: Long? = null,

    var dipaAwalKomponen2: Long? = null,

    var dipaAwalKomponen3: Long? = null,

    var dipaAwalKomponen4: Long? = null,

    var dipaRevisiKomponen1: Long? = null,

    var dipaRevisiKomponen2: Long? = null,

    var dipaRevisiKomponen3: Long? = null,

    var dipaRevisiKomponen4: Long? = null,

    var dipaRealisasiKomponen1: Long? = null,

    var dipaRealisasiKomponen2: Long? = null,

    var dipaRealisasiKomponen3: Long? = null,

    var dipaRealisasiKomponen4: Long? = null,

    var dipaAwalRupiahMurni: Long? = null,

    var dipaRevisiRupiahMurni: Long? = null,

    var dipaRealisasiRupiahMurni: Long? = null,

    var hambatanKomponen1: String? = null,

    var hambatanKomponen2: String? = null,

    var hambatanKomponen3: String? = null,

    var hambatanKomponen4: String? = null,

    var fokusKomponen1: String? = null,

    var fokusKomponen2: String? = null,

    var fokusKomponen3: String? = null,

    var fokusKomponen4: String? = null,

    var pdo: MutableList<AwpDataPdoReqeust> = emptyList<AwpDataPdoReqeust>().toMutableList(),

    var files: MutableList<AwpDataPdoFileRequest> = emptyList<AwpDataPdoFileRequest>().toMutableList(),

    var deletedFilesIds: List<String>? = null,

    var otherFiles: MutableList<AwpDataPdoOtherFileRequest> = emptyList<AwpDataPdoOtherFileRequest>().toMutableList(),

    var deletedOtherFilesIds: List<String>? = null,
)

data class AwpDataPdoReqeust(
    var pdoId: String? = null,
    var target: String? = null,
    var iri: List<AwpDataPdoIriReqeust>? = null,
)

data class AwpDataPdoIriReqeust(
    var iriId: String? = null,
    var target: String? = null,
)

data class AwpDataPdoFileRequest(
    var fileId: String? = null,
    var component: String? = null
)

data class AwpDataPdoOtherFileRequest(
    var fileId: String? = null,
    var key: String? = null
)


