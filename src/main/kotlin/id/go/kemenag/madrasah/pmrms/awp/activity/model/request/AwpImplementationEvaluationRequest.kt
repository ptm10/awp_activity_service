package id.go.kemenag.madrasah.pmrms.awp.activity.model.request

import id.go.kemenag.madrasah.pmrms.awp.activity.constant.VALIDATOR_MSG_REQUIRED
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

data class AwpImplementationEvaluationRequest(

    @field:NotEmpty(message = "id $VALIDATOR_MSG_REQUIRED")
    var id: String? = null,

    @field:NotNull(message = "Status $VALIDATOR_MSG_REQUIRED")
    var approveStatus: Int? = null,

    @field:NotNull(message = "Catatan $VALIDATOR_MSG_REQUIRED")
    var note: String? = null,

    @field:NotEmpty(message = "Answer $VALIDATOR_MSG_REQUIRED")
    var answers: List<AwpImplementationQualityAtEntryAnswerRequest>? = null,

    var qualitAtEntryNote: String? = null,
)

data class AwpImplementationQualityAtEntryAnswerRequest(

    var qualityAtEntryQuestionId: String? = null,

    var answer: Boolean? = null,

    var revision: String? = null
)
