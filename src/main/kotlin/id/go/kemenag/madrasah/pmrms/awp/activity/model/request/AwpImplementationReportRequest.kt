package id.go.kemenag.madrasah.pmrms.awp.activity.model.request

data class AwpImplementationReportRequest(

    var awpImplementationId: String? = null,

    var startDate: String? = null,

    var endDate: String? = null,

    var eventOtherInformation: String? = null,

    var eventVolume: Int? = null,

    var summary: String? = null,

    var conclusionsAndRecommendations: String? = null,

    var locationOtherInformation: String? = null,

    var activityModeId: String? = null,

    var participantTarget: String? = null,

    var nameOtherInformation: String? = null,

    var newPokIds: MutableList<String>? = emptyList<String>().toMutableList(),

    var deletedPokIds: MutableList<String>? = mutableListOf(),

    var newProvincesRegencies: MutableList<NewImplementationReportProvincesRegencies>? = emptyList<NewImplementationReportProvincesRegencies>().toMutableList(),

    var deletedProvincesRegeciesIds: List<String>? = null,

    var rraEventReports: List<RraEventReport>? = null,

    var monevIds: MutableList<ImplementationReportMonev>? = emptyList<ImplementationReportMonev>().toMutableList(),

    var newMonev: MutableList<NewImplementationReportMonev>? = emptyList<NewImplementationReportMonev>().toMutableList(),

    var bastFileId: String? = null
)

data class NewImplementationReportProvincesRegencies(
    var provinceId: String? = null,
    var regencyIds: MutableList<String>? = null
)

data class RraEventReport(
    var eventReportId: String? = null,
    var rraFileId: String? = null
)

data class ImplementationReportMonev(
    var awpReportResultChainId: String? = null,
    var awpReportSuccessIndicators: MutableList<MonevSuccessIndicatorAwpRequest>? = null
)

data class NewImplementationReportMonev(
    var resultChainId: String? = null,
    var successIndicators: MutableList<MonevSuccessIndicatorAwpRequest>? = null
)

data class MonevSuccessIndicatorAwpRequest(
    var id: String? = null,
    var contribution: String? = null
)

