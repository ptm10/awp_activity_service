package id.go.kemenag.madrasah.pmrms.awp.activity.model.request

import id.go.kemenag.madrasah.pmrms.awp.activity.constant.VALIDATOR_MSG_REQUIRED
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

data class AwpImplementationRequest(

    var awpId: String? = null,

    @field:NotEmpty(message = "Komponen $VALIDATOR_MSG_REQUIRED")
    var componentId: String? = null,

    var subComponentId: String? = null,

    var subSubComponentId: String? = null,

    @field:NotEmpty(message = "Nama Kegiatan $VALIDATOR_MSG_REQUIRED")
    var name: String? = null,

    var nameOtherInformation: String? = null,

    var locationOtherInformation: String? = null,

    var componentCodeId: String? = null,

    var budgetPok: Long? = null,

    @field:NotNull(message = "Budget AWP $VALIDATOR_MSG_REQUIRED")
    var budgetAwp: Long? = null,

    var projectOfficer: String? = null,

    var lspContractNumber: String? = null,

    var lspContractData: String? = null,

    var lspPic: String? = null,

    @field:NotEmpty(message = "Tujuan Kegiatan $VALIDATOR_MSG_REQUIRED")
    var purpose: String? = null,

    @field:NotEmpty(message = "Deskripsi Kegiatan $VALIDATOR_MSG_REQUIRED")
    var description: String? = null,

    @field:NotEmpty(message = "Cara Pengelolaan Kegiatan $VALIDATOR_MSG_REQUIRED")
    var eventManagementTypeId: String? = null,

    var activityModeId: String? = null,

    var participantTarget: String? = null,

    @field:NotEmpty(message = "Mulai Kegiatan $VALIDATOR_MSG_REQUIRED")
    var startDate: String? = null,

    @field:NotEmpty(message = "Akhir Kegiatan $VALIDATOR_MSG_REQUIRED")
    var endDate: String? = null,

    var eventOtherInformation: String? = null,

    var newProvincesRegencies: MutableList<NewImplementationProvincesRegencies>? = emptyList<NewImplementationProvincesRegencies>().toMutableList(),

    var provincesRegenciesIds: List<ImplementationProvincesRegencies>? = null,

    @field:NotNull(message = "Jumlah Event Dalam Kegiatan $VALIDATOR_MSG_REQUIRED")
    var eventVolume: Long? = null,

    @field:NotEmpty(message = "Jenis Kegiatan $VALIDATOR_MSG_REQUIRED")
    var eventTypeId: String? = null,

    var nsInstitution: String? = null,

    var nsMale: Long? = null,

    var nsFemale: Long? = null,

    @field:NotNull(message = "Jumlah Narasumber $VALIDATOR_MSG_REQUIRED")
    var nsCount: Long? = null,

    var nsOtherInformation: String? = null,

    var participantMale: Long? = null,

    var participantFemale: Long? = null,

    @field:NotNull(message = "Jumlah Peserta $VALIDATOR_MSG_REQUIRED")
    var participantCount: Long? = null,

    var participantOtherInformation: String? = null,

    @field:NotNull(message = "Asumsi $VALIDATOR_MSG_REQUIRED")
    var assumption: String? = null,

    @field:NotNull(message = "Keterangan Resiko $VALIDATOR_MSG_REQUIRED")
    var descriptionRisk: String? = null,

    @field:NotNull(message = "Resiko $VALIDATOR_MSG_REQUIRED")
    var risk: String? = null,

    var impactRisk: Int? = null,

    var potentialRisk: Int? = null,

    var mitigationRisk: String? = null,

    var symptomsRisk: String? = null,

    @field:NotNull(message = "Output Kegiatan $VALIDATOR_MSG_REQUIRED")
    var eventOutput: String? = null,

    var newPdoIds: MutableList<String>? = emptyList<String>().toMutableList(),

    var pdoIds: List<String>? = null,

    var newIriIds: MutableList<String>? = emptyList<String>().toMutableList(),

    var iriIds: List<String>? = null,

    var rabFileId: String? = null,

    var newPokIds: MutableList<String>? = emptyList<String>().toMutableList(),

    var pokIds: List<String>? = null,

    var newMonev: MutableList<NewImplementationMonev>? = emptyList<NewImplementationMonev>().toMutableList(),

    var monevIds: MutableList<ImplementationMonev>? = emptyList<ImplementationMonev>().toMutableList()
)

data class NewImplementationProvincesRegencies(
    var provinceId: String? = null,
    var regencyIds: MutableList<String>? = null
)


data class ImplementationProvincesRegencies(
    var awpProvincesId: String? = null,
    var awpProvincesRegenciesId: List<String>? = null
)

data class NewImplementationMonev(
    var resultChainId: String? = null,
    var successIndicatorIds: MutableList<String>? = null
)

data class ImplementationMonev(
    var awpResultChainId: String? = null,
    var awpSuccessIndicatorIds: List<String>? = null
)

