package id.go.kemenag.madrasah.pmrms.awp.activity.model.request

data class AwpImplementationScheduleRequest(

    var self: Boolean? = false,

    var staff: Boolean? = false,

    var eventManagementTypeId: String? = null,

    var componentIds: List<String>? = null
)
