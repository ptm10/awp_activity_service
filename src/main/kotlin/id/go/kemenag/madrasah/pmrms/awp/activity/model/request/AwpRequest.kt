package id.go.kemenag.madrasah.pmrms.awp.activity.model.request

import id.go.kemenag.madrasah.pmrms.awp.activity.constant.VALIDATOR_MSG_REQUIRED
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

data class AwpRequest(
    @field:NotNull(message = "Tahun $VALIDATOR_MSG_REQUIRED")
    var year: Int? = null,

    @field:NotEmpty(message = "Komponen $VALIDATOR_MSG_REQUIRED")
    var componentId: String? = null,

    var subComponentId: String? = null,

    var subSubComponentId: String? = null,

    var subSubComponentCode: String? = null,

    var subSubComponentName: String? = null,

    var componentCodeId: String? = null,

    var componentCode: String? = null,

    @field:NotEmpty(message = "Nama Kegiatan $VALIDATOR_MSG_REQUIRED")
    var name: String? = null,

    var nameOtherInformation: String? = null,

    @field:NotNull(message = "Budget AWP $VALIDATOR_MSG_REQUIRED")
    var budgetAwp: Long? = null,

    @field:NotEmpty(message = "Tujuan Kegiatan $VALIDATOR_MSG_REQUIRED")
    var purpose: String? = null,

    @field:NotEmpty(message = "Deskripsi Kegiatan $VALIDATOR_MSG_REQUIRED")
    var description: String? = null,

    @field:NotEmpty(message = "Cara Pengelolaan Kegiatan $VALIDATOR_MSG_REQUIRED")
    var eventManagementTypeId: String? = null,

    @field:NotEmpty(message = "Mulai Kegiatan $VALIDATOR_MSG_REQUIRED")
    var startDate: String? = null,

    @field:NotEmpty(message = "Akhir Kegiatan $VALIDATOR_MSG_REQUIRED")
    var endDate: String? = null,

    var eventOtherInformation: String? = null,

    var newProvincesRegencies: List<NewProvincesRegencies>? = null,

    var provincesRegenciesIds: List<ProvincesRegencies>? = null,

    var locationOtherInformation: String? = null,

    @field:NotNull(message = "Jumlah Event Dalam Kegiatan $VALIDATOR_MSG_REQUIRED")
    var eventVolume: Long? = null,

    @field:NotEmpty(message = "Jenis Kegiatan $VALIDATOR_MSG_REQUIRED")
    var eventTypeId: String? = null,

    var nsCount: Long? = null,

    var nsInstitution: String? = null,

    var nsOtherInformation: String? = null,

    @field:NotNull(message = "Jumlah Peserta $VALIDATOR_MSG_REQUIRED")
    var participantCount: Long? = null,

    var participantOtherInformation: String? = null,

    @field:NotNull(message = "Output Kegiatan $VALIDATOR_MSG_REQUIRED")
    var eventOutput: String? = null,

    var newPdoIds: List<String>? = null,

    var pdoIds: List<String>? = null,

    var newIriIds: List<String>? = null,

    var iriIds: List<String>? = null,

    var rabFileId: String? = null,

    var activityModeId: String? = null,

    var participantTarget: String? = null,

    var descriptionRisk: String? = null,

    var mitigationRisk: String? = null,

    var risk: String? = null,

    var potentialRisk: Int? = null,

    var impactRisk: Int? = null,

    var symptomsRisk: String? = null
)

data class NewProvincesRegencies(
    var provinceId: String? = null,
    var regencyIds: List<String>? = null
)

data class ProvincesRegencies(
    var awpProvincesId: String? = null,
    var awpProvincesRegenciesId: List<String>? = null
)

