package id.go.kemenag.madrasah.pmrms.awp.activity.model.request

data class EventFormRequest(

    var eventTypeId: String? = null,

    var newQuestions: List<EventQuestion>? = null,

    var deletedQuestionIds: List<String>? = null
)

data class EventQuestion(
    var questionType: Int? = null,
    var createdFor: Int? = null,
    var questionCategory: Int? = null,
    var question: String? = null
)
