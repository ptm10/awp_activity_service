package id.go.kemenag.madrasah.pmrms.awp.activity.model.request

import id.go.kemenag.madrasah.pmrms.awp.activity.constant.VALIDATOR_MSG_REQUIRED
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

data class EventParticipantQuestionRequest(

    @field:NotEmpty(message = "Event Type Id $VALIDATOR_MSG_REQUIRED")
    var eventTypeId: String? = null,

    @field:NotEmpty(message = "Name $VALIDATOR_MSG_REQUIRED")
    var name: String? = null,

    @field:NotNull(message = "Queston Type $VALIDATOR_MSG_REQUIRED")
    var questionType: Int? = null,

    @field:NotEmpty(message = "Queston $VALIDATOR_MSG_REQUIRED")
    var question: String? = null,
)
