package id.go.kemenag.madrasah.pmrms.awp.activity.model.request

import id.go.kemenag.madrasah.pmrms.awp.activity.constant.VALIDATOR_MSG_REQUIRED
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

data class EventProgressReportRequest(
    @field:NotEmpty(message = "Event Id $VALIDATOR_MSG_REQUIRED")
    var eventId: String? = null,

    @field:NotNull(message = "Output Event $VALIDATOR_MSG_REQUIRED")
    var eventOutput: String? = null,

    var participantMale: Long? = null,

    var participantFemale: Long? = null,

    @field:NotNull(message = "Jumlah Peserta $VALIDATOR_MSG_REQUIRED")
    var participantCount: Long? = null,

    var participantOtherInformation: String? = null,

    var participantTarget: String? = null,

    var locationOtherInformation: String? = null,

    @field:NotEmpty(message = "Mulai Laporan Kemajuan Event $VALIDATOR_MSG_REQUIRED")
    var startDate: String? = null,

    @field:NotEmpty(message = "Akhir Laporan Kemajuan Event $VALIDATOR_MSG_REQUIRED")
    var endDate: String? = null,

    var eventOtherInformation: String? = null,

    var activityModeId: String? = null,

    @field:NotNull(message = "Provinsi $VALIDATOR_MSG_REQUIRED")
    var provinceId: String? = null,

    var regencyId: String? = null,

    var location: String? = null,

    var summary: String? = null,

    var attachmentFileId: String? = null,

    var newStaff: MutableList<String>? = mutableListOf(),

    var deletedStaffIds: MutableList<String>? = mutableListOf(),

    var newOtherStaff: MutableList<EventProgressReportOtherStaffRequest>? = mutableListOf(),

    var deletedOtherStaffIds: MutableList<String>? = mutableListOf(),

    var newDocuments: MutableList<EventProgressReportDocument>? = mutableListOf(),

    var deletedDocumentIds: MutableList<String>? = mutableListOf(),

    var budgetAvailable: Boolean? = null,

    var pokEvent: Boolean? = null,

    var rraFileId: String? = null
)


data class EventProgressReportOtherStaffRequest(
    var name: String? = null,
    var position: String? = null,
    var institution: String? = null
)

data class EventProgressReportDocument(
    var title: String? = null,
    var eventDocumentTypeId: String? = null,
    var date: String? = null,
    var place: String? = null,
    var description: String? = null,
    var fileId: String? = null
)

