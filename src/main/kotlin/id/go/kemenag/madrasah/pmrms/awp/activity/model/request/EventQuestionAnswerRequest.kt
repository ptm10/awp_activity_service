package id.go.kemenag.madrasah.pmrms.awp.activity.model.request

import id.go.kemenag.madrasah.pmrms.awp.activity.constant.VALIDATOR_MSG_REQUIRED
import javax.validation.constraints.NotEmpty

data class EventQuestionAnswerRequest(

    @field:NotEmpty(message = "Identity id $VALIDATOR_MSG_REQUIRED")
    var id: String? = null,

    var identityId: String? = null,

    var username: String? = null,

    var gender: Char? = null,

    var institution: String? = null,

    @field:NotEmpty(message = "Email $VALIDATOR_MSG_REQUIRED")
    var email: String? = null,

    var phoneNumber: String? = null,

    var answers: List<EventQuestionAnswerRequestAnswer>? = null
)


data class EventQuestionAnswerRequestAnswer(

    var questionId: String? = null,

    var answer: Any? = null
)
