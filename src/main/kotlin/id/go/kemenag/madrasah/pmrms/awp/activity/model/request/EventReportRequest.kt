package id.go.kemenag.madrasah.pmrms.awp.activity.model.request

import id.go.kemenag.madrasah.pmrms.awp.activity.constant.VALIDATOR_MSG_REQUIRED
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

data class EventReportRequest(
    @field:NotEmpty(message = "Event Id $VALIDATOR_MSG_REQUIRED")
    var eventId: String? = null,

    @field:NotNull(message = "Topik $VALIDATOR_MSG_REQUIRED")
    var topic: String? = null,

    var eventCode: String? = null,

    @field:NotNull(message = "Provinsi $VALIDATOR_MSG_REQUIRED")
    var provinceId: String? = null,

    var regencyId: String? = null,

    var startDate: String? = null,

    var endDate: String? = null,

    var location: String? = null,

    var participantTarget: String? = null,

    var locationOtherInformation: String? = null,

    var participantMale: Long? = null,

    var participantFemale: Long? = null,

    @field:NotNull(message = "Jumlah Peserta $VALIDATOR_MSG_REQUIRED")
    var participantCount: Long? = null,

    var participantOtherInformation: String? = null,

    var implementationSummary: String? = null,

    var eventEvaluationDescription: String? = null,

    var conclusionsAndRecommendations: String? = null,

    var eventOutput: String? = null,

    var activityModeId: String? = null,

    var attachmentFileId: String? = null,

    var rraFileId: String? = null,

    var budgetSource: String? = null,

    var budgetYear: Long? = null,

    var makNumber: String? = null,

    var budgetCeiling: Long? = null,

    var rraBudget: Long? = null,

    var paragraph1: String? = null,

    var paragraph2: String? = null,

    var p3DurationDays: Int? = null,

    var p3DurationMonth: Int? = null,

    var p3DurationYear: Int? = null,

    var p3EventType: String? = null,

    var p3StartDate: String? = null,

    var p3EndDate: String? = null,

    var paragraph4: String? = null,

    var paragraph5: String? = null,

    var technicalImplementer: String? = null,

    var totalDuration: String? = null,

    var schemaEvent: String? = null,

    var newDocuments: MutableList<EventProgressReportDocument>? = mutableListOf(),

    var deletedDocumentIds: MutableList<String>? = mutableListOf(),

    var newEventReportEvaluationPoints: MutableList<EventReportEvaluationPointsReqeust>? = mutableListOf(),

    var deletedEventReportEvaluationPointsId: MutableList<String>? = mutableListOf(),

    var newEventReportParticipantTargets: MutableList<EventReportParticipantTargetPositionReqeust>? = mutableListOf(),

    var deletedEventRerportParticipantTargetsId: MutableList<String>? = mutableListOf(),

    var newPokIds: MutableList<String>? = emptyList<String>().toMutableList(),

    var deletedPokIds: MutableList<String>? = mutableListOf(),

    var questionAnswer: MutableList<EventReportQuestionAnswer>? = emptyList<EventReportQuestionAnswer>().toMutableList(),
)

data class EventReportEvaluationPointsReqeust(
    var newEvaluationPoint: String? = null,
    var evaluationPointId: String? = null,
    var value: Int? = null,
    var note: String? = null
)

data class EventReportParticipantTargetPositionReqeust(
    var newParticipantTarget: String? = null,
    var participantTargetId: String? = null,
    var value: Int? = null
)

data class EventReportQuestionAnswer(
    var eventQuestionId: String? = null,
    var answer: Any? = null,
    var comment: String? = null
)
