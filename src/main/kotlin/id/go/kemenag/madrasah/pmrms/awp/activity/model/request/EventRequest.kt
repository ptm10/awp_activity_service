package id.go.kemenag.madrasah.pmrms.awp.activity.model.request

import id.go.kemenag.madrasah.pmrms.awp.activity.constant.VALIDATOR_MSG_REQUIRED
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

data class EventRequest(
    @field:NotEmpty(message = "Awp Implementation id $VALIDATOR_MSG_REQUIRED")
    var awpImplementationId: String? = null,

    @field:NotEmpty(message = "Nama Event $VALIDATOR_MSG_REQUIRED")
    var name: String? = null,

    var nameOtherInformation: String? = null,

    var locationOtherInformation: String? = null,

    @field:NotEmpty(message = "Mulai Event $VALIDATOR_MSG_REQUIRED")
    var startDate: String? = null,

    @field:NotEmpty(message = "Akhir Event $VALIDATOR_MSG_REQUIRED")
    var endDate: String? = null,

    var eventOtherInformation: String? = null,

    var activityModeId: String? = null,

    var participantTarget: String? = null,

    @field:NotEmpty(message = "Jenis Event $VALIDATOR_MSG_REQUIRED")
    var eventTypeId: String? = null,

    var background: String? = null,

    @field:NotEmpty(message = "Deskripsi Event $VALIDATOR_MSG_REQUIRED")
    var description: String? = null,

    @field:NotEmpty(message = "Tujuan Event $VALIDATOR_MSG_REQUIRED")
    var purpose: String? = null,

    @field:NotNull(message = "Output Event $VALIDATOR_MSG_REQUIRED")
    var eventOutput: String? = null,

    var participantMale: Long? = null,

    var participantFemale: Long? = null,

    @field:NotNull(message = "Jumlah Peserta $VALIDATOR_MSG_REQUIRED")
    var participantCount: Long? = null,

    var participantOtherInformation: String? = null,

    var contactPerson: String? = null,

    @field:NotNull(message = "Provinsi $VALIDATOR_MSG_REQUIRED")
    var provinceId: String? = null,

    var regencyId: String? = null,

    var location: String? = null,

    var rabFileId: String? = null,

    var agendaFileId: String? = null,

    var pokEvent: Boolean? = null,

    @field:NotNull(message = "Budget POK $VALIDATOR_MSG_REQUIRED")
    var budgetPok: Long? = null,

    var newStaff: MutableList<String>? = mutableListOf(),

    var staffIds: List<String>? = null,

    var newOtherStaff: MutableList<EventOtherStaffRequest>? = mutableListOf(),

    var otherStaffIds: List<String>? = null,

    var newEventAgendas: List<NewEventAgendaRequest>? = null,

    var eventAgendas: List<EventAgendaRequest>? = null,

    var newEventRab: List<NewEventRabRequest>? = null,

    var eventRab: List<EventRabRequest>? = null,

    var newPokIds: MutableList<String>? = emptyList<String>().toMutableList(),

    var pokIds: List<String>? = null
)

data class NewEventRabRequest(
    var code: String? = null,
    var name: String? = null,
    var details: List<NewEventRabDetailRequest>? = null
)

data class NewEventRabDetailRequest(
    var name: String? = null,
    var quantity: Long? = null,
    var price: Long? = null
)

data class EventOtherStaffRequest(
    var name: String? = null,
    var position: String? = null,
    var institution: String? = null
)

data class NewEventAgendaRequest(
    var date: String? = null,
    var details: List<NewEventAgendaDetailRequest>
)

data class NewEventAgendaDetailRequest(
    var startTime: String? = null,
    var endTime: String? = null,
    var activity: String? = null,
    var pic: String? = null
)

data class EventAgendaRequest(
    var eventAgendaId: String? = null,
    var eventAgendaDetailIds: List<String>? = null,
)

data class EventRabRequest(
    var eventRabId: String? = null,
    var eventRabDetailIds: List<String>? = null,
)

