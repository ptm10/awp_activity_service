package id.go.kemenag.madrasah.pmrms.awp.activity.model.request

data class EventScheduleRequest(

    var self: Boolean? = false,

    var staff: Boolean? = false,

    var componentIds: List<String>? = null
)
