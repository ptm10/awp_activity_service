package id.go.kemenag.madrasah.pmrms.awp.activity.model.request

import javax.validation.constraints.NotEmpty

data class IdRequest(
    @field:NotEmpty
    var id: String? = null
)
