package id.go.kemenag.madrasah.pmrms.awp.activity.model.request

data class MonevRequest(

    var pdoId: String? = null,

    var iriId: String? = null,

    var baselineValue: String? = null,

    var targetTotal: String? = null,

    var cumulativeTarget2020: String? = null,

    var cumulativeTargetDummy2020: Boolean? = null,

    var cumulativeTarget2021: String? = null,

    var cumulativeTargetDummy2021: Boolean? = null,

    var cumulativeTarget2022: String? = null,

    var cumulativeTargetDummy2022: Boolean? = null,

    var cumulativeTarget2023: String? = null,

    var cumulativeTargetDummy2023: Boolean? = null,

    var cumulativeTarget2024: String? = null,

    var cumulativeTargetDummy2024: Boolean? = null
)

