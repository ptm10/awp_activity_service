package id.go.kemenag.madrasah.pmrms.awp.activity.model.request

import id.go.kemenag.madrasah.pmrms.awp.activity.constant.VALIDATOR_MSG_REQUIRED
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

data class MonevResultChainRequest(

    @field:NotNull(message = "Tahun $VALIDATOR_MSG_REQUIRED")
    var year: Int? = null,

    @field:NotEmpty(message = "Type Monev $VALIDATOR_MSG_REQUIRED")
    var monevTypeId: String? = null,

    var componentId: String? = null,

    var subComponentId: String? = null,

    var parrentId: String? = null,

    @field:NotEmpty(message = "Code $VALIDATOR_MSG_REQUIRED")
    var code: String? = null,

    var name: String? = null,

    var newSuccessIndicators: List<MonevSuccessIndicatorDataRequest>? = null,

    var deletedSuccessIndicatorsIds: List<String>? = null,

    var newQuestions: List<MonevResultChainQuestionRequest>? = null,

    var deletedQuestionIds: List<String>? = null
)

data class MonevSuccessIndicatorDataRequest(

    var code: String? = null,

    var name: String? = null,

    var pdoId: String? = null,

    var iriId: String? = null,

    var baselineValue: String? = null,

    var targetTotal: String? = null,

    var cumulativeTarget2020: String? = null,

    var cumulativeTargetDummy2020: Boolean? = null,

    var notApplicable2020: Boolean? = null,

    var cumulativeTarget2021: String? = null,

    var cumulativeTargetDummy2021: Boolean? = null,

    var notApplicable2021: Boolean? = null,

    var cumulativeTarget2022: String? = null,

    var cumulativeTargetDummy2022: Boolean? = null,

    var notApplicable2022: Boolean? = null,

    var cumulativeTarget2023: String? = null,

    var cumulativeTargetDummy2023: Boolean? = null,

    var notApplicable2023: Boolean? = null,

    var cumulativeTarget2024: String? = null,

    var cumulativeTargetDummy2024: Boolean? = null,

    var notApplicable2024: Boolean? = null
)

data class MonevResultChainQuestionRequest(

    var question: String? = null,

    var yesOrNo: Boolean? = null
)

