package id.go.kemenag.madrasah.pmrms.awp.activity.model.request

import id.go.kemenag.madrasah.pmrms.awp.activity.constant.VALIDATOR_MSG_REQUIRED
import javax.validation.constraints.NotNull

data class MonevResultChainUpdateResultRequest(

    @field:NotNull(message = "Tahun $VALIDATOR_MSG_REQUIRED")
    var year: Int? = null,

    var successIndicatorResult: List<SuccessIndicatorResultRequest>? = null,

    var answers: List<ResultChainAnswerRequest>? = null,

    var status: Int? = null
)

data class SuccessIndicatorResultRequest(

    var id: String? = null,

    var result: String? = null,

    var newFilesIds: List<String>? = null,

    var deletedFilesIds: List<String>? = null
)

data class ResultChainAnswerRequest(

    var id: String? = null,

    var yesOrNoAnswer: Boolean? = null,

    var answer: String? = null
)

