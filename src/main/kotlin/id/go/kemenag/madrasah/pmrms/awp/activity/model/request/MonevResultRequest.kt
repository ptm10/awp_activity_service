package id.go.kemenag.madrasah.pmrms.awp.activity.model.request

import id.go.kemenag.madrasah.pmrms.awp.activity.constant.VALIDATOR_MSG_REQUIRED
import javax.validation.constraints.NotNull

data class MonevResultRequest(
    @field:NotNull(message = "Tahun $VALIDATOR_MSG_REQUIRED")
    var year: Int? = null,

    var resultStatus: Int? = null,

    var successIndicators: List<SuccessIndicator>? = null,

    var answers: List<MonevAnswer>? = null
)


data class SuccessIndicator(

    var successIndicatorId: String? = null,

    var result: String? = null
)


data class MonevAnswer(

    var questionId: String? = null,

    var yesOrNoAnswer: Boolean? = null,

    var answer: String? = null
)

