package id.go.kemenag.madrasah.pmrms.awp.activity.model.request

import id.go.kemenag.madrasah.pmrms.awp.activity.constant.VALIDATOR_MSG_REQUIRED
import javax.validation.constraints.NotEmpty

data class ProjectReportDocumentRequest(

    @field:NotEmpty(message = "Start Date $VALIDATOR_MSG_REQUIRED")
    var startDate: String? = null,

    @field:NotEmpty(message = "End Date $VALIDATOR_MSG_REQUIRED")
    var endDate: String? = null,

    var componentIds: List<String>? = null,
)
