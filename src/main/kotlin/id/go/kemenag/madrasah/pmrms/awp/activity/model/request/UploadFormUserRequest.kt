package id.go.kemenag.madrasah.pmrms.awp.activity.model.request

import id.go.kemenag.madrasah.pmrms.awp.activity.constant.VALIDATOR_MSG_REQUIRED
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

data class UploadFormUserRequest(

    @field:NotEmpty(message = "File Id $VALIDATOR_MSG_REQUIRED")
    var fileId: String? = null,

    @field:NotEmpty(message = "Event Id $VALIDATOR_MSG_REQUIRED")
    var eventId: String? = null,

    @field:NotNull(message = "Created For $VALIDATOR_MSG_REQUIRED")
    var createdFor: Int? = null
)
