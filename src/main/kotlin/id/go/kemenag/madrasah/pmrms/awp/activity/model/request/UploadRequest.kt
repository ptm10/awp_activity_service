package id.go.kemenag.madrasah.pmrms.awp.activity.model.request

import org.springframework.web.multipart.MultipartFile

data class UploadRequest(

    var files: MutableList<MultipartFile>? = null
)

