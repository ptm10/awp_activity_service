package id.go.kemenag.madrasah.pmrms.awp.activity.model.response

data class ErrorMessage(
    var key: String? = null,
    var message: String? = null
)
