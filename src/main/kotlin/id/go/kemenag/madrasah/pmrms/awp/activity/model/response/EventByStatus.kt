package id.go.kemenag.madrasah.pmrms.awp.activity.model.response

data class EventByStatus(

    var done: String? = "00",

    var onGoing: String? = "00",

    var planning: String? = "00"
)
