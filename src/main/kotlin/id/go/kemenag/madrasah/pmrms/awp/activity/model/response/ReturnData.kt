package id.go.kemenag.madrasah.pmrms.awp.activity.model.response

data class ReturnData(
    var success: Boolean? = false,
    var data: Any? = null,
    var message: String? = null
)
