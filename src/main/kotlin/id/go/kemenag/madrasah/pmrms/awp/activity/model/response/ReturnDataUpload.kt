package id.go.kemenag.madrasah.pmrms.awp.activity.model.response

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.Files

data class ReturnDataUpload(
    var success: Boolean? = false,
    var data: List<Files>? = emptyList(),
    var message: String? = null
)

