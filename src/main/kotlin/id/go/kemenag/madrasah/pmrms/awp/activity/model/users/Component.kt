package id.go.kemenag.madrasah.pmrms.awp.activity.model.users

import java.util.*

data class Component(
    var id: String? = UUID.randomUUID().toString(),

    var code: String? = null,

    var description: String? = null,
)
