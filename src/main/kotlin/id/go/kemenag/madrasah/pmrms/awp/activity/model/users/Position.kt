package id.go.kemenag.madrasah.pmrms.resources.model.users

import kotlin.Unit

data class Position(

    var id: String? = null,

    var unitId: String? = null,

    var unit: Unit? = null,

    var name: String? = null
)
