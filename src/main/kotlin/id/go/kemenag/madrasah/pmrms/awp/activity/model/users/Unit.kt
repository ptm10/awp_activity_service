package id.go.kemenag.madrasah.pmrms.awp.activity.model.users

data class Unit(

    var id: String? = null,

    var name: String? = null
)
