package id.go.kemenag.madrasah.pmrms.awp.activity.model.users

data class UsersRole(

    var id: String? = null,

    var userId: String? = null,

    var roleId: String? = null,

    var role: Role? = null,
)
