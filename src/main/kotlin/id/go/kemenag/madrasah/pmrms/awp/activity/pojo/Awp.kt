package id.go.kemenag.madrasah.pmrms.awp.activity.pojo

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import id.go.kemenag.madrasah.pmrms.awp.activity.constant.AWP_STATUS_NEW
import org.hibernate.annotations.NotFound
import org.hibernate.annotations.NotFoundAction
import org.hibernate.annotations.Where
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "awp", schema = "public")
data class Awp(
    @Id
    @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @Column(name = "year")
    var year: Int? = null,

    @Column(name = "component_id")
    var componentId: String? = null,

    @ManyToOne
    @JoinColumn(name = "component_id", insertable = false, updatable = false, nullable = true)
    var component: Component? = null,

    @Column(name = "subcomponent_id")
    var subComponentId: String? = null,

    @ManyToOne
    @JoinColumn(name = "subcomponent_id", insertable = false, updatable = false, nullable = true)
    var subComponent: Component? = null,

    @Column(name = "sub_sub_component_id")
    var subSubComponentId: String? = null,

    @Column(name = "sub_sub_component_name")
    var subSubComponentName: String? = null,

    @ManyToOne
    @JoinColumn(name = "sub_sub_component_id", insertable = false, updatable = false, nullable = true)
    var subSubComponent: Component? = null,

    @Column(name = "name")
    var name: String? = null,

    @Column(name = "name_other_information")
    var nameOtherInformation: String? = null,

    @Column(name = "component_code_id")
    var componentCodeId: String? = null,

    @ManyToOne
    @JoinColumn(name = "component_code_id", insertable = false, updatable = false, nullable = true)
    var componentCode: Component? = null,

    @Column(name = "budget_awp")
    var budgetAwp: Long? = null,

    @Column(name = "event_management_type_id")
    var eventManagementTypeId: String? = null,

    @ManyToOne
    @JoinColumn(name = "event_management_type_id", insertable = false, updatable = false, nullable = true)
    var eventManagementType: EventManagementType? = null,

    @Column(name = "activity_mode_id")
    var activityModeId: String? = null,

    @ManyToOne
    @JoinColumn(name = "activity_mode_id", insertable = false, updatable = false, nullable = true)
    var activityMode: ActivityMode? = null,

    @Column(name = "purpose")
    var purpose: String? = null,

    @Column(name = "description")
    var description: String? = null,

    @Column(name = "start_date")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+7")
    var startDate: Date? = null,

    @Column(name = "end_date")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+7")
    var endDate: Date? = null,

    @Column(name = "event_other_information")
    var eventOtherInformation: String? = null,

    @OneToMany(mappedBy = "awpId")
    @Where(clause = "active = true")
    var provincesRegencies: MutableList<AwpProvinces>? = emptyList<AwpProvinces>().toMutableList(),

    @Column(name = "location_other_information")
    var locationOtherInformation: String? = null,

    @Column(name = "event_volume")
    var eventVolume: Long? = null,

    @Column(name = "event_type_id")
    var eventTypeId: String? = null,

    @ManyToOne
    @JoinColumn(name = "event_type_id", insertable = false, updatable = false, nullable = true)
    var eventType: EventType? = null,

    @Column(name = "ns_institution")
    var nsInstitution: String? = null,

    @Column(name = "ns_count")
    var nsCount: Long? = 0,

    @Column(name = "ns_other_information")
    var nsOtherInformation: String? = null,

    @Column(name = "participant_count")
    var participantCount: Long? = 0,

    @Column(name = "participant_other_information")
    var participantOtherInformation: String? = null,

    @Column(name = "event_output")
    var eventOutput: String? = null,

    @Column(name = "participant_target")
    var participantTarget: String? = null,

    @Column(name = "description_risk")
    var descriptionRisk: String? = null,

    @Column(name = "mitigation_risk")
    var mitigationRisk: String? = null,

    @Column(name = "rab_file_id")
    var rabFileId: String? = null,

    @NotFound(action = NotFoundAction.IGNORE)
    @ManyToOne
    @JoinColumn(name = "rab_file_id", insertable = false, updatable = false, nullable = true)
    var rabFile: Files? = null,

    @OneToMany(mappedBy = "awpId")
    @Where(clause = "active = true")
    var pdo: MutableList<AwpPdo>? = emptyList<AwpPdo>().toMutableList(),

    @OneToMany(mappedBy = "awpId")
    @Where(clause = "active = true")
    var iri: MutableList<AwpIri>? = emptyList<AwpIri>().toMutableList(),

    //keterangan resiko
    @Column(name = "risk")
    var risk: String? = null,

    // Kemungkinan Terjadi
    @Column(name = "potential_risk")
    var potentialRisk: Int? = null,

    // dampak
    @Column(name = "impact_risk")
    var impactRisk: Int? = null,

    // Gejala Risiko
    @Column(name = "symptoms_risk")
    var symptomsRisk: String? = null,

    @Column(name = "status")
    var status: Int? = AWP_STATUS_NEW,

    @Column(name = "created_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var createdAt: Date? = Date(),

    @Column(name = "updated_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var updatedAt: Date? = Date(),

    @Column(name = "created_by")
    var createdBy: String? = null,

    @ManyToOne
    @JoinColumn(name = "created_by", insertable = false, updatable = false, nullable = true)
    var createdByUser: VUsersResources? = null,

    @Column(name = "updated_by")
    var updatedBy: String? = null,

    @ManyToOne
    @JoinColumn(name = "updated_by", insertable = false, updatable = false, nullable = true)
    var updatedByUser: VUsersResources? = null,

    @Column(name = "active")
    @JsonIgnore
    var active: Boolean? = true
)
