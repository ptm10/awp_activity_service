package id.go.kemenag.madrasah.pmrms.awp.activity.pojo

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import org.hibernate.annotations.Where
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "awp_data", schema = "public")
data class AwpData(
    @Id
    @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @Column(name = "year")
    var year: Int? = null,

    @Column(name = "keterangan_kontribusi_komponent")
    var keteranganKontribusiKomponent: String? = null,

    @Column(name = "keterangan_kemajuan_program")
    var keteranganKemajuanProgram: String? = null,

    @Column(name = "keterangan_kendala")
    var keteranganKendala: String? = null,

    @Column(name = "keterangan_pembelajaran")
    var keteranganPembelajaran: String? = null,

    @Column(name = "keterangan_rencana_kerja")
    var keteranganRencanaKerja: String? = null,

    @Column(name = "keterangan_usulan_kegiatan")
    var keteranganUsulanKegiatan: String? = null,

    @Column(name = "dipa_awal_komponen_1")
    var dipaAwalKomponen1: Long? = null,

    @Column(name = "dipa_awal_komponen_2")
    var dipaAwalKomponen2: Long? = null,

    @Column(name = "dipa_awal_komponen_3")
    var dipaAwalKomponen3: Long? = null,

    @Column(name = "dipa_awal_komponen_4")
    var dipaAwalKomponen4: Long? = null,

    @Column(name = "dipa_revisi_komponen_1")
    var dipaRevisiKomponen1: Long? = null,

    @Column(name = "dipa_revisi_komponen_2")
    var dipaRevisiKomponen2: Long? = null,

    @Column(name = "dipa_revisi_komponen_3")
    var dipaRevisiKomponen3: Long? = null,

    @Column(name = "dipa_revisi_komponen_4")
    var dipaRevisiKomponen4: Long? = null,

    @Column(name = "dipa_realisasi_komponen_1")
    var dipaRealisasiKomponen1: Long? = null,

    @Column(name = "dipa_realisasi_komponen_2")
    var dipaRealisasiKomponen2: Long? = null,

    @Column(name = "dipa_realisasi_komponen_3")
    var dipaRealisasiKomponen3: Long? = null,

    @Column(name = "dipa_realisasi_komponen_4")
    var dipaRealisasiKomponen4: Long? = null,

    @Column(name = "dipa_awal_rupiah_murni")
    var dipaAwalRupiahMurni: Long? = null,

    @Column(name = "dipa_revisi_rupiah_murni")
    var dipaRevisiRupiahMurni: Long? = null,

    @Column(name = "dipa_realisasi_rupiah_murni")
    var dipaRealisasiRupiahMurni: Long? = null,

    @Column(name = "hambatan_komponen_1")
    var hambatanKomponen1: String? = null,

    @Column(name = "hambatan_komponen_2")
    var hambatanKomponen2: String? = null,

    @Column(name = "hambatan_komponen_3")
    var hambatanKomponen3: String? = null,

    @Column(name = "hambatan_komponen_4")
    var hambatanKomponen4: String? = null,

    @Column(name = "fokus_komponen_1")
    var fokusKomponen1: String? = null,

    @Column(name = "fokus_komponen_2")
    var fokusKomponen2: String? = null,

    @Column(name = "fokus_komponen_3")
    var fokusKomponen3: String? = null,

    @Column(name = "fokus_komponen_4")
    var fokusKomponen4: String? = null,

    @OneToMany(mappedBy = "awpDataId")
    @Where(clause = "active = true")
    var pdo: MutableList<AwpDataPdo>? = emptyList<AwpDataPdo>().toMutableList(),

    @OneToMany(mappedBy = "awpDataId")
    @Where(clause = "active = true")
    var files: MutableList<AwpDataFiles>? = emptyList<AwpDataFiles>().toMutableList(),

    @OneToMany(mappedBy = "awpDataId")
    @Where(clause = "active = true")
    var otherFiles: MutableList<AwpDataOtherFiles>? = emptyList<AwpDataOtherFiles>().toMutableList(),

    @Column(name = "created_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var createdAt: Date? = Date(),

    @Column(name = "updated_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var updatedAt: Date? = Date(),

    @Column(name = "created_by")
    var createdBy: String? = null,

    @ManyToOne
    @JoinColumn(name = "created_by", insertable = false, updatable = false, nullable = true)
    var createdByUser: VUsersResources? = null,

    @Column(name = "updated_by")
    var updatedBy: String? = null,

    @ManyToOne
    @JoinColumn(name = "updated_by", insertable = false, updatable = false, nullable = true)
    var updatedByUser: VUsersResources? = null,

    @Column(name = "active")
    @JsonIgnore
    var active: Boolean? = true
)
