package id.go.kemenag.madrasah.pmrms.awp.activity.pojo

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import org.hibernate.annotations.Where
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "awp_data_pdo", schema = "public")
data class AwpDataPdo(
    @Id
    @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @Column(name = "awp_data_id")
    var awpDataId: String? = null,

    @Column(name = "pdo_id")
    var pdoId: String? = null,

    @ManyToOne
    @JoinColumn(name = "pdo_id", insertable = false, updatable = false, nullable = true)
    var pdo: Pdo? = null,

    @OneToMany(mappedBy = "awpDataPdoId")
    @Where(clause = "active = true")
    var iri: MutableList<AwpDataPdoIri>? = emptyList<AwpDataPdoIri>().toMutableList(),

    @Column(name = "target")
    var target: String? = null,

    @Column(name = "created_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var createdAt: Date? = Date(),

    @Column(name = "updated_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var updatedAt: Date? = Date(),

    @Column(name = "active")
    var active: Boolean? = true
)
