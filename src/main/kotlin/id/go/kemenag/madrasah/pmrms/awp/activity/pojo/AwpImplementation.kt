package id.go.kemenag.madrasah.pmrms.awp.activity.pojo

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import id.go.kemenag.madrasah.pmrms.awp.activity.constant.AWP_IMPLEMENTATION_STATUS_PLANNING
import id.go.kemenag.madrasah.pmrms.awp.activity.constant.EVENT_REPORT_STATUS_APPROVED
import org.hibernate.annotations.NotFound
import org.hibernate.annotations.NotFoundAction
import org.hibernate.annotations.Where
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "awp_implementation", schema = "public")
data class AwpImplementation(
    @Id
    @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @Column(name = "awp_id")
    var awpId: String? = null,

    @ManyToOne
    @JoinColumn(name = "awp_id", insertable = false, updatable = false, nullable = true)
    var awp: Awp? = null,

    @Column(name = "component_id")
    var componentId: String? = null,

    @ManyToOne
    @JoinColumn(name = "component_id", insertable = false, updatable = false, nullable = true)
    var component: Component? = null,

    @Column(name = "subcomponent_id")
    var subComponentId: String? = null,

    @ManyToOne
    @JoinColumn(name = "subcomponent_id", insertable = false, updatable = false, nullable = true)
    var subComponent: Component? = null,

    @Column(name = "sub_sub_component_id")
    var subSubComponentId: String? = null,

    @ManyToOne
    @JoinColumn(name = "sub_sub_component_id", insertable = false, updatable = false, nullable = true)
    var subSubComponent: Component? = null,

    @Column(name = "name")
    var name: String? = null,

    @Column(name = "name_other_information")
    var nameOtherInformation: String? = null,

    @Column(name = "location_other_information")
    var locationOtherInformation: String? = null,

    @Column(name = "component_code_id")
    var componentCodeId: String? = null,

    @ManyToOne
    @JoinColumn(name = "component_code_id", insertable = false, updatable = false, nullable = true)
    var componentCode: Component? = null,

    @Column(name = "budget_pok")
    var budgetPok: Long? = null,

    @Column(name = "budget_awp")
    var budgetAwp: Long? = null,

    @Column(name = "project_officer")
    var projectOfficer: String? = null,

    @ManyToOne
    @JoinColumn(name = "project_officer", insertable = false, updatable = false, nullable = true)
    var projectOfficerResources: Resources? = null,

    @Column(name = "lsp_contract_number")
    var lspContractNumber: String? = null,

    @Column(name = "lsp_contract_data")
    var lspContractData: String? = null,

    @Column(name = "lsp_pic")
    var lspPic: String? = null,

    @ManyToOne
    @JoinColumn(name = "lsp_pic", insertable = false, updatable = false, nullable = true)
    var lspPicUser: VUsersResources? = null,

    @Column(name = "event_management_type_id")
    var eventManagementTypeId: String? = null,

    @ManyToOne
    @JoinColumn(name = "event_management_type_id", insertable = false, updatable = false, nullable = true)
    var eventManagementType: EventManagementType? = null,

    @Column(name = "activity_mode_id")
    var activityModeId: String? = null,

    @ManyToOne
    @JoinColumn(name = "activity_mode_id", insertable = false, updatable = false, nullable = true)
    var activityMode: ActivityMode? = null,

    @Column(name = "purpose")
    var purpose: String? = null,

    @Column(name = "description")
    var description: String? = null,

    @Column(name = "start_date")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+7")
    var startDate: Date? = null,

    @Column(name = "end_date")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+7")
    var endDate: Date? = null,

    @Column(name = "event_other_information")
    var eventOtherInformation: String? = null,

    @OneToMany(mappedBy = "awpImplementationId")
    @Where(clause = "active = true")
    var provincesRegencies: MutableList<AwpImplementationProvinces>? = emptyList<AwpImplementationProvinces>().toMutableList(),

    @OneToMany(mappedBy = "awpImplementationId")
    @Where(clause = "active = true")
    var monev: MutableList<AwpImplementationResultChain>? = emptyList<AwpImplementationResultChain>().toMutableList(),

    @Column(name = "event_volume")
    var eventVolume: Long? = null,

    @Column(name = "event_type_id")
    var eventTypeId: String? = null,

    @ManyToOne
    @JoinColumn(name = "event_type_id", insertable = false, updatable = false, nullable = true)
    var eventType: EventType? = null,

    @Column(name = "ns_institution")
    var nsInstitution: String? = null,

    @Column(name = "ns_male")
    var nsMale: Long? = 0,

    @Column(name = "ns_female")
    var nsFemale: Long? = 0,

    @Column(name = "ns_count")
    var nsCount: Long? = 0,

    @Column(name = "ns_other_information")
    var nsOtherInformation: String? = null,

    @Column(name = "participant_male")
    var participantMale: Long? = 0,

    @Column(name = "participant_female")
    var participantFemale: Long? = 0,

    @Column(name = "participant_count")
    var participantCount: Long? = 0,

    @Column(name = "participant_other_information")
    var participantOtherInformation: String? = null,

    @Column(name = "participant_target")
    var participantTarget: String? = null,

    @Column(name = "assumption")
    var assumption: String? = null,

    @Column(name = "description_risk")
    var descriptionRisk: String? = null,

    @Column(name = "risk")
    var risk: String? = null,

    @Column(name = "impact_risk")
    var impactRisk: Int? = null,

    @Column(name = "potential_risk")
    var potentialRisk: Int? = null,

    @Column(name = "mitigation_risk")
    var mitigationRisk: String? = null,

    @Column(name = "symptoms_risk")
    var symptomsRisk: String? = null,

    @Column(name = "event_output")
    var eventOutput: String? = null,

    @Column(name = "rab_file_id")
    var rabFileId: String? = null,

    @NotFound(action = NotFoundAction.IGNORE)
    @ManyToOne
    @JoinColumn(name = "rab_file_id", insertable = false, updatable = false, nullable = true)
    var rabFile: Files? = null,

    @OneToMany(mappedBy = "awpImplementationId")
    @Where(clause = "active = true")
    var pdo: MutableList<AwpImplementationPdo>? = emptyList<AwpImplementationPdo>().toMutableList(),

    @OneToMany(mappedBy = "awpImplementationId")
    @Where(clause = "active = true")
    var iri: MutableList<AwpImplementationIri>? = emptyList<AwpImplementationIri>().toMutableList(),

    @OneToMany(mappedBy = "awpImplementationId")
    @Where(clause = "active = true")
    var pok: MutableList<AwpImplementationPok>? = emptyList<AwpImplementationPok>().toMutableList(),

    @OneToMany(mappedBy = "awpImplementationId")
    @Where(clause = "active = true")
    var awpImplementationQualityAtEntryAnswers: MutableList<AwpImplementationQualityAtEntryAnswer>? = emptyList<AwpImplementationQualityAtEntryAnswer>().toMutableList(),

    @Column(name = "quality_at_entry_note")
    var qualitAtEntryNote: String? = null,

    @OneToMany(mappedBy = "awpImplementationId")
    @Where(clause = "active = true")
    @OrderBy("updatedAt DESC")
    var approvalNotes: MutableList<AwpImplementationApprovalNote>? = emptyList<AwpImplementationApprovalNote>().toMutableList(),

    @OneToMany(mappedBy = "awpImplementationId")
    @Where(clause = "active = true")
    var events: MutableList<AwpImplementationEvent>? = emptyList<AwpImplementationEvent>().toMutableList(),

    @Column(name = "status")
    var status: Int? = AWP_IMPLEMENTATION_STATUS_PLANNING,

    @Column(name = "created_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var createdAt: Date? = Date(),

    @Column(name = "updated_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var updatedAt: Date? = Date(),

    @Column(name = "created_by")
    var createdBy: String? = null,

    @ManyToOne
    @JoinColumn(name = "created_by", insertable = false, updatable = false, nullable = true)
    var createdByUser: VUsersResources? = null,

    @Column(name = "updated_by")
    var updatedBy: String? = null,

    @ManyToOne
    @JoinColumn(name = "updated_by", insertable = false, updatable = false, nullable = true)
    var updatedByUser: VUsersResources? = null,

    @Column(name = "active")
    @JsonIgnore
    var active: Boolean? = true,

    @Transient
    var creatorAllowedEdit: Boolean? = false,

    @Transient
    var coordinatorAllowedApprove: Boolean? = false,

    @Transient
    var pmuAllowedApprove: Boolean? = false,

    @Transient
    var lspAllowCreateEventReport: Boolean? = false,

    @Transient
    var creatorAllowedCreateReport: Boolean? = false,

    @Transient
    var lspAllowCreateReport: Boolean? = false,

    @Transient
    var coordinatorConsultanAllowedCtreateConceptNote: Boolean? = false,
) {
    @get:Transient
    val allowedDownloadReport: Boolean
        get() = setAllowedDownloadReport()

    private fun setAllowedDownloadReport(): Boolean {
        if (events?.isNotEmpty() == true) {
            events?.forEach { e ->
                val f = e.repots?.filter { ef -> ef.status == EVENT_REPORT_STATUS_APPROVED }
                if (f?.isNotEmpty() == true) {
                    return true
                }
            }
        }

        return false
    }
}
