package id.go.kemenag.madrasah.pmrms.awp.activity.pojo

import com.fasterxml.jackson.annotation.JsonIgnore
import org.hibernate.annotations.Where
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "event", schema = "public")
data class AwpImplementationEvent(
    @Id
    @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @JsonIgnore
    @Column(name = "awp_implementation_id")
    var awpImplementationId: String? = null,

    @JsonIgnore
    @OneToMany(mappedBy = "eventId")
    @Where(clause = "active = true")
    var repots: MutableList<EventEventReport>? = emptyList<EventEventReport>().toMutableList(),

    @Column(name = "active")
    var active: Boolean? = true
)


