package id.go.kemenag.madrasah.pmrms.awp.activity.pojo

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "awp_implementation_quality_at_entry_answer", schema = "public")
data class AwpImplementationQualityAtEntryAnswer(
    @Id
    @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @Column(name = "awp_implementation_id")
    var awpImplementationId: String? = null,

    @Column(name = "quality_at_entry_question_id")
    var qualityAtEntryQuestionId: String? = null,

    @ManyToOne
    @JoinColumn(name = "quality_at_entry_question_id", insertable = false, updatable = false, nullable = true)
    var qualityAtEntryQuestion: QualityAtEntryQuestion? = null,

    @Column(name = "answer")
    var answer: Boolean? = null,

    @Column(name = "revision")
    var revision: String? = null,

    @Column(name = "created_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var createdAt: Date? = Date(),

    @Column(name = "updated_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var updatedAt: Date? = Date(),

    @Column(name = "active")
    var active: Boolean? = true
)
