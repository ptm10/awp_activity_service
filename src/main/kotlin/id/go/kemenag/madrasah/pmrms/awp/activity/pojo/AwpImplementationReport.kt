package id.go.kemenag.madrasah.pmrms.awp.activity.pojo

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import id.go.kemenag.madrasah.pmrms.awp.activity.constant.AWP_IMPLEMENTATION_REPORT_STATUS_NEW
import org.hibernate.annotations.NotFound
import org.hibernate.annotations.NotFoundAction
import org.hibernate.annotations.Where
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "awp_implementation_report", schema = "public")
data class AwpImplementationReport(
    @Id
    @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @Column(name = "awp_implementation_id")
    var awpImplementationId: String? = null,

    @ManyToOne
    @JoinColumn(name = "awp_implementation_id", insertable = false, updatable = false, nullable = true)
    var awpImplementation: AwpImplementation? = null,

    @Column(name = "start_date")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+7")
    var startDate: Date? = null,

    @Column(name = "end_date")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+7")
    var endDate: Date? = null,

    @Column(name = "event_other_information")
    var eventOtherInformation: String? = null,

    @OneToMany(mappedBy = "awpImplementationReportId")
    @Where(clause = "active = true")
    var monev: MutableList<AwpImplementationReportResultChain>? = emptyList<AwpImplementationReportResultChain>().toMutableList(),

    @Column(name = "event_volume")
    var eventVolume: Int? = null,

    @Column(name = "summary")
    var summary: String? = null,

    @Column(name = "conclusions_and_recommendations")
    var conclusionsAndRecommendations: String? = null,

    @Column(name = "location_other_information")
    var locationOtherInformation: String? = null,

    @Column(name = "activity_mode_id")
    var activityModeId: String? = null,

    @ManyToOne
    @JoinColumn(name = "activity_mode_id", insertable = false, updatable = false, nullable = true)
    var activityMode: ActivityMode? = null,

    @Column(name = "participant_target")
    var participantTarget: String? = null,

    @Column(name = "name_other_information")
    var nameOtherInformation: String? = null,

    @Column(name = "status")
    var status: Int? = AWP_IMPLEMENTATION_REPORT_STATUS_NEW,

    @OneToMany(mappedBy = "awpImplementationReportId")
    @Where(clause = "active = true")
    var pok: MutableList<AwpImplementationReportPok>? = emptyList<AwpImplementationReportPok>().toMutableList(),

    @Column(name = "bast_file_id")
    var bastFileId: String? = null,

    @NotFound(action = NotFoundAction.IGNORE)
    @ManyToOne
    @JoinColumn(name = "bast_file_id", insertable = false, updatable = false, nullable = true)
    var bastFile: Files? = null,

    @OneToMany(mappedBy = "awpImplementationReportId")
    @Where(clause = "active = true")
    var provincesRegencies: MutableList<AwpImplementationReportProvinces>? = emptyList<AwpImplementationReportProvinces>().toMutableList(),

    @OneToMany(mappedBy = "awpImplementationReportId")
    @Where(clause = "active = true")
    @OrderBy("updatedAt DESC")
    var approvalNotes: MutableList<AwpImplementationReportApprovalNote>? = emptyList<AwpImplementationReportApprovalNote>().toMutableList(),

    @Column(name = "created_by")
    var createdBy: String? = null,

    @ManyToOne
    @JoinColumn(name = "created_by", insertable = false, updatable = false, nullable = true)
    var createdByUser: VUsersResources? = null,

    @Transient
    var creatorAllowedEdit: Boolean? = false,

    @Transient
    var coordinatorConsultanAllowEdit: Boolean? = false,

    @Transient
    var allowApprove: Boolean? = false,

    @Column(name = "updated_by")
    var updatedBy: String? = null,

    @ManyToOne
    @JoinColumn(name = "updated_by", insertable = false, updatable = false, nullable = true)
    var updatedByUser: VUsersResources? = null,

    @Column(name = "created_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var createdAt: Date? = Date(),

    @Column(name = "updated_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var updatedAt: Date? = Date(),

    @Column(name = "active")
    @JsonIgnore
    var active: Boolean? = true
)
