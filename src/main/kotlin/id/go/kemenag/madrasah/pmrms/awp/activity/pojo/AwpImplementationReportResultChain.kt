package id.go.kemenag.madrasah.pmrms.awp.activity.pojo

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import org.hibernate.annotations.NotFound
import org.hibernate.annotations.NotFoundAction
import org.hibernate.annotations.Where
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "awp_implementation_report_result_chain", schema = "public")
data class AwpImplementationReportResultChain(
    @Id
    @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @Column(name = "awp_implementation_report_id")
    var awpImplementationReportId: String? = null,

    @Column(name = "result_chain_id")
    var resultChainId: String? = null,

    @NotFound(action = NotFoundAction.IGNORE)
    @ManyToOne
    @JoinColumn(name = "result_chain_id", insertable = false, updatable = false, nullable = true)
    var resultChain: MonevResultChain? = null,

    @OneToMany(mappedBy = "awpImplementationReportResultChainId")
    @Where(clause = "active = true")
    var successIndicator: MutableList<AwpImplementationReportSuccessIndicator>? = emptyList<AwpImplementationReportSuccessIndicator>().toMutableList(),

    @Column(name = "allow_delete")
    var allowDelete: Boolean? = true,

    @Column(name = "created_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var createdAt: Date? = Date(),

    @Column(name = "updated_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var updatedAt: Date? = Date(),

    @Column(name = "active")
    var active: Boolean? = true
)
