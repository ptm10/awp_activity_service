package id.go.kemenag.madrasah.pmrms.awp.activity.pojo

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "awp_pdo", schema = "public")
data class AwpPdo(
    @Id
    @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @Column(name = "awp_id")
    var awpId: String? = null,

    @Column(name = "pdo_id")
    var pdoId: String? = null,

    @ManyToOne
    @JoinColumn(name = "pdo_id", insertable = false, updatable = false, nullable = true)
    var pdo: Pdo? = null,

    @Column(name = "created_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var createdAt: Date? = Date(),

    @Column(name = "updated_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var updatedAt: Date? = Date(),

    @Column(name = "active")
    var active: Boolean? = true
)
