package id.go.kemenag.madrasah.pmrms.awp.activity.pojo

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import id.go.kemenag.madrasah.pmrms.awp.activity.constant.EVENT_MANAGEMENT_TYPE_CONTRACT
import id.go.kemenag.madrasah.pmrms.awp.activity.constant.EVENT_STATUS_IMPLEMENTATION
import id.go.kemenag.madrasah.pmrms.awp.activity.constant.EVENT_STATUS_PLANNING
import id.go.kemenag.madrasah.pmrms.awp.activity.helpers.getUserLogin
import org.hibernate.annotations.NotFound
import org.hibernate.annotations.NotFoundAction
import org.hibernate.annotations.Where
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "event", schema = "public")
data class Event(
    @Id
    @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @Column(name = "awp_implementation_id")
    var awpImplementationId: String? = null,

    @ManyToOne
    @JoinColumn(name = "awp_implementation_id", insertable = false, updatable = false, nullable = true)
    var awpImplementation: AwpImplementation? = null,

    @Column(name = "name")
    var name: String? = null,

    @Column(name = "name_other_information")
    var nameOtherInformation: String? = null,

    @Column(name = "location_other_information")
    var locationOtherInformation: String? = null,

    @Column(name = "start_date")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+7")
    var startDate: Date? = null,

    @Column(name = "end_date")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+7")
    var endDate: Date? = null,

    @Column(name = "event_other_information")
    var eventOtherInformation: String? = null,

    @Column(name = "event_type_id")
    var eventTypeId: String? = null,

    @ManyToOne
    @JoinColumn(name = "event_type_id", insertable = false, updatable = false, nullable = true)
    var eventType: EventType? = null,

    @Column(name = "background")
    var background: String? = null,

    @Column(name = "description")
    var description: String? = null,

    @Column(name = "activity_mode_id")
    var activityModeId: String? = null,

    @ManyToOne
    @JoinColumn(name = "activity_mode_id", insertable = false, updatable = false, nullable = true)
    var activityMode: ActivityMode? = null,

    @Column(name = "purpose")
    var purpose: String? = null,

    @Column(name = "event_output")
    var eventOutput: String? = null,

    @Column(name = "participant_male")
    var participantMale: Long? = 0,

    @Column(name = "participant_female")
    var participantFemale: Long? = 0,

    @Column(name = "participant_count")
    var participantCount: Long? = 0,

    @Column(name = "participant_other_information")
    var participantOtherInformation: String? = null,

    @Column(name = "participant_target")
    var participantTarget: String? = null,

    @Column(name = "contact_person")
    var contactPerson: String? = null,

    @ManyToOne
    @JoinColumn(name = "contact_person", insertable = false, updatable = false, nullable = true)
    var contactPersonResources: Resources? = null,

    @Column(name = "province_id")
    var provinceId: String? = null,

    @ManyToOne
    @JoinColumn(name = "province_id", insertable = false, updatable = false, nullable = true)
    var province: Province? = null,

    @Column(name = "regency_id")
    var regencyId: String? = null,

    @ManyToOne
    @JoinColumn(name = "regency_id", insertable = false, updatable = false, nullable = true)
    var regency: Regency? = null,

    @Column(name = "location")
    var location: String? = null,

    @Column(name = "budget_available")
    var budgetAvailable: Boolean? = null,

    @Column(name = "pok_event")
    var pokEvent: Boolean? = null,

    @Column(name = "budget_pok")
    var budgetPok: Long? = null,

    @OneToMany(mappedBy = "eventId")
    @Where(clause = "active = true")
    var rab: MutableList<EventRab>? = emptyList<EventRab>().toMutableList(),

    @OneToMany(mappedBy = "eventId")
    @Where(clause = "active = true")
    var agendas: MutableList<EventAgenda>? = emptyList<EventAgenda>().toMutableList(),

    @OneToMany(mappedBy = "eventId")
    @Where(clause = "active = true")
    var staff: MutableList<EventStaff>? = emptyList<EventStaff>().toMutableList(),

    @OneToMany(mappedBy = "eventId")
    @Where(clause = "active = true")
    var otherStaff: MutableList<EventOtherStaff>? = emptyList<EventOtherStaff>().toMutableList(),

    @OneToMany(mappedBy = "eventId")
    @Where(clause = "active = true")
    @OrderBy("updatedAt DESC")
    var approvalNotes: MutableList<EventApprovalNote>? = emptyList<EventApprovalNote>().toMutableList(),

    @OneToMany(mappedBy = "eventId")
    @Where(clause = "active = true")
    var pok: MutableList<EventPok>? = emptyList<EventPok>().toMutableList(),

    @Column(name = "status")
    var status: Int? = EVENT_STATUS_PLANNING,

    @Column(name = "rab_file_id")
    var rabFileId: String? = null,

    @NotFound(action = NotFoundAction.IGNORE)
    @ManyToOne
    @JoinColumn(name = "rab_file_id", insertable = false, updatable = false, nullable = true)
    var rabFile: Files? = null,

    @Column(name = "agenda_file_id")
    var agendaFileId: String? = null,

    @NotFound(action = NotFoundAction.IGNORE)
    @ManyToOne
    @JoinColumn(name = "agenda_file_id", insertable = false, updatable = false, nullable = true)
    var agendaFile: Files? = null,

    @OneToMany(mappedBy = "eventId")
    @Where(clause = "active = true")
    var repots: MutableList<EventEventReport>? = emptyList<EventEventReport>().toMutableList(),

    @JsonIgnore
    @OneToMany(mappedBy = "eventId")
    @Where(clause = "active = true")
    var excelFormFiles: MutableList<EventFormExcelFile>? = emptyList<EventFormExcelFile>().toMutableList(),

    @Column(name = "created_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var createdAt: Date? = Date(),

    @Column(name = "updated_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var updatedAt: Date? = Date(),

    @Column(name = "created_by")
    var createdBy: String? = null,

    @ManyToOne
    @JoinColumn(name = "created_by", insertable = false, updatable = false, nullable = true)
    var createdByUser: VUsersResources? = null,

    @Column(name = "updated_by")
    var updatedBy: String? = null,

    @ManyToOne
    @JoinColumn(name = "updated_by", insertable = false, updatable = false, nullable = true)
    var updatedByUser: VUsersResources? = null,

    @Column(name = "active")
    @JsonIgnore
    var active: Boolean? = true,

    @Transient
    var creatorAllowedEdit: Boolean? = false,

    @Transient
    var creatorAllowCreateReport: Boolean? = false,

    @Transient
    var coordinatorAllowedApprove: Boolean? = false,

    @Transient
    var pmuTreasurerAllowedApprove: Boolean? = false,

    @Transient
    var pmuHeadAllowedApprove: Boolean? = false,

    @Transient
    var staffOrAssistantAllowCreateProgressReport: Boolean? = false,

    @Transient
    var staffOrAssistantAllowCreateReport: Boolean? = false
) {
    @get:Transient
    val allowUploadQuestionnaireForm: Boolean
        get() = setAllowUploadQuestionnaireForm()

    private fun setAllowUploadQuestionnaireForm(): Boolean {
        var allowed = false
        if (status == EVENT_STATUS_IMPLEMENTATION) {
            if (createdBy == getUserLogin()?.id) {
                allowed = true
            }

            val filterStaff = staff?.filter { sf ->
                sf.resources?.id == getUserLogin()?.resourcesId
            }

            if (filterStaff?.isNotEmpty() == true) {
                allowed = true
            }

            if (awpImplementation?.eventManagementTypeId == EVENT_MANAGEMENT_TYPE_CONTRACT) {
                if (awpImplementation?.lspPic == getUserLogin()?.id) {
                    allowed = true
                }
            }
        }

        return allowed
    }

    @get:Transient
    val countUploadedFormFiles: Long
        get() = setCountUploadedFormFiles()

    private fun setCountUploadedFormFiles(): Long {
        return excelFormFiles?.size?.toLong() ?: 0
    }
}


