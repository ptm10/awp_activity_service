package id.go.kemenag.madrasah.pmrms.awp.activity.pojo

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "event_approval_note", schema = "public")
data class EventApprovalNote(
    @Id
    @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @Column(name = "event_id")
    var eventId: String? = null,

    @Column(name = "user_id")
    var userId: String? = null,

    @ManyToOne
    @JoinColumn(name = "user_id", insertable = false, updatable = false, nullable = true)
    var user: VUsersResources? = null,

    @Column(name = "created_from")
    var createdFrom: Int? = null,

    @Column(name = "note")
    var note: String? = null,

    @Column(name = "approval_status")
    var approvalStatus: Int? = null,

    @Column(name = "created_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var createdAt: Date? = Date(),

    @Column(name = "updated_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var updatedAt: Date? = Date(),

    @Column(name = "active")
    var active: Boolean? = true
)
