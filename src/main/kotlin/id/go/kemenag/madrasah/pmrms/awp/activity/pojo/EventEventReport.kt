package id.go.kemenag.madrasah.pmrms.awp.activity.pojo

import com.fasterxml.jackson.annotation.JsonIgnore
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "event_report", schema = "public")
data class EventEventReport(
    @Id
    @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @JsonIgnore
    @Column(name = "event_id")
    var eventId: String? = null,

    @JsonIgnore
    @Column(name = "status")
    var status: Int? = null,

    @Column(name = "active")
    @JsonIgnore
    var active: Boolean? = true
)
