package id.go.kemenag.madrasah.pmrms.awp.activity.pojo

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import org.hibernate.annotations.NotFound
import org.hibernate.annotations.NotFoundAction
import org.hibernate.annotations.Where
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "event_form_excel_file", schema = "public")
data class EventFormExcelFile(
    @Id
    @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @Column(name = "event_id")
    var eventId: String? = null,

    @ManyToOne
    @JoinColumn(name = "event_id", insertable = false, updatable = false, nullable = true)
    var event: EventFormExcelFileEvent? = null,

    @Column(name = "created_for")
    var createdFor: Int? = null,

    @Column(name = "file_id")
    var fileId: String? = null,

    @NotFound(action = NotFoundAction.IGNORE)
    @ManyToOne
    @JoinColumn(name = "file_id", insertable = false, updatable = false, nullable = true)
    var file: Files? = null,

    @OneToMany(mappedBy = "eventFormFileExcelId")
    @Where(clause = "active = true")
    var emails: MutableList<EventFormExcelFileEmail>? = emptyList<EventFormExcelFileEmail>().toMutableList(),

    @Column(name = "created_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var createdAt: Date? = Date(),

    @Column(name = "updated_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var updatedAt: Date? = Date(),

    @Column(name = "active")
    var active: Boolean? = true
)
