package id.go.kemenag.madrasah.pmrms.awp.activity.pojo

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import id.go.kemenag.madrasah.pmrms.awp.activity.constant.EVENT_FORM_EXCEL_FILE_EMAIL_STATUS_NEW
import id.go.kemenag.madrasah.pmrms.awp.activity.constant.EVENT_FORM_EXCEL_FILE_QUESTIONNAIRE_STATUS_NOT_DONE
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "event_form_excel_file_email", schema = "public")
data class EventFormExcelFileEmail(
    @Id
    @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @Column(name = "event_form_file_excel_id")
    var eventFormFileExcelId: String? = null,

    @ManyToOne
    @JoinColumn(name = "event_form_file_excel_id", insertable = false, updatable = false, nullable = true)
    var eventFormFileExcel: EventFormExcelFileEmailEventFormExcelFile? = null,

    @Column(name = "name")
    var name: String? = null,

    @Column(name = "email")
    var email: String? = null,

    @Column(name = "email_sent_status")
    var emailSentStatus: Int? = EVENT_FORM_EXCEL_FILE_EMAIL_STATUS_NEW,

    @Column(name = "questionnaire_status")
    var questionnaireStatus: Int? = EVENT_FORM_EXCEL_FILE_QUESTIONNAIRE_STATUS_NOT_DONE,

    @Column(name = "created_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var createdAt: Date? = Date(),

    @Column(name = "updated_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var updatedAt: Date? = Date(),

    @Column(name = "active")
    @JsonIgnore
    var active: Boolean? = true
)
