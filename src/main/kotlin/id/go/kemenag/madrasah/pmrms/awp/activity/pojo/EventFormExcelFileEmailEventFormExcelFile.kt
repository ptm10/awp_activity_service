package id.go.kemenag.madrasah.pmrms.awp.activity.pojo

import com.fasterxml.jackson.annotation.JsonIgnore
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "event_form_excel_file", schema = "public")
data class EventFormExcelFileEmailEventFormExcelFile(
    @Id
    @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @Column(name = "event_id")
    var eventId: String? = UUID.randomUUID().toString(),

    @Column(name = "created_for")
    var createdFor: Int? = null,

    @Column(name = "active")
    @JsonIgnore
    var active: Boolean? = true
)
