package id.go.kemenag.madrasah.pmrms.awp.activity.pojo

import com.fasterxml.jackson.annotation.JsonIgnore
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "event", schema = "public")
data class EventFormExcelFileEvent(
    @Id
    @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @Column(name = "name")
    var name: String? = null,

    @Column(name = "active")
    @JsonIgnore
    var active: Boolean? = true
)


