package id.go.kemenag.madrasah.pmrms.awp.activity.pojo

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import id.go.kemenag.madrasah.pmrms.awp.activity.constant.EVENT_PROGRESS_REPORT_STATUS_NEW
import org.hibernate.annotations.NotFound
import org.hibernate.annotations.NotFoundAction
import org.hibernate.annotations.Where
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "event_progress_report", schema = "public")
data class EventProgressReport(
    @Id
    @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @Column(name = "event_id")
    var eventId: String? = null,

    @ManyToOne
    @JoinColumn(name = "event_id", insertable = false, updatable = false, nullable = true)
    var event: Event? = null,

    @Column(name = "event_output")
    var eventOutput: String? = null,

    @Column(name = "participant_male")
    var participantMale: Long? = 0,

    @Column(name = "participant_female")
    var participantFemale: Long? = 0,

    @Column(name = "participant_count")
    var participantCount: Long? = 0,

    @Column(name = "participant_other_information")
    var participantOtherInformation: String? = null,

    @Column(name = "participant_target")
    var participantTarget: String? = null,

    @Column(name = "start_date")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+7")
    var startDate: Date? = null,

    @Column(name = "end_date")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+7")
    var endDate: Date? = null,

    @Column(name = "event_other_information")
    var eventOtherInformation: String? = null,

    @Column(name = "location_other_information")
    var locationOtherInformation: String? = null,

    @Column(name = "province_id")
    var provinceId: String? = null,

    @ManyToOne
    @JoinColumn(name = "province_id", insertable = false, updatable = false, nullable = true)
    var province: Province? = null,

    @Column(name = "regency_id")
    var regencyId: String? = null,

    @ManyToOne
    @JoinColumn(name = "regency_id", insertable = false, updatable = false, nullable = true)
    var regency: Regency? = null,

    @Column(name = "location")
    var location: String? = null,

    @Column(name = "summary")
    var summary: String? = null,

    @Column(name = "attachment_file_id")
    var attachmentFileId: String? = null,

    @NotFound(action = NotFoundAction.IGNORE)
    @ManyToOne
    @JoinColumn(name = "attachment_file_id", insertable = false, updatable = false, nullable = true)
    var attachmentFile: Files? = null,

    @OneToMany(mappedBy = "eventProgressReportId")
    @Where(clause = "active = true")
    var staff: MutableList<EventProgressReportStaff>? = emptyList<EventProgressReportStaff>().toMutableList(),

    @OneToMany(mappedBy = "eventProgressReportId")
    @Where(clause = "active = true")
    var otherStaff: MutableList<EventProgressReportOtherStaff>? = emptyList<EventProgressReportOtherStaff>().toMutableList(),

    @OneToMany(mappedBy = "eventProgressReportId")
    @Where(clause = "active = true")
    var documents: MutableList<EventProgressReportDocuments>? = emptyList<EventProgressReportDocuments>().toMutableList(),

    @Column(name = "budget_available")
    var budgetAvailable: Boolean? = null,

    @Column(name = "pok_event")
    var pokEvent: Boolean? = null,

    @Column(name = "activity_mode_id")
    var activityModeId: String? = null,

    @ManyToOne
    @JoinColumn(name = "activity_mode_id", insertable = false, updatable = false, nullable = true)
    var activityMode: ActivityMode? = null,

    @Column(name = "rra_file_id")
    var rraFileId: String? = null,

    @NotFound(action = NotFoundAction.IGNORE)
    @ManyToOne
    @JoinColumn(name = "rra_file_id", insertable = false, updatable = false, nullable = true)
    var rraFile: Files? = null,

    @Column(name = "status")
    var status: Int? = EVENT_PROGRESS_REPORT_STATUS_NEW,

    @OneToMany(mappedBy = "eventProgressReportId")
    @Where(clause = "active = true")
    @OrderBy("updatedAt DESC")
    var approvalNotes: MutableList<EventProgressReportApprovalNote>? = emptyList<EventProgressReportApprovalNote>().toMutableList(),

    @Column(name = "created_by")
    var createdBy: String? = null,

    @ManyToOne
    @JoinColumn(name = "created_by", insertable = false, updatable = false, nullable = true)
    var createdByUser: VUsersResources? = null,

    @Column(name = "updated_by")
    var updatedBy: String? = null,

    @ManyToOne
    @JoinColumn(name = "updated_by", insertable = false, updatable = false, nullable = true)
    var updatedByUser: VUsersResources? = null,

    @Column(name = "created_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var createdAt: Date? = Date(),

    @Column(name = "updated_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var updatedAt: Date? = Date(),

    @Column(name = "active")
    @JsonIgnore
    var active: Boolean? = true,

    @Transient
    var creatorAllowedEdit: Boolean? = false,

    @Transient
    var consultanAllowedApprove: Boolean? = false
)
