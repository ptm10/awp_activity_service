package id.go.kemenag.madrasah.pmrms.awp.activity.pojo

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "event_progress_report_documents", schema = "public")
data class EventProgressReportDocuments(
    @Id
    @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @Column(name = "event_progress_report_id")
    var eventProgressReportId: String? = null,

    @Column(name = "event_document_type_id")
    var eventDocumentTypeId: String? = null,

    @ManyToOne
    @JoinColumn(name = "event_document_type_id", insertable = false, updatable = false, nullable = true)
    var eventDocumentType: EventDocumentType? = null,

    @Column(name = "title")
    var title: String? = null,

    @Column(name = "date")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+7")
    var date: Date? = null,

    @Column(name = "place")
    var place: String? = null,

    @Column(name = "description")
    var description: String? = null,

    @Column(name = "file_id")
    var fileId: String? = null,

    @ManyToOne
    @JoinColumn(name = "file_id", insertable = false, updatable = false, nullable = true)
    var file: Files? = null,

    @Column(name = "created_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var createdAt: Date? = Date(),

    @Column(name = "updated_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var updatedAt: Date? = Date(),

    @Column(name = "active")
    var active: Boolean? = true
)
