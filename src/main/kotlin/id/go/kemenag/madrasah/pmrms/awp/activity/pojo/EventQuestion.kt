package id.go.kemenag.madrasah.pmrms.awp.activity.pojo

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "event_question", schema = "public")
data class EventQuestion(
    @Id
    @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @Column(name = "event_form_id")
    var eventFormId: String? = null,

    @ManyToOne
    @JoinColumn(name = "event_form_id", insertable = false, updatable = false, nullable = true)
    var eventForm: EventQeustionEventForm? = null,

    @Column(name = "created_for")
    var createdFor: Int? = null,

    @Column(name = "question_category")
    var questionCategory: Int? = null,

    @Column(name = "question_type")
    var questionType: Int? = null,

    @Column(name = "question")
    var question: String? = null,

    @Column(name = "created_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var createdAt: Date? = Date(),

    @Column(name = "updated_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var updatedAt: Date? = Date(),

    @Column(name = "active")
    var active: Boolean? = true
)
