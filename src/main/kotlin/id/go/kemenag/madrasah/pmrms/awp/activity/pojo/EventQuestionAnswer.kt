package id.go.kemenag.madrasah.pmrms.awp.activity.pojo

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import org.hibernate.annotations.Where
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "event_question_answer", schema = "public")
data class EventQuestionAnswer(
    @Id
    @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @Column(name = "username")
    var username: String? = null,

    @Column(name = "gender")
    var gender: Char? = null,

    @Column(name = "institution")
    var institution: String? = null,

    @Column(name = "email")
    var email: String? = null,

    @Column(name = "phone_number")
    var phoneNumber: String? = null,

    @Column(name = "event_id")
    var eventId: String? = null,

    @ManyToOne
    @JoinColumn(name = "event_id", insertable = false, updatable = false, nullable = true)
    var event: EventQuestionAnswerEventEvent? = null,

    @Column(name = "filled_by")
    var filledBy: Int? = null,

    @OneToMany(mappedBy = "eventQuestionAnswerId")
    @Where(clause = "active = true")
    var answers: MutableList<EventQuestionAnswerDetail>? = emptyList<EventQuestionAnswerDetail>().toMutableList(),

    @Column(name = "created_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var createdAt: Date? = Date(),

    @Column(name = "updated_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var updatedAt: Date? = Date(),

    @Column(name = "active")
    @JsonIgnore
    var active: Boolean? = true
)
