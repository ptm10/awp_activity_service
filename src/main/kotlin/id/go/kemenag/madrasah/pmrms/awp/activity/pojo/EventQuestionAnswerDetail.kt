package id.go.kemenag.madrasah.pmrms.awp.activity.pojo

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "event_question_answer_detail", schema = "public")
data class EventQuestionAnswerDetail(
    @Id
    @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @Column(name = "event_question_answer_id")
    var eventQuestionAnswerId: String? = null,

    @Column(name = "event_question_id")
    var eventQuestionId: String? = null,

    @ManyToOne
    @JoinColumn(name = "event_question_id", insertable = false, updatable = false, nullable = true)
    var eventQuestion: EventQuestion? = null,

    @Column(name = "yes_or_no_answer")
    var yesOrNoAnswer: Boolean? = null,

    @Column(name = "scale_asnwer")
    var scaleAsnwer: Int? = null,

    @Column(name = "explanation_answer")
    var explanationAnswer: String? = null,

    @Column(name = "comment")
    var comment: String? = null,

    @Column(name = "created_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var createdAt: Date? = Date(),

    @Column(name = "updated_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var updatedAt: Date? = Date(),

    @Column(name = "active")
    @JsonIgnore
    var active: Boolean? = true
)
