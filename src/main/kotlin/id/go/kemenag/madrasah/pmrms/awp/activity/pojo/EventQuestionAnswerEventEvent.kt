package id.go.kemenag.madrasah.pmrms.awp.activity.pojo

import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "event", schema = "public")
data class EventQuestionAnswerEventEvent(
    @Id
    @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @Column(name = "event_type_id")
    var eventTypeId: String? = null
)


