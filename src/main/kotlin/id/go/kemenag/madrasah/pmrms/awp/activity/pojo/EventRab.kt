package id.go.kemenag.madrasah.pmrms.awp.activity.pojo

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import org.hibernate.annotations.Where
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "event_rab", schema = "public")
data class EventRab(
    @Id
    @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @Column(name = "event_id")
    var eventId: String? = null,

    @Column(name = "code")
    var code: String? = null,

    @Column(name = "name")
    var name: String? = null,

    @Column(name = "subtotal")
    var subtotal: Long? = null,

    @OneToMany(mappedBy = "eventRabId")
    @Where(clause = "active = true")
    var details: MutableList<EventRabDetail>? = emptyList<EventRabDetail>().toMutableList(),

    @Column(name = "created_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var createdAt: Date? = Date(),

    @Column(name = "updated_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var updatedAt: Date? = Date(),

    @Column(name = "active")
    var active: Boolean? = true
)
