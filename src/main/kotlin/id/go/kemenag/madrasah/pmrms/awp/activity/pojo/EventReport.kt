package id.go.kemenag.madrasah.pmrms.awp.activity.pojo

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import id.go.kemenag.madrasah.pmrms.awp.activity.constant.EVENT_REPORT_STATUS_DRAFT
import org.hibernate.annotations.Where
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "event_report", schema = "public")
data class EventReport(
    @Id
    @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @Column(name = "event_id")
    var eventId: String? = null,

    @Column(name = "topic")
    var topic: String? = null,

    @Column(name = "event_code")
    var eventCode: String? = null,

    @ManyToOne
    @JoinColumn(name = "event_id", insertable = false, updatable = false, nullable = true)
    var event: Event? = null,

    @Column(name = "province_id")
    var provinceId: String? = null,

    @ManyToOne
    @JoinColumn(name = "province_id", insertable = false, updatable = false, nullable = true)
    var province: Province? = null,

    @Column(name = "regency_id")
    var regencyId: String? = null,

    @ManyToOne
    @JoinColumn(name = "regency_id", insertable = false, updatable = false, nullable = true)
    var regency: Regency? = null,

    @Column(name = "location")
    var location: String? = null,

    @Column(name = "participant_target")
    var participantTarget: String? = null,

    @Column(name = "location_other_information")
    var locationOtherInformation: String? = null,

    @Column(name = "start_date")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+7")
    var startDate: Date? = null,

    @Column(name = "end_date")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+7")
    var endDate: Date? = null,

    @Column(name = "participant_male")
    var participantMale: Long? = 0,

    @Column(name = "participant_female")
    var participantFemale: Long? = 0,

    @Column(name = "participant_count")
    var participantCount: Long? = 0,

    @Column(name = "participant_other_information")
    var participantOtherInformation: String? = null,

    @Column(name = "implementation_summary")
    var implementationSummary: String? = null,

    @Column(name = "event_evaluation_description")
    var eventEvaluationDescription: String? = null,

    @Column(name = "conclusions_and_recommendations")
    var conclusionsAndRecommendations: String? = null,

    @Column(name = "event_output")
    var eventOutput: String? = null,

    @Column(name = "activity_mode_id")
    var activityModeId: String? = null,

    @ManyToOne
    @JoinColumn(name = "activity_mode_id", insertable = false, updatable = false, nullable = true)
    var activityMode: ActivityMode? = null,

    @Column(name = "attachment_file_id")
    var attachmentFileId: String? = null,

    @ManyToOne
    @JoinColumn(name = "attachment_file_id", insertable = false, updatable = false, nullable = true)
    var attachmentFile: Files? = null,

    @Column(name = "rra_file_id")
    var rraFileId: String? = null,

    @ManyToOne
    @JoinColumn(name = "rra_file_id", insertable = false, updatable = false, nullable = true)
    var rraFile: Files? = null,

    @Column(name = "status")
    var status: Int? = EVENT_REPORT_STATUS_DRAFT,

    @OneToMany(mappedBy = "eventReportId")
    @Where(clause = "active = true")
    var documents: MutableList<EventReportDocuments>? = emptyList<EventReportDocuments>().toMutableList(),

    @OneToMany(mappedBy = "eventReportId")
    @Where(clause = "active = true")
    @OrderBy("updatedAt DESC")
    var evaluationPoints: MutableList<EventReportEvaluationPoints>? = emptyList<EventReportEvaluationPoints>().toMutableList(),

    @OneToMany(mappedBy = "eventReportId")
    @Where(clause = "active = true")
    @OrderBy("updatedAt DESC")
    var participantTargets: MutableList<EventReportParticipantTarget>? = emptyList<EventReportParticipantTarget>().toMutableList(),

    @OneToMany(mappedBy = "eventReportId")
    @Where(clause = "active = true")
    @OrderBy("updatedAt DESC")
    var approvalNotes: MutableList<EventReportApprovalNote>? = emptyList<EventReportApprovalNote>().toMutableList(),

    @OneToMany(mappedBy = "eventReportId")
    @Where(clause = "active = true")
    var pok: MutableList<EventReportPok>? = emptyList<EventReportPok>().toMutableList(),

    @Column(name = "budget_source")
    var budgetSource: String? = null,

    @Column(name = "budget_year")
    var budgetYear: Long? = null,

    @Column(name = "mak_number")
    var makNumber: String? = null,

    @Column(name = "budget_ceiling")
    var budgetCeiling: Long? = null,

    @Column(name = "rra_budget")
    var rraBudget: Long? = null,

    @Column(name = "paragraph_1")
    var paragraph1: String? = null,

    @Column(name = "paragraph_2")
    var paragraph2: String? = null,

    @Column(name = "p3_duration_days")
    var p3DurationDays: Int? = null,

    @Column(name = "p3_duration_month")
    var p3DurationMonth: Int? = null,

    @Column(name = "p3_duration_year")
    var p3DurationYear: Int? = null,

    @Column(name = "p3_event_type_id")
    var p3EventTypeId: String? = null,

    @ManyToOne
    @JoinColumn(name = "p3_event_type_id", insertable = false, updatable = false, nullable = true)
    var p3EventType: EventType? = null,

    @Column(name = "p3_start_date")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+7")
    var p3StartDate: Date? = null,

    @Column(name = "p3_end_date")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+7")
    var p3EndDate: Date? = null,

    @Column(name = "paragraph_4")
    var paragraph4: String? = null,

    @Column(name = "paragraph_5")
    var paragraph5: String? = null,

    @Column(name = "technical_implementer")
    var technicalImplementer: String? = null,

    @ManyToOne
    @JoinColumn(name = "technical_implementer", insertable = false, updatable = false, nullable = true)
    var technicalImplementerResources: Resources? = null,

    @Column(name = "total_duration")
    var totalDuration: String? = null,

    @Column(name = "schema_event")
    var schemaEvent: String? = null,

    @Column(name = "created_by")
    var createdBy: String? = null,

    @ManyToOne
    @JoinColumn(name = "created_by", insertable = false, updatable = false, nullable = true)
    var createdByUser: VUsersResources? = null,

    @Column(name = "updated_by")
    var updatedBy: String? = null,

    @ManyToOne
    @JoinColumn(name = "updated_by", insertable = false, updatable = false, nullable = true)
    var updatedByUser: VUsersResources? = null,

    @Column(name = "created_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var createdAt: Date? = Date(),

    @Column(name = "updated_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var updatedAt: Date? = Date(),

    @Column(name = "active")
    @JsonIgnore
    var active: Boolean? = true,

    @Transient
    var creatorAllowedEdit: Boolean? = false,

    @Transient
    var consultanAllowedApprove: Boolean? = false
)
