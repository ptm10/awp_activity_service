package id.go.kemenag.madrasah.pmrms.awp.activity.pojo

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "event_report_evaluation_points", schema = "public")
data class EventReportEvaluationPoints(
    @Id
    @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @Column(name = "event_report_id")
    var eventReportId: String? = null,

    @Column(name = "event_evaluation_point_id")
    var eventEvaluationPointId: String? = null,

    @ManyToOne
    @JoinColumn(name = "event_evaluation_point_id", insertable = false, updatable = false, nullable = true)
    var evaluationPoint: EventEvaluationPoint? = null,

    @Column(name = "value")
    var value: Int? = null,

    @Column(name = "note")
    var note: String? = null,

    @Column(name = "created_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var createdAt: Date? = Date(),

    @Column(name = "updated_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var updatedAt: Date? = Date(),

    @Column(name = "active")
    var active: Boolean? = true
)
