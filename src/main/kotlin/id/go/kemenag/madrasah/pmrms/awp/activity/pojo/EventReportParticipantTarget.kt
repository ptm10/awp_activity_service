package id.go.kemenag.madrasah.pmrms.awp.activity.pojo

import com.fasterxml.jackson.annotation.JsonFormat
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "event_report_participant_target", schema = "public")
data class EventReportParticipantTarget(
    @Id
    @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @Column(name = "event_report_id")
    var eventReportId: String? = null,

    @Column(name = "participant_target_position_id")
    var participantTargetPositionId: String? = null,

    @ManyToOne
    @JoinColumn(name = "participant_target_position_id", insertable = false, updatable = false, nullable = true)
    var participanTargetPosition: ParticipanTargetPosition? = null,

    @Column(name = "value")
    var value: Int? = null,

    @Column(name = "created_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var createdAt: Date? = Date(),

    @Column(name = "updated_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var updatedAt: Date? = Date(),

    @Column(name = "active")
    var active: Boolean? = true
)
