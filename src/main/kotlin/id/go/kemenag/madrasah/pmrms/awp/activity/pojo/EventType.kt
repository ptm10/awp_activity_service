package id.go.kemenag.madrasah.pmrms.awp.activity.pojo

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "event_type", schema = "master")
data class EventType(
    @Id
    @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @Column(name = "name")
    var name: String? = null,

    @Column(name = "type")
    var type: Int? = null,

    @Column(name = "hours_number")
    var hoursNumber: String? = null,

    @Column(name = "implementation")
    var implementation: String? = null,

    @Column(name = "purpose")
    var purpose: String? = null,

    @Column(name = "leader")
    var leader: String? = null,

    @Column(name = "participant")
    var participant: String? = null,

    @Column(name = "executor")
    var executor: String? = null,

    @Column(name = "interaction_pattern")
    var interactionPattern: String? = null,

    @Column(name = "topic")
    var topic: String? = null,

    @Column(name = "sample")
    var sample: String? = null,

    @Column(name = "output")
    var output: String? = null,

    @Column(name = "created_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var createdAt: Date? = Date(),

    @Column(name = "updated_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var updatedAt: Date? = Date(),

    @Column(name = "active")
    @JsonIgnore
    var active: Boolean? = true
)
