package id.go.kemenag.madrasah.pmrms.awp.activity.pojo

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import org.hibernate.annotations.NotFound
import org.hibernate.annotations.NotFoundAction
import org.hibernate.annotations.Where
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "monev_result_chain", schema = "public")
data class MonevResultChain(
    @Id
    @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @Column(name = "year")
    var year: Int? = null,

    @Column(name = "monev_type_id")
    var monevTypeId: String? = null,

    @Column(name = "component_id")
    var componentId: String? = null,

    @ManyToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "component_id", insertable = false, updatable = false, nullable = true)
    var component: Component? = null,

    @Column(name = "sub_component_id")
    var subComponentId: String? = null,

    @ManyToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "sub_component_id", insertable = false, updatable = false, nullable = true)
    var subComponent: Component? = null,

    @Column(name = "code")
    var code: String? = null,

    @Column(name = "name")
    var name: String? = null,

    @Column(name = "status_2020")
    var status2020: Int? = null,

    @Column(name = "status_2021")
    var status2021: Int? = null,

    @Column(name = "status_2022")
    var status2022: Int? = null,

    @Column(name = "status_2023")
    var status2023: Int? = null,

    @Column(name = "status_2024")
    var status2024: Int? = null,

    @OneToMany(mappedBy = "monevResultChainId")
    @Where(clause = "active = true")
    @OrderBy("code")
    var successIndicator: MutableList<MonevSuccessIndicator>? = emptyList<MonevSuccessIndicator>().toMutableList(),

    @OneToMany(mappedBy = "monevResultChainId")
    @Where(clause = "active = true")
    @OrderBy("year, number")
    var questions: MutableList<MonevResultChainQuestion>? = emptyList<MonevResultChainQuestion>().toMutableList(),

    @Column(name = "parrent_id")
    var parrentId: String? = null,

    @Column(name = "created_by")
    var createdBy: String? = null,

    @ManyToOne
    @JoinColumn(name = "created_by", insertable = false, updatable = false, nullable = true)
    var createdByUser: VUsersResources? = null,

    @Column(name = "updated_by")
    var updatedBy: String? = null,

    @ManyToOne
    @JoinColumn(name = "updated_by", insertable = false, updatable = false, nullable = true)
    var updatedByUser: VUsersResources? = null,

    @Column(name = "created_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var createdAt: Date? = Date(),

    @Column(name = "updated_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var updatedAt: Date? = Date(),

    @Column(name = "deleted_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var deletedAt: Date? = Date(),

    @Column(name = "deleted_by")
    var deletedBy: String? = null,

    @ManyToOne
    @JoinColumn(name = "deleted_by", insertable = false, updatable = false, nullable = true)
    var deletedByUser: VUsersResources? = null,

    @Column(name = "active")
    @JsonIgnore
    var active: Boolean? = true
)
