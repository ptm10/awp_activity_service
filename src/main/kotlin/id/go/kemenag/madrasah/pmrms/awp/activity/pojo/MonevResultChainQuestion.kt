package id.go.kemenag.madrasah.pmrms.awp.activity.pojo

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "monev_result_chain_question", schema = "public")
data class MonevResultChainQuestion(
    @Id
    @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @Column(name = "monev_result_chain_id")
    var monevResultChainId: String? = null,

    @Column(name = "monev_question_id")
    var monevQuestionId: String? = null,

    @ManyToOne
    @JoinColumn(name = "monev_question_id", insertable = false, updatable = false, nullable = true)
    var monevQuestion: MonevQuestion? = null,

    @Column(name = "number")
    var number: Int? = null,

    @Column(name = "yes_or_no_answer")
    var yesOrNoAnswer: Boolean? = null,

    @Column(name = "answer")
    var answer: String? = null,

    @Column(name = "year")
    var year: Int? = null,

    @Column(name = "created_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var createdAt: Date? = Date(),

    @Column(name = "updated_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var updatedAt: Date? = Date(),

    @Column(name = "active")
    var active: Boolean? = true
)
