package id.go.kemenag.madrasah.pmrms.awp.activity.pojo

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import org.hibernate.annotations.Where
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "monev_success_indicator", schema = "public")
data class MonevSuccessIndicator(
    @Id
    @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @Column(name = "monev_result_chain_id")
    var monevResultChainId: String? = null,

    @ManyToOne
    @JoinColumn(name = "monev_result_chain_id", insertable = false, updatable = false, nullable = true)
    var monevResultChain: MonevSuccessIndicatorResultChain? = null,

    @Column(name = "code")
    var code: String? = null,

    @Column(name = "name")
    var name: String? = null,

    @Column(name = "pdo_id")
    var pdoId: String? = null,

    @ManyToOne
    @JoinColumn(name = "pdo_id", insertable = false, updatable = false, nullable = true)
    var pdo: Pdo? = null,

    @Column(name = "iri_id")
    var iriId: String? = null,

    @ManyToOne
    @JoinColumn(name = "iri_id", insertable = false, updatable = false, nullable = true)
    var iri: Iri? = null,

    @Column(name = "baseline_value")
    var baselineValue: String? = null,

    @Column(name = "target_total")
    var targetTotal: String? = null,

    @Column(name = "cumulative_target_2020")
    var cumulativeTarget2020: String? = null,

    @Column(name = "cumulative_target_dummy_2020")
    var cumulativeTargetDummy2020: Boolean? = null,

    @Column(name = "not_applicable_2020")
    var notApplicable2020: Boolean? = null,

    @Column(name = "cumulative_target_2021")
    var cumulativeTarget2021: String? = null,

    @Column(name = "cumulative_target_dummy_2021")
    var cumulativeTargetDummy2021: Boolean? = null,

    @Column(name = "not_applicable_2021")
    var notApplicable2021: Boolean? = null,

    @Column(name = "cumulative_target_2022")
    var cumulativeTarget2022: String? = null,

    @Column(name = "cumulative_target_dummy_2022")
    var cumulativeTargetDummy2022: Boolean? = null,

    @Column(name = "not_applicable_2022")
    var notApplicable2022: Boolean? = null,

    @Column(name = "cumulative_target_2023")
    var cumulativeTarget2023: String? = null,

    @Column(name = "cumulative_target_dummy_2023")
    var cumulativeTargetDummy2023: Boolean? = null,

    @Column(name = "not_applicable_2023")
    var notApplicable2023: Boolean? = null,

    @Column(name = "cumulative_target_2024")
    var cumulativeTarget2024: String? = null,

    @Column(name = "cumulative_target_dummy_2024")
    var cumulativeTargetDummy2024: Boolean? = null,

    @Column(name = "not_applicable_2024")
    var notApplicable2024: Boolean? = null,

    @Column(name = "result_2020")
    var result2020: String? = null,

    @Column(name = "result_2021")
    var result2021: String? = null,

    @Column(name = "result_2022")
    var result2022: String? = null,

    @Column(name = "result_2023")
    var result2023: String? = null,

    @Column(name = "result_2024")
    var result2024: String? = null,

    @OneToMany(mappedBy = "successIndicatorId")
    @Where(clause = "active = true")
    var files: MutableList<MonevSuccessIndicatorFiles>? = emptyList<MonevSuccessIndicatorFiles>().toMutableList(),

    @Column(name = "created_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var createdAt: Date? = Date(),

    @Column(name = "updated_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var updatedAt: Date? = Date(),

    @Column(name = "deleted_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var deletedAt: Date? = Date(),

    @Column(name = "deleted_by")
    var deletedBy: String? = null,

    @ManyToOne
    @JoinColumn(name = "deleted_by", insertable = false, updatable = false, nullable = true)
    var deletedByUser: VUsersResources? = null,

    @Column(name = "active")
    var active: Boolean? = true
)
