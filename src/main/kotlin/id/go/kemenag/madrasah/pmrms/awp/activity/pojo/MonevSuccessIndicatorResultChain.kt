package id.go.kemenag.madrasah.pmrms.awp.activity.pojo

import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "monev_result_chain", schema = "public")
data class MonevSuccessIndicatorResultChain(
    @Id
    @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @Column(name = "year")
    var year: Int? = null,

    @Column(name = "monev_type_id")
    var monevTypeId: String? = null
)
