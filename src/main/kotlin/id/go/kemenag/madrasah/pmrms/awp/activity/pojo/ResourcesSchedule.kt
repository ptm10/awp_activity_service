package id.go.kemenag.madrasah.pmrms.awp.activity.pojo

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import id.go.kemenag.madrasah.pmrms.awp.activity.constant.APPROVAL_STATUS_WAITING
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "resources_schedule", schema = "public")
data class ResourcesSchedule(
    @Id
    @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @Column(name = "resources_id")
    var resourcesId: String? = null,

    @ManyToOne
    @JoinColumn(name = "resources_id", insertable = false, updatable = false, nullable = true)
    var resources: Resources? = null,

    @Column(name = "title")
    var title: String? = null,

    @Column(name = "description")
    var description: String? = null,

    @Column(name = "start_date")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+7")
    var startDate: Date? = null,

    @Column(name = "end_date")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+7")
    var endDate: Date? = null,

    @Column(name = "days_length")
    var daysLength: Int? = null,

    @Column(name = "taskId")
    var taskId: String? = null,

    @Column(name = "taskType")
    var taskType: Int? = null,

    @Column(name = "approveStatus")
    var approveStatus: Int? = APPROVAL_STATUS_WAITING,

    @Column(name = "created_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var createdAt: Date? = Date(),

    @Column(name = "updated_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var updatedAt: Date? = Date(),

    @Column(name = "active")
    @JsonIgnore
    var active: Boolean? = true
)
