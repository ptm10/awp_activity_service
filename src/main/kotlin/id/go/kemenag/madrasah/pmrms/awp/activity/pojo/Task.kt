package id.go.kemenag.madrasah.pmrms.awp.activity.pojo

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import id.go.kemenag.madrasah.pmrms.awp.activity.constant.TASK_NUMBER_TASK
import id.go.kemenag.madrasah.pmrms.awp.activity.constant.TASK_STATUS_NEW
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "task", schema = "public")
data class Task(
    @Id
    @Column(name = "id")
    var id: String? = UUID.randomUUID().toString(),

    @Column(name = "created_by")
    var createdBy: String? = null,

    @ManyToOne
    @JoinColumn(name = "created_by", insertable = false, updatable = false, nullable = true)
    var createdByUser: Users? = null,

    @Column(name = "created_for")
    var createdFor: String? = null,

    @ManyToOne
    @JoinColumn(name = "created_for", insertable = false, updatable = false, nullable = true)
    var createdForUser: Users? = null,

    @Column(name = "task_type")
    var taskType: Int? = null,

    @Column(name = "task_id")
    var taskId: String? = null,

    @Column(name = "task_number")
    var taskNumber: Int? = TASK_NUMBER_TASK,

    @Column(name = "description")
    var description: String? = null,

    @Column(name = "done")
    var done: Boolean? = false,

    @Column(name = "updated_by")
    var updatedBy: String? = null,

    @ManyToOne
    @JoinColumn(name = "updated_by", insertable = false, updatable = false, nullable = true)
    var updatedByUser: Users? = null,

    @Column(name = "status")
    var status: Int? = TASK_STATUS_NEW,

    @Column(name = "created_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var createdAt: Date? = Date(),

    @Column(name = "updated_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var updatedAt: Date? = Date(),

    @Column(name = "active")
    @JsonIgnore
    var active: Boolean? = true
)
