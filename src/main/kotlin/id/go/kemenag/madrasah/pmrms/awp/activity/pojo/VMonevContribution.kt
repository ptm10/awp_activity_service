package id.go.kemenag.madrasah.pmrms.awp.activity.pojo

import com.fasterxml.jackson.annotation.JsonFormat
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "v_monev_contribution", schema = "public")
data class VMonevContribution(
    @Id
    @Column(name = "id")
    var id: String? = null,

    @Column(name = "year")
    var year: Int? = null,

    @Column(name = "success_indicator_id")
    var successIndicatorId: String? = null,

    @Column(name = "contribution")
    var contribution: String? = null,

    @Column(name = "component_code")
    var componentCode: String? = null,

    @Column(name = "name")
    var name: String? = null,

    @Column(name = "created_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var createdAt: Date? = Date(),

    @Column(name = "updated_at")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT+7")
    var updatedAt: Date? = Date(),

    @Column(name = "active")
    var active: Boolean? = true
)
