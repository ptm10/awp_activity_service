package id.go.kemenag.madrasah.pmrms.awp.activity.pojo

import org.hibernate.annotations.Where
import javax.persistence.*

@Entity
@Table(name = "v_user_resources", schema = "auth")
data class VUsersResourcesComponent(
    @Id
    @Column(name = "id")
    var id: String? = null,

    @Column(name = "component_id")
    var componentId: String? = null,

    @ManyToOne
    @JoinColumn(name = "resources_id", insertable = false, updatable = false, nullable = true)
    @Where(clause = "active = true")
    var resources: UsersResources? = null,

    @Column(name = "active")
    var active: Boolean? = true
)
