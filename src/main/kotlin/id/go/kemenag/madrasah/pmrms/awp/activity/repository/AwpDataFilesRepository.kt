package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.AwpDataFiles
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.query.Param
import java.util.*

interface AwpDataFilesRepository : JpaRepository<AwpDataFiles, String> {

    fun findByIdAndActive(@Param("id") id: String?, active: Boolean = true): Optional<AwpDataFiles>

    fun findByAwpDataIdAndFileIdAndActive(
        @Param("awpDataId") awpDataId: String?,
        @Param("fileId") fileId: String?,
        active: Boolean = true
    ): Optional<AwpDataFiles>
}
