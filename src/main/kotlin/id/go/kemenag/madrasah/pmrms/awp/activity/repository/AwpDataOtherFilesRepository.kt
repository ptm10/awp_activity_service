package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.AwpDataOtherFiles
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.query.Param
import java.util.*

interface AwpDataOtherFilesRepository : JpaRepository<AwpDataOtherFiles, String> {

    fun findByIdAndActive(@Param("id") id: String?, active: Boolean = true): Optional<AwpDataOtherFiles>

    fun findByAwpDataIdAndFileIdAndActive(
        @Param("awpDataId") awpDataId: String?,
        @Param("fileId") fileId: String?,
        active: Boolean = true
    ): Optional<AwpDataOtherFiles>

    fun getByKeyAndAwpDataIdAndActive(@Param("key") key: String?, @Param("awpDataId") awpDataId: String?, active: Boolean = true) : List<AwpDataOtherFiles>
}
