package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.AwpDataPdoIri
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.query.Param
import java.util.*

interface AwpDataPdoIriRepository : JpaRepository<AwpDataPdoIri, String> {

    fun findByIdAndActive(@Param("id") id: String?, active: Boolean = true): Optional<AwpDataPdoIri>

    fun findByAwpDataPdoIdAndIriIdAndActive(@Param("awpDataPdoId") awpDataPdoId: String?, @Param("iriId") iriId: String?, active: Boolean = true): Optional<AwpDataPdoIri>
}
