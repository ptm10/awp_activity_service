package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.AwpDataPdo
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.query.Param
import java.util.*

interface AwpDataPdoRepository : JpaRepository<AwpDataPdo, String> {

    fun findByIdAndActive(@Param("id") id: String?, active: Boolean = true): Optional<AwpDataPdo>

    fun findByAwpDataIdAndPdoIdAndActive(
        @Param("awpDataId") awpDataId: String?,
        @Param("pdoId") pdoId: String?,
        active: Boolean = true
    ): Optional<AwpDataPdo>
}
