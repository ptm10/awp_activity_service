package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.AwpData
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.query.Param
import java.util.*

interface AwpDataRepository : JpaRepository<AwpData, String> {

    fun findByIdAndActive(@Param("id") id: String?, active: Boolean = true): Optional<AwpData>

    fun findByYearAndActive(@Param("year") year: Int?, active: Boolean = true): Optional<AwpData>
}
