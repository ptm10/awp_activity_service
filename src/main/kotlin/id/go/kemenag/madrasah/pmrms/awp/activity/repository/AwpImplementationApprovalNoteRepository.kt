package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.AwpImplementationApprovalNote
import org.springframework.data.jpa.repository.JpaRepository

interface AwpImplementationApprovalNoteRepository : JpaRepository<AwpImplementationApprovalNote, String> {
}
