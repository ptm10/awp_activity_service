package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.AwpImplementationIri
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.query.Param
import java.util.*

interface AwpImplementationIriRepository : JpaRepository<AwpImplementationIri, String> {

    fun findByIdAndActive(@Param("id") id: String?, active: Boolean = true): Optional<AwpImplementationIri>

    fun getByAwpImplementationIdAndIriIdAndActive(@Param("awpImplementationId") awpImplementationId: String?, @Param("iriId") iriId: String?, active: Boolean = true): List<AwpImplementationIri>
}
