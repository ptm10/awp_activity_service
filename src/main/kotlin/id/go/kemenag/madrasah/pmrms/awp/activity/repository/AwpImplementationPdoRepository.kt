package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.AwpImplementationPdo
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.query.Param
import java.util.*

interface AwpImplementationPdoRepository : JpaRepository<AwpImplementationPdo, String> {

    fun findByIdAndActive(@Param("id") id: String?, active: Boolean = true): Optional<AwpImplementationPdo>

    fun getByAwpImplementationIdAndPdoIdAndActive(@Param("awpImplementationId") awpImplementationId: String?, @Param("pdoId") ipdoId: String?, active: Boolean = true): List<AwpImplementationPdo>
}
