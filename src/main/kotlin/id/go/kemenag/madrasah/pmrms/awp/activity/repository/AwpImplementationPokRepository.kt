package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.AwpImplementationPok
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.query.Param
import java.util.*

interface AwpImplementationPokRepository : JpaRepository<AwpImplementationPok, String> {

    fun findByIdAndActive(@Param("id") id: String?, active: Boolean = true): Optional<AwpImplementationPok>
}
