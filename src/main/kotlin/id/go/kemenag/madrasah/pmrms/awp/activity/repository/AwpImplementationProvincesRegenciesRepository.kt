package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.AwpImplementationProvincesRegencies
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.query.Param
import java.util.*

interface AwpImplementationProvincesRegenciesRepository : JpaRepository<AwpImplementationProvincesRegencies, String> {

    fun findByIdAndActive(
        @Param("id") id: String?,
        active: Boolean = true
    ): Optional<AwpImplementationProvincesRegencies>

    fun getByAwpImplementationProvincesIdAndRegencyIdAndActive(
        @Param("awpImplementationProvincesId") awpImplementationProvincesId: String?,
        @Param("regencyId") regencyId: String?,
        active: Boolean = true
    ): List<AwpImplementationProvincesRegencies>
}
