package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.AwpImplementationProvinces
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.query.Param
import java.util.*

interface AwpImplementationProvincesRepository : JpaRepository<AwpImplementationProvinces, String> {

    fun findByIdAndActive(@Param("id") id: String?, active: Boolean = true): Optional<AwpImplementationProvinces>

    fun getByAwpImplementationIdAndProvinceIdAndActive(
        @Param("awpImplementationId") awpImplementationId: String?,
        @Param("provinceId") provinceId: String?,
        active: Boolean = true
    ): List<AwpImplementationProvinces>
}
