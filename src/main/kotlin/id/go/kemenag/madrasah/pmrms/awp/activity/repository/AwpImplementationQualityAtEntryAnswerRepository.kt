package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.AwpImplementationQualityAtEntryAnswer
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.query.Param
import java.util.*

interface AwpImplementationQualityAtEntryAnswerRepository :
    JpaRepository<AwpImplementationQualityAtEntryAnswer, String> {

    fun findByIdAndActive(
        @Param("id") id: String?,
        active: Boolean = true
    ): Optional<AwpImplementationQualityAtEntryAnswer>

    fun findByAwpImplementationIdAndQualityAtEntryQuestionIdAndActive(
        @Param("awpImplementationId") awpImplementationId: String?,
        @Param("qualityAtEntryQuestionId") qualityAtEntryQuestionId: String?,
        active: Boolean = true
    ): Optional<AwpImplementationQualityAtEntryAnswer>
}
