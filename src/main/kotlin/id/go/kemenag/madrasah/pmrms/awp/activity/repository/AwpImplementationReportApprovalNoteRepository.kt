package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.AwpImplementationReportApprovalNote
import org.springframework.data.jpa.repository.JpaRepository

interface AwpImplementationReportApprovalNoteRepository : JpaRepository<AwpImplementationReportApprovalNote, String> {
}
