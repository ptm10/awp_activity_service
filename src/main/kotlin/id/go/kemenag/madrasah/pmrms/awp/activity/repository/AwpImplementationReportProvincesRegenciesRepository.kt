package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.AwpImplementationReportProvincesRegencies
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.query.Param
import java.util.*

interface AwpImplementationReportProvincesRegenciesRepository :
    JpaRepository<AwpImplementationReportProvincesRegencies, String> {

    fun findByIdAndActive(
        @Param("id") id: String?,
        active: Boolean = true
    ): Optional<AwpImplementationReportProvincesRegencies>

    fun getByAwpImplementationReportProvincesIdAndRegencyIdAndActive(
        @Param("awpImplementationReportProvincesId") awpImplementationReportProvincesId: String?,
        @Param("regencyId") regencyId: String?,
        active: Boolean = true
    ): List<AwpImplementationReportProvincesRegencies>
}
