package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.AwpImplementationReportProvinces
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.query.Param
import java.util.*

interface AwpImplementationReportProvincesRepository : JpaRepository<AwpImplementationReportProvinces, String> {

    fun findByIdAndActive(@Param("id") id: String?, active: Boolean = true): Optional<AwpImplementationReportProvinces>

    fun getByAwpImplementationReportIdAndProvinceIdAndActive(
        @Param("awpImplementationReportId") awpImplementationReportId: String?,
        @Param("provinceId") provinceId: String?,
        active: Boolean = true
    ): List<AwpImplementationReportProvinces>
}
