package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.AwpImplementationReport
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.query.Param
import java.util.*

interface AwpImplementationReportRepository : JpaRepository<AwpImplementationReport, String> {

    fun findByIdAndActive(@Param("id") id: String?, active: Boolean = true): Optional<AwpImplementationReport>

    fun getByAwpImplementationIdAndActive(@Param("awpImplementationId") awpImplementationId: String?, active: Boolean = true) : List<AwpImplementationReport>
}
