package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.AwpImplementationReportResultChain
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.query.Param
import java.util.*

interface AwpImplementationReportResultChainRepository : JpaRepository<AwpImplementationReportResultChain, String> {

    fun findByIdAndActive(
        @Param("id") id: String?,
        active: Boolean = true
    ): Optional<AwpImplementationReportResultChain>

    fun findByAwpImplementationReportIdAndResultChainIdAndActive(
        awpImplementationReportId: String?,
        resultChainId: String?,
        active: Boolean = true
    ): Optional<AwpImplementationReportResultChain>
}
