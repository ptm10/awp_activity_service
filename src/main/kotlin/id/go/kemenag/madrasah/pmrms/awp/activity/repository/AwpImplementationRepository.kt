package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.constant.EVENT_MANAGEMENT_TYPE_CONTRACT
import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.AwpImplementation
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import java.util.*

interface AwpImplementationRepository : JpaRepository<AwpImplementation, String> {

    fun findByIdAndActive(@Param("id") id: String?, active: Boolean = true): Optional<AwpImplementation>

    fun findByAwpIdAndActive(@Param("id") id: String?, active: Boolean = true): Optional<AwpImplementation>

    @Query("select id from AwpImplementation ai where ai.componentId = :componentId and ai.eventManagementTypeId = :eventManagementType and ai.lspPic is null and ai.active = :active")
    fun getIdsLspChoice(
        @Param("componentId") componentId: String?,
        eventManagementType: String? = EVENT_MANAGEMENT_TYPE_CONTRACT,
        active: Boolean? = true
    ): List<String>

    fun findByComponentCodeIdAndAwpYearAndActive(
        @Param("componentCodeId") componentCodeId: String?,
        @Param("year") year: Int?,
        active: Boolean = true
    ): List<AwpImplementation>

    @Query("select id from AwpImplementation where active = :active")
    fun getAllIds(active: Boolean? = true): MutableList<String>

    @Query("select id from AwpImplementation where componentId in(:componentIds) and active = true group by id")
    fun getIdsByComponentIds(componentIds: List<String>?): List<String>

    @Query("select id from AwpImplementation where eventManagementTypeId = :eventManagementTypeId and active = true group by id")
    fun getIdsByEventManagementType(eventManagementTypeId: String?): List<String>

    @Query("select id from AwpImplementation where projectOfficerResources.id = :resourcesId and active = true group by id")
    fun getByProjectOfficerResourcesId(resourcesId: String?): List<String>

    @Query("select id from AwpImplementation where lspPicUser.id = :userId and active = true group by id")
    fun getByLspUserId(userId: String?): List<String>
}
