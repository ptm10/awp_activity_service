package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.AwpImplementationResultChain
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.query.Param
import java.util.*

interface AwpImplementationResultChainRepository : JpaRepository<AwpImplementationResultChain, String> {

    fun findByIdAndActive(@Param("id") id: String?, active: Boolean = true): Optional<AwpImplementationResultChain>

    fun getByAwpImplementationIdAndActive(@Param("awpImplementationId") awpImplementationId: String?, active: Boolean = true): List<AwpImplementationResultChain>
}
