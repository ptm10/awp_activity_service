package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.AwpProvincesRegencies
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.query.Param
import java.util.*

interface AwpProvincesRegenciesRepository : JpaRepository<AwpProvincesRegencies, String> {

    fun findByIdAndActive(@Param("id") id: String?, active: Boolean = true): Optional<AwpProvincesRegencies>
}
