package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.Awp
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import java.util.*

interface AwpRepository : JpaRepository<Awp, String> {

    fun findByIdAndActive(id: String?, active: Boolean = true): Optional<Awp>

    @Query(
        "select distinct(a.year) as year from Awp a " +
                "where a.active = true order by a.year"
    )
    fun getListyear(): List<Int>?

    @Query("from Awp a where lower(a.name) = lower(trim(:name)) and a.componentCode.code = :code and a.year = :year and a.active = :active")
    fun findByNameComponentCodeYearActive(
        @Param("name") name: String?,
        @Param("code") code: String?,
        @Param("year") year: Int?,
        @Param("active") active: Boolean = true
    ): Optional<Awp>

    fun getByIdInAndActive(id: List<String>, active: Boolean = true): List<Awp>

    fun getByYearAndActive(year: Int?, active: Boolean = true): List<Awp>

    fun getByActive(active: Boolean = true): List<Awp>

    fun getByYearAndComponentCodeIdAndActive(year: Int?, componentCodeId: String?, active: Boolean = true): List<Awp>
}
