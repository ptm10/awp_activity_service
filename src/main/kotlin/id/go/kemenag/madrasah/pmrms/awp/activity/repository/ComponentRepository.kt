package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.Component
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import java.util.*

interface ComponentRepository : JpaRepository<Component, String> {

    fun findByIdAndActive(@Param("id") id: String?, active: Boolean = true): Optional<Component>

    @Query(
        "SELECT c FROM Component c " +
                "WHERE c.active = true " +
                "AND substring(c.code, 1, 1) IN(:codes) " +
                "ORDER BY c.code"
    )
    fun getByUserComponentCodes(@Param("codes") codes: List<String>): List<Component>

    @Query(
        "SELECT c FROM Component c " +
                "WHERE c.active = true " +
                "AND c.code LIKE :code% " +
                "AND (array_length(string_to_array(code, '.'), 1) - 1) = :length " +
                "AND c.year = :year " +
                "ORDER BY CAST(REPLACE(c.code, '.', '') AS text)"
    )
    fun getSubComponent(@Param("code") code: String?, @Param("length") length: Int?, @Param("year") year: Int?): List<Component>

    fun findByCodeAndActive(@Param("code") code: String?, active: Boolean = true): Optional<Component>

    @Query(
        "SELECT c FROM Component c " +
                "WHERE c.active = :active " +
                "AND c.code = :code " +
                "AND c.id IN(:idsIn)"
    )
    fun findByCodeActiveIdin(
        @Param("code") code: String?,
        @Param("idsIn") idsIn: List<String>,
        active: Boolean = true
    ): Optional<Component>

    @Query("from Component c where trim(lower(c.code)) = trim(lower(:code)) and c.year = :year and c.active = :active")
    fun findByCodeYearActive(
        @Param("code") code: String?,
        @Param("year") year: Int?,
        @Param("active") active: Boolean? = true
    ): Optional<Component>

    @Query("select max(array_length(string_to_array(code, '.'), 1) - 1) FROM component", nativeQuery = true)
    fun getMaxDotCode(): Int

    fun getByIdInAndActive(ids: List<String>?, active: Boolean = true): List<Component>
}
