package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.EventAgendaDetail
import org.springframework.data.jpa.repository.JpaRepository

interface EventAgendaDetailRepository : JpaRepository<EventAgendaDetail, String> {}
