package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import org.springframework.data.jpa.repository.JpaRepository

interface EventAgendaRepository : JpaRepository<id.go.kemenag.madrasah.pmrms.awp.activity.pojo.EventAgenda, String> {}
