package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.EventApprovalNote
import org.springframework.data.jpa.repository.JpaRepository

interface EventApprovalNoteRepository : JpaRepository<EventApprovalNote, String> {
}
