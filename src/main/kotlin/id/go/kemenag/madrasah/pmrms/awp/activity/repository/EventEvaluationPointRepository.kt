package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.EventEvaluationPoint
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import java.util.*

interface EventEvaluationPointRepository : JpaRepository<EventEvaluationPoint, String> {

    @Query("from EventEvaluationPoint ep where trim(lower(ep.name)) = trim(lower(:name)) and ep.active = :active")
    fun findByName(
        @Param("name") name: String?,
        @Param("active") active: Boolean = true
    ): Optional<EventEvaluationPoint>
}
