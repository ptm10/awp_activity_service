package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.EventFormExcelFileEmail
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import java.util.*

interface EventFormExcelFileEmailRepository : JpaRepository<EventFormExcelFileEmail, String> {

    fun findByIdAndActive(@Param("id") id: String?, active: Boolean = true): Optional<EventFormExcelFileEmail>

    fun getAllByActive(active: Boolean = true): List<EventFormExcelFileEmail>

    @Query("from EventFormExcelFileEmail c where c.eventFormFileExcelId = :eventFormFileExcelId and trim(lower(c.email)) = trim(lower(:email)) and c.active = :active")
    fun findByEventFormFileExcelIdAndEmailAndActive(@Param("eventFormFileExcelId") eventFormFileExcelId: String?, @Param("email") email: String?, active: Boolean = true): Optional<EventFormExcelFileEmail>
}
