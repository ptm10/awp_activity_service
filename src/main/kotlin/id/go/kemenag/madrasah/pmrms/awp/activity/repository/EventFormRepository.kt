package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.EventForm
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.query.Param
import java.util.*

interface EventFormRepository : JpaRepository<EventForm, String> {

    fun findByIdAndActive(@Param("id") id: String?, active: Boolean = true): Optional<EventForm>

    fun getByEventTypeIdAndActive(@Param("eventTypeId") eventTypeId: String?, active: Boolean = true): List<EventForm>
}
