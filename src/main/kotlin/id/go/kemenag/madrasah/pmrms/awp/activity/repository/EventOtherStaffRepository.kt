package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.EventOtherStaff
import org.springframework.data.jpa.repository.JpaRepository

interface EventOtherStaffRepository : JpaRepository<EventOtherStaff, String> {
}
