package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.EventProgressReportApprovalNote
import org.springframework.data.jpa.repository.JpaRepository

interface EventProgressReportApprovalNoteRepository : JpaRepository<EventProgressReportApprovalNote, String> {
}
