package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.EventProgressReportDocuments
import org.springframework.data.jpa.repository.JpaRepository

interface EventProgressReportDocumentsRepository : JpaRepository<EventProgressReportDocuments, String> {
}
