package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.EventProgressReportOtherStaff
import org.springframework.data.jpa.repository.JpaRepository

interface EventProgressReportOtherStaffRepository : JpaRepository<EventProgressReportOtherStaff, String> {
}
