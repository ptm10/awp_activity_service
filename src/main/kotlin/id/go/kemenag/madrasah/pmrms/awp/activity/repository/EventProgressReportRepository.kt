package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.constant.EVENT_PROGRESS_REPORT_STATUS_APPROVED_BY_CONSULTAN
import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.EventProgressReport
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import java.util.*

interface EventProgressReportRepository : JpaRepository<EventProgressReport, String> {

    fun findByIdAndActive(
        @Param("id") id: String?, @Param("active") active: Boolean = true
    ): Optional<EventProgressReport>

    @Query("from EventProgressReport pr where pr.eventId = :eventId and pr.createdBy = :createdBy and pr.active = :active and pr.status = :status order by pr.createdAt desc")
    fun lastReport(
        @Param("eventId") eventId: String?,
        @Param("createdBy") createdBy: String?,
        @Param("status") status: Int? = EVENT_PROGRESS_REPORT_STATUS_APPROVED_BY_CONSULTAN,
        @Param("active") active: Boolean? = true
    ): List<EventProgressReport>

    @Query("from EventProgressReport pr where pr.eventId = :eventId and pr.active = :active and pr.status = :status order by pr.createdAt desc")
    fun lastReportWithoutUser(
        @Param("eventId") eventId: String?,
        @Param("status") status: Int? = EVENT_PROGRESS_REPORT_STATUS_APPROVED_BY_CONSULTAN,
        @Param("active") active: Boolean? = true
    ): List<EventProgressReport>

    @Query("from EventProgressReport pr where pr.eventId = :eventId and pr.active = :active and pr.status <> :status order by pr.createdAt")
    fun getByEventIdStatus(
        @Param("eventId") eventId: String?,
        @Param("status") status: Int? = EVENT_PROGRESS_REPORT_STATUS_APPROVED_BY_CONSULTAN,
        @Param("active") active: Boolean? = true
    ): List<EventProgressReport>
}
