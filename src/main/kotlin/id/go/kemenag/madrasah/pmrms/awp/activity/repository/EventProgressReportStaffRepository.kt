package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.EventProgressReportStaff
import org.springframework.data.jpa.repository.JpaRepository

interface EventProgressReportStaffRepository : JpaRepository<EventProgressReportStaff, String> {
}
