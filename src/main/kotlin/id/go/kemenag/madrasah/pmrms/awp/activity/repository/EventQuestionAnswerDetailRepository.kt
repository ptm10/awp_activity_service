package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.EventQuestionAnswerDetail
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.query.Param
import java.util.*

interface EventQuestionAnswerDetailRepository : JpaRepository<EventQuestionAnswerDetail, String> {

    fun findByIdAndActive(@Param("id") id: String?, active: Boolean = true): Optional<EventQuestionAnswerDetail>

    fun findByEventQuestionAnswerIdAndEventQuestionIdAndActive(@Param("eventQuestionAnswerId") eventQuestionAnswerId: String?, @Param("eventQuestionId") eventQuestionId: String?, active: Boolean = true) : Optional<EventQuestionAnswerDetail>

    fun getByEventQuestionAnswerIdInAndActive(@Param("eventQuestionAnswerIds") eventQuestionAnswerIds: List<String>, active: Boolean = true) : List<EventQuestionAnswerDetail>
}
