package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.EventQuestionAnswer
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import java.util.*

interface EventQuestionAnswerRepository : JpaRepository<EventQuestionAnswer, String> {

    fun findByIdAndActive(@Param("id") id: String?, active: Boolean = true): Optional<EventQuestionAnswer>

    fun getByEventIdAndFilledByAndActiveOrderByCreatedAtDesc(
        @Param("eventId") eventId: String?,
        @Param("filledBy") filledBy: Int?,
        active: Boolean = true
    ): List<EventQuestionAnswer>

    @Query("from EventQuestionAnswer c where trim(lower(c.email)) = trim(lower(:email)) and c.eventId = :eventId and c.filledBy = :filledBy and c.active = :active")
    fun findByEmailEventIdFilledByActive(
        @Param("email") email: String?,
        @Param("eventId") eventId: String?,
        @Param("filledBy") filledBy: Int?,
        active: Boolean = true
    ): Optional<EventQuestionAnswer>
}
