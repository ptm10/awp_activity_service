package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.EventQuestion
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import java.util.*

interface EventQuestionRepository : JpaRepository<EventQuestion, String> {

    fun findByIdAndActive(@Param("id") id: String?, active: Boolean = true): Optional<EventQuestion>

    @Query("from EventQuestion a where a.eventFormId = :eventFormId and a.createdFor = :createdFor and a.questionType = :questionType and a.questionCategory = :questionCategory and lower(a.question) = lower(trim(:question)) and a.active = :active")
    fun eventFormIdCreatedForQuestionTypeQuestionCategoryQuestion(@Param("eventFormId") eventFormId: String?,
                                                                  @Param("createdFor") createdFor: Int?,
                                                                  @Param("questionType") questionType: Int?,
                                                                  @Param("questionCategory") questionCategory: Int?,
                                                                  @Param("question") question: String?,
                                                                  active: Boolean = true): Optional<EventQuestion>

    @Query("select distinct question from EventQuestion where active = :active order by question")
    fun getDistinctQuestions(active: Boolean = true) : List<String>
}
