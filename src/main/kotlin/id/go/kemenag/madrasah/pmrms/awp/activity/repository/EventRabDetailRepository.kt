package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.EventRabDetail
import org.springframework.data.jpa.repository.JpaRepository

interface EventRabDetailRepository : JpaRepository<EventRabDetail, String> {
}
