package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.EventRab
import org.springframework.data.jpa.repository.JpaRepository

interface EventRabRepository : JpaRepository<EventRab, String> {
}
