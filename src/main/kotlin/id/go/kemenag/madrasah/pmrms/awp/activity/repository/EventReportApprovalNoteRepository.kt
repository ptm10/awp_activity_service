package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.EventReportApprovalNote
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.query.Param
import java.util.*

interface EventReportApprovalNoteRepository : JpaRepository<EventReportApprovalNote, String> {

    fun findByIdAndActive(
        @Param("id") id: String?,
        @Param("active") active: Boolean = true
    ): Optional<EventReportApprovalNote>
}
