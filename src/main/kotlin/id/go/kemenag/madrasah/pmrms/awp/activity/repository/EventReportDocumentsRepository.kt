package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.EventReportDocuments
import org.springframework.data.jpa.repository.JpaRepository

interface EventReportDocumentsRepository : JpaRepository<EventReportDocuments, String> {
}
