package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.EventReportParticipantTarget
import org.springframework.data.jpa.repository.JpaRepository

interface EventReportParticipantTargetRepository : JpaRepository<EventReportParticipantTarget, String> {
}
