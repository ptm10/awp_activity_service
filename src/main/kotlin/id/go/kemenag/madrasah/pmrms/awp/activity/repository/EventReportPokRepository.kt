package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.EventReportPok
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.query.Param
import java.util.*

interface EventReportPokRepository : JpaRepository<EventReportPok, String> {

    fun findByIdAndActive(@Param("id") id: String?, active: Boolean = true): Optional<EventReportPok>
}
