package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.EventReport
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import java.util.*

interface EventReportRepository : JpaRepository<EventReport, String> {

    fun findByIdAndActive(
        @Param("id") id: String?,
        @Param("active") active: Boolean = true
    ): Optional<EventReport>

    fun findByEventIdAndActive(eventId: String?, active: Boolean? = true): Optional<EventReport>

    fun countByEventIdInAndActive(eventIds: List<String>, active: Boolean? = true): Long

    @Query("from EventReport a where ((a.startDate between :startDate and :endDate) or (a.endDate between :startDate and :endDate)) and a.status = :status and a.active = :active")
    fun getByDateBetwwenStatus(
        startDate: Date?,
        endDate: Date?,
        status: Int?,
        active: Boolean = true
    ): List<EventReport>

    fun getByEventIdAndStatusAndActive(
        eventId: String?,
        status: Int?,
        active: Boolean? = true
    ): List<EventReport>
}
