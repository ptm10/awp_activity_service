package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.Event
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import java.util.*

interface EventRepository : JpaRepository<Event, String> {

    fun findByIdAndActive(@Param("id") id: String?, active: Boolean = true): Optional<Event>

    @Query("select id from Event where awpImplementation.componentId in(:componentIds) and active = true group by id")
    fun getIdsByComponentIds(componentIds: List<String>?): List<String>

    @Query("select id from Event where awpImplementation.subSubComponentId in(:subComponentIds) and active = true group by id")
    fun getIdsBySubComponentIds(subComponentIds: List<String>?): List<String>

    @Query("from Event where id in(:ids) and active = :active")
    fun getByIds(ids: List<String>, active: Boolean? = true): List<Event>

    @Query("select id from Event where active = :active")
    fun getAllIds(active: Boolean? = true): MutableList<String>

    fun findByIdAndEndDateBeforeAndStatusAndActive(
        id: String?,
        afterDate: Date,
        status: Int,
        active: Boolean? = true
    ): Optional<Event>

    @Query("from Event where :dateBetween between startDate and endDate and id = :id and status = :status and active = :active")
    fun findByIdAndDateBetWeenStatus(
        id: String?,
        dateBetween: Date,
        status: Int,
        active: Boolean? = true
    ): Optional<Event>

    fun findByIdAndStatusAndActive(
        @Param("id") id: String?,
        @Param("status") status: Int,
        active: Boolean = true
    ): Optional<Event>

    fun countByAwpImplementationIdAndActive(awpImplementationId: String?, active: Boolean? = true): Long

    @Query("select id from Event where awpImplementationId = :awpImplementationId and active = true")
    fun getIdsByAwpImplementationIdActive(awpImplementationId: String?): List<String>

    fun getByAwpImplementationIdAndStatusAndActive(
        awpImplementationId: String?,
        status: Int?,
        active: Boolean? = true
    ): List<Event>
}
