package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.EventStaff
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import java.util.*

interface EventStaffRepository : JpaRepository<EventStaff, String> {

    @Query(
        "from EventStaff where resourcesId = :resourcesId " +
                "and active = true " +
                "and event.active = true " +
                "and ((event.startDate between :startDate and :endDate) or (event.endDate between :startDate and :endDate)) " +
                "and event.status = :eventStatus " +
                "order by event.endDate desc"
    )
    fun findByResourcesIdStartDateEndDate(
        resourcesId: String?,
        startDate: Date?,
        endDate: Date?,
        eventStatus: Int
    ): List<EventStaff>

    @Query("select eventId from EventStaff where resourcesId = :resourcesId and active = true group by eventId")
    fun getEventIdsByResourcesId(resourcesId: String?): List<String>

    @Query("select eventId from EventStaff where resources.supervisiorId in(:supervisiorIds) and active = true group by eventId")
    fun getEventIdsBySupervisiorIds(supervisiorIds: List<String>?): List<String>

    @Query("select eventId from EventStaff where resources.id in(:resourcesIds) and active = true group by eventId")
    fun getEventIdsByResourcesIds(resourcesIds: List<String>?): List<String>

    @Query("select event.awpImplementationId from EventStaff where resourcesId = :resourcesId and active = true group by event.awpImplementationId")
    fun getAwpImplementationIdsByResourcesId(resourcesId: String?): List<String>

    @Query("select event.awpImplementationId from EventStaff where resources.id in(:resourcesIds) and active = true group by event.awpImplementationId")
    fun getAwpImplementationIdsByResourcesIds(resourcesIds: List<String>?): List<String>

    @Query("select event.awpImplementationId from EventStaff where resources.supervisiorId in(:supervisiorIds) and active = true group by event.awpImplementationId")
    fun getAwpImplementationIdsBySupervisiorIds(supervisiorIds: List<String>?): List<String>
}
