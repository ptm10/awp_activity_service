package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.Iri
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.query.Param
import java.util.*

interface IriRepository : JpaRepository<Iri, String> {

    fun findByIdAndActive(@Param("id") id: String?, active: Boolean = true): Optional<Iri>

    fun getByCodeAndActive(@Param("code") code: String?, active: Boolean = true): List<Iri>

    fun getByCodeAndActiveOrderByName(code: String?, active: Boolean? = true): List<Iri>
}
