package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.LogActivity
import org.springframework.data.jpa.repository.JpaRepository

interface LogActivityRepository : JpaRepository<LogActivity, String> {

}
