package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.MonevQuestion
import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.MonevResultChain
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import java.util.*

interface MonevQuestionRepository : JpaRepository<MonevQuestion, String> {

    fun findByIdAndActive(@Param("id") id: String?, active: Boolean = true): Optional<MonevQuestion>

    @Query("from MonevQuestion c where c.yesOrNo = :yesOrNo and trim(lower(c.question)) = trim(lower(:question)) and c.active = :active")
    fun findByQuestionYesOrNoActive(@Param("question") question: String?, @Param("yesOrNo") yesOrNo: Boolean?, active: Boolean = true): Optional<MonevQuestion>
}
