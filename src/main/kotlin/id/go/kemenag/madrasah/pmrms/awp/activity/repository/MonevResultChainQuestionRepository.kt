package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.MonevResultChainQuestion
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.query.Param
import java.util.*

interface MonevResultChainQuestionRepository : JpaRepository<MonevResultChainQuestion, String> {

    fun findByIdAndActive(@Param("id") id: String?, active: Boolean? = true): Optional<MonevResultChainQuestion>

    fun findByYearAndMonevResultChainIdAndMonevQuestionIdAndActive(@Param("year") year: Int?, @Param("monevResultChainId") monevResultChainId: String?, @Param("monevQuestionId") monevQuestionId: String?, active: Boolean? = true) : Optional<MonevResultChainQuestion>

    fun findByYearAndMonevResultChainIdAndActiveOrderByNumber(@Param("year") year: Int?, @Param("monevResultChainId") monevResultChainId: String?, active: Boolean? = true) : List<MonevResultChainQuestion>
}
