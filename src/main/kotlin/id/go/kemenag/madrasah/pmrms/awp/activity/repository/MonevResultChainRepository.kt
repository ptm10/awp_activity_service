package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.MonevResultChain
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import java.util.*

interface MonevResultChainRepository : JpaRepository<MonevResultChain, String> {

    fun findByIdAndActive(@Param("id") id: String?, active: Boolean = true): Optional<MonevResultChain>

    @Query("from MonevResultChain c where c.year = :year and trim(lower(c.code)) = trim(lower(:code)) and c.active = :active")
    fun findByYearCodeActive(@Param("year") year: Int?, @Param("code") code: String?, active: Boolean = true): Optional<MonevResultChain>

    fun getAllByYearAndActiveOrderByCode(@Param("year") year: Int?, active: Boolean = true): List<MonevResultChain>
}
