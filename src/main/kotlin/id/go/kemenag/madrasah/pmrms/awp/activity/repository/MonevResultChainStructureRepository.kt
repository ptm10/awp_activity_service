package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.MonevResultChainStructure
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.query.Param
import java.util.*

interface MonevResultChainStructureRepository : JpaRepository<MonevResultChainStructure, String> {

    fun findByIdAndActive(@Param("id") id: String?, active: Boolean = true): Optional<MonevResultChainStructure>

    fun getByYearAndActive(@Param("year") year: Int, active: Boolean = true): List<MonevResultChainStructure>
}
