package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.MonevSuccessIndicator
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.query.Param
import java.util.*

interface MonevSuccessIndicatorRepository : JpaRepository<MonevSuccessIndicator, String> {

    fun findByIdAndActive(@Param("id") id: String?, active: Boolean = true): Optional<MonevSuccessIndicator>

    fun findByMonevResultChainIdAndCodeAndActive(
        @Param("monevResultChainId") monevResultChainId: String?,
        @Param("code") code: String?,
        active: Boolean = true
    ): Optional<MonevSuccessIndicator>
}
