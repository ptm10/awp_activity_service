package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.Pdo
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.query.Param
import java.util.*

interface PdoRepository : JpaRepository<Pdo, String> {

    fun findByIdAndActive(@Param("id") id: String?, active: Boolean = true): Optional<Pdo>

    fun getByActiveOrderByCode(active: Boolean = true): List<Pdo>
}
