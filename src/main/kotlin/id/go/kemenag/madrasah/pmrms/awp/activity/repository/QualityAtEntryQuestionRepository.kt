package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.QualityAtEntryQuestion
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.query.Param
import java.util.*

interface QualityAtEntryQuestionRepository : JpaRepository<QualityAtEntryQuestion, String> {

    fun findByIdAndActive(@Param("id") id: String?, active: Boolean = true): Optional<QualityAtEntryQuestion>

    fun getByActiveOrderByNumber(active: Boolean = true): List<QualityAtEntryQuestion>
}
