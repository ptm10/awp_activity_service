package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.Resources
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import java.util.*

interface ResourcesRepository : JpaRepository<Resources, String> {

    fun findByIdAndActive(@Param("id") id: String?, active: Boolean = true): Optional<Resources>

    @Query("select r.id from Resources r where r.supervisiorId = :supervisiorId and r.active = :active")
    fun getBySupervisiorIdAndActive(
        @Param("supervisiorId") supervisiorId: String?,
        @Param("active") active: Boolean = true
    ): List<String>

    fun countBySupervisiorIdAndActive(supervisiorId: String?, active: Boolean = true): Int
}
