package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.ResourcesSchedule
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface ResourcesScheduleRepository : JpaRepository<ResourcesSchedule, String> {

    fun findByResourcesIdAndTaskIdAndTaskTypeAndActive(
        resourceId: String?,
        taskId: String?,
        taskType: Int?,
        active: Boolean? = true
    ): Optional<ResourcesSchedule>
}
