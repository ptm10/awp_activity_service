package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.RikRisk
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import java.util.*

interface RikRiskRepository : JpaRepository<RikRisk, String> {

    fun findByIdAndActive(@Param("id") id: String?, active: Boolean = true): Optional<RikRisk>

    @Query("from RikRisk ep where trim(lower(ep.name)) = trim(lower(:name)) and ep.active = :active")
    fun findByNameActive(@Param("name") name: String?, active: Boolean = true): Optional<RikRisk>
}
