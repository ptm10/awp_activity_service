package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.constant.TASK_NUMBER_TASK
import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.Task
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface TaskRepository : JpaRepository<Task, String> {

    fun findByIdAndActive(id: String?, active: Boolean? = true): Optional<Task>

    fun findByTaskIdAndTaskTypeAndCreatedForAndTaskNumberAndActive(
        taskId: String?,
        taskType: Int?,
        createdFor: String?,
        taskNumber: Int? = TASK_NUMBER_TASK,
        active: Boolean? = true
    ): Optional<Task>
}
