package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.UsersRole
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query

interface UserRoleRepository : JpaRepository<UsersRole, String> {

    @Query("select ur.userId from UsersRole ur where ur.roleId in(:rolesId) and ur.active = true GROUP BY ur.userId having count(distinct ur.roleId) = :length")
    fun getUserIdsByRoleIds(rolesId: List<String>, length: Long): List<String>
}
