package id.go.kemenag.madrasah.pmrms.awp.activity.repository

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.UsersRoleViewUserResources
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query

interface UserRoleViewUserResourcesRepository : JpaRepository<UsersRoleViewUserResources, String> {

    @Query(
        "select ur.userId from UsersRoleViewUserResources ur where " +
                "ur.roleId in(:rolesId) " +
                "and ur.active = true " +
                "and ur.user.componentId = :componentId " +
                "and ur.user.resources.active = true " +
                "and trim(lower(ur.user.resources.position.name)) = trim(lower(:position)) " +
                "GROUP BY ur.userId having count(distinct ur.roleId) >= :length"
    )
    fun getUserIdsByRoleIdsComponentIdPosition(
        rolesId: List<String>,
        length: Long,
        componentId: String?,
        position: String?,
    ): List<String>

    @Query(
        "select ur.userId from UsersRoleViewUserResources ur where " +
                "ur.roleId in(:rolesId) " +
                "and ur.active = true " +
                "and ur.user.componentId = :componentId " +
                "and ur.user.resources.active = true " +
                "and ur.user.resources.supervisiorId is null " +
                "GROUP BY ur.userId having count(distinct ur.roleId) >= :length"
    )
    fun getPmuHead(
        rolesId: List<String>,
        length: Long,
        componentId: String?,
    ): List<String>
}
