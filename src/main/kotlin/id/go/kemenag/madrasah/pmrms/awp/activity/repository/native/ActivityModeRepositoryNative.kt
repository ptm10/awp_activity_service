package id.go.kemenag.madrasah.pmrms.awp.activity.repository.native

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.ActivityMode
import org.springframework.stereotype.Repository

@Repository
class ActivityModeRepositoryNative : BaseRepositoryNative<ActivityMode>(ActivityMode::class.java)
