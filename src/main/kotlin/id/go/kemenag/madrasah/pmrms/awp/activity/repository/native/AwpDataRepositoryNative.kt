package id.go.kemenag.madrasah.pmrms.awp.activity.repository.native

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.AwpData
import org.springframework.stereotype.Repository

@Repository
class AwpDataRepositoryNative : BaseRepositoryNative<AwpData>(AwpData::class.java)
