package id.go.kemenag.madrasah.pmrms.awp.activity.repository.native

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.AwpImplementationReport
import org.springframework.stereotype.Repository

@Repository
class AwpImplementationReportRepositoryNative :
    BaseRepositoryNative<AwpImplementationReport>(AwpImplementationReport::class.java)
