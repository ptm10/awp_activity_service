package id.go.kemenag.madrasah.pmrms.awp.activity.repository.native

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.AwpImplementation
import org.springframework.stereotype.Repository

@Repository
class AwpImplementationRepositoryNative : BaseRepositoryNative<AwpImplementation>(AwpImplementation::class.java)
