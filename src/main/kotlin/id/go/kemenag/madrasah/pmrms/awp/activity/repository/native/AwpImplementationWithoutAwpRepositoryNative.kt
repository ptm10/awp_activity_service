package id.go.kemenag.madrasah.pmrms.awp.activity.repository.native

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.AwpImplementationWithoutAwp
import org.springframework.stereotype.Repository

@Repository
class AwpImplementationWithoutAwpRepositoryNative :
    BaseRepositoryNative<AwpImplementationWithoutAwp>(AwpImplementationWithoutAwp::class.java)
