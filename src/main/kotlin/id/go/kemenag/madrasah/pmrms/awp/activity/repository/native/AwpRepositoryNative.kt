package id.go.kemenag.madrasah.pmrms.awp.activity.repository.native

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.Awp
import org.springframework.stereotype.Repository

@Repository
class AwpRepositoryNative : BaseRepositoryNative<Awp>(Awp::class.java)
