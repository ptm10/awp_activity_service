package id.go.kemenag.madrasah.pmrms.awp.activity.repository.native

import id.go.kemenag.madrasah.pmrms.awp.activity.helpers.getUserComponetCodes
import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.Component
import org.apache.commons.lang3.StringUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.support.PageableExecutionUtils
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import javax.persistence.EntityManager
import javax.persistence.Query

@Suppress("UNCHECKED_CAST")
@Repository
class ComponentRepositoryNative {

    @Autowired
    private lateinit var em: EntityManager

    private val collection = "Component"

    var byUserComponentCode = true

    @Transactional
    fun getPage(req: Pagination2Request): Page<Component>? {
        try {
            var queryStr = "SELECT s FROM $collection s WHERE s.active = true"
            if (byUserComponentCode) {
                queryStr += " AND substring(s.code, 1, 1) IN(:codes)"
            }

            if (!req.paramLike.isNullOrEmpty()) {
                queryStr += " AND ("

                req.paramLike?.forEachIndexed { index, element ->
                    queryStr += "LOWER(s.${element.field}) LIKE LOWER(CONCAT('%',:${element.field},'%'))"

                    if ((index + 1) < req.paramLike!!.size) {
                        queryStr += " OR "
                    }
                }

                queryStr += ")"
            }

            if (!req.paramIs.isNullOrEmpty()) {
                queryStr += " AND ("

                req.paramIs?.forEachIndexed { index, element ->
                    queryStr += "s.${element.field} = :${element.field}"

                    if ((index + 1) < req.paramIs!!.size) {
                        queryStr += " AND "
                    }
                }

                queryStr += ")"
            }

            if (!req.paramIn.isNullOrEmpty()) {
                queryStr += " AND ("

                req.paramIn?.forEachIndexed { index, element ->
                    queryStr += "s.${element.field} IN(:${element.field})"

                    if ((index + 1) < req.paramIs!!.size) {
                        queryStr += " AND "
                    }
                }

                queryStr += ")"
            }

            if (!req.paramNotIn.isNullOrEmpty()) {
                queryStr += " AND ("

                req.paramNotIn?.forEachIndexed { index, element ->
                    queryStr += "s.${element.field} NOT IN(:${element.field})"

                    if ((index + 1) < req.paramIs!!.size) {
                        queryStr += " AND "
                    }
                }

                queryStr += ")"
            }

            if (!req.sort.isNullOrEmpty()) {
                queryStr += " ORDER BY "

                req.sort?.forEachIndexed { index, element ->
                    queryStr += "s.${element.field} ${element.direction}"

                    if ((index + 1) < req.sort!!.size) {
                        queryStr += ", "
                    }
                }
            }

            val query: Query = em.createQuery(queryStr, Component::class.java)

            if (byUserComponentCode) {
                query.setParameter("codes", getUserComponetCodes())
            }

            req.paramLike?.forEach {
                query.setParameter(it.field, it.value)
            }

            req.paramIs?.forEach {
                query.setParameter(it.field, it.value)
            }

            req.paramIn?.forEach {
                query.setParameter(it.field, it.value)
            }

            req.paramNotIn?.forEach {
                query.setParameter(it.field, it.value)
            }

            val size: Long = query.resultList.size.toLong()

            if (req.enablePage == true) {
                query.firstResult = req.page!! * req.size!!
                query.maxResults = req.size!!
            }

            val list: List<Component> = query.resultList as List<Component>

            var pageable: Pageable = PageRequest.of(req.page!!, req.size!!)
            if (req.enablePage != true) {
                pageable = Pageable.unpaged()
            }

            return PageableExecutionUtils.getPage(
                list,
                pageable
            ) {
                size
            }
        } catch (e: Exception) {
            throw e
        }
    }

    fun getSubComponent(code: String, year: Int, maxDotLengt: Int): List<Component> {
        return try {
            var queryStr = "select c from $collection c "
            queryStr += "where c.active = true "
            queryStr += "and lower(c.code) like lower(concat('',:code,'%')) "
            queryStr += "and (length(c.code) - length(replace(c.code,'.',''))) = :codeLength "
            queryStr += "and (c.year = :year or c.year is null)"
            queryStr += "order by "

            for (idx in 0..maxDotLengt) {
                val spltIdx = idx + 1
                queryStr += "length(split_part(c.code, '.', $spltIdx)), split_part(c.code, '.', $spltIdx) asc"
                if (idx != maxDotLengt) {
                    queryStr += ", "
                }
            }

            val query: Query = em.createQuery(queryStr, Component::class.java)
            query.setParameter("code", "${code}.")
            query.setParameter("year", year)
            query.setParameter("codeLength", StringUtils.countMatches(code, '.') + 1)

            query.resultList as List<Component>
        } catch (e: Exception) {
            e.printStackTrace()
            emptyList()
        }
    }
}
