package id.go.kemenag.madrasah.pmrms.awp.activity.repository.native

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.EventDocumentType
import org.springframework.stereotype.Repository

@Repository
class EventDocumentTypeRepositoryNative :
    BaseRepositoryNative<EventDocumentType>(EventDocumentType::class.java)
