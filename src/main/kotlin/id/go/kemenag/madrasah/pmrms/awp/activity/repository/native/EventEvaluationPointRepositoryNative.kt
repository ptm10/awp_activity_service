package id.go.kemenag.madrasah.pmrms.awp.activity.repository.native

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.EventEvaluationPoint
import org.springframework.stereotype.Repository

@Repository
class EventEvaluationPointRepositoryNative :
    BaseRepositoryNative<EventEvaluationPoint>(EventEvaluationPoint::class.java)
