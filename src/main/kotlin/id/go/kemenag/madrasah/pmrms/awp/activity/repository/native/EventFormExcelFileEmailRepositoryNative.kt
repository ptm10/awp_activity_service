package id.go.kemenag.madrasah.pmrms.awp.activity.repository.native

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.EventFormExcelFileEmail
import org.springframework.stereotype.Repository

@Repository
class EventFormExcelFileEmailRepositoryNative :
    BaseRepositoryNative<EventFormExcelFileEmail>(EventFormExcelFileEmail::class.java)
