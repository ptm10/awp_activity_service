package id.go.kemenag.madrasah.pmrms.awp.activity.repository.native

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.EventForm
import org.springframework.stereotype.Repository

@Repository
class EventFormRepositoryNative : BaseRepositoryNative<EventForm>(EventForm::class.java)
