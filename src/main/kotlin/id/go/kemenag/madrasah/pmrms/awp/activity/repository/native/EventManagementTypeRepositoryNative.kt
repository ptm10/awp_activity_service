package id.go.kemenag.madrasah.pmrms.awp.activity.repository.native

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.EventManagementType
import org.springframework.stereotype.Repository

@Repository
class EventManagementTypeRepositoryNative : BaseRepositoryNative<EventManagementType>(EventManagementType::class.java)
