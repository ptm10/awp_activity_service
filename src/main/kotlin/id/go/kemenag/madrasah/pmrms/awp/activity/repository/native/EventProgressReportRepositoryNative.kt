package id.go.kemenag.madrasah.pmrms.awp.activity.repository.native

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.EventProgressReport
import org.springframework.stereotype.Repository

@Repository
class EventProgressReportRepositoryNative : BaseRepositoryNative<EventProgressReport>(EventProgressReport::class.java)
