package id.go.kemenag.madrasah.pmrms.awp.activity.repository.native

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.EventQuestionAnswerDetail
import org.springframework.stereotype.Repository

@Repository
class EventQuestionAnswerDetailRepositoryNative :
    BaseRepositoryNative<EventQuestionAnswerDetail>(EventQuestionAnswerDetail::class.java)
