package id.go.kemenag.madrasah.pmrms.awp.activity.repository.native

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.EventQuestionAnswer
import org.springframework.stereotype.Repository

@Repository
class EventQuestionAnswerRepositoryNative : BaseRepositoryNative<EventQuestionAnswer>(EventQuestionAnswer::class.java)
