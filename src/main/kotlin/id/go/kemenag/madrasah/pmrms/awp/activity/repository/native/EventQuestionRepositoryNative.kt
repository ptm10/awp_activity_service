package id.go.kemenag.madrasah.pmrms.awp.activity.repository.native

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.EventQuestion
import org.springframework.stereotype.Repository

@Repository
class EventQuestionRepositoryNative : BaseRepositoryNative<EventQuestion>(EventQuestion::class.java)
