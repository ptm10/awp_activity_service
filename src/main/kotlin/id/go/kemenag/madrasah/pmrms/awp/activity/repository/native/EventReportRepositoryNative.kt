package id.go.kemenag.madrasah.pmrms.awp.activity.repository.native

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.EventReport
import org.springframework.stereotype.Repository

@Repository
class EventReportRepositoryNative : BaseRepositoryNative<EventReport>(EventReport::class.java)
