package id.go.kemenag.madrasah.pmrms.awp.activity.repository.native

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.Event
import org.springframework.stereotype.Repository

@Repository
class EventRepositoryNative : BaseRepositoryNative<Event>(Event::class.java)
