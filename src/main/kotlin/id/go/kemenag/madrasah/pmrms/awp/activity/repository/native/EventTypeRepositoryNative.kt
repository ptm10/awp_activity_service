package id.go.kemenag.madrasah.pmrms.awp.activity.repository.native

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.EventType
import org.springframework.stereotype.Repository

@Repository
class EventTypeRepositoryNative : BaseRepositoryNative<EventType>(EventType::class.java)
