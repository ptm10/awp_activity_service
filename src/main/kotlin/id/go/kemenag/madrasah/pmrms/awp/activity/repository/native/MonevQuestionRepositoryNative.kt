package id.go.kemenag.madrasah.pmrms.awp.activity.repository.native

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.MonevQuestion
import org.springframework.stereotype.Repository

@Repository
class MonevQuestionRepositoryNative : BaseRepositoryNative<MonevQuestion>(MonevQuestion::class.java)
