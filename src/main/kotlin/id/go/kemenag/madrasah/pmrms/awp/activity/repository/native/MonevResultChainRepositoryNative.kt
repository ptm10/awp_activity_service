package id.go.kemenag.madrasah.pmrms.awp.activity.repository.native

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.MonevResultChain
import org.springframework.stereotype.Repository

@Repository
class MonevResultChainRepositoryNative : BaseRepositoryNative<MonevResultChain>(MonevResultChain::class.java)
