package id.go.kemenag.madrasah.pmrms.awp.activity.repository.native

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.MonevSuccessIndicator
import org.springframework.stereotype.Repository

@Repository
class MonevSuccessIndicatorRepositoryNative :
    BaseRepositoryNative<MonevSuccessIndicator>(MonevSuccessIndicator::class.java)
