package id.go.kemenag.madrasah.pmrms.awp.activity.repository.native

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.ParticipanTargetPosition
import org.springframework.stereotype.Repository

@Repository
class ParticipantTargetPositionRepositoryNative : BaseRepositoryNative<ParticipanTargetPosition>(ParticipanTargetPosition::class.java)
