package id.go.kemenag.madrasah.pmrms.awp.activity.repository.native

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.Pok
import org.springframework.stereotype.Repository

@Repository
class PokRepositoryNative : BaseRepositoryNative<Pok>(Pok::class.java)
