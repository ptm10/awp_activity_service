package id.go.kemenag.madrasah.pmrms.awp.activity.repository.native

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.Province
import org.springframework.stereotype.Repository

@Repository
class ProvinceRepositoryNative : BaseRepositoryNative<Province>(Province::class.java)
