package id.go.kemenag.madrasah.pmrms.awp.activity.repository.native

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.QualityAtEntryQuestion
import org.springframework.stereotype.Repository

@Repository
class QualityAtEntryQuestionRepositoryNative :
    BaseRepositoryNative<QualityAtEntryQuestion>(QualityAtEntryQuestion::class.java)
