package id.go.kemenag.madrasah.pmrms.awp.activity.repository.native

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.Regency
import org.springframework.stereotype.Repository

@Repository
class RegencyRepositoryNative : BaseRepositoryNative<Regency>(Regency::class.java)
