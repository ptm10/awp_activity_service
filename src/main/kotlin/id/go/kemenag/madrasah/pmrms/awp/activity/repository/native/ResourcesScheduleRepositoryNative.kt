package id.go.kemenag.madrasah.pmrms.awp.activity.repository.native

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.ResourcesSchedule
import org.springframework.stereotype.Repository

@Repository
class ResourcesScheduleRepositoryNative : BaseRepositoryNative<ResourcesSchedule>(ResourcesSchedule::class.java)
