package id.go.kemenag.madrasah.pmrms.awp.activity.repository.native

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.RikRisk
import org.springframework.stereotype.Repository

@Repository
class RikRiskRepositoryNative : BaseRepositoryNative<RikRisk>(RikRisk::class.java)
