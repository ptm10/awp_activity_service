package id.go.kemenag.madrasah.pmrms.awp.activity.repository.native

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.VMonevContribution
import org.springframework.stereotype.Repository

@Repository
class VMonevContributionRepositoryNative : BaseRepositoryNative<VMonevContribution>(VMonevContribution::class.java)
