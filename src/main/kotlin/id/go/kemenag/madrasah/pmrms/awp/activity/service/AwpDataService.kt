package id.go.kemenag.madrasah.pmrms.awp.activity.service

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import id.go.kemenag.madrasah.pmrms.awp.activity.constant.HEADER_STRING
import id.go.kemenag.madrasah.pmrms.awp.activity.constant.VALIDATOR_MSG_HAS_ADDED
import id.go.kemenag.madrasah.pmrms.awp.activity.constant.VALIDATOR_MSG_NOT_FOUND
import id.go.kemenag.madrasah.pmrms.awp.activity.helpers.*
import id.go.kemenag.madrasah.pmrms.awp.activity.model.ReturnDataFiles
import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.AwpDataRequest
import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.awp.activity.model.response.ErrorMessage
import id.go.kemenag.madrasah.pmrms.awp.activity.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.*
import id.go.kemenag.madrasah.pmrms.awp.activity.repository.*
import id.go.kemenag.madrasah.pmrms.awp.activity.repository.native.AwpDataRepositoryNative
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import java.util.*
import javax.servlet.http.HttpServletRequest

@Suppress("UNCHECKED_CAST")
@Service
class AwpDataService {

    @Autowired
    private lateinit var repo: AwpDataRepository

    @Autowired
    private lateinit var repoPdo: PdoRepository

    @Autowired
    private lateinit var repoIri: IriRepository

    @Autowired
    private lateinit var repoAwpDataPdo: AwpDataPdoRepository

    @Autowired
    private lateinit var repoAwpDataPdoIri: AwpDataPdoIriRepository

    @Autowired
    private lateinit var repoAwpDataFiles: AwpDataFilesRepository

    @Autowired
    private lateinit var repoAwpDataOtherFiles: AwpDataOtherFilesRepository

    @Autowired
    private lateinit var repoNative: AwpDataRepositoryNative

    @Value("\${url.files}")
    private lateinit var filesUrl: String

    @Autowired
    private lateinit var httpServletRequest: HttpServletRequest

    @Autowired
    private lateinit var logActivityService: LogActivityService

    fun datatable(req: Pagination2Request): ResponseEntity<ReturnData> {
        return try {
            responseSuccess(data = repoNative.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }

    fun updateData(year: Int, request: AwpDataRequest): ResponseEntity<ReturnData> {
        val update = repo.findByYearAndActive(year)
        return if (update.isPresent) {
            saveOrUpdate(request, update.get())
        } else {
            responseNotFound()
        }
    }

    fun saveData(req: AwpDataRequest): ResponseEntity<ReturnData> {
        return saveOrUpdate(req)
    }

    fun saveOrUpdate(req: AwpDataRequest, update: AwpData? = null): ResponseEntity<ReturnData> {
        var oldData: AwpData? = null
        if (update != null) {
            val objectMapper = ObjectMapper()
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            oldData = objectMapper.readValue(
                objectMapper.writeValueAsString(update), AwpData::class.java
            ) as AwpData
        }

        try {
            val awpData = validate(req, update)
            val listMessage = awpData["listMessage"] as List<ErrorMessage>
            if (listMessage.isNotEmpty()) {
                return responseBadRequest(data = listMessage)
            }

            val data: AwpData?
            data = update ?: AwpData()
            data.year = req.year
            data.keteranganKontribusiKomponent = req.keteranganKontribusiKomponent
            data.keteranganKemajuanProgram = req.keteranganKemajuanProgram
            data.keteranganKendala = req.keteranganKendala
            data.keteranganPembelajaran = req.keteranganPembelajaran
            data.keteranganRencanaKerja = req.keteranganRencanaKerja
            data.keteranganUsulanKegiatan = req.keteranganUsulanKegiatan
            data.dipaAwalKomponen1 = req.dipaAwalKomponen1
            data.dipaAwalKomponen2 = req.dipaAwalKomponen2
            data.dipaAwalKomponen3 = req.dipaAwalKomponen3
            data.dipaAwalKomponen4 = req.dipaAwalKomponen4
            data.dipaRevisiKomponen1 = req.dipaRevisiKomponen1
            data.dipaRevisiKomponen2 = req.dipaRevisiKomponen2
            data.dipaRevisiKomponen3 = req.dipaRevisiKomponen3
            data.dipaRevisiKomponen4 = req.dipaRevisiKomponen4
            data.dipaRealisasiKomponen1 = req.dipaRealisasiKomponen1
            data.dipaRealisasiKomponen2 = req.dipaRealisasiKomponen2
            data.dipaRealisasiKomponen3 = req.dipaRealisasiKomponen3
            data.dipaRealisasiKomponen4 = req.dipaRealisasiKomponen4
            data.dipaAwalRupiahMurni = req.dipaAwalRupiahMurni
            data.dipaRevisiRupiahMurni = req.dipaRevisiRupiahMurni
            data.dipaRealisasiRupiahMurni = req.dipaRealisasiRupiahMurni
            data.hambatanKomponen1 = req.hambatanKomponen1
            data.hambatanKomponen2 = req.hambatanKomponen2
            data.hambatanKomponen3 = req.hambatanKomponen3
            data.hambatanKomponen4 = req.hambatanKomponen4
            data.fokusKomponen1 = req.fokusKomponen1
            data.fokusKomponen2 = req.fokusKomponen2
            data.fokusKomponen3 = req.fokusKomponen3
            data.fokusKomponen4 = req.fokusKomponen4

            if (update == null) {
                data.createdBy = getUserLogin()?.id
            } else {
                data.updatedAt = Date()
                data.updatedBy = getUserLogin()?.id
            }

            repo.save(data)

            req.pdo.forEach {
                var pdo = AwpDataPdo()
                val checkPdo = repoAwpDataPdo.findByAwpDataIdAndPdoIdAndActive(data.id, it.pdoId)
                if (checkPdo.isPresent) {
                    pdo = checkPdo.get()
                    pdo.updatedAt = Date()
                }

                pdo.awpDataId = data.id
                pdo.pdoId = it.pdoId
                pdo.target = it.target

                repoAwpDataPdo.save(pdo)

                it.iri?.forEach { ir ->
                    var iri = AwpDataPdoIri()
                    val checkIri = repoAwpDataPdoIri.findByAwpDataPdoIdAndIriIdAndActive(pdo.id, ir.iriId)
                    if (checkIri.isPresent) {
                        iri = checkIri.get()
                        iri.updatedAt = Date()
                    }

                    iri.awpDataPdoId = pdo.id
                    iri.iriId = ir.iriId
                    iri.target = ir.target
                    repoAwpDataPdoIri.save(iri)
                }
            }

            req.files.forEach {
                var files = AwpDataFiles()
                val checkFiles = repoAwpDataFiles.findByAwpDataIdAndFileIdAndActive(data.id, it.fileId)
                if (checkFiles.isPresent) {
                    files = checkFiles.get()
                    files.updatedAt = Date()
                }

                files.awpDataId = data.id
                files.fileId = it.fileId
                files.component = it.component
                repoAwpDataFiles.save(files)
            }

            req.deletedFilesIds?.forEach {
                val checkFiles = repoAwpDataFiles.findByIdAndActive(it)
                if (checkFiles.isPresent) {
                    checkFiles.get().updatedAt = Date()
                    checkFiles.get().active = false
                    repoAwpDataFiles.save(checkFiles.get())
                }
            }

            req.otherFiles.forEach {
                var files = AwpDataOtherFiles()
                val checkFiles = repoAwpDataOtherFiles.findByAwpDataIdAndFileIdAndActive(data.id, it.fileId)
                if (checkFiles.isPresent) {
                    files = checkFiles.get()
                    files.updatedAt = Date()
                }

                files.awpDataId = data.id
                files.fileId = it.fileId
                files.key = it.key
                repoAwpDataOtherFiles.save(files)
            }

            req.deletedOtherFilesIds?.forEach {
                val checkFiles = repoAwpDataOtherFiles.findByIdAndActive(it)
                if (checkFiles.isPresent) {
                    checkFiles.get().updatedAt = Date()
                    checkFiles.get().active = false
                    repoAwpDataOtherFiles.save(checkFiles.get())
                }
            }

            return if (update == null) {
                responseCreated(data = data)
            } else {
                val ipAdress = httpServletRequest.getHeader("X-FORWARDED-FOR") ?: httpServletRequest.remoteAddr
                val userId = getUserLogin()?.id ?: ""
                Thread {
                    logActivityService.writeChangeData(
                        data,
                        oldData,
                        "awp_data",
                        ipAdress,
                        userId
                    )
                }.start()

                responseSuccess(data = data)
            }
        } catch (e: Exception) {
            throw e
        }
    }

    private fun validate(request: AwpDataRequest, update: AwpData? = null): Map<String, Any?> {
        val rData = mutableMapOf<String, Any?>()
        try {
            val listMessage: MutableList<ErrorMessage> = emptyList<ErrorMessage>().toMutableList()

            val checkByYear = repo.findByYearAndActive(request.year)
            if (checkByYear.isPresent) {
                val errorMessage = ErrorMessage(
                    "year",
                    "Awp Data Year ${request.year} $VALIDATOR_MSG_HAS_ADDED"
                )

                if (update == null) {
                    listMessage.add(errorMessage)
                } else {
                    if (update.id != checkByYear.get().id) {
                        listMessage.add(errorMessage)
                    }
                }
            }

            request.files.forEach {
                val checkFile: ReturnDataFiles? = RequestHelpers.filesGetById(
                    it.fileId ?: "", filesUrl, httpServletRequest.getHeader(HEADER_STRING)
                )

                var checkedFile: Files? = null
                if (checkFile != null) {
                    if (checkFile.data != null) {
                        checkedFile = checkFile.data
                    }
                }

                if (checkedFile == null) {
                    listMessage.add(
                        ErrorMessage(
                            "files", "files Id ${it.fileId} $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                }
            }

            request.pdo.forEach {
                val checkPdo = repoPdo.findByIdAndActive(it.pdoId)
                if (!checkPdo.isPresent) {
                    listMessage.add(
                        ErrorMessage(
                            "pdo", "Pdo Id ${it.pdoId} $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                }

                it.iri?.forEach { ir ->
                    val checkIri = repoIri.findByIdAndActive(ir.iriId)
                    if (!checkIri.isPresent) {
                        listMessage.add(
                            ErrorMessage(
                                "iri", "Iri Id ${ir.iriId} $VALIDATOR_MSG_NOT_FOUND"
                            )
                        )
                    }
                }
            }

            rData["listMessage"] = listMessage
        } catch (e: Exception) {
            throw e
        }

        return rData
    }

    fun getByYear(year: Int): ResponseEntity<ReturnData> {
        try {
            val responseData: AwpData?
            val data = repo.findByYearAndActive(year)
            if (!data.isPresent) {
                responseData = AwpData()
                responseData.year = year
                repo.save(responseData)

                pdoWithIri().forEach {
                    val awpDataPdo = AwpDataPdo()
                    awpDataPdo.awpDataId = responseData.id
                    awpDataPdo.pdoId = it.id
                    awpDataPdo.pdo = Pdo(
                        it.id,
                        it.name,
                        it.code,
                        it.description,
                        it.y_2020,
                        it.y_2021,
                        it.y_2022,
                        it.y_2023,
                        it.y_2024
                    )

                    repoAwpDataPdo.save(awpDataPdo)

                    it.iri.forEach { ri ->
                        val awpPdoIri = AwpDataPdoIri()
                        awpPdoIri.awpDataPdoId = awpDataPdo.id
                        awpPdoIri.iriId = ri.id
                        awpPdoIri.iri = ri
                        awpDataPdo.iri?.add(awpPdoIri)
                        repoAwpDataPdoIri.save(awpPdoIri)
                    }

                    responseData.pdo?.add(awpDataPdo)
                }
            } else {
                responseData = data.get()
            }

            return responseSuccess(data = responseData)
        } catch (e: Exception) {
            throw e
        }
    }

    private fun pdoWithIri(): List<PdoWithIri> {
        try {
            val datas = mutableListOf<PdoWithIri>()

            repoPdo.getByActiveOrderByCode().sortedBy { s -> s.code }.forEach {
                datas.add(
                    PdoWithIri(
                        id = it.id,
                        code = it.code,
                        name = it.name,
                        iri = repoIri.getByCodeAndActiveOrderByName(it.code).toMutableList()
                    )
                )
            }
            return datas.toList()
        } catch (e: Exception) {
            throw e
        }
    }

    data class PdoWithIri(

        var id: String? = null,

        var code: String? = null,

        var name: String? = null,

        var description: String? = null,

        var y_2020: String? = null,

        var y_2021: String? = null,

        var y_2022: String? = null,

        var y_2023: String? = null,

        var y_2024: String? = null,

        var iri: MutableList<Iri> = emptyList<Iri>().toMutableList()
    )
}
