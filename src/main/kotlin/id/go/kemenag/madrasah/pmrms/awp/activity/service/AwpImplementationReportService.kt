package id.go.kemenag.madrasah.pmrms.awp.activity.service

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import id.go.kemenag.madrasah.pmrms.awp.activity.constant.*
import id.go.kemenag.madrasah.pmrms.awp.activity.exception.UnautorizedException
import id.go.kemenag.madrasah.pmrms.awp.activity.helpers.*
import id.go.kemenag.madrasah.pmrms.awp.activity.model.ReturnDataFiles
import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.*
import id.go.kemenag.madrasah.pmrms.awp.activity.model.response.ErrorMessage
import id.go.kemenag.madrasah.pmrms.awp.activity.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.*
import id.go.kemenag.madrasah.pmrms.awp.activity.repository.*
import id.go.kemenag.madrasah.pmrms.awp.activity.repository.native.AwpImplementationReportRepositoryNative
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import javax.servlet.http.HttpServletRequest

@Suppress("UNCHECKED_CAST")
@Service
class AwpImplementationReportService {

    @Autowired
    private lateinit var repo: AwpImplementationReportRepository

    @Autowired
    private lateinit var repoNative: AwpImplementationReportRepositoryNative

    @Autowired
    private lateinit var repoComponent: ComponentRepository

    @Autowired
    private lateinit var repoAwpImplementation: AwpImplementationRepository

    @Autowired
    private lateinit var repoAwpImplementationReportPok: AwpImplementationReportPokRepository

    @Autowired
    private lateinit var repoAwpImplementationReportProvincesRegencies: AwpImplementationReportProvincesRegenciesRepository

    @Autowired
    private lateinit var repoAwpImplementationReportProvinces: AwpImplementationReportProvincesRepository

    @Autowired
    private lateinit var repoActivityMode: ActivityModeRepository

    @Autowired
    private lateinit var repoResources: ResourcesRepository

    @Autowired
    private lateinit var repoProvince: ProvinceRepository

    @Autowired
    private lateinit var repoRegency: RegencyRepository

    @Autowired
    private lateinit var repoAwpImplementationReportApprovalNote: AwpImplementationReportApprovalNoteRepository

    @Autowired
    private lateinit var repoPok: PokRepository

    @Autowired
    private lateinit var repoEventReport: EventReportRepository

    @Autowired
    private lateinit var repoAwpImplementationReportResultChain: AwpImplementationReportResultChainRepository

    @Autowired
    private lateinit var repoAwpImplementationReportSuccessIndicator: AwpImplementationReportSuccessIndicatorRepository

    @Autowired
    private lateinit var repoAwpImplementationResultChainRepository: AwpImplementationResultChainRepository

    @Autowired
    private lateinit var repoMonevResultChain: MonevResultChainRepository

    @Autowired
    private lateinit var repoMonevSuccessIndicator: MonevSuccessIndicatorRepository

    @Autowired
    private lateinit var serviceTask: TaskService

    @Autowired
    private lateinit var httpServletRequest: HttpServletRequest

    @Autowired
    private lateinit var repoUserRoleViewUserResources: UserRoleViewUserResourcesRepository

    @Autowired
    private lateinit var logActivityService: LogActivityService

    @Value("\${url.files}")
    private lateinit var filesUrl: String

    private var findCoordinatorCounter = 0

    fun datatableAll(req: Pagination2Request): ResponseEntity<ReturnData> {
        return try {
            responseSuccess(data = repoNative.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }

    fun getDetail(id: String): ResponseEntity<ReturnData> {
        try {
            val data = repo.findByIdAndActive(id)
            if (!data.isPresent) {
                return responseNotFound()
            }

            // creator allow edit
            if (data.get().createdBy == getUserLogin()?.id) {
                if (data.get().status == AWP_IMPLEMENTATION_REPORT_STATUS_NEW || data.get().status == AWP_IMPLEMENTATION_REPORT_STATUS_REVISION) {
                    data.get().creatorAllowedEdit = true
                }
            }

            // allow approve
            if (resourcesHasAllRoles(
                    listOf(ROLE_ID_CONSULTAN), data.get().awpImplementation?.projectOfficerResources
                ) || resourcesHasAllRoles(
                    listOf(ROLE_ID_COORDINATOR), data.get().awpImplementation?.projectOfficerResources
                )
            ) {
                if (data.get().awpImplementation?.projectOfficerResources?.userId == getUserLogin()?.id) {
                    if (data.get().status == AWP_IMPLEMENTATION_REPORT_STATUS_NEW) {
                        data.get().allowApprove = true
                    }
                }

                if (!userHasRoles(listOf(ROLE_ID_LSP))) {
                    if (data.get().createdBy == getUserLogin()?.id) {
                        data.get().coordinatorConsultanAllowEdit = true
                    }
                }
            }
            return responseSuccess(data = data)
        } catch (e: Exception) {
            throw e
        }
    }

    fun datatable(req: Pagination2Request): ResponseEntity<ReturnData> {
        return try {
            if (!userHasRoles(listOf(ROLE_ID_ADMINSTRATOR))) {
                if (!userHasRoles(listOf(ROLE_ID_LSP))) {
                    if (getUserLogin()?.component?.code != "4.4") {
                        var componentId: String? = getUserLogin()?.component?.id
                        if (getUserLogin()?.component?.code?.contains("4.") == true) {
                            val findComponent = repoComponent.findByCodeAndActive("4")
                            if (findComponent.isPresent) {
                                componentId = findComponent.get().id
                            }
                        }
                        if (componentId != null) {
                            req.paramIs?.add(ParamSearch("awpImplementation.componentId", "string", componentId))
                        } else {
                            req.paramIs?.add(
                                ParamSearch(
                                    "awpImplementation.componentId", "string", System.currentTimeMillis().toString()
                                )
                            )
                        }
                    }
                } else {
                    req.paramIs?.add(
                        ParamSearch(
                            "awpImplementation.lspPic", "string", getUserLogin()?.id
                        )
                    )
                }
            }
            responseSuccess(data = repoNative.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }

    fun updateData(id: String, request: AwpImplementationReportRequest): ResponseEntity<ReturnData> {
        val update = repo.findByIdAndActive(id)
        return if (update.isPresent) {
            saveOrUpdate(request, update.get())
        } else {
            responseNotFound()
        }
    }

    fun saveData(req: AwpImplementationReportRequest): ResponseEntity<ReturnData> {
        return saveOrUpdate(req)
    }

    fun saveOrUpdate(
        req: AwpImplementationReportRequest, update: AwpImplementationReport? = null
    ): ResponseEntity<ReturnData> {
        var oldData: AwpImplementationReport? = null
        if (update != null) {
            val objectMapper = ObjectMapper()
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            oldData = objectMapper.readValue(
                objectMapper.writeValueAsString(update), AwpImplementationReport::class.java
            ) as AwpImplementationReport
        }

        try {
            if (!userHasRoles(listOf(ROLE_ID_CONSULTAN, ROLE_ID_COORDINATOR, ROLE_ID_LSP))) {
                throw UnautorizedException(message = VALIDATOR_MSG_NOT_HAVE_ACCESS)
            }

            val validate = validate(req, update)
            val listMessage = validate["listMessage"] as List<ErrorMessage>
            if (listMessage.isNotEmpty()) {
                return responseBadRequest(data = listMessage)
            }

            val data: AwpImplementationReport?
            data = update ?: AwpImplementationReport()


            val implementationResultChain: List<AwpImplementationResultChain> =
                repoAwpImplementationResultChainRepository.getByAwpImplementationIdAndActive(req.awpImplementationId)


            val awpImplementation = validate["awpImplementation"] as AwpImplementation

            if (awpImplementation.eventManagementTypeId == EVENT_MANAGEMENT_TYPE_CONTRACT) {
                if (awpImplementation.lspPic != getUserLogin()?.id) {
                    throw UnautorizedException(message = VALIDATOR_MSG_NOT_HAVE_ACCESS)
                }
            } else {
                if (awpImplementation.createdBy != getUserLogin()?.id) {
                    throw UnautorizedException(message = VALIDATOR_MSG_NOT_HAVE_ACCESS)
                }
            }

            data.awpImplementationId = req.awpImplementationId
            data.startDate = validate["startDate"] as Date
            data.endDate = validate["endDate"] as Date
            data.eventOtherInformation = req.eventOtherInformation
            data.eventVolume = req.eventVolume
            data.summary = req.summary
            data.conclusionsAndRecommendations = req.conclusionsAndRecommendations
            data.locationOtherInformation = req.locationOtherInformation
            data.activityModeId = req.activityModeId
            data.participantTarget = req.participantTarget
            data.nameOtherInformation = req.nameOtherInformation
            data.bastFileId = req.bastFileId

            var taskTitle = "Membuat"
            if (update != null) {
                data.updatedBy = getUserLogin()?.id
                data.updatedAt = Date()

                if (data.status == AWP_IMPLEMENTATION_REPORT_STATUS_REVISION) {
                    taskTitle = "Merevisi"

                    serviceTask.approveRejectFromTask(
                        APPROVAL_STATUS_APPROVE,
                        data.id,
                        TASK_TYPE_AWP_IMPLEMENTATION_REPORT,
                        getUserLogin()?.id,
                        TASK_NUMBER_TASK_REVISION
                    )
                } else {
                    data.status = AWP_IMPLEMENTATION_REPORT_STATUS_NEW
                }

                update.pok?.forEach {
                    if (req.deletedPokIds?.contains(it.id) == true) {
                        it.active = false
                        it.updatedAt = Date()
                        repoAwpImplementationReportPok.save(it)
                    }
                }

                update.provincesRegencies?.forEach {
                    if (req.deletedProvincesRegeciesIds?.contains(it.id) == true) {
                        it.active = false
                        it.updatedAt = Date()
                        repoAwpImplementationReportProvinces.save(it)
                    }
                }

                update.monev?.forEach {
                    val filter: List<ImplementationReportMonev>? =
                        req.monevIds?.filter { f -> f.awpReportResultChainId == it.id }

                    val filterResultChain = implementationResultChain.filter { irc ->
                        irc.resultChainId == it.resultChainId
                    }

                    if (filter.isNullOrEmpty()) {
                        it.active = false
                        it.updatedAt = Date()
                        repoAwpImplementationReportResultChain.save(it)
                    }

                    it.successIndicator?.forEach { r ->
                        val filterSuccessIndicator: List<ImplementationReportMonev>? = req.monevIds?.filter { f ->
                            val msi: MonevSuccessIndicatorAwpRequest? = f.awpReportSuccessIndicators?.find { faw ->
                                faw.id == r.id
                            }
                            msi != null
                        }

                        val filterSuccessIndicatorImplementation = filterResultChain.filter { frc ->
                            val filterSuccessIndicatorImplementation = frc.successIndicator?.filter { frcs ->
                                frcs.successIndicatorId == r.successIndicatorId
                            }
                            filterSuccessIndicatorImplementation?.isNotEmpty() == true
                        }

                        if (filterSuccessIndicator.isNullOrEmpty()) {
                            r.active = false
                            r.updatedAt = Date()
                            repoAwpImplementationReportSuccessIndicator.save(r)
                            it.successIndicator?.remove(r)
                        } else {
                            filterSuccessIndicator.forEach { fsi ->
                                fsi.awpReportSuccessIndicators?.forEach { fsia ->
                                    val successIndicator =
                                        repoAwpImplementationReportSuccessIndicator.findByIdAndActive(fsia.id)

                                    if (successIndicator.isPresent) {
                                        successIndicator.get().updatedAt = Date()
                                        successIndicator.get().contribution = fsia.contribution
                                        successIndicator.get().allowDelete =
                                            filterSuccessIndicatorImplementation.isEmpty()
                                        repoAwpImplementationReportSuccessIndicator.save(successIndicator.get())
                                    }
                                }
                            }

                        }
                    }
                }
            } else {
                data.createdBy = getUserLogin()?.id
            }

            repo.save(data)

            req.newPokIds?.forEach {
                data.pok?.add(
                    repoAwpImplementationReportPok.save(
                        AwpImplementationReportPok(
                            awpImplementationReportId = data.id, pokId = it
                        )
                    )
                )
            }

            req.newProvincesRegencies?.forEach {
                val awpProvinces = repoAwpImplementationReportProvinces.save(
                    AwpImplementationReportProvinces(
                        awpImplementationReportId = data.id, provinceId = it.provinceId
                    )
                )
                data.provincesRegencies?.add(awpProvinces)

                it.regencyIds?.forEach { r ->
                    awpProvinces.regencies?.add(
                        repoAwpImplementationReportProvincesRegencies.save(
                            AwpImplementationReportProvincesRegencies(
                                awpImplementationReportProvincesId = awpProvinces.id, regencyId = r
                            )
                        )
                    )
                }
            }

            req.rraEventReports?.forEach { r ->
                val eventReport = repoEventReport.findByIdAndActive(r.eventReportId)
                if (eventReport.isPresent) {
                    eventReport.get().rraFileId = r.rraFileId
                    eventReport.get().updatedAt = Date()
                    repoEventReport.save(eventReport.get())
                }
            }

            req.newMonev?.forEach {
                val filterResultChain = implementationResultChain.filter { arc ->
                    arc.resultChainId == it.resultChainId
                }

                val awpResultChain: AwpImplementationReportResultChain?

                val checkAwpResultChain =
                    repoAwpImplementationReportResultChain.findByAwpImplementationReportIdAndResultChainIdAndActive(
                        data.id,
                        it.resultChainId
                    )

                awpResultChain = if (checkAwpResultChain.isPresent) {
                    checkAwpResultChain.get()
                } else {
                    AwpImplementationReportResultChain()
                }

                awpResultChain.awpImplementationReportId = data.id
                awpResultChain.resultChainId = it.resultChainId
                awpResultChain.allowDelete = filterResultChain.isEmpty()
                repoAwpImplementationReportResultChain.save(awpResultChain)

                if (data.monev?.contains(awpResultChain) == false) {
                    data.monev?.add(awpResultChain)
                }

                it.successIndicators?.forEach { si ->
                    val filterSuccessIndicatorImplementation = filterResultChain.filter { frc ->
                        val filterSuccessIndicatorImplementation = frc.successIndicator?.filter { frcs ->
                            frcs.successIndicatorId == si.id
                        }

                        filterSuccessIndicatorImplementation?.isNotEmpty() == true
                    }

                    repoAwpImplementationReportSuccessIndicator.save(
                        AwpImplementationReportSuccessIndicator(
                            awpImplementationReportResultChainId = awpResultChain.id,
                            successIndicatorId = si.id,
                            contribution = si.contribution,
                            allowDelete = filterSuccessIndicatorImplementation.isEmpty()
                        )
                    )
                }
            }

            if (userHasRoles(listOf(ROLE_ID_LSP))) {
                val taskDescription = "$taskTitle Laporan Kegiatan ${awpImplementation.name}"

                var approvalUser = validate["coordinatorId"] as String?
                if (data.awpImplementation?.eventManagementTypeId == EVENT_MANAGEMENT_TYPE_CONTRACT) {
                    approvalUser = data.awpImplementation?.createdBy
                }

                serviceTask.createOrUpdateFromTask(
                    createdBy = getUserLogin()?.id,
                    taskId = data.id,
                    taskType = TASK_TYPE_AWP_IMPLEMENTATION_REPORT,
                    description = taskDescription,
                    createdFor = approvalUser
                )
            } else {
                data.status = AWP_IMPLEMENTATION_REPORT_STATUS_APPROVED
                data.awpImplementation?.status = AWP_IMPLEMENTATION_STATUS_DONE
                repo.save(data)

                if (update == null) {
                    sendNotifApprovalUser(data)
                }
            }

            if (data.status == AWP_IMPLEMENTATION_REPORT_STATUS_REVISION && oldData != null) {
                if (compareDataChange(oldData, data)) {
                    data.status = AWP_IMPLEMENTATION_REPORT_STATUS_NEW
                    repo.save(data)
                }
            }

            return if (update == null) {
                responseCreated(data = data)
            } else {
                val ipAdress = httpServletRequest.getHeader("X-FORWARDED-FOR") ?: httpServletRequest.remoteAddr
                val userId = getUserLogin()?.id ?: ""
                Thread {
                    logActivityService.writeChangeData(
                        data,
                        oldData,
                        "awp_implementation_report",
                        ipAdress,
                        userId
                    )
                }.start()

                responseSuccess(data = data)
            }
        } catch (e: Exception) {
            throw e
        }
    }

    fun approve(request: AwpImplementationReportApproveRequest): ResponseEntity<ReturnData> {
        try {
            if (!userHasRoles(listOf(ROLE_ID_CONSULTAN, ROLE_ID_COORDINATOR))) {
                throw UnautorizedException(message = VALIDATOR_MSG_NOT_HAVE_ACCESS)
            }

            val find = repo.findByIdAndActive(request.id)
            if (!find.isPresent) {
                return responseNotFound()
            }

            val implementationResultChain: List<AwpImplementationResultChain> =
                repoAwpImplementationResultChainRepository.getByAwpImplementationIdAndActive(find.get().awpImplementationId)

            val validate = validateApproveConsultan(find.get(), request)
            val listMessage = validate["listMessage"] as List<ErrorMessage>
            if (listMessage.isNotEmpty()) {
                return responseBadRequest(data = listMessage)
            }

            var status = AWP_IMPLEMENTATION_REPORT_STATUS_APPROVED
            if (request.approveStatus == APPROVAL_STATUS_REJECT) {
                status = AWP_IMPLEMENTATION_REPORT_STATUS_REVISION

                serviceTask.createRevisionTask(
                    taskId = find.get().id,
                    taskType = TASK_TYPE_AWP_IMPLEMENTATION_REPORT,
                    createdFor = getUserLogin()?.id,
                    description = "Revisi Laporan Kegiatan ${find.get().awpImplementation?.name}"
                )
            }

            find.get().status = status
            if (status == AWP_IMPLEMENTATION_REPORT_STATUS_APPROVED) {
                find.get().awpImplementation?.status = AWP_IMPLEMENTATION_STATUS_DONE
            }
            find.get().updatedBy = getUserLogin()?.id
            find.get().updatedAt = Date()

            repo.save(find.get())

            request.newMonev?.forEach {
                val filterResultChain = implementationResultChain.filter { arc ->
                    arc.resultChainId == it.resultChainId
                }

                val awpResultChain: AwpImplementationReportResultChain?

                val checkAwpResultChain =
                    repoAwpImplementationReportResultChain.findByAwpImplementationReportIdAndResultChainIdAndActive(
                        find.get().id,
                        it.resultChainId
                    )

                awpResultChain = if (checkAwpResultChain.isPresent) {
                    checkAwpResultChain.get()
                } else {
                    AwpImplementationReportResultChain()
                }

                awpResultChain.resultChainId = it.resultChainId
                awpResultChain.awpImplementationReportId = find.get().id
                awpResultChain.allowDelete = filterResultChain.isEmpty()
                repoAwpImplementationReportResultChain.save(awpResultChain)

                it.successIndicators?.forEach { si ->
                    val filterSuccessIndicatorImplementation = filterResultChain.filter { frc ->
                        val filterSuccessIndicatorImplementation = frc.successIndicator?.filter { frcs ->
                            frcs.successIndicatorId == si.id
                        }

                        filterSuccessIndicatorImplementation?.isNotEmpty() == true
                    }

                    repoAwpImplementationReportSuccessIndicator.save(
                        AwpImplementationReportSuccessIndicator(
                            awpImplementationReportResultChainId = awpResultChain.id,
                            successIndicatorId = si.id,
                            contribution = si.contribution,
                            allowDelete = filterSuccessIndicatorImplementation.isEmpty()
                        )
                    )
                }
            }

            repoAwpImplementationReportApprovalNote.save(
                AwpImplementationReportApprovalNote(
                    awpImplementationReportId = find.get().id,
                    userId = getUserLogin()?.id,
                    createdFrom = AWP_IMPLEMENTATION_REPORT_APPROVAL_MESSAGE_FROM_CONSULTAN,
                    note = request.note,
                    approvalStatus = request.approveStatus,
                )
            )

            serviceTask.approveRejectFromTask(
                APPROVAL_STATUS_APPROVE,
                find.get().id,
                TASK_TYPE_AWP_IMPLEMENTATION_REPORT,
                getUserLogin()?.id,
                TASK_NUMBER_TASK_REVISION
            )

            serviceTask.approveRejectFromTask(
                request.approveStatus,
                find.get().id,
                TASK_TYPE_AWP_IMPLEMENTATION_REPORT,
                getUserLogin()?.id,
                TASK_NUMBER_TASK
            )

            if (request.approveStatus == APPROVAL_STATUS_APPROVE) {
                sendNotifApprovalUser(find.get())
            }

            return responseSuccess()
        } catch (e: Exception) {
            throw e
        }
    }

    private fun validate(
        request: AwpImplementationReportRequest, update: AwpImplementationReport? = null
    ): Map<String, Any?> {
        val rData = mutableMapOf<String, Any?>()
        try {
            val listMessage: MutableList<ErrorMessage> = emptyList<ErrorMessage>().toMutableList()

            val awpImplementation = repoAwpImplementation.findByIdAndActive(request.awpImplementationId)
            if (!awpImplementation.isPresent) {
                listMessage.add(
                    ErrorMessage(
                        "awpImplementation",
                        "Awp Impementation id ${request.awpImplementationId} $VALIDATOR_MSG_NOT_FOUND"
                    )
                )
            } else {
                rData["awpImplementation"] = awpImplementation.get()
            }

            var dateStart: Date? = null
            var dateEnd: Date? = null
            val dform: DateFormat = SimpleDateFormat("yyyy-MM-dd")

            try {
                dateStart = dform.parse(request.startDate)
            } catch (_: Exception) {
                listMessage.add(
                    ErrorMessage(
                        "startDate", "Tanggal Mulai $VALIDATOR_MSG_NOT_VALID"
                    )
                )
            }

            try {
                dateEnd = dform.parse(request.endDate)
            } catch (_: Exception) {
                listMessage.add(
                    ErrorMessage(
                        "endDate", "Tanggal Selesai $VALIDATOR_MSG_NOT_VALID"
                    )
                )
            }

            if (dateEnd?.before(dateStart) == true) {
                listMessage.add(
                    ErrorMessage(
                        "endDate", "Tanggal Selesai $VALIDATOR_MSG_NOT_VALID"
                    )
                )
            }

            rData["startDate"] = dateStart as Date
            rData["endDate"] = dateEnd as Date

            if (!request.activityModeId.isNullOrEmpty()) {
                val activityMode = repoActivityMode.findByIdAndActive(request.activityModeId)
                if (!activityMode.isPresent) {
                    listMessage.add(
                        ErrorMessage(
                            "activityModeId", "Activity Mode id ${request.activityModeId} $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                }
            }

            if (awpImplementation.isPresent) {
                findCoordinatorCounter = 0
                val coordinatorId = findCoordinator(awpImplementation.get().projectOfficer)
                if (coordinatorId.isNullOrEmpty()) {
                    listMessage.add(
                        ErrorMessage(
                            "projectOfficer", "Koordinator Project Officer $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                }

                rData["coordinatorId"] = coordinatorId
            }

            request.newPokIds?.forEach {
                val checkPok = repoPok.findByIdAndActive(it)
                if (!checkPok.isPresent) {
                    listMessage.add(
                        ErrorMessage("newPokIds", "Pok id $it $VALIDATOR_MSG_NOT_FOUND")
                    )
                }
            }

            request.newProvincesRegencies?.forEach {
                val checkProvince = repoProvince.findByIdAndActive(it.provinceId)
                if (!checkProvince.isPresent) {
                    listMessage.add(
                        ErrorMessage(
                            "newProvincesRegencies", "Province id ${it.provinceId} $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                }

                it.regencyIds?.forEach { r ->
                    val checkRegency = repoRegency.findByIdAndActive(r)
                    if (!checkRegency.isPresent) {
                        listMessage.add(
                            ErrorMessage(
                                "newProvincesRegencies", "Regency id $r $VALIDATOR_MSG_NOT_FOUND"
                            )
                        )
                    }
                }
            }

            request.rraEventReports?.forEach { r ->
                if (!r.rraFileId.isNullOrEmpty()) {
                    val checkFile: ReturnDataFiles? = RequestHelpers.filesGetById(
                        r.rraFileId ?: "", filesUrl, httpServletRequest.getHeader(HEADER_STRING)
                    )

                    var checkedFile: Files? = null
                    if (checkFile != null) {
                        if (checkFile.data != null) {
                            checkedFile = checkFile.data
                        }
                    }

                    if (checkedFile == null) {
                        listMessage.add(
                            ErrorMessage(
                                "rraEventReports", "Rra File Id ${r.rraFileId} $VALIDATOR_MSG_NOT_FOUND"
                            )
                        )
                    }
                }
            }

            request.newMonev?.forEach {
                val checkResultChain = repoMonevResultChain.findByIdAndActive(it.resultChainId)
                if (!checkResultChain.isPresent) {
                    listMessage.add(
                        ErrorMessage(
                            "newMonev", "Monev Result Chain id ${it.resultChainId} $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                }

                it.successIndicators?.forEach { r ->
                    val checkSuccessIndicator = repoMonevSuccessIndicator.findByIdAndActive(r.id)
                    if (!checkSuccessIndicator.isPresent) {
                        listMessage.add(
                            ErrorMessage(
                                "newMonev", "Monev Success Indicator id ${r.id} $VALIDATOR_MSG_NOT_FOUND"
                            )
                        )
                    }
                }
            }

            if (!request.bastFileId.isNullOrEmpty()) {
                val checkFile: ReturnDataFiles? = RequestHelpers.filesGetById(
                    request.bastFileId ?: "", filesUrl, httpServletRequest.getHeader(HEADER_STRING)
                )

                var checkedFile: Files? = null
                if (checkFile != null) {
                    if (checkFile.data != null) {
                        checkedFile = checkFile.data
                    }
                }

                if (checkedFile == null) {
                    listMessage.add(
                        ErrorMessage(
                            "bastFileId", "Rab File Id ${request.bastFileId} $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                }
            }

            rData["listMessage"] = listMessage
        } catch (e: Exception) {
            throw e
        }

        return rData
    }

    private fun sendNotifApprovalUser(data: AwpImplementationReport) {
        try {
            val usersNotif = mutableListOf<String>()

            if (resourcesHasAllRoles(listOf(ROLE_ID_CONSULTAN), data.awpImplementation?.projectOfficerResources)) {
                findCoordinatorCounter = 0
                val coordinatorId = findCoordinator(data.awpImplementation?.projectOfficerResources?.supervisiorId)
                if (!coordinatorId.isNullOrEmpty()) {
                    usersNotif.add(coordinatorId)
                }
            }

            // user treasurer
            usersNotif.addAll(
                repoUserRoleViewUserResources.getUserIdsByRoleIdsComponentIdPosition(
                    listOf(ROLE_ID_COORDINATOR, ROLE_ID_ADMINSTRATOR), 2, COMPONENT_ID_PMU, POSITION_TREASURER_PMU
                )
            )

            // user pmu
            usersNotif.addAll(
                repoUserRoleViewUserResources.getPmuHead(
                    listOf(ROLE_ID_COORDINATOR, ROLE_ID_ADMINSTRATOR), 2, COMPONENT_ID_PMU
                )
            )

            usersNotif.forEach {
                serviceTask.createTaskNotification(
                    taskId = data.id,
                    taskType = TASK_TYPE_AWP_IMPLEMENTATION_REPORT,
                    createdBy = getUserLogin()?.id,
                    createdFor = it,
                    description = "Laporan Kegiatan ${data.awpImplementation?.name ?: " - "}"
                )
            }
        } catch (e: Exception) {
            throw e
        }
    }

    private fun findCoordinator(supervisiorId: String?): String? {
        findCoordinatorCounter++
        try {
            if (findCoordinatorCounter > APP_FIND_COORDINATOR_LIMIT) {
                return null
            }

            val findResources = repoResources.findByIdAndActive(supervisiorId)
            if (findResources.isPresent) {
                val filterRole = findResources.get().user?.roles?.filter {
                    it.roleId == ROLE_ID_COORDINATOR
                }
                return if (!filterRole.isNullOrEmpty()) {
                    findResources.get().userId
                } else {
                    if (!findResources.get().supervisiorId.isNullOrEmpty()) {
                        findCoordinator(findResources.get().supervisiorId)
                    } else {
                        null
                    }
                }
            } else {
                return null
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return null
    }

    private fun validateApproveConsultan(
        data: AwpImplementationReport,
        request: AwpImplementationReportApproveRequest
    ): Map<String, Any> {
        val rData = mutableMapOf<String, Any>()
        try {
            val listMessage: MutableList<ErrorMessage> = emptyList<ErrorMessage>().toMutableList()

            findCoordinatorCounter = 0
            val coordinatorId = findCoordinator(data.awpImplementation?.projectOfficer)
            if (coordinatorId.isNullOrEmpty()) {
                listMessage.add(
                    ErrorMessage(
                        "projectOfficer", "Koordinator Project Officer $VALIDATOR_MSG_NOT_FOUND"
                    )
                )
            }

            if (coordinatorId == null) {
                listMessage.add(
                    ErrorMessage(
                        "approveStatus", "User $POSITION_COORDINATOR $VALIDATOR_MSG_NOT_FOUND"
                    )
                )
            } else {
                rData["userCoordinator"] = coordinatorId
            }

            request.newMonev?.forEach {
                val checkResultChain = repoMonevResultChain.findByIdAndActive(it.resultChainId)
                if (!checkResultChain.isPresent) {
                    listMessage.add(
                        ErrorMessage(
                            "newMonev", "Monev Result Chain id ${it.resultChainId} $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                }

                it.successIndicators?.forEach { r ->
                    val checkSuccessIndicator = repoMonevSuccessIndicator.findByIdAndActive(r.id)
                    if (!checkSuccessIndicator.isPresent) {
                        listMessage.add(
                            ErrorMessage(
                                "newMonev", "Monev Success Indicator id ${r.id} $VALIDATOR_MSG_NOT_FOUND"
                            )
                        )
                    }
                }
            }

            rData["listMessage"] = listMessage
        } catch (e: Exception) {
            throw e
        }

        return rData
    }
}

