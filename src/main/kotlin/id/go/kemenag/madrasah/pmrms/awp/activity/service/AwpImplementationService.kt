package id.go.kemenag.madrasah.pmrms.awp.activity.service

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.lowagie.text.DocumentException
import id.go.kemenag.madrasah.pmrms.awp.activity.constant.*
import id.go.kemenag.madrasah.pmrms.awp.activity.exception.UnautorizedException
import id.go.kemenag.madrasah.pmrms.awp.activity.helpers.*
import id.go.kemenag.madrasah.pmrms.awp.activity.model.ReturnDataFiles
import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.*
import id.go.kemenag.madrasah.pmrms.awp.activity.model.response.ErrorMessage
import id.go.kemenag.madrasah.pmrms.awp.activity.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.*
import id.go.kemenag.madrasah.pmrms.awp.activity.repository.*
import id.go.kemenag.madrasah.pmrms.awp.activity.repository.native.AwpImplementationRepositoryNative
import id.go.kemenag.madrasah.pmrms.awp.activity.repository.native.AwpImplementationWithoutAwpRepositoryNative
import id.go.kemenag.madrasah.pmrms.awp.activity.repository.native.ComponentRepositoryNative
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.io.ClassPathResource
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import org.thymeleaf.context.Context
import org.thymeleaf.spring5.SpringTemplateEngine
import org.xhtmlrenderer.pdf.ITextRenderer
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Suppress("UNCHECKED_CAST")
@Service
class AwpImplementationService {

    @Autowired
    private lateinit var repo: AwpImplementationRepository

    @Autowired
    private lateinit var repoNative: AwpImplementationRepositoryNative

    @Autowired
    private lateinit var repoNativeWithoutAwp: AwpImplementationWithoutAwpRepositoryNative

    @Autowired
    private lateinit var templateEngine: SpringTemplateEngine

    @Autowired
    private lateinit var response: HttpServletResponse

    @Autowired
    private lateinit var repoComponent: ComponentRepository

    @Autowired
    private lateinit var repoComponentNative: ComponentRepositoryNative

    @Autowired
    private lateinit var repoEventManagementType: EventManagementTypeRepository

    @Autowired
    private lateinit var repoEventType: EventTypeRepository

    @Autowired
    private lateinit var repoProvince: ProvinceRepository

    @Autowired
    private lateinit var repoRegency: RegencyRepository

    @Autowired
    private lateinit var repoIri: IriRepository

    @Autowired
    private lateinit var repoPdo: PdoRepository

    @Autowired
    private lateinit var repoAwpIri: AwpImplementationIriRepository

    @Autowired
    private lateinit var repoAwpPdo: AwpImplementationPdoRepository

    @Autowired
    private lateinit var repoResources: ResourcesRepository

    @Autowired
    private lateinit var repoAwpProvinces: AwpImplementationProvincesRepository

    @Autowired
    private lateinit var repoAwpProvincesRegencies: AwpImplementationProvincesRegenciesRepository

    @Autowired
    private lateinit var repoQualityAtEntryQuestion: QualityAtEntryQuestionRepository

    @Autowired
    private lateinit var repoAwpImplementationQualityAtEntryAnswerRepository: AwpImplementationQualityAtEntryAnswerRepository

    @Autowired
    private lateinit var repoUserRoleViewUserResources: UserRoleViewUserResourcesRepository

    @Autowired
    private lateinit var repoAwpImplementationApprovalNote: AwpImplementationApprovalNoteRepository

    @Autowired
    private lateinit var repoAwpPok: AwpImplementationPokRepository

    @Autowired
    private lateinit var repoPok: PokRepository

    @Autowired
    private lateinit var repoUser: UserRepository

    @Autowired
    private lateinit var serviceTask: TaskService

    @Autowired
    private lateinit var httpServletRequest: HttpServletRequest

    @Autowired
    private lateinit var repoActivityMode: ActivityModeRepository

    @Autowired
    private lateinit var repoAwpImplementationReport: AwpImplementationReportRepository

    @Autowired
    private lateinit var repoMonevResultChain: MonevResultChainRepository

    @Autowired
    private lateinit var repoMonevSuccessIndicator: MonevSuccessIndicatorRepository

    @Autowired
    private lateinit var repoAwpImplementationResultChain: AwpImplementationResultChainRepository

    @Autowired
    private lateinit var repoAwpImplementationSuccessIndicator: AwpImplementationSuccessIndicatorRepository

    @Autowired
    private lateinit var repoEvent: EventRepository

    @Autowired
    private lateinit var repoEventReport: EventReportRepository

    @Autowired
    private lateinit var repoEventStaff: EventStaffRepository

    @Autowired
    private lateinit var repoRikRisk: RikRiskRepository

    @Autowired
    private lateinit var serviceEvent: EventService

    @Autowired
    private lateinit var logActivityService: LogActivityService

    @Value("\${url.files}")
    private lateinit var filesUrl: String

    @Value("\${directories.pdf}")
    private lateinit var dirPdf: String

    private val pdfResources = "/static/"

    private var findCoordinatorCounter = 0

    fun datatableAll(req: Pagination2Request): ResponseEntity<ReturnData> {
        return try {
            responseSuccess(data = repoNative.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }

    fun datatable(req: Pagination2Request): ResponseEntity<ReturnData> {
        return try {
            if (!userHasRoles(listOf(ROLE_ID_ADMINSTRATOR))) {
                if (!userHasRoles(listOf(ROLE_ID_LSP))) {
                    if (getUserLogin()?.component?.code != "4.4") {
                        var componentId: String? = getUserLogin()?.component?.id
                        if (getUserLogin()?.component?.code?.contains("4.") == true) {
                            val findComponent = repoComponent.findByCodeAndActive("4")
                            if (findComponent.isPresent) {
                                componentId = findComponent.get().id
                            }
                        }
                        if (componentId != null) {
                            req.paramIs?.add(ParamSearch("componentId", "string", componentId))
                        } else {
                            req.paramIs?.add(
                                ParamSearch(
                                    "componentId", "string", System.currentTimeMillis().toString()
                                )
                            )
                        }
                    }
                } else {
                    req.paramIs?.add(
                        ParamSearch(
                            "lspPic", "string", getUserLogin()?.id
                        )
                    )
                }
            }

            repoNative.setOrderByStrLength(
                listOf("componentCode.code")
            )

            responseSuccess(data = repoNative.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }

    fun datatableConceptNote(req: Pagination2Request): ResponseEntity<ReturnData> {
        return try {
            if (userHasRoles(listOf(ROLE_ID_CONSULTAN, ROLE_ID_COORDINATOR))) {
                req.paramIs?.add(ParamSearch("projectOfficer", "string", getUserLogin()?.resourcesId))
                req.paramIs?.add(ParamSearch("status", "int", AWP_IMPLEMENTATION_STATUS_IMPLEMENTATION.toString()))
                req.paramIs?.add(ParamSearch("eventManagementTypeId", "string", EVENT_MANAGEMENT_TYPE_SWAKELOLA))
            } else {
                req.paramIs?.add(ParamSearch("projectOfficer", "string", System.currentTimeMillis().toString()))
                req.paramIs?.add(ParamSearch("eventManagementTypeId", "string", EVENT_MANAGEMENT_TYPE_SWAKELOLA))
            }

            responseSuccess(data = repoNative.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }

    fun datatableLspChoice(req: Pagination2Request): ResponseEntity<ReturnData> {
        return try {
            val searchCompoennt: ParamSearch? = req.paramIs?.find {
                it.field == "componentId"
            }

            req.paramIn?.add(
                ParamArray(
                    "id",
                    "string",
                    repo.getIdsLspChoice(searchCompoennt?.value ?: System.currentTimeMillis().toString())
                )
            )

            req.paramIs?.remove(searchCompoennt)

            responseSuccess(data = repoNative.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }

    fun getDetail(id: String): ResponseEntity<ReturnData> {
        try {
            val data = repo.findByIdAndActive(id)
            if (!data.isPresent) {
                return responseNotFound()
            }

            // creator allow edit
            if (data.get().createdBy == getUserLogin()?.id) {
                if (data.get().status == AWP_IMPLEMENTATION_STATUS_PLANNING || data.get().status == AWP_IMPLEMENTATION_STATUS_PLANNING_REVISION) {
                    data.get().creatorAllowedEdit = true
                }
            }

            if (resourcesHasAllRoles(listOf(ROLE_ID_CONSULTAN), data.get().projectOfficerResources)) {
                // approve by coordinator
                findCoordinatorCounter = 0
                val coordinatorId = findCoordinator(data.get().projectOfficerResources?.supervisiorId)
                if (getUserLogin()?.id == coordinatorId) {
                    if (data.get().status == AWP_IMPLEMENTATION_STATUS_PLANNING || data.get().status == AWP_IMPLEMENTATION_STATUS_EVELUATION_REVISION) {
                        data.get().coordinatorAllowedApprove = true
                    }
                }
            }

            // approve by pmu
            val usersPmu = repoUserRoleViewUserResources.getPmuHead(
                listOf(ROLE_ID_COORDINATOR, ROLE_ID_ADMINSTRATOR), 2, COMPONENT_ID_PMU
            )
            if (usersPmu.contains(getUserLogin()?.id)) {
                if (resourcesHasAllRoles(listOf(ROLE_ID_CONSULTAN), data.get().projectOfficerResources)) {
                    if (data.get().status == AWP_IMPLEMENTATION_STATUS_EVALUATED) {
                        data.get().pmuAllowedApprove = true
                    }
                }

                if (resourcesHasAllRoles(listOf(ROLE_ID_COORDINATOR), data.get().projectOfficerResources)) {
                    if (data.get().status == AWP_IMPLEMENTATION_STATUS_PLANNING) {
                        data.get().pmuAllowedApprove = true
                    }
                }
            }


            if (data.get().status == AWP_IMPLEMENTATION_STATUS_IMPLEMENTATION) {
                val dform: DateFormat = SimpleDateFormat("yyyy-MM-dd")
                val now: Date = dform.parse(dform.format(Date()))
                val endDate: Date = dform.parse(dform.format(data.get().endDate))

                var createdReport = false
                val checkReport = repoAwpImplementationReport.getByAwpImplementationIdAndActive(data.get().id)
                if (checkReport.isNotEmpty()) {
                    createdReport = true
                }

                if (data.get().eventManagementTypeId == EVENT_MANAGEMENT_TYPE_CONTRACT) {
                    val repots =
                        repoEventReport.countByEventIdInAndActive(repoEvent.getIdsByAwpImplementationIdActive(data.get().id))
                    val eventVolume = data.get().eventVolume ?: 0

                    if (repots < eventVolume) {
                        // lsp allow create event report lsp
                        if (data.get().lspPic == getUserLogin()?.id) {
                            if (now >= data.get().startDate && now <= data.get().endDate) {
                                data.get().lspAllowCreateEventReport = true
                            }

                            if (now > data.get().endDate && !createdReport) {
                                data.get().lspAllowCreateReport = true

                                if (daysBetweenDates(now, endDate) > APP_LSP_ALLOW_CREATE_REPORT_DAYS_LIMIT) {
                                    data.get().lspAllowCreateReport = false
                                }
                            }
                        }
                    }
                } else {
                    if (now > data.get().endDate && !createdReport) {
                        if (data.get().createdBy == getUserLogin()?.id) {
                            data.get().creatorAllowedCreateReport = true
                        }
                    }
                }

                // coordintaor consultan allowed ctreate concept note
                if (userHasRoles(listOf(ROLE_ID_CONSULTAN, ROLE_ID_COORDINATOR))) {
                    if (data.get().projectOfficer == getUserLogin()?.id) {
                        data.get().coordinatorConsultanAllowedCtreateConceptNote = true
                    }
                }
            }


            return responseSuccess(data = data)
        } catch (e: Exception) {
            throw e
        }
    }

    fun downloadPdf(id: String?) {
        try {
            val findData = repo.findByIdAndActive(id)
            if (!findData.isPresent) {
                response.sendError(HttpServletResponse.SC_NOT_FOUND)
            } else {
                val file: Path = Paths.get(generatePdf(findData.get())!!.absolutePath)
                if (Files.exists(file)) {
                    response.contentType = "application/pdf"
                    response.addHeader(
                        "Content-Disposition", "attachment; filename=" + file.fileName
                    )
                    Files.copy(file, response.outputStream)
                    response.outputStream.flush()
                }
            }
        } catch (ex: DocumentException) {
            ex.printStackTrace()
        } catch (ex: IOException) {
            ex.printStackTrace()
        }
    }

    fun updateData(id: String, request: AwpImplementationRequest): ResponseEntity<ReturnData> {
        val update = repo.findByIdAndActive(id)
        return if (update.isPresent) {
            saveOrUpdate(request, update.get())
        } else {
            responseNotFound()
        }
    }

    fun saveOrUpdate(req: AwpImplementationRequest, update: AwpImplementation? = null): ResponseEntity<ReturnData> {
        var oldData: AwpImplementation? = null
        if (update != null) {
            val objectMapper = ObjectMapper()
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            oldData = objectMapper.readValue(
                objectMapper.writeValueAsString(update), AwpImplementation::class.java
            ) as AwpImplementation
        }

        try {
            if (!userHasRoles(listOf(ROLE_ID_CONSULTAN, ROLE_ID_COORDINATOR))) {
                throw UnautorizedException(message = VALIDATOR_MSG_NOT_HAVE_ACCESS)
            }

            val validate = validate(req, update)
            val listMessage = validate["listMessage"] as List<ErrorMessage>
            if (listMessage.isNotEmpty()) {
                return responseBadRequest(data = listMessage)
            }

            val data: AwpImplementation?
            data = update ?: AwpImplementation()

            data.componentId = req.componentId
            data.subComponentId = req.subComponentId
            data.subSubComponentId = req.subSubComponentId
            data.name = req.name?.trim()
            data.nameOtherInformation = req.nameOtherInformation
            data.locationOtherInformation = req.locationOtherInformation
            data.componentCodeId = validate["componentCodeId"] as String
            data.budgetPok = req.budgetPok
            data.budgetAwp = req.budgetAwp

            if (data.projectOfficer.isNullOrEmpty()) { // new planning
                data.createdBy = getUserLogin()?.id

                repoQualityAtEntryQuestion.getByActiveOrderByNumber().forEach {
                    val awpImplementationQualityAtEntryAnswer = AwpImplementationQualityAtEntryAnswer()
                    awpImplementationQualityAtEntryAnswer.awpImplementationId = data.id
                    awpImplementationQualityAtEntryAnswer.qualityAtEntryQuestionId = it.id
                    awpImplementationQualityAtEntryAnswer.answer = null
                    awpImplementationQualityAtEntryAnswer.revision = null
                    repoAwpImplementationQualityAtEntryAnswerRepository.save(awpImplementationQualityAtEntryAnswer)
                }
            }

            data.projectOfficer = validate["projectOfficer"] as String
            data.lspContractNumber = req.lspContractNumber
            data.lspContractData = req.lspContractData
            data.lspPic = req.lspPic
            data.purpose = req.purpose
            data.description = req.description
            data.eventManagementTypeId = req.eventManagementTypeId
            data.activityModeId = req.activityModeId
            data.startDate = validate["startDate"] as Date
            data.endDate = validate["endDate"] as Date
            data.eventOtherInformation = req.eventOtherInformation
            data.eventVolume = req.eventVolume
            data.eventTypeId = req.eventTypeId
            data.nsMale = req.nsMale
            data.nsFemale = req.nsFemale
            data.nsCount = req.nsCount
            data.nsInstitution = req.nsInstitution
            data.nsOtherInformation = req.nsOtherInformation
            data.participantMale = req.participantMale
            data.participantFemale = req.participantFemale
            data.participantCount = req.participantCount
            data.participantOtherInformation = req.participantOtherInformation
            data.participantTarget = req.participantTarget
            data.assumption = req.assumption
            data.descriptionRisk = req.descriptionRisk

            val rikRisk = validate["rikRisk"] as RikRisk
            data.risk = rikRisk.name

            data.impactRisk = req.impactRisk
            data.potentialRisk = req.potentialRisk
            data.mitigationRisk = req.mitigationRisk
            data.symptomsRisk = req.symptomsRisk
            data.eventOutput = req.eventOutput
            data.rabFileId = req.rabFileId
            data.updatedBy = getUserLogin()?.id
            data.updatedAt = Date()

            var taskTitle = "Membuat"
            if (update != null) {
                data.updatedBy = getUserLogin()?.id
                data.updatedAt = Date()

                if (data.status == AWP_IMPLEMENTATION_STATUS_PLANNING_REVISION) {
                    taskTitle = "Merevisi"

                    serviceTask.approveRejectFromTask(
                        APPROVAL_STATUS_APPROVE,
                        data.id,
                        TASK_TYPE_AWP_IMPLEMENTATION,
                        getUserLogin()?.id,
                        TASK_NUMBER_TASK_REVISION
                    )
                } else {
                    data.status = AWP_IMPLEMENTATION_STATUS_PLANNING
                }

                update.provincesRegencies?.forEach {
                    val filter: List<ImplementationProvincesRegencies>? =
                        req.provincesRegenciesIds?.filter { f -> f.awpProvincesId == it.id }

                    if (filter.isNullOrEmpty()) {
                        it.active = false
                        it.updatedAt = Date()
                        repoAwpProvinces.save(it)
                    }

                    it.regencies?.forEach { r ->
                        val filterRegency: List<ImplementationProvincesRegencies>? =
                            req.provincesRegenciesIds?.filter { f -> f.awpProvincesRegenciesId?.contains(r.id) == true }

                        if (filterRegency.isNullOrEmpty()) {
                            r.active = false
                            r.updatedAt = Date()
                            repoAwpProvincesRegencies.save(r)
                        }
                    }
                }

                update.monev?.forEach {
                    val filter: List<ImplementationMonev>? = req.monevIds?.filter { f -> f.awpResultChainId == it.id }

                    if (filter.isNullOrEmpty()) {
                        it.active = false
                        it.updatedAt = Date()
                        repoAwpImplementationResultChain.save(it)
                    }

                    it.successIndicator?.forEach { r ->
                        val filterSuccessIndicator: List<ImplementationMonev>? =
                            req.monevIds?.filter { f -> f.awpSuccessIndicatorIds?.contains(r.id) == true }

                        if (filterSuccessIndicator.isNullOrEmpty()) {
                            r.active = false
                            r.updatedAt = Date()
                            repoAwpImplementationSuccessIndicator.save(r)
                        }
                    }
                }

                update.iri?.forEach {
                    if (req.iriIds?.contains(it.id) == false) {
                        it.active = false
                        it.updatedAt = Date()
                        repoAwpIri.save(it)
                    }
                }

                update.pdo?.forEach {
                    if (req.pdoIds?.contains(it.id) == false) {
                        it.active = false
                        it.updatedAt = Date()
                        repoAwpPdo.save(it)
                    }
                }

                update.pok?.forEach {
                    if (req.pokIds?.contains(it.id) == false) {
                        it.active = false
                        it.updatedAt = Date()
                        repoAwpPok.save(it)
                    }
                }
            }

            repo.save(data)

            req.newIriIds?.forEach {
                data.iri?.add(
                    repoAwpIri.save(
                        AwpImplementationIri(
                            awpImplementationId = data.id, iriId = it
                        )
                    )
                )
            }

            req.newPdoIds?.forEach {
                data.pdo?.add(
                    repoAwpPdo.save(
                        AwpImplementationPdo(
                            awpImplementationId = data.id, pdoId = it
                        )
                    )
                )
            }

            req.newPokIds?.forEach {
                data.pok?.add(
                    repoAwpPok.save(
                        AwpImplementationPok(
                            awpImplementationId = data.id, pokId = it
                        )
                    )
                )
            }

            req.newProvincesRegencies?.forEach {
                val awpProvinces = repoAwpProvinces.save(
                    AwpImplementationProvinces(
                        awpImplementationId = data.id, provinceId = it.provinceId
                    )
                )
                data.provincesRegencies?.add(awpProvinces)

                it.regencyIds?.forEach { r ->
                    awpProvinces.regencies?.add(
                        repoAwpProvincesRegencies.save(
                            AwpImplementationProvincesRegencies(
                                awpImplementationProvincesId = awpProvinces.id, regencyId = r
                            )
                        )
                    )
                }
            }

            req.newMonev?.forEach {
                val awpResultChain = repoAwpImplementationResultChain.save(
                    AwpImplementationResultChain(
                        awpImplementationId = data.id, resultChainId = it.resultChainId
                    )
                )
                data.monev?.add(awpResultChain)

                it.successIndicatorIds?.forEach { si ->
                    repoAwpImplementationSuccessIndicator.save(
                        AwpImplementationSuccessIndicator(
                            awpImplementationResultChainId = awpResultChain.id, successIndicatorId = si
                        )
                    )
                }
            }

            val taskDescription = "$taskTitle Rencana Implementasi Kegiatan ${data.name}"
            val approvalUser = validate["approvalUser"] as String?

            serviceTask.createOrUpdateFromTask(
                createdBy = getUserLogin()?.id,
                taskId = data.id,
                taskType = TASK_TYPE_AWP_IMPLEMENTATION,
                description = taskDescription,
                createdFor = approvalUser
            )

            if (data.status == AWP_IMPLEMENTATION_STATUS_PLANNING_REVISION && update != null) {
                if (compareDataChange(oldData, data)) {
                    data.status = AWP_IMPLEMENTATION_STATUS_PLANNING
                    repo.save(data)
                }
            }

            return if (update == null) {
                responseCreated(data = data)
            } else {
                val ipAdress = httpServletRequest.getHeader("X-FORWARDED-FOR") ?: httpServletRequest.remoteAddr
                val userId = getUserLogin()?.id ?: ""
                Thread {
                    logActivityService.writeChangeData(
                        data,
                        oldData,
                        "awp_implementation",
                        ipAdress,
                        userId
                    )
                }.start()

                responseSuccess(data = data)
            }
        } catch (e: Exception) {
            throw e
        }
    }

    fun approveByCoordinator(request: AwpImplementationEvaluationRequest): ResponseEntity<ReturnData> {
        try {
            if (!userHasRoles(listOf(ROLE_ID_COORDINATOR))) {
                throw UnautorizedException(message = VALIDATOR_MSG_NOT_HAVE_ACCESS)
            }

            val find = repo.findByIdAndActive(request.id)
            if (!find.isPresent) {
                return responseNotFound()
            }

            val validate = validateApproveCoordinator(request)
            val listMessage = validate["listMessage"] as List<ErrorMessage>
            if (listMessage.isNotEmpty()) {
                return responseBadRequest(data = listMessage)
            }

            request.answers?.forEach {
                var awpImplementationQualityAtEntryAnswer = AwpImplementationQualityAtEntryAnswer()
                val checkAwpImplementationQualityAtEntryAnswer =
                    repoAwpImplementationQualityAtEntryAnswerRepository.findByAwpImplementationIdAndQualityAtEntryQuestionIdAndActive(
                        request.id, it.qualityAtEntryQuestionId
                    )
                if (checkAwpImplementationQualityAtEntryAnswer.isPresent) {
                    awpImplementationQualityAtEntryAnswer = checkAwpImplementationQualityAtEntryAnswer.get()
                    awpImplementationQualityAtEntryAnswer.updatedAt = Date()
                }

                awpImplementationQualityAtEntryAnswer.awpImplementationId = find.get().id
                awpImplementationQualityAtEntryAnswer.qualityAtEntryQuestionId = it.qualityAtEntryQuestionId
                awpImplementationQualityAtEntryAnswer.answer = it.answer
                awpImplementationQualityAtEntryAnswer.revision = it.revision

                repoAwpImplementationQualityAtEntryAnswerRepository.save(awpImplementationQualityAtEntryAnswer)
            }

            var status = AWP_IMPLEMENTATION_STATUS_EVALUATED
            if (request.approveStatus == APPROVAL_STATUS_REJECT) {
                status = AWP_IMPLEMENTATION_STATUS_PLANNING_REVISION

                serviceTask.createRevisionTask(
                    taskId = find.get().id,
                    taskType = TASK_TYPE_AWP_IMPLEMENTATION,
                    createdFor = getUserLogin()?.id,
                    description = "Revisi Rencana Implementasi Kegiatan ${find.get().name}"
                )
            } else {
                // create task for pmu
                var title = ""
                if (find.get().status == AWP_IMPLEMENTATION_STATUS_EVELUATION_REVISION) {
                    title = " Revisi"
                }

                val taskDescription = "Persetujuan$title Evaluasi Kegiatan ${find.get().name}"
                val usersPmu = validate["usersPmu"] as List<String>

                usersPmu.forEach {
                    serviceTask.createOrUpdateFromTask(
                        createdBy = getUserLogin()?.id,
                        taskId = find.get().id,
                        taskType = TASK_TYPE_AWP_IMPLEMENTATION,
                        description = taskDescription,
                        createdFor = it
                    )
                }
            }


            find.get().qualitAtEntryNote = request.qualitAtEntryNote
            find.get().status = status
            find.get().updatedBy = getUserLogin()?.id
            find.get().updatedAt = Date()
            repo.save(find.get())

            repoAwpImplementationApprovalNote.save(
                AwpImplementationApprovalNote(
                    awpImplementationId = find.get().id,
                    userId = getUserLogin()?.id,
                    createdFrom = AWP_IMPLEMENTATION_APPROVAL_MESSAGE_FROM_COORDINATOR,
                    note = request.note,
                    approvalStatus = request.approveStatus,
                )
            )

            serviceTask.approveRejectFromTask(
                APPROVAL_STATUS_APPROVE,
                find.get().id,
                TASK_TYPE_AWP_IMPLEMENTATION,
                getUserLogin()?.id,
                TASK_NUMBER_TASK_REVISION
            )

            serviceTask.approveRejectFromTask(
                request.approveStatus, find.get().id, TASK_TYPE_AWP_IMPLEMENTATION, getUserLogin()?.id, TASK_NUMBER_TASK
            )
            return responseSuccess()
        } catch (e: Exception) {
            throw e
        }
    }

    fun approveByPmu(request: ApproveRequest): ResponseEntity<ReturnData> {
        try {
            if (!userHasAllRoles(listOf(ROLE_ID_COORDINATOR, ROLE_ID_ADMINSTRATOR))) {
                throw UnautorizedException(message = VALIDATOR_MSG_NOT_HAVE_ACCESS)
            }

            val find = repo.findByIdAndActive(request.id)
            if (!find.isPresent) {
                return responseNotFound()
            }

            var status = AWP_IMPLEMENTATION_STATUS_IMPLEMENTATION
            if (request.approveStatus == APPROVAL_STATUS_REJECT) {
                status = AWP_IMPLEMENTATION_STATUS_EVELUATION_REVISION

                if (resourcesHasAllRoles(listOf(ROLE_ID_COORDINATOR), find.get().projectOfficerResources)) {
                    status = AWP_IMPLEMENTATION_STATUS_PLANNING_REVISION
                }
            }

            find.get().status = status
            find.get().updatedBy = getUserLogin()?.id
            find.get().updatedAt = Date()
            repo.save(find.get())

            repoAwpImplementationApprovalNote.save(
                AwpImplementationApprovalNote(
                    awpImplementationId = find.get().id,
                    userId = getUserLogin()?.id,
                    createdFrom = AWP_IMPLEMENTATION_APPROVAL_MESSAGE_FROM_PMU_HEAD,
                    note = request.note,
                    approvalStatus = request.approveStatus,
                )
            )

            if (request.approveStatus == APPROVAL_STATUS_REJECT) {
                serviceTask.createRevisionTask(
                    taskId = find.get().id,
                    taskType = TASK_TYPE_AWP_IMPLEMENTATION,
                    createdFor = getUserLogin()?.id,
                    description = "Revisi Rencana Implementation Kegiatan ${find.get().name}"
                )
            }

            serviceTask.approveRejectFromTask(
                request.approveStatus, find.get().id, TASK_TYPE_AWP_IMPLEMENTATION, getUserLogin()?.id
            )

            return responseSuccess()
        } catch (e: Exception) {
            throw e
        }
    }

    fun schedule(req: AwpImplementationScheduleRequest): ResponseEntity<ReturnData> {
        return try {
            var ids = repo.getAllIds()

            if ((req.self != false) || (req.staff != false) || !req.componentIds.isNullOrEmpty()) {
                ids.clear()
            }

            if (req.self == true) {
                if (!userHasRoles(listOf(ROLE_ID_LSP))) {
                    val byResources = repoEventStaff.getAwpImplementationIdsByResourcesId(getUserLogin()?.resourcesId)
                    if (byResources.isNotEmpty()) {
                        ids.addAll(byResources)
                    }

                    val projectOfficers = repo.getByProjectOfficerResourcesId(getUserLogin()?.resourcesId)
                    if (projectOfficers.isNotEmpty()) {
                        ids.addAll(projectOfficers)
                    }
                } else {
                    val byLsp = repo.getByLspUserId(getUserLogin()?.id)
                    if (byLsp.isNotEmpty()) {
                        ids.addAll(byLsp)
                    }
                }
            }

            if (req.staff == true) {
                val staffSupervisiorIds = serviceEvent.getStaffSupervisiorId(getUserLogin()?.resourcesId ?: "", true)
                if (staffSupervisiorIds.isNotEmpty()) {
                    val staffSupervisiorNotSelf = staffSupervisiorIds.filter { f ->
                        f != getUserLogin()?.resourcesId
                    }

                    if (staffSupervisiorNotSelf.isNotEmpty()) {
                        val byIdsStaffSupervisior =
                            repoEventStaff.getAwpImplementationIdsByResourcesIds(staffSupervisiorNotSelf)
                        if (byIdsStaffSupervisior.isNotEmpty()) {
                            ids.addAll(byIdsStaffSupervisior)
                        }
                    }

                    val byStaff = repoEventStaff.getAwpImplementationIdsBySupervisiorIds(staffSupervisiorIds)
                    if (byStaff.isNotEmpty()) {
                        ids.addAll(byStaff)
                    }
                }
            }

            if (!req.componentIds.isNullOrEmpty()) {
                val byComponent = repo.getIdsByComponentIds(req.componentIds)
                if (byComponent.isNotEmpty()) {
                    ids.addAll(byComponent)
                }
            }

            if (!req.eventManagementTypeId.isNullOrEmpty()) {
                val byEventManagementType = repo.getIdsByEventManagementType(req.eventManagementTypeId)
                if (byEventManagementType.isNotEmpty()) {
                    ids.forEach {
                        if (!byEventManagementType.contains(it)) {
                            ids = ids.filter { idf ->
                                idf != it
                            }.toMutableList()
                        }
                    }
                } else {
                    ids.clear()
                }
            }

            val pageRequest = Pagination2Request()
            pageRequest.enablePage = false

            if (ids.isNotEmpty()) {
                pageRequest.paramIn?.add(ParamArray("id", "string", ids.distinct()))
                pageRequest.sort?.add(Sort("startDate", "asc"))
            } else {
                pageRequest.paramIs?.add(ParamSearch("id", "string", System.currentTimeMillis().toString()))
            }

            responseSuccess(data = repoNativeWithoutAwp.getPage(pageRequest)?.content)
        } catch (e: Exception) {
            throw e
        }
    }

    private fun validate(request: AwpImplementationRequest, update: AwpImplementation? = null): Map<String, Any?> {
        val rData = mutableMapOf<String, Any?>()
        try {
            val listMessage: MutableList<ErrorMessage> = emptyList<ErrorMessage>().toMutableList()

            val checkComponent = repoComponent.findByIdAndActive(request.componentId)
            if (!checkComponent.isPresent) {
                listMessage.add(
                    ErrorMessage(
                        "componentId", "Component id ${request.componentId} $VALIDATOR_MSG_NOT_FOUND"
                    )
                )
            }

            if (!request.subComponentId.isNullOrEmpty()) {
                val checkSubComponent = repoComponent.findByIdAndActive(request.subComponentId)
                if (!checkSubComponent.isPresent) {
                    listMessage.add(
                        ErrorMessage(
                            "subComponentId", "Sub Component id ${request.subComponentId} $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                }
            }

            if (!request.subSubComponentId.isNullOrEmpty()) {
                val checkSubSubComponent = repoComponent.findByIdAndActive(request.subSubComponentId)
                if (!checkSubSubComponent.isPresent) {
                    listMessage.add(
                        ErrorMessage(
                            "subSubComponentId",
                            "Sub Sub Component id ${request.subSubComponentId} $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                }
            }

            val checkEventManagementType = repoEventManagementType.findByIdAndActive(request.eventManagementTypeId)
            if (!checkEventManagementType.isPresent) {
                listMessage.add(
                    ErrorMessage(
                        "eventManagementTypeId",
                        "Cara Pengelolaan Kegiatan id ${request.eventManagementTypeId} $VALIDATOR_MSG_NOT_FOUND"
                    )
                )
            }

            var dateStart: Date? = null
            var dateEnd: Date? = null
            val dform: DateFormat = SimpleDateFormat("yyyy-MM-dd")

            try {
                dateStart = dform.parse(request.startDate)
            } catch (_: Exception) {
                listMessage.add(
                    ErrorMessage(
                        "startDate", "Tanggal Mulai $VALIDATOR_MSG_NOT_VALID"
                    )
                )
            }

            try {
                dateEnd = dform.parse(request.endDate)
            } catch (_: Exception) {
                listMessage.add(
                    ErrorMessage(
                        "endDate", "Tanggal Selesai $VALIDATOR_MSG_NOT_VALID"
                    )
                )
            }

            if (dateEnd?.before(dateStart) == true) {
                listMessage.add(
                    ErrorMessage(
                        "endDate", "Tanggal Selesai $VALIDATOR_MSG_NOT_VALID"
                    )
                )
            }

            if (dateStart?.before(dform.parse(dform.format(update?.awp?.startDate))) == true) {
                listMessage.add(
                    ErrorMessage(
                        "startDate", "Tanggal Mulai $VALIDATOR_MSG_NOT_VALID"
                    )
                )
            }

            if (dateStart != null) {
                val dformY: DateFormat = SimpleDateFormat("yyyy")
                val df = dformY.format(dateStart).toString().toInt()

                if (update?.awp?.year != df) {
                    listMessage.add(
                        ErrorMessage(
                            "startDate", "Tahun Tanggal Mulai tidak sesuai dengan tahun AWP"
                        )
                    )
                }
            }

            if (dateEnd != null) {
                val dformY: DateFormat = SimpleDateFormat("yyyy")
                val df = dformY.format(dateEnd).toString().toInt()

                if (update?.awp?.year != df) {
                    listMessage.add(
                        ErrorMessage(
                            "endDate", "Tahun Tanggal Selesai tidak sesuai dengan tahun AWP"
                        )
                    )
                }
            }

            rData["startDate"] = dateStart as Date
            rData["endDate"] = dateEnd as Date

            request.newProvincesRegencies?.forEach {
                val checkProvince = repoProvince.findByIdAndActive(it.provinceId)
                if (!checkProvince.isPresent) {
                    listMessage.add(
                        ErrorMessage(
                            "newProvincesRegencies", "Province id ${it.provinceId} $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                }

                it.regencyIds?.forEach { r ->
                    val checkRegency = repoRegency.findByIdAndActive(r)
                    if (!checkRegency.isPresent) {
                        listMessage.add(
                            ErrorMessage(
                                "newProvincesRegencies", "Regency id $r $VALIDATOR_MSG_NOT_FOUND"
                            )
                        )
                    }
                }
            }

            request.newMonev?.forEach {
                val checkResultChain = repoMonevResultChain.findByIdAndActive(it.resultChainId)
                if (!checkResultChain.isPresent) {
                    listMessage.add(
                        ErrorMessage(
                            "newMonev", "Monev Result Chain id ${it.resultChainId} $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                }

                it.successIndicatorIds?.forEach { r ->
                    val checkSuccessIndicator = repoMonevSuccessIndicator.findByIdAndActive(r)
                    if (!checkSuccessIndicator.isPresent) {
                        listMessage.add(
                            ErrorMessage(
                                "newMonev", "Monev Success Indicator id $r $VALIDATOR_MSG_NOT_FOUND"
                            )
                        )
                    }
                }
            }

            val checkEnventType = repoEventType.findByIdAndActive(request.eventTypeId)
            if (!checkEnventType.isPresent) {
                listMessage.add(
                    ErrorMessage(
                        "eventTypeId", "Event Type ${request.eventTypeId} $VALIDATOR_MSG_NOT_FOUND"
                    )
                )
            }

            request.newPdoIds?.forEach {
                val checkPdo = repoPdo.findByIdAndActive(it)
                if (!checkPdo.isPresent) {
                    listMessage.add(
                        ErrorMessage(
                            "newPdoIds", "Pdo id $it $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                }
            }

            request.newIriIds?.forEach {
                val checkIri = repoIri.findByIdAndActive(it)
                if (!checkIri.isPresent) {
                    listMessage.add(
                        ErrorMessage(
                            "newIriIds", "Iri id $it $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                }
            }

            request.newPokIds?.forEach {
                val checkPok = repoPok.findByIdAndActive(it)
                if (!checkPok.isPresent) {
                    listMessage.add(
                        ErrorMessage(
                            "newPokIds", "POK id $it $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                }
            }

            if (!request.rabFileId.isNullOrEmpty()) {
                val checkFile: ReturnDataFiles? = RequestHelpers.filesGetById(
                    request.rabFileId ?: "", filesUrl, httpServletRequest.getHeader(HEADER_STRING)
                )

                var checkedFile: id.go.kemenag.madrasah.pmrms.awp.activity.pojo.Files? = null
                if (checkFile != null) {
                    if (checkFile.data != null) {
                        checkedFile = checkFile.data
                    }
                }

                if (checkedFile == null) {
                    listMessage.add(
                        ErrorMessage(
                            "rabFileId", "Rab File Id ${request.rabFileId} $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                }
            }

            if (!request.activityModeId.isNullOrEmpty()) {
                val activityMode = repoActivityMode.findByIdAndActive(request.activityModeId)
                if (!activityMode.isPresent) {
                    listMessage.add(
                        ErrorMessage(
                            "activityModeId", "Activity Mode id ${request.activityModeId} $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                }
            }


            var componentCodeId: String? = null

            if (!request.componentCodeId.isNullOrEmpty()) {
                componentCodeId = request.componentCodeId
            } else {
                var lastComponent = request.componentId
                if (!request.subComponentId.isNullOrEmpty()) {
                    lastComponent = request.subComponentId
                }
                if (!request.subSubComponentId.isNullOrEmpty()) {
                    lastComponent = request.subSubComponentId
                }

                val component = repoComponent.findByIdAndActive(lastComponent)
                if (component.isPresent) {
                    val dYform: DateFormat = SimpleDateFormat("yyyy")
                    val components = repoComponentNative.getSubComponent(
                        component.get().code ?: "",
                        update?.awp?.year ?: dYform.format(Date()).toInt(),
                        repoComponent.getMaxDotCode()
                    )

                    var findLastComponent: Component = component.get()
                    if (components.isNotEmpty()) {
                        findLastComponent = components[components.size - 1]
                    }

                    val lastChar = findLastComponent.code?.last().toString()

                    val componentCode = try {
                        findLastComponent.code?.last().toString().toInt().plus(1).toString().let {
                            findLastComponent.code?.replace(lastChar, it)
                        }.toString()
                    } catch (_: Exception) {
                        findLastComponent.code?.last()?.plus(1).toString().let {
                            findLastComponent.code?.replace(lastChar, it)
                        }.toString()
                    }

                    val findComponent = repoComponent.findByCodeAndActive(componentCode)
                    if (findComponent.isPresent) {
                        findComponent.get().description = request.name
                        repoComponent.save(findComponent.get())
                        componentCodeId = findComponent.get().id
                    } else {
                        val cComponent = repoComponent.save(
                            Component(
                                code = componentCode, description = request.name
                            )
                        )
                        componentCodeId = cComponent.id
                    }
                }
            }

            if (userHasRoles(listOf(ROLE_ID_ADMINSTRATOR)) && !userHasRoles(listOf(ROLE_ID_CONSULTAN)) && !userHasRoles(
                    listOf(
                        ROLE_ID_COORDINATOR
                    )
                )
            ) {
                if (request.projectOfficer.isNullOrEmpty()) {
                    listMessage.add(
                        ErrorMessage(
                            "projectOfficer", "Project Officer $VALIDATOR_MSG_REQUIRED"
                        )
                    )
                }
            }

            var projectOfficer = request.projectOfficer

            if (projectOfficer.isNullOrEmpty()) {
                projectOfficer = getUserLogin()?.resourcesId
            }

            if (projectOfficer.isNullOrEmpty()) {
                listMessage.add(
                    ErrorMessage(
                        "projectOfficer", "Project Officer $VALIDATOR_MSG_REQUIRED"
                    )
                )
            }

            val resources = repoResources.findByIdAndActive(projectOfficer)
            if (!resources.isPresent) {
                listMessage.add(
                    ErrorMessage(
                        "projectOfficer", "Project Officer $projectOfficer $VALIDATOR_MSG_NOT_FOUND"
                    )
                )
            }

            var approvalUser: String? = null
            if (userHasRoles(listOf(ROLE_ID_CONSULTAN))) {
                findCoordinatorCounter = 0
                approvalUser = findCoordinator(resources.get().supervisiorId)
                if (approvalUser.isNullOrEmpty()) {
                    listMessage.add(
                        ErrorMessage(
                            "projectOfficer", "Koordinator Project Officer $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                }
            } else if (userHasRoles(listOf(ROLE_ID_COORDINATOR))) {
                val usersPmuHead = repoUserRoleViewUserResources.getPmuHead(
                    listOf(ROLE_ID_COORDINATOR, ROLE_ID_ADMINSTRATOR),
                    2,
                    COMPONENT_ID_PMU,
                )

                if (usersPmuHead.isEmpty()) {
                    listMessage.add(
                        ErrorMessage(
                            "projectOfficer", "User $POSITION_HEAD_PMU $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                } else {
                    approvalUser = usersPmuHead[0]
                }
            }

            rData["approvalUser"] = approvalUser

            if (!request.lspPic.isNullOrEmpty()) {
                val checkUserLsp = repoUser.findByIdAndActive(request.lspPic)
                if (!checkUserLsp.isPresent) {
                    listMessage.add(
                        ErrorMessage(
                            "lspPic", "LSP PIC Id ${request.lspPic} $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                }
            }

            rData["projectOfficer"] = projectOfficer
            rData["componentCodeId"] = componentCodeId

            if (componentCodeId == null) {
                listMessage.add(
                    ErrorMessage(
                        "componentCodeId", "Komponen Code Id $VALIDATOR_MSG_NOT_FOUND"
                    )
                )
            }

            val rikRisk = repoRikRisk.findByNameActive(request.risk)
            if (!rikRisk.isPresent) {
                val createRikRIsk = repoRikRisk.save(
                    RikRisk(
                        name = request.risk
                    )
                )
                rData["rikRisk"] = createRikRIsk
            } else {
                rData["rikRisk"] = rikRisk.get()
            }

            rData["listMessage"] = listMessage
        } catch (e: Exception) {
            throw e
        }

        return rData
    }

    fun generatePdf(data: AwpImplementation?): File? {
        val ctx = Context()
        ctx.setVariable("componentCode", data?.component?.code ?: "-")
        ctx.setVariable("componentDescription", data?.component?.description ?: "-")
        ctx.setVariable("subComponentDescription", data?.subComponent?.description ?: "-")
        ctx.setVariable("pok", "-")
        ctx.setVariable("budgetPok", data?.budgetAwp ?: "-")
        ctx.setVariable("executor", data?.eventManagementType?.name ?: "-")
        ctx.setVariable("tpMale", data?.participantCount ?: 0)
        ctx.setVariable("tpFemale", 0)
        ctx.setVariable("tpTotal", data?.participantCount ?: 0)
        ctx.setVariable("purpose", data?.purpose ?: "-")
        ctx.setVariable("description", data?.description ?: "-")
        ctx.setVariable("startTime", data?.startDate ?: "-")
        ctx.setVariable("endTime", data?.endDate ?: "-")
        ctx.setVariable("volume", data?.eventVolume ?: "-")
        ctx.setVariable("regency", "-")
        ctx.setVariable("province", "-")
        ctx.setVariable("nsMale", data?.nsCount ?: 0)
        ctx.setVariable("nsFemale", 0)
        ctx.setVariable("tpTotal", data?.nsCount ?: 0)
        ctx.setVariable("institution", data?.nsInstitution ?: "-")
        ctx.setVariable("result", data?.eventOutput ?: "-")
        ctx.setVariable("pdo", "-")
        ctx.setVariable("pok", "-")
        ctx.setVariable("participantMale", data?.participantCount ?: 0)
        ctx.setVariable("participantFemale", 0)
        ctx.setVariable("participantTotal", data?.participantCount ?: 0)
        ctx.setVariable("notes", "-")

        val html: String = loadAndFillTemplate(ctx)
        return renderPdf(html)
    }

    private fun renderPdf(html: String): File? {
        val path = Paths.get(dirPdf)
        if (!Files.exists(path)) {
            Files.createDirectories(path)
        }

        val file = File.createTempFile("awp-", ".pdf", path.toFile())

        val outputStream: OutputStream = FileOutputStream(file)
        val renderer = ITextRenderer()
        renderer.setDocumentFromString(
            html, ClassPathResource(pdfResources).url.toExternalForm()
        )
        renderer.layout()
        renderer.createPDF(outputStream)
        outputStream.close()
        file.deleteOnExit()
        return file
    }

    private fun loadAndFillTemplate(context: Context): String {
        return templateEngine.process("awp-pdf", context)
    }

    private fun validateApproveCoordinator(request: AwpImplementationEvaluationRequest): Map<String, Any> {
        val rData = mutableMapOf<String, Any>()
        try {
            val listMessage: MutableList<ErrorMessage> = emptyList<ErrorMessage>().toMutableList()

            val usersPmu = repoUserRoleViewUserResources.getPmuHead(
                listOf(ROLE_ID_COORDINATOR, ROLE_ID_ADMINSTRATOR), 2, COMPONENT_ID_PMU
            )

            if (usersPmu.isEmpty()) {
                listMessage.add(
                    ErrorMessage(
                        "approveStatus", "User $POSITION_HEAD_PMU $VALIDATOR_MSG_NOT_FOUND"
                    )
                )
            } else {
                rData["usersPmu"] = usersPmu
            }

            request.answers?.forEach {
                val check = repoQualityAtEntryQuestion.findByIdAndActive(it.qualityAtEntryQuestionId)
                if (!check.isPresent) {
                    listMessage.add(
                        ErrorMessage(
                            "qualityAtEntryQuestionId",
                            "Id Quality at Entry id ${it.qualityAtEntryQuestionId} $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                }
            }

            rData["listMessage"] = listMessage
        } catch (e: Exception) {
            throw e
        }

        return rData
    }

    private fun findCoordinator(supervisiorId: String?): String? {
        findCoordinatorCounter++
        try {
            if (findCoordinatorCounter > APP_FIND_COORDINATOR_LIMIT) {
                return null
            }

            val findResources = repoResources.findByIdAndActive(supervisiorId)
            if (findResources.isPresent) {
                val filterRole = findResources.get().user?.roles?.filter {
                    it.roleId == ROLE_ID_COORDINATOR
                }
                return if (!filterRole.isNullOrEmpty()) {
                    findResources.get().userId
                } else {
                    if (!findResources.get().supervisiorId.isNullOrEmpty()) {
                        findCoordinator(findResources.get().supervisiorId)
                    } else {
                        null
                    }
                }
            } else {
                return null
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return null
    }
}

