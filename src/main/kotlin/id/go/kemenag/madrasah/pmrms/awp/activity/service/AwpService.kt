package id.go.kemenag.madrasah.pmrms.awp.activity.service

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import id.go.kemenag.madrasah.pmrms.awp.activity.constant.*
import id.go.kemenag.madrasah.pmrms.awp.activity.exception.UnautorizedException
import id.go.kemenag.madrasah.pmrms.awp.activity.helpers.*
import id.go.kemenag.madrasah.pmrms.awp.activity.model.ReturnDataFiles
import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.AwpRequest
import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.ParamArray
import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.ProvincesRegencies
import id.go.kemenag.madrasah.pmrms.awp.activity.model.response.ErrorMessage
import id.go.kemenag.madrasah.pmrms.awp.activity.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.*
import id.go.kemenag.madrasah.pmrms.awp.activity.repository.*
import id.go.kemenag.madrasah.pmrms.awp.activity.repository.native.AwpRepositoryNative
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import javax.servlet.http.HttpServletRequest


@Suppress("UNCHECKED_CAST", "NAME_SHADOWING")
@Service
class AwpService {

    @Autowired
    private lateinit var repo: AwpRepository

    @Autowired
    private lateinit var repoNative: AwpRepositoryNative

    @Autowired
    private lateinit var repoComponent: ComponentRepository

    @Autowired
    private lateinit var repoEventManagementType: EventManagementTypeRepository

    @Autowired
    private lateinit var repoEventType: EventTypeRepository

    @Autowired
    private lateinit var repoProvince: ProvinceRepository

    @Autowired
    private lateinit var repoRegency: RegencyRepository

    @Autowired
    private lateinit var repoIri: IriRepository

    @Autowired
    private lateinit var repoPdo: PdoRepository

    @Autowired
    private lateinit var repoAwpIri: AwpIriRepository

    @Autowired
    private lateinit var repoAwpPdo: AwpPdoRepository

    @Autowired
    private lateinit var repoAwpProvinces: AwpProvincesRepository

    @Autowired
    private lateinit var repoAwpProvincesRegencies: AwpProvincesRegenciesRepository

    @Autowired
    private lateinit var repoAwpImplementation: AwpImplementationRepository

    @Autowired
    private lateinit var repoAwpImplementationProvincesRegencies: AwpImplementationProvincesRegenciesRepository

    @Autowired
    private lateinit var repoAwpImplementationIri: AwpImplementationIriRepository

    @Autowired
    private lateinit var repoAwpImplementationPdo: AwpImplementationPdoRepository

    @Autowired
    private lateinit var repoAwpImplementationProvinces: AwpImplementationProvincesRepository

    @Autowired
    private lateinit var httpServletRequest: HttpServletRequest

    @Autowired
    private lateinit var repoActivityMode: ActivityModeRepository

    @Value("\${url.files}")
    private lateinit var filesUrl: String

    @Autowired
    private lateinit var logActivityService: LogActivityService

    fun datatable(req: Pagination2Request): ResponseEntity<ReturnData> {
        return try {/*if (!userHasRoles(listOf(ROLE_ID_ADMINSTRATOR))) {
                if (getUserLogin()?.component?.code != "4.4") {
                    var componentId: String? = getUserLogin()?.component?.id
                    if (getUserLogin()?.component?.code?.contains("4.") == true) {
                        val findComponent = repoComponent.findByCodeAndActive("4")
                        if (findComponent.isPresent) {
                            componentId = findComponent.get().id
                        }
                    }

                    if (componentId != null) {
                        req.paramIs?.add(ParamSearch("componentId", "string", componentId))
                    } else {
                        req.paramIs?.add(ParamSearch("componentId", "string", System.currentTimeMillis().toString()))
                    }
                }
            }*/

            val filterComponent: ParamArray? = req.paramIn?.find { f ->
                f.field == "componentId"
            }

            val componentIds = mutableListOf<String>()
            val subComponentIds = mutableListOf<String>()
            filterComponent?.value?.forEach {
                val component = repoComponent.findByIdAndActive(it.toString())
                if (component.isPresent) {
                    if (component.get().code?.contains(".") == true) {
                        subComponentIds.add(component.get().id ?: "")
                    } else {
                        componentIds.add(component.get().id ?: "")
                    }
                }
            }

            if (componentIds.isNotEmpty()) {
                req.paramIn?.add(ParamArray("componentId", "string", componentIds))
            }

            if (subComponentIds.isNotEmpty()) {
                req.paramIn?.add(ParamArray("subComponentId", "string", subComponentIds))
            }

            if (filterComponent != null) {
                req.paramIn?.remove(filterComponent)
            }

            repoNative.setOrderByStrLength(
                listOf("componentCode.code")
            )

            responseSuccess(data = repoNative.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }

    fun listYear(): ResponseEntity<ReturnData> {
        try {
            return responseSuccess(data = repo.getListyear())
        } catch (e: Exception) {
            throw e
        }
    }

    fun getDetail(id: String): ResponseEntity<ReturnData> {
        try {
            val data = repo.findByIdAndActive(id)
            if (data.isPresent) {
                return responseSuccess(data = data)
            }
            return responseNotFound()
        } catch (e: Exception) {
            throw e
        }
    }

    fun delete(id: String): ResponseEntity<ReturnData> {
        try {
            val data = repo.findByIdAndActive(id)
            if (data.isPresent) {
                data.get().active = false
                data.get().updatedAt = Date()
                repo.save(data.get())

                data.get().provincesRegencies?.forEach {
                    it.updatedAt = Date()
                    it.active = false
                    repoAwpProvinces.save(it)
                }

                data.get().pdo?.forEach {
                    it.updatedAt = Date()
                    it.active = false
                    repoAwpPdo.save(it)
                }

                data.get().iri?.forEach {
                    it.updatedAt = Date()
                    it.active = false
                    repoAwpIri.save(it)
                }

                return responseSuccess()
            }
            return responseNotFound()
        } catch (e: Exception) {
            throw e
        }
    }

    fun saveData(request: AwpRequest): ResponseEntity<ReturnData> {
        try {
            return saveOrUpdate(request)
        } catch (e: Exception) {
            throw e
        }
    }

    fun updateData(id: String, request: AwpRequest): ResponseEntity<ReturnData> {
        val update = repo.findByIdAndActive(id)
        return if (update.isPresent) {
            saveOrUpdate(request, update.get())
        } else {
            responseNotFound()
        }
    }


    fun saveOrUpdate(req: AwpRequest, update: Awp? = null): ResponseEntity<ReturnData> {
        var oldData: Awp? = null
        if (update != null) {
            val objectMapper = ObjectMapper()
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            oldData = objectMapper.readValue(
                objectMapper.writeValueAsString(update), Awp::class.java
            ) as Awp
        }

        try {
            if (!userHasRoles(listOf(ROLE_ID_ADMINSTRATOR))) {
                throw UnautorizedException(message = VALIDATOR_MSG_NOT_HAVE_ACCESS)
            }

            val validate = validate(req, update)
            val listMessage = validate["listMessage"] as List<ErrorMessage>
            if (listMessage.isNotEmpty()) {
                return responseBadRequest(data = listMessage)
            }

            val data: Awp?
            data = update ?: Awp()

            data.year = req.year
            data.componentId = req.componentId
            data.subComponentId = req.subComponentId
            data.subSubComponentId = validate["subSubComponentId"] as String?

            if (!req.subSubComponentName.isNullOrEmpty()) {
                data.subSubComponentName = req.subSubComponentName
            }

            data.name = req.name.toString().trim()
            data.nameOtherInformation = req.nameOtherInformation?.trim()
            data.componentCodeId = validate["componentCodeId"] as String
            data.budgetAwp = req.budgetAwp
            data.purpose = req.purpose
            data.description = req.description
            data.eventManagementTypeId = req.eventManagementTypeId
            data.startDate = validate["startDate"] as Date
            data.endDate = validate["endDate"] as Date
            data.eventOtherInformation = req.eventOtherInformation
            data.eventVolume = req.eventVolume
            data.eventTypeId = req.eventTypeId
            data.nsCount = req.nsCount
            data.nsInstitution = req.nsInstitution
            data.nsOtherInformation = req.nsOtherInformation
            data.participantCount = req.participantCount
            data.participantOtherInformation = req.participantOtherInformation
            data.eventOutput = req.eventOutput
            data.locationOtherInformation = req.locationOtherInformation?.trim()
            data.rabFileId = req.rabFileId
            data.activityModeId = req.activityModeId
            data.participantTarget = req.participantTarget
            data.descriptionRisk = req.descriptionRisk
            data.mitigationRisk = req.mitigationRisk
            data.risk = req.risk
            data.potentialRisk = req.potentialRisk
            data.impactRisk = req.impactRisk
            data.symptomsRisk = req.symptomsRisk

            // create or update awp implementation
            var awpImplementation = AwpImplementation()
            val findAwpImplementation = repoAwpImplementation.findByAwpIdAndActive(data.id)
            if (findAwpImplementation.isPresent) {
                awpImplementation = findAwpImplementation.get()
            }

            awpImplementation.awpId = data.id
            awpImplementation.componentId = req.componentId
            awpImplementation.subComponentId = req.subComponentId
            awpImplementation.subSubComponentId = validate["subSubComponentId"] as String?
            awpImplementation.name = req.name
            awpImplementation.nameOtherInformation = req.nameOtherInformation
            awpImplementation.locationOtherInformation = req.locationOtherInformation
            awpImplementation.componentCodeId = validate["componentCodeId"] as String
            awpImplementation.budgetAwp = req.budgetAwp
            awpImplementation.purpose = req.purpose
            awpImplementation.description = req.description
            awpImplementation.eventManagementTypeId = req.eventManagementTypeId
            awpImplementation.startDate = validate["startDate"] as Date
            awpImplementation.endDate = validate["endDate"] as Date
            awpImplementation.eventOtherInformation = req.eventOtherInformation
            awpImplementation.eventVolume = req.eventVolume
            awpImplementation.eventTypeId = req.eventTypeId
            awpImplementation.nsCount = req.nsCount
            awpImplementation.nsInstitution = req.nsInstitution
            awpImplementation.nsOtherInformation = req.nsOtherInformation
            awpImplementation.participantCount = req.participantCount
            awpImplementation.participantOtherInformation = req.participantOtherInformation
            awpImplementation.eventOutput = req.eventOutput
            awpImplementation.rabFileId = awpImplementation.rabFileId
            awpImplementation.activityModeId = req.activityModeId
            awpImplementation.participantTarget = req.participantTarget
            awpImplementation.descriptionRisk = req.descriptionRisk
            awpImplementation.mitigationRisk = req.mitigationRisk
            awpImplementation.risk = req.risk
            awpImplementation.potentialRisk = req.potentialRisk
            awpImplementation.impactRisk = req.impactRisk
            awpImplementation.symptomsRisk = req.symptomsRisk

            if (update == null || !findAwpImplementation.isPresent) {
                data.createdBy = getUserLogin()?.id
                // update awp implementation
                awpImplementation.createdBy = getUserLogin()?.id
                awpImplementation.status = AWP_IMPLEMENTATION_STATUS_NEW
            } else {
                data.updatedBy = getUserLogin()?.id
                data.updatedAt = Date()

                awpImplementation.updatedBy = getUserLogin()?.id
                awpImplementation.updatedAt = Date()

                val deletedAwpProvinces: MutableList<String> = mutableListOf()
                val deletedAwpRegencies: MutableList<String> = mutableListOf()

                update.provincesRegencies?.forEach {
                    val filter: List<ProvincesRegencies>? =
                        req.provincesRegenciesIds?.filter { f -> f.awpProvincesId == it.id }

                    if (filter.isNullOrEmpty()) {
                        it.active = false
                        it.updatedAt = Date()
                        repoAwpProvinces.save(it)
                        deletedAwpProvinces.add(it.provinceId ?: "")
                    }

                    it.regencies?.forEach { r ->
                        val filterRegency: List<ProvincesRegencies>? =
                            req.provincesRegenciesIds?.filter { f -> f.awpProvincesRegenciesId?.contains(r.id) == true }

                        if (filterRegency.isNullOrEmpty()) {
                            r.active = false
                            r.updatedAt = Date()
                            repoAwpProvincesRegencies.save(r)
                            deletedAwpRegencies.add(r.regencyId ?: "")
                        }
                    }
                }

                deletedAwpProvinces.forEach {
                    repoAwpImplementationProvinces.getByAwpImplementationIdAndProvinceIdAndActive(
                        awpImplementation.id, it
                    ).forEach { ip ->
                        ip.active = false
                        ip.updatedAt = Date()
                        repoAwpImplementationProvinces.save(ip)

                        deletedAwpRegencies.forEach { dr ->
                            repoAwpImplementationProvincesRegencies.getByAwpImplementationProvincesIdAndRegencyIdAndActive(
                                ip.id, dr
                            ).forEach { ipr ->
                                ipr.active = false
                                ipr.updatedAt = Date()
                                repoAwpImplementationProvincesRegencies.save(ipr)
                            }
                        }
                    }
                }

                update.iri?.forEach {
                    if (req.iriIds?.contains(it.id) == false) {
                        it.active = false
                        it.updatedAt = Date()
                        repoAwpIri.save(it)

                        repoAwpImplementationIri.getByAwpImplementationIdAndIriIdAndActive(
                            awpImplementation.id, it.iriId
                        ).forEach { ir ->
                            ir.active = false
                            ir.updatedAt = Date()
                            repoAwpImplementationIri.save(ir)
                        }
                    }
                }

                update.pdo?.forEach {
                    if (req.pdoIds?.contains(it.id) == false) {
                        it.active = false
                        it.updatedAt = Date()
                        repoAwpPdo.save(it)

                        repoAwpImplementationPdo.getByAwpImplementationIdAndPdoIdAndActive(
                            awpImplementation.id, it.pdoId
                        ).forEach { ip ->
                            ip.active = false
                            ip.updatedAt = Date()
                            repoAwpImplementationPdo.save(ip)
                        }
                    }
                }
            }

            repo.save(data)
            repoAwpImplementation.save(awpImplementation)

            req.newIriIds?.forEach {
                data.iri?.add(
                    repoAwpIri.save(
                        AwpIri(
                            awpId = data.id, iriId = it
                        )
                    )
                )
            }

            req.newPdoIds?.forEach {
                data.pdo?.add(
                    repoAwpPdo.save(
                        AwpPdo(
                            awpId = data.id, pdoId = it
                        )
                    )
                )
            }

            req.newProvincesRegencies?.forEach {
                val awpProvinces = repoAwpProvinces.save(AwpProvinces(awpId = data.id, provinceId = it.provinceId))
                data.provincesRegencies?.add(awpProvinces)

                it.regencyIds?.forEach { r ->
                    awpProvinces.regencies?.add(
                        repoAwpProvincesRegencies.save(
                            AwpProvincesRegencies(
                                awpProvincesId = awpProvinces.id, regencyId = r
                            )
                        )
                    )
                }
            }

            // create awp implementation
            req.newIriIds?.forEach {
                repoAwpImplementationIri.save(
                    AwpImplementationIri(
                        awpImplementationId = awpImplementation.id, iriId = it
                    )
                )
            }

            req.newPdoIds?.forEach {
                repoAwpImplementationPdo.save(
                    AwpImplementationPdo(
                        awpImplementationId = awpImplementation.id, pdoId = it
                    )
                )
            }

            req.newProvincesRegencies?.forEach {
                val awpProvinces = repoAwpImplementationProvinces.save(
                    AwpImplementationProvinces(
                        awpImplementationId = awpImplementation.id, provinceId = it.provinceId
                    )
                )

                it.regencyIds?.forEach { r ->
                    repoAwpImplementationProvincesRegencies.save(
                        AwpImplementationProvincesRegencies(
                            awpImplementationProvincesId = awpProvinces.id, regencyId = r
                        )
                    )
                }
            }

            return if (update == null) {
                responseCreated(data = data)
            } else {
                val ipAdress = httpServletRequest.getHeader("X-FORWARDED-FOR") ?: httpServletRequest.remoteAddr
                val userId = getUserLogin()?.id ?: ""
                Thread {
                    logActivityService.writeChangeData(
                        data,
                        oldData,
                        "awp",
                        ipAdress,
                        userId
                    )
                }.start()

                responseSuccess(data = data)
            }
        } catch (e: Exception) {
            throw e
        }
    }

    private fun validate(request: AwpRequest, update: Awp? = null): Map<String, Any?> {
        val rData = mutableMapOf<String, Any?>()
        try {
            val listMessage: MutableList<ErrorMessage> = emptyList<ErrorMessage>().toMutableList()

            val checkComponent = repoComponent.findByIdAndActive(request.componentId)
            if (!checkComponent.isPresent) {
                listMessage.add(
                    ErrorMessage(
                        "componentId", "Component id ${request.componentId} $VALIDATOR_MSG_NOT_FOUND"
                    )
                )
            }

            if (!request.subComponentId.isNullOrEmpty()) {
                val checkSubComponent = repoComponent.findByIdAndActive(request.subComponentId)
                if (!checkSubComponent.isPresent) {
                    listMessage.add(
                        ErrorMessage(
                            "subComponentId", "Sub Component id ${request.subComponentId} $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                }
            }

            if (!request.subSubComponentId.isNullOrEmpty()) {
                val checkSubSubComponent = repoComponent.findByIdAndActive(request.subSubComponentId)
                if (!checkSubSubComponent.isPresent) {
                    listMessage.add(
                        ErrorMessage(
                            "subSubComponentId",
                            "Sub Sub Component id ${request.subSubComponentId} $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                } else {
                    if (!request.subSubComponentName.isNullOrEmpty()) {
                        checkSubSubComponent.get().updatedAt = Date()
                        checkSubSubComponent.get().description = request.subSubComponentName
                        repoComponent.save(checkSubSubComponent.get())
                    }
                }
            }

            if (!request.componentCodeId.isNullOrEmpty()) {
                val checkComponentCodeId = repoComponent.findByIdAndActive(request.componentCodeId)
                if (!checkComponentCodeId.isPresent) {
                    listMessage.add(
                        ErrorMessage(
                            "componentCodeId", "Component Code id ${request.subSubComponentId} $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                } else {
                    if (!request.name.isNullOrEmpty()) {
                        checkComponentCodeId.get().updatedAt = Date()
                        checkComponentCodeId.get().description = request.name
                        repoComponent.save(checkComponentCodeId.get())
                    }

                    val check = repo.getByYearAndComponentCodeIdAndActive(request.year, request.componentCodeId)
                    if (check.isNotEmpty()) {
                        if (update == null) {
                            listMessage.add(
                                ErrorMessage(
                                    "componentCodeId",
                                    "Awp Kode Kegiatan ${checkComponentCodeId.get().code} Tahun ${request.year} $VALIDATOR_MSG_HAS_ADDED"
                                )
                            )
                        } else {
                            if (check[0].id != update.id) {
                                listMessage.add(
                                    ErrorMessage(
                                        "componentCodeId",
                                        "Awp Kode Kegiatan ${checkComponentCodeId.get().code} Tahun ${request.year} $VALIDATOR_MSG_HAS_ADDED"
                                    )
                                )
                            }
                        }
                    }
                }
            }

            val checkEventManagementType = repoEventManagementType.findByIdAndActive(request.eventManagementTypeId)
            if (!checkEventManagementType.isPresent) {
                listMessage.add(
                    ErrorMessage(
                        "eventManagementTypeId",
                        "Cara Pengelolaan Kegiatan id ${request.eventManagementTypeId} $VALIDATOR_MSG_NOT_FOUND"
                    )
                )
            }

            if (request.activityModeId != null) {
                val checkActivityMode = repoActivityMode.findByIdAndActive(request.activityModeId)
                if (!checkActivityMode.isPresent) {
                    listMessage.add(
                        ErrorMessage(
                            "activityModeId", "Moda Kegiatan id ${request.activityModeId} $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                }
            }

            var dateStart: Date? = null
            var dateEnd: Date? = null
            val dform: DateFormat = SimpleDateFormat("yyyy-MM-dd")

            try {
                dateStart = dform.parse(request.startDate)
            } catch (_: Exception) {
                listMessage.add(
                    ErrorMessage(
                        "startDate", "Tanggal Mulai $VALIDATOR_MSG_NOT_VALID"
                    )
                )
            }

            try {
                dateEnd = dform.parse(request.endDate)
            } catch (_: Exception) {
                listMessage.add(
                    ErrorMessage(
                        "endDate", "Tanggal Selesai $VALIDATOR_MSG_NOT_VALID"
                    )
                )
            }

            if (dateEnd?.before(dateStart) == true) {
                listMessage.add(
                    ErrorMessage(
                        "endDate", "Tanggal Selesai $VALIDATOR_MSG_NOT_VALID"
                    )
                )
            }

            rData["startDate"] = dateStart as Date
            rData["endDate"] = dateEnd as Date

            request.newProvincesRegencies?.forEach {
                val checkProvince = repoProvince.findByIdAndActive(it.provinceId)
                if (!checkProvince.isPresent) {
                    listMessage.add(
                        ErrorMessage(
                            "newProvincesRegencies", "Province id ${it.provinceId} $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                }

                it.regencyIds?.forEach { r ->
                    val checkRegency = repoRegency.findByIdAndActive(r)
                    if (!checkRegency.isPresent) {
                        listMessage.add(
                            ErrorMessage(
                                "newProvincesRegencies", "Regency id $r $VALIDATOR_MSG_NOT_FOUND"
                            )
                        )
                    }
                }
            }

            val checkEnventType = repoEventType.findByIdAndActive(request.eventTypeId)
            if (!checkEnventType.isPresent) {
                listMessage.add(
                    ErrorMessage(
                        "eventTypeId", "Event Type ${request.eventTypeId} $VALIDATOR_MSG_NOT_FOUND"
                    )
                )
            }

            request.newPdoIds?.forEach {
                val checkPdo = repoPdo.findByIdAndActive(it)
                if (!checkPdo.isPresent) {
                    listMessage.add(
                        ErrorMessage(
                            "pdoIds", "Pdo id $it $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                }
            }

            request.newIriIds?.forEach {
                val checkIri = repoIri.findByIdAndActive(it)
                if (!checkIri.isPresent) {
                    listMessage.add(
                        ErrorMessage(
                            "iriIds", "Iri id $it $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                }
            }

            if (update == null) {
                if (request.newProvincesRegencies.isNullOrEmpty()) {
                    listMessage.add(
                        ErrorMessage(
                            "newProvincesRegencies", "Tempat Pelaksanaan $VALIDATOR_MSG_REQUIRED"
                        )
                    )
                }
            }

            if (!request.rabFileId.isNullOrEmpty()) {
                val checkFile: ReturnDataFiles? = RequestHelpers.filesGetById(
                    request.rabFileId ?: "", filesUrl, httpServletRequest.getHeader(HEADER_STRING)
                )

                var checkedFile: Files? = null
                if (checkFile != null) {
                    if (checkFile.data != null) {
                        checkedFile = checkFile.data
                    }
                }

                if (checkedFile == null) {
                    listMessage.add(
                        ErrorMessage(
                            "rabFileId", "Rab File Id ${request.rabFileId} $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                }
            }

            var subSubComponentId = request.subSubComponentId
            if (!request.subSubComponentCode.isNullOrEmpty()) {
                val subComponent = repoComponent.findByCodeYearActive(request.subSubComponentCode, request.year)
                if (!subComponent.isPresent) {
                    val newSubcomponent = repoComponent.save(
                        Component(
                            code = request.subSubComponentCode?.trim(),
                            description = request.subSubComponentName,
                            year = request.year
                        )
                    )

                    subSubComponentId = newSubcomponent.id
                } else {
                    subComponent.get().description = request.subSubComponentName
                    repoComponent.save(subComponent.get())
                    subSubComponentId = subComponent.get().id
                }
            }

            rData["subSubComponentId"] = subSubComponentId

            var componentCodeId = request.componentCodeId
            if (!request.componentCode.isNullOrEmpty()) {
                val componentCode = repoComponent.findByCodeYearActive(request.componentCode, request.year)
                if (!componentCode.isPresent) {
                    val newComponentCode = repoComponent.save(
                        Component(
                            code = request.componentCode?.trim(), description = request.name, year = request.year
                        )
                    )

                    componentCodeId = newComponentCode.id
                } else {
                    componentCode.get().description = request.name
                    repoComponent.save(componentCode.get())
                    componentCodeId = componentCode.get().id
                }
            }

            rData["componentCodeId"] = componentCodeId

            val checkComponentCode = repoComponent.findByIdAndActive(componentCodeId)
            if (!checkComponentCode.isPresent) {
                listMessage.add(
                    ErrorMessage(
                        "componentCodeId", "Kode Kegiatan id $componentCodeId $VALIDATOR_MSG_NOT_FOUND"
                    )
                )
            } else {
                val checkAwpName =
                    repo.findByNameComponentCodeYearActive(request.name, request.componentCodeId, request.year)
                if (checkAwpName.isPresent) {
                    val checkMessage = ErrorMessage(
                        "name",
                        "Nama AWP ${request.name} Kode Kegiatan ${checkComponentCode.get().code} Tahun ${request.year} $VALIDATOR_MSG_HAS_ADDED"
                    )

                    if (update == null) {
                        listMessage.add(checkMessage)
                    } else {
                        if (update.id != checkAwpName.get().id) {
                            listMessage.add(checkMessage)
                        }
                    }
                }
            }

            rData["listMessage"] = listMessage
        } catch (e: Exception) {
            throw e
        }

        return rData
    }

    fun componentSubccomponentFixing(): ResponseEntity<ReturnData> {
        try {
            var updateComponentCount = 0
            var updateSubcomponentCount = 0
            val objectMapper = ObjectMapper()

            repo.getByActive().forEach {
                var updateComponent = false
                var updateSubComponent = false

                if (it.componentId != null) {
                    if (it.year != it.component?.year) {
                        val component: Component
                        val checkComponent = repoComponent.findByCodeYearActive(it.component?.code, it.year)
                        component = if (checkComponent.isPresent) {
                            checkComponent.get()
                        } else {
                            repoComponent.save(
                                Component(
                                    code = it.component?.code, description = it.component?.description, year = it.year
                                )
                            )
                        }

                        it.componentId = component.id
                        repo.save(it)
                        println("updateComponent : ${objectMapper.writeValueAsString(component)}")
                        updateComponent = true
                        updateComponentCount++
                    }

                    if (it.subComponentId != null) {
                        if (it.year != it.subComponent?.year) {
                            val subComponent: Component
                            val checkSubcomponent = repoComponent.findByCodeYearActive(it.subComponent?.code, it.year)
                            subComponent = if (checkSubcomponent.isPresent) {
                                checkSubcomponent.get()
                            } else {
                                repoComponent.save(
                                    Component(
                                        code = it.subComponent?.code,
                                        description = it.subComponent?.description,
                                        year = it.year
                                    )
                                )
                            }

                            it.subComponentId = subComponent.id
                            repo.save(it)
                            println("updateSubComponent : ${objectMapper.writeValueAsString(subComponent)}")
                            updateSubComponent = true
                            updateSubcomponentCount++
                        }
                    }
                }

                if (updateComponent || updateSubComponent) {
                    val awpImplementation = repoAwpImplementation.findByAwpIdAndActive(it.id)
                    if (awpImplementation.isPresent) {
                        if (updateComponent) {
                            awpImplementation.get().componentId = it.componentId
                        }
                        if (updateSubComponent) {
                            awpImplementation.get().subComponentId = it.subComponentId
                        }

                        repoAwpImplementation.save(awpImplementation.get())
                    }
                }
            }

            return responseSuccess(
                data = mapOf(
                    "updateComponentCount" to updateComponentCount, "updateSubcomponentCount" to updateSubcomponentCount
                )
            )
        } catch (e: Exception) {
            throw e
        }
    }
}

