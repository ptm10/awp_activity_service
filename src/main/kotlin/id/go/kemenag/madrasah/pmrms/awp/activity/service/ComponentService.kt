package id.go.kemenag.madrasah.pmrms.awp.activity.service

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import id.go.kemenag.madrasah.pmrms.awp.activity.constant.COMPONENT_OPTIONS
import id.go.kemenag.madrasah.pmrms.awp.activity.constant.COMPONENT_PARRENTS
import id.go.kemenag.madrasah.pmrms.awp.activity.constant.ROLE_ID_ADMINSTRATOR
import id.go.kemenag.madrasah.pmrms.awp.activity.constant.VALIDATOR_MSG_HAS_ADDED
import id.go.kemenag.madrasah.pmrms.awp.activity.helpers.*
import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.ParamArray
import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.Sort
import id.go.kemenag.madrasah.pmrms.awp.activity.model.response.ErrorMessage
import id.go.kemenag.madrasah.pmrms.awp.activity.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.Component
import id.go.kemenag.madrasah.pmrms.awp.activity.repository.ComponentRepository
import id.go.kemenag.madrasah.pmrms.awp.activity.repository.native.ComponentRepositoryNative
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import javax.servlet.http.HttpServletRequest

@Suppress("UNCHECKED_CAST", "NAME_SHADOWING")
@Service
class ComponentService {

    @Autowired
    private lateinit var repo: ComponentRepository

    @Autowired
    private lateinit var repoNative: ComponentRepositoryNative

    @Autowired
    private lateinit var logActivityService: LogActivityService

    @Autowired
    private lateinit var httpServletRequest: HttpServletRequest

    fun groupList(year: Int?, viewRole: Boolean = true): ResponseEntity<ReturnData> {
        try {
            var userComponents = getUserComponetCodes()
            if (userHasRoles(listOf(ROLE_ID_ADMINSTRATOR)) || getUserLogin()?.component?.code == "4.4" || !viewRole) {
                userComponents = COMPONENT_PARRENTS
            }

            val components: List<Component> = repo.getByUserComponentCodes(userComponents)
            val parents: List<Component> = components.filter {
                userComponents.contains(it.code)
            }

            val datas: MutableList<ComponentSubComponent> = emptyList<ComponentSubComponent>().toMutableList()

            val dform: DateFormat = SimpleDateFormat("yyyy")

            val findYear = year ?: dform.format(Date()).toInt()

            val maxDotLengt = repo.getMaxDotCode()

            parents.forEach {
                val componentSubComponent = ComponentSubComponent()
                componentSubComponent.id = it.id
                componentSubComponent.code = it.code
                componentSubComponent.description = it.description
                componentSubComponent.createdAt = it.createdAt
                componentSubComponent.updatedAt = it.updatedAt
                componentSubComponent.subComponent = findSubComponent(it, findYear, maxDotLengt)

                datas.add(componentSubComponent)
            }

            return responseSuccess(data = datas)
        } catch (e: Exception) {
            throw e
        }
    }

    private fun findSubComponent(
        component: Component,
        findYear: Int,
        maxDotLengt: Int,
    ): List<ComponentSubComponent> {
        val datas: MutableList<ComponentSubComponent> = emptyList<ComponentSubComponent>().toMutableList()
        try {
            repoNative.getSubComponent(component.code ?: "", findYear, repo.getMaxDotCode()).forEach {
                val componentSubComponent = ComponentSubComponent()
                componentSubComponent.id = it.id
                componentSubComponent.code = it.code
                componentSubComponent.description = it.description
                componentSubComponent.createdAt = it.createdAt
                componentSubComponent.updatedAt = it.updatedAt
                componentSubComponent.subComponent = findSubComponent(it, findYear, maxDotLengt)

                datas.add(componentSubComponent)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return datas
    }

    fun datatable(req: Pagination2Request): ResponseEntity<ReturnData> {
        return try {
            repoNative.byUserComponentCode = false
            req.paramIn?.add(ParamArray("code", "string", COMPONENT_OPTIONS))
            req.sort?.add(Sort("code", "asc"))
            responseSuccess(data = repoNative.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }

    fun getSubComponent(id: String?, year: Int?): ResponseEntity<ReturnData> {
        try {
            val dform: DateFormat = SimpleDateFormat("yyyy")
            val component = repo.findByIdAndActive(id)
            val findYear = year ?: dform.format(Date()).toInt()
            return responseSuccess(
                data = repoNative.getSubComponent(component.get().code ?: "", findYear, repo.getMaxDotCode())
            )
        } catch (e: Exception) {
            throw e
        }
    }

    fun datatableParrent(req: Pagination2Request): ResponseEntity<ReturnData> {
        return try {
            repoNative.byUserComponentCode = false
            req.paramIn?.add(ParamArray("code", "string", COMPONENT_PARRENTS))
            req.sort?.add(Sort("code", "asc"))
            responseSuccess(data = repoNative.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }

    fun saveData(request: Component): ResponseEntity<ReturnData> {
        try {
            return saveOrUpdate(request)
        } catch (e: Exception) {
            throw e
        }
    }

    fun updateData(id: String, request: Component): ResponseEntity<ReturnData> {
        val update = repo.findByIdAndActive(id)
        return if (update.isPresent) {
            saveOrUpdate(request, update.get())
        } else {
            responseNotFound()
        }
    }

    fun saveOrUpdate(req: Component, update: Component? = null): ResponseEntity<ReturnData> {
        var oldData: Component? = null
        if (update != null) {
            val objectMapper = ObjectMapper()
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            oldData = objectMapper.readValue(
                objectMapper.writeValueAsString(update), Component::class.java
            ) as Component
        }

        try {
            val validate = validate(req, update)
            val listMessage = validate["listMessage"] as List<ErrorMessage>
            if (listMessage.isNotEmpty()) {
                return responseBadRequest(data = listMessage)
            }

            val data: Component?
            data = update ?: Component()

            data.code = req.code
            data.year = req.year
            data.description = req.description

            if (update == null) {
                data.createdAt = Date()
                data.updatedAt = Date()
            } else {
                data.updatedAt = Date()
            }

            repo.save(data)

            return if (update == null) {
                responseCreated(data = data)
            } else {
                val ipAdress = httpServletRequest.getHeader("X-FORWARDED-FOR") ?: httpServletRequest.remoteAddr
                val userId = getUserLogin()?.id ?: ""
                Thread {
                    logActivityService.writeChangeData(
                        data,
                        oldData,
                        "component",
                        ipAdress,
                        userId
                    )
                }.start()

                responseSuccess(data = data)
            }
        } catch (e: Exception) {
            throw e
        }
    }

    private fun validate(request: Component, update: Component? = null): Map<String, Any?> {
        val rData = mutableMapOf<String, Any?>()
        try {
            val listMessage: MutableList<ErrorMessage> = emptyList<ErrorMessage>().toMutableList()

            val check = repo.findByCodeYearActive(request.code, request.year)
            if (check.isPresent) {
                val message = ErrorMessage(
                    "code", "Kode Komponen ${request.code} Tahun ${request.year} $VALIDATOR_MSG_HAS_ADDED"
                )
                if (update == null) {
                    listMessage.add(message)
                } else {
                    if (check.get().id != update.id) {
                        listMessage.add(message)
                    }
                }
            }

            rData["listMessage"] = listMessage
        } catch (e: Exception) {
            throw e
        }

        return rData
    }


    data class ComponentSubComponent(
        var id: String? = null,

        var code: String? = null,

        var description: String? = null,

        var createdAt: Date? = Date(),

        var updatedAt: Date? = Date(),

        var subComponent: List<ComponentSubComponent> = emptyList()
    )
}
