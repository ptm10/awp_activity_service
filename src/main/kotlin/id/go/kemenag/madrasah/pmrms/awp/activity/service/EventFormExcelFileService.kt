package id.go.kemenag.madrasah.pmrms.awp.activity.service

import id.go.kemenag.madrasah.pmrms.awp.activity.constant.EVENT_FORM_EXCEL_FILE_EMAIL_STATUS_FAILED
import id.go.kemenag.madrasah.pmrms.awp.activity.constant.EVENT_FORM_EXCEL_FILE_EMAIL_STATUS_SUCCESS
import id.go.kemenag.madrasah.pmrms.awp.activity.helpers.responseSuccess
import id.go.kemenag.madrasah.pmrms.awp.activity.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.awp.activity.repository.EventFormExcelFileRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service

@Suppress("UNCHECKED_CAST", "NAME_SHADOWING")
@Service
class EventFormExcelFileService {

    @Autowired
    private lateinit var repo: EventFormExcelFileRepository

    fun result(eventId: String?, createdFor: Int?): ResponseEntity<ReturnData> {
        try {
            val data = repo.findByEventIdAndCreatedForAndActive(eventId, createdFor)
            val res = EventFormExcelFileResult()

            if (data.isPresent) {
                res.fileName = data.get().file?.filename
                res.successCount = data.get().emails?.count {
                    it.emailSentStatus == EVENT_FORM_EXCEL_FILE_EMAIL_STATUS_SUCCESS
                }?.toLong() ?: 0

                data.get().emails?.filter { ef -> ef.emailSentStatus == EVENT_FORM_EXCEL_FILE_EMAIL_STATUS_FAILED }
                    ?.forEach {
                        res.failed?.add(it.email ?: "")
                    }
            }

            return responseSuccess(data = res)
        } catch (e: Exception) {
            throw e
        }
    }

    data class EventFormExcelFileResult(
        var fileName: String? = null,
        var successCount: Long? = 0,
        var failed: MutableList<String>? = mutableListOf(),
    )
}
