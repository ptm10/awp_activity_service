package id.go.kemenag.madrasah.pmrms.awp.activity.service

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import id.go.kemenag.madrasah.pmrms.awp.activity.constant.VALIDATOR_MSG_NOT_FOUND
import id.go.kemenag.madrasah.pmrms.awp.activity.helpers.*
import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.EventFormRequest
import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.awp.activity.model.response.ErrorMessage
import id.go.kemenag.madrasah.pmrms.awp.activity.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.EventForm
import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.EventQuestion
import id.go.kemenag.madrasah.pmrms.awp.activity.repository.EventFormRepository
import id.go.kemenag.madrasah.pmrms.awp.activity.repository.EventQuestionRepository
import id.go.kemenag.madrasah.pmrms.awp.activity.repository.EventTypeRepository
import id.go.kemenag.madrasah.pmrms.awp.activity.repository.native.EventFormRepositoryNative
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import java.util.*
import javax.servlet.http.HttpServletRequest

@Suppress("UNCHECKED_CAST")
@Service
class EventFormService {

    @Autowired
    private lateinit var repo: EventFormRepository

    @Autowired
    private lateinit var repoNative: EventFormRepositoryNative

    @Autowired
    private lateinit var repoEventQuestion: EventQuestionRepository

    @Autowired
    private lateinit var repoEventType: EventTypeRepository

    @Autowired
    private lateinit var logActivityService: LogActivityService

    @Autowired
    private lateinit var httpServletRequest: HttpServletRequest

    fun saveData(request: EventFormRequest): ResponseEntity<ReturnData> {
        try {
            return saveOrUpdate(request)
        } catch (e: Exception) {
            throw e
        }
    }

    fun updateData(id: String, request: EventFormRequest): ResponseEntity<ReturnData> {
        val update = repo.findByIdAndActive(id)
        return if (update.isPresent) {
            saveOrUpdate(request, update.get())
        } else {
            responseNotFound()
        }
    }

    fun saveOrUpdate(
        req: EventFormRequest, update: EventForm? = null
    ): ResponseEntity<ReturnData> {
        var oldData: EventForm? = null
        if (update != null) {
            val objectMapper = ObjectMapper()
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            oldData = objectMapper.readValue(
                objectMapper.writeValueAsString(update), EventForm::class.java
            ) as EventForm
        }

        try {
            val validate = validate(req, update)
            val listMessage = validate["listMessage"] as List<ErrorMessage>
            if (listMessage.isNotEmpty()) {
                return responseBadRequest(data = listMessage)
            }

            val data: EventForm?
            if (update != null) {
                data = update
                data.updatedAt = Date()
            } else {
                val datas = repo.getByEventTypeIdAndActive(req.eventTypeId)
                data = if (datas.isNotEmpty()) {
                    datas[0]
                } else {
                    EventForm()
                }

                data.eventTypeId = req.eventTypeId
            }

            repo.save(data)

            req.newQuestions?.forEach {
                var save = EventQuestion()
                val check = repoEventQuestion.eventFormIdCreatedForQuestionTypeQuestionCategoryQuestion(
                    data.id,
                    it.createdFor,
                    it.questionType,
                    it.questionCategory,
                    it.question
                )
                if (check.isPresent) {
                    save = check.get()
                }

                save.eventFormId = data.id
                save.createdFor = it.createdFor
                save.questionType = it.questionType
                save.questionCategory = it.questionCategory
                save.question = it.question?.trim()
                repoEventQuestion.save(save)
            }

            req.deletedQuestionIds?.forEach {
                val del = repoEventQuestion.findByIdAndActive(it)
                if (del.isPresent) {
                    del.get().active = false
                    del.get().updatedAt = Date()
                    repoEventQuestion.save(del.get())
                }
            }

            return if (update == null) {
                responseCreated(data = data)
            } else {
                val ipAdress = httpServletRequest.getHeader("X-FORWARDED-FOR") ?: httpServletRequest.remoteAddr
                val userId = getUserLogin()?.id ?: ""
                Thread {
                    logActivityService.writeChangeData(
                        data,
                        oldData,
                        "event_form",
                        ipAdress,
                        userId
                    )
                }.start()

                responseSuccess(data = data)
            }
        } catch (e: Exception) {
            throw e
        }
    }

    fun getDetail(id: String): ResponseEntity<ReturnData> {
        try {
            val data = repo.findByIdAndActive(id)
            if (!data.isPresent) {
                return responseNotFound()
            }

            return responseSuccess(data = data.get())
        } catch (e: Exception) {
            throw e
        }
    }

    fun datatable(req: Pagination2Request): ResponseEntity<ReturnData> {
        return try {
            responseSuccess(data = repoNative.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }

    fun delete(id: String): ResponseEntity<ReturnData> {
        try {
            val data = repo.findByIdAndActive(id)
            if (data.isPresent) {
                data.get().active = false
                data.get().updatedAt = Date()
                repo.save(data.get())

                data.get().questions?.forEach {
                    it.active = false
                    it.updatedAt = Date()
                    repoEventQuestion.save(it)
                }

                return responseSuccess()
            }
            return responseNotFound()
        } catch (e: Exception) {
            throw e
        }
    }

    private fun validate(
        request: EventFormRequest, update: EventForm? = null
    ): Map<String, Any?> {
        val rData = mutableMapOf<String, Any?>()
        try {
            val listMessage: MutableList<ErrorMessage> = emptyList<ErrorMessage>().toMutableList()

            if (!request.eventTypeId.isNullOrEmpty()) {
                val checkEventType = repoEventType.findByIdAndActive(request.eventTypeId)
                if (!checkEventType.isPresent) {
                    listMessage.add(
                        ErrorMessage(
                            "eventTypeId", "Event Type Id ${request.eventTypeId} $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                }
            }


            rData["listMessage"] = listMessage
        } catch (e: Exception) {
            throw e
        }
        return rData
    }
}
