package id.go.kemenag.madrasah.pmrms.awp.activity.service

import id.go.kemenag.madrasah.pmrms.awp.activity.helpers.responseSuccess
import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.awp.activity.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.awp.activity.repository.native.EventManagementTypeRepositoryNative
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service

@Service
class EventManagementTypeService {

    @Autowired
    private lateinit var repoNative: EventManagementTypeRepositoryNative

    fun datatable(req: Pagination2Request): ResponseEntity<ReturnData> {
        try {
            return responseSuccess(data = repoNative.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }
}
