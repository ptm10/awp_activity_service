package id.go.kemenag.madrasah.pmrms.awp.activity.service

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import id.go.kemenag.madrasah.pmrms.awp.activity.constant.*
import id.go.kemenag.madrasah.pmrms.awp.activity.exception.UnautorizedException
import id.go.kemenag.madrasah.pmrms.awp.activity.helpers.*
import id.go.kemenag.madrasah.pmrms.awp.activity.model.ReturnDataFiles
import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.*
import id.go.kemenag.madrasah.pmrms.awp.activity.model.response.ErrorMessage
import id.go.kemenag.madrasah.pmrms.awp.activity.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.*
import id.go.kemenag.madrasah.pmrms.awp.activity.repository.*
import id.go.kemenag.madrasah.pmrms.awp.activity.repository.native.EventProgressReportRepositoryNative
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import javax.servlet.http.HttpServletRequest


@Suppress("UNCHECKED_CAST")
@Service
class EventProgressReportService {

    @Autowired
    private lateinit var repoNative: EventProgressReportRepositoryNative

    @Autowired
    private lateinit var repo: EventProgressReportRepository

    @Autowired
    private lateinit var repoComponent: ComponentRepository

    @Autowired
    private lateinit var repoEvent: EventRepository

    @Autowired
    private lateinit var repoProvince: ProvinceRepository

    @Autowired
    private lateinit var repoRegency: RegencyRepository

    @Autowired
    private lateinit var httpServletRequest: HttpServletRequest

    @Autowired
    private lateinit var repoResources: ResourcesRepository

    @Autowired
    private lateinit var repoEventProgressReportStaff: EventProgressReportStaffRepository

    @Autowired
    private lateinit var repoEventProgressReportOtherStaff: EventProgressReportOtherStaffRepository

    @Autowired
    private lateinit var repoEventProgressReportDocuments: EventProgressReportDocumentsRepository

    @Autowired
    private lateinit var repoEventProgressReportApprovalNote: EventProgressReportApprovalNoteRepository

    @Autowired
    private lateinit var serviceTask: TaskService

    @Autowired
    private lateinit var repoActivityMode: ActivityModeRepository

    @Autowired
    private lateinit var repoEventStaff: EventStaffRepository

    @Autowired
    private lateinit var repoEventDocumentType: EventDocumentTypeRepository

    @Autowired
    private lateinit var logActivityService: LogActivityService

    @Value("\${url.files}")
    private lateinit var filesUrl: String

    fun getDetail(id: String): ResponseEntity<ReturnData> {
        try {
            val data = repo.findByIdAndActive(id)
            if (!data.isPresent) {
                return responseNotFound()
            }

            // creator allow edit
            if (data.get().createdBy == getUserLogin()?.id) {
                if (data.get().status == EVENT_PROGRESS_REPORT_STATUS_NEW || data.get().status == EVENT_PROGRESS_REPORT_STATUS_REVISION) {
                    data.get().creatorAllowedEdit = true
                }
            }

            // approve by consultan
            val consultanId = data.get().event?.createdBy
            if (getUserLogin()?.id == consultanId) {
                val reports = repo.getByEventIdStatus(data.get().eventId)
                if (reports.isNotEmpty()) {
                    if (data.get().id == reports[0].id) {
                        if (data.get().status == EVENT_PROGRESS_REPORT_STATUS_NEW) {
                            data.get().consultanAllowedApprove = true
                        }
                    }
                }
            }

            return responseSuccess(data = data)
        } catch (e: Exception) {
            throw e
        }
    }

    fun lastReport(eventId: String?): ResponseEntity<ReturnData> {
        try {
            val data = repo.lastReportWithoutUser(eventId)
            if (data.isEmpty()) {
                return responseSuccess(
                    data = EventProgressReport(
                        id = null, status = null, createdAt = null, updatedAt = null
                    )
                )
            }

            return responseSuccess(data = data[0])
        } catch (e: Exception) {
            throw e
        }
    }

    fun datatableAll(req: Pagination2Request): ResponseEntity<ReturnData> {
        return try {
            responseSuccess(data = repoNative.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }

    fun datatable(req: Pagination2Request): ResponseEntity<ReturnData> {
        return try {
            if (!userHasRoles(listOf(ROLE_ID_ADMINSTRATOR))) {
                if (!userHasRoles(listOf(ROLE_ID_PCU))) {
                    if (getUserLogin()?.component?.code != "4.4") {
                        var componentId: String? = getUserLogin()?.component?.id
                        if (getUserLogin()?.component?.code?.contains("4.") == true) {
                            val findComponent = repoComponent.findByCodeAndActive("4")
                            if (findComponent.isPresent) {
                                componentId = findComponent.get().id
                            }
                        }

                        if (componentId != null) {
                            req.paramIs?.add(ParamSearch("event.awpImplementation.componentId", "string", componentId))
                        } else {
                            req.paramIs?.add(
                                ParamSearch(
                                    "event.awpImplementation.componentId",
                                    "string",
                                    System.currentTimeMillis().toString()
                                )
                            )
                        }
                    }
                } else {
                    val ids = repoEventStaff.getEventIdsByResourcesId(getUserLogin()?.resourcesId)
                    if (ids.isNotEmpty()) {
                        req.paramIn?.add(ParamArray("eventId", "string", ids))
                    } else {
                        req.paramIs?.add(ParamSearch("eventId", "string", System.currentTimeMillis().toString()))
                    }
                }
            }
            responseSuccess(data = repoNative.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }

    fun updateData(id: String, request: EventProgressReportRequest): ResponseEntity<ReturnData> {
        val update = repo.findByIdAndActive(id)
        return if (update.isPresent) {
            saveOrUpdate(request, update.get())
        } else {
            responseNotFound()
        }
    }

    fun saveData(req: EventProgressReportRequest): ResponseEntity<ReturnData> {
        return saveOrUpdate(req)
    }

    fun saveOrUpdate(
        req: EventProgressReportRequest, update: EventProgressReport? = null
    ): ResponseEntity<ReturnData> {
        var oldData: EventProgressReport? = null
        if (update != null) {
            val objectMapper = ObjectMapper()
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            oldData = objectMapper.readValue(
                objectMapper.writeValueAsString(update), EventProgressReport::class.java
            ) as EventProgressReport
        }

        try {
            if (!userHasRoles(listOf(ROLE_ID_STAFF, ROLE_ID_ASSISTANT, ROLE_ID_PCU))) {
                throw UnautorizedException(message = VALIDATOR_MSG_NOT_HAVE_ACCESS)
            }

            val validate = validate(req, update)
            val listMessage = validate["listMessage"] as List<ErrorMessage>
            if (listMessage.isNotEmpty()) {
                return responseBadRequest(data = listMessage)
            }

            val event = validate["event"] as Event

            if (!userHasRoles(listOf(ROLE_ID_PCU))) {
                if (getUserLogin()?.componentId != event.awpImplementation?.componentId) {
                    throw UnautorizedException(message = VALIDATOR_MSG_NOT_HAVE_ACCESS)
                }
            }

            val data: EventProgressReport?
            data = update ?: EventProgressReport()

            if (update != null) {
                if (getUserLogin()?.id != data.createdBy) {
                    throw UnautorizedException(message = VALIDATOR_MSG_NOT_HAVE_ACCESS)
                }
            }

            data.eventId = req.eventId
            data.event = event
            data.startDate = validate["startDate"] as Date
            data.endDate = validate["endDate"] as Date
            data.eventOtherInformation = req.eventOtherInformation
            data.locationOtherInformation = req.locationOtherInformation
            data.eventOutput = req.eventOutput
            data.participantMale = req.participantMale
            data.participantFemale = req.participantFemale
            data.participantCount = req.participantCount
            data.participantOtherInformation = req.participantOtherInformation
            data.participantTarget = req.participantTarget
            data.provinceId = req.provinceId
            data.regencyId = req.regencyId
            data.location = req.location
            data.summary = req.summary
            data.activityModeId = req.activityModeId
            data.attachmentFileId = req.attachmentFileId
            data.budgetAvailable = req.budgetAvailable
            data.pokEvent = req.pokEvent
            data.rraFileId = req.rraFileId

            var taskTitle = "Membuat"
            if (update != null) {
                data.updatedBy = getUserLogin()?.id
                data.updatedAt = Date()

                if (data.status == EVENT_PROGRESS_REPORT_STATUS_REVISION) {
                    taskTitle = "Merevisi"

                    serviceTask.approveRejectFromTask(
                        APPROVAL_STATUS_APPROVE,
                        data.id,
                        TASK_TYPE_EVENT_PROGRESS_REPORT,
                        getUserLogin()?.id,
                        TASK_NUMBER_TASK_REVISION
                    )
                } else {
                    data.status = EVENT_PROGRESS_REPORT_STATUS_NEW
                }

                update.staff?.forEach {
                    if (req.deletedStaffIds?.contains(it.id) == true) {
                        it.active = false
                        it.updatedAt = Date()
                        repoEventProgressReportStaff.save(it)
                    }
                }

                update.otherStaff?.forEach {
                    if (req.deletedOtherStaffIds?.contains(it.id) == true) {
                        it.active = false
                        it.updatedAt = Date()
                        repoEventProgressReportOtherStaff.save(it)
                    }
                }

                update.documents?.forEach {
                    if (req.deletedDocumentIds?.contains(it.id) == true) {
                        it.active = false
                        it.updatedAt = Date()
                        repoEventProgressReportDocuments.save(it)
                    }
                }
            } else {
                data.createdBy = getUserLogin()?.id
            }

            repo.save(data)

            val resourcesList = validate["resourcesList"] as List<Resources>

            req.newStaff?.forEach {
                data.staff?.add(
                    repoEventProgressReportStaff.save(
                        EventProgressReportStaff(eventProgressReportId = data.id,
                            resourcesId = it,
                            resources = resourcesList.find { fr ->
                                fr.id == it
                            })
                    )
                )
            }

            req.newOtherStaff?.forEach {
                data.otherStaff?.add(
                    repoEventProgressReportOtherStaff.save(
                        EventProgressReportOtherStaff(
                            eventProgressReportId = data.id,
                            name = it.name,
                            position = it.position,
                            institution = it.institution
                        )
                    )
                )
            }

            val documents = validate["documents"] as List<EventProgressReportDocuments>

            documents.forEach {
                data.documents?.add(
                    repoEventProgressReportDocuments.save(
                        EventProgressReportDocuments(
                            eventProgressReportId = data.id,
                            eventDocumentTypeId = it.eventDocumentTypeId,
                            title = it.title,
                            date = it.date,
                            place = it.place,
                            description = it.description,
                            fileId = it.fileId
                        )
                    )
                )
            }

            val taskDescription = "$taskTitle Laporan Kemajuan Event ${data.event?.name}"

            event.status = EVENT_STATUS_IMPLEMENTATION

            serviceTask.createOrUpdateFromTask(
                createdBy = getUserLogin()?.id,
                taskId = data.id,
                taskType = TASK_TYPE_EVENT_PROGRESS_REPORT,
                description = taskDescription,
                createdFor = data.event?.createdBy
            )

            if (data.status == EVENT_PROGRESS_REPORT_STATUS_REVISION && oldData != null) {
                if (compareDataChange(oldData, data)) {
                    data.status = EVENT_PROGRESS_REPORT_STATUS_NEW
                    repo.save(data)
                }
            }

            return if (update == null) {
                responseCreated(data = data)
            } else {
                val ipAdress = httpServletRequest.getHeader("X-FORWARDED-FOR") ?: httpServletRequest.remoteAddr
                val userId = getUserLogin()?.id ?: ""
                Thread {
                    logActivityService.writeChangeData(
                        data,
                        oldData,
                        "event_progress_report",
                        ipAdress,
                        userId
                    )
                }.start()

                responseSuccess(data = data)
            }
        } catch (e: Exception) {
            throw e
        }
    }

    fun approveByConsultan(request: ApproveRequest): ResponseEntity<ReturnData> {
        try {
            if (!userHasRoles(listOf(ROLE_ID_CONSULTAN, ROLE_ID_COORDINATOR))) {
                throw UnautorizedException(message = VALIDATOR_MSG_NOT_HAVE_ACCESS)
            }

            val find = repo.findByIdAndActive(request.id)
            if (!find.isPresent) {
                return responseNotFound()
            }

            if (find.get().event?.createdBy != getUserLogin()?.id) {
                throw UnautorizedException(message = VALIDATOR_MSG_NOT_HAVE_ACCESS)
            }

            var status = EVENT_PROGRESS_REPORT_STATUS_APPROVED_BY_CONSULTAN
            if (request.approveStatus == APPROVAL_STATUS_REJECT) {
                status = EVENT_PROGRESS_REPORT_STATUS_REVISION

                serviceTask.createRevisionTask(
                    taskId = find.get().id,
                    taskType = TASK_TYPE_EVENT_PROGRESS_REPORT,
                    createdFor = getUserLogin()?.id,
                    description = "Revisi Laporan Kemajuan Event ${find.get().event?.name}"
                )
            }

            find.get().status = status
            find.get().updatedBy = getUserLogin()?.id
            find.get().updatedAt = Date()
            repo.save(find.get())

            var createdFrom = EVENT_REPORT_APPROVAL_MESSAGE_FROM_CONSULTAN
            if (userHasRoles(listOf(ROLE_ID_COORDINATOR))) {
                createdFrom = EVENT_REPORT_APPROVAL_MESSAGE_FROM_COORDINATOR
            }

            repoEventProgressReportApprovalNote.save(
                EventProgressReportApprovalNote(
                    eventProgressReportId = find.get().id,
                    userId = getUserLogin()?.id,
                    createdFrom = createdFrom,
                    note = request.note,
                    approvalStatus = request.approveStatus,
                )
            )

            serviceTask.approveRejectFromTask(
                request.approveStatus,
                find.get().id,
                TASK_TYPE_EVENT_PROGRESS_REPORT,
                getUserLogin()?.id,
                TASK_NUMBER_TASK
            )

            return responseSuccess()
        } catch (e: Exception) {
            throw e
        }
    }

    private fun validate(
        request: EventProgressReportRequest, update: EventProgressReport? = null
    ): Map<String, Any?> {
        val rData = mutableMapOf<String, Any?>()
        try {
            val listMessage: MutableList<ErrorMessage> = emptyList<ErrorMessage>().toMutableList()

            val event = repoEvent.findByIdAndActive(request.eventId)
            if (!event.isPresent) {
                listMessage.add(
                    ErrorMessage(
                        "eventId", "Event id ${request.eventId} $VALIDATOR_MSG_NOT_FOUND"
                    )
                )
            } else {
                rData["event"] = event.get()
            }

            var dateStart: Date? = null
            var dateEnd: Date? = null
            val dform: DateFormat = SimpleDateFormat("yyyy-MM-dd")

            try {
                dateStart = dform.parse(request.startDate)
            } catch (_: Exception) {
                listMessage.add(
                    ErrorMessage(
                        "startDate", "Tanggal Mulai $VALIDATOR_MSG_NOT_VALID"
                    )
                )
            }

            try {
                dateEnd = dform.parse(request.endDate)
            } catch (_: Exception) {
                listMessage.add(
                    ErrorMessage(
                        "endDate", "Tanggal Selesai $VALIDATOR_MSG_NOT_VALID"
                    )
                )
            }

            if (dateEnd?.before(dateStart) == true) {
                listMessage.add(
                    ErrorMessage(
                        "endDate", "Tanggal Selesai $VALIDATOR_MSG_NOT_VALID"
                    )
                )
            }

            rData["startDate"] = dateStart as Date
            rData["endDate"] = dateEnd as Date


            val province = repoProvince.findByIdAndActive(request.provinceId)
            if (!province.isPresent) {
                listMessage.add(
                    ErrorMessage(
                        "provinceId", "Provinsi id ${request.provinceId} $VALIDATOR_MSG_NOT_FOUND"
                    )
                )
            }

            if (!request.regencyId.isNullOrEmpty()) {
                val regency = repoRegency.findByIdAndActive(request.regencyId)
                if (!regency.isPresent) {
                    listMessage.add(
                        ErrorMessage(
                            "regencyId", "Regency id ${request.regencyId} $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                }
            }

            if (!request.attachmentFileId.isNullOrEmpty()) {
                val checkFile: ReturnDataFiles? = RequestHelpers.filesGetById(
                    request.attachmentFileId ?: "", filesUrl, httpServletRequest.getHeader(HEADER_STRING)
                )

                var checkedFile: Files? = null
                if (checkFile != null) {
                    if (checkFile.data != null) {
                        checkedFile = checkFile.data
                    }
                }

                if (checkedFile == null) {
                    listMessage.add(
                        ErrorMessage(
                            "attachmentFileId",
                            "Attachment File Id ${request.attachmentFileId} $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                }
            }

            val resourcesList = mutableListOf<Resources>()
            request.newStaff?.forEach {
                val resources = repoResources.findByIdAndActive(it)
                if (!resources.isPresent) {
                    listMessage.add(
                        ErrorMessage(
                            "newStaff", "Resources id $it $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                } else {
                    resourcesList.add(resources.get())
                }
            }
            rData["resourcesList"] = resourcesList

            val documents = mutableListOf<EventProgressReportDocuments>()
            request.newDocuments?.forEach {
                var date: Date? = null
                try {
                    date = dform.parse(it.date)
                } catch (_: Exception) {
                    listMessage.add(
                        ErrorMessage(
                            "newDocuments", "Tanggal $VALIDATOR_MSG_NOT_VALID"
                        )
                    )
                }

                val checkFile: ReturnDataFiles? = RequestHelpers.filesGetById(
                    it.fileId ?: "", filesUrl, httpServletRequest.getHeader(HEADER_STRING)
                )

                var checkedFile: Files? = null
                if (checkFile != null) {
                    if (checkFile.data != null) {
                        checkedFile = checkFile.data
                    }
                }

                if (checkedFile == null) {
                    listMessage.add(
                        ErrorMessage(
                            "newDocuments", "File id ${it.fileId} $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                }

                if (!it.eventDocumentTypeId.isNullOrEmpty()) {
                    val check = repoEventDocumentType.findByIdAndActive(it.eventDocumentTypeId)
                    if (!check.isPresent) {
                        listMessage.add(
                            ErrorMessage(
                                "newDocuments", "File id ${it.fileId} $VALIDATOR_MSG_NOT_FOUND"
                            )
                        )
                    }
                }


                documents.add(
                    EventProgressReportDocuments(
                        title = it.title,
                        eventDocumentTypeId = it.eventDocumentTypeId,
                        date = date,
                        place = it.place,
                        description = it.description,
                        fileId = it.fileId
                    )
                )
            }

            rData["documents"] = documents

            if (!request.rraFileId.isNullOrEmpty()) {
                val checkFile: ReturnDataFiles? = RequestHelpers.filesGetById(
                    request.rraFileId ?: "", filesUrl, httpServletRequest.getHeader(HEADER_STRING)
                )

                var checkedFile: Files? = null
                if (checkFile != null) {
                    if (checkFile.data != null) {
                        checkedFile = checkFile.data
                    }
                }

                if (checkedFile == null) {
                    listMessage.add(
                        ErrorMessage(
                            "rraFileId", "Rincian Realisai Anggaran ${request.rraFileId} $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                }
            }

            if (!request.activityModeId.isNullOrEmpty()) {
                val activityMode = repoActivityMode.findByIdAndActive(request.activityModeId)
                if (!activityMode.isPresent) {
                    listMessage.add(
                        ErrorMessage(
                            "activityModeId", "Activity Mode id ${request.regencyId} $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                }
            }

            rData["listMessage"] = listMessage
        } catch (e: Exception) {
            throw e
        }

        return rData
    }
}
