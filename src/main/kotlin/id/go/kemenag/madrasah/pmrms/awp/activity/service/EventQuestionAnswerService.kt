package id.go.kemenag.madrasah.pmrms.awp.activity.service

import id.go.kemenag.madrasah.pmrms.awp.activity.constant.*
import id.go.kemenag.madrasah.pmrms.awp.activity.helpers.gender
import id.go.kemenag.madrasah.pmrms.awp.activity.helpers.responseBadRequest
import id.go.kemenag.madrasah.pmrms.awp.activity.helpers.responseSuccess
import id.go.kemenag.madrasah.pmrms.awp.activity.helpers.round
import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.EventQuestionAnswerRequest
import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.awp.activity.model.response.ErrorMessage
import id.go.kemenag.madrasah.pmrms.awp.activity.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.EventFormExcelFileEmail
import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.EventQuestion
import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.EventQuestionAnswer
import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.EventQuestionAnswerDetail
import id.go.kemenag.madrasah.pmrms.awp.activity.repository.*
import id.go.kemenag.madrasah.pmrms.awp.activity.repository.native.EventQuestionAnswerRepositoryNative
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import java.util.*

@Suppress("UNCHECKED_CAST")
@Service
class EventQuestionAnswerService {

    @Autowired
    private lateinit var repoNative: EventQuestionAnswerRepositoryNative

    @Autowired
    private lateinit var repo: EventQuestionAnswerRepository

    @Autowired
    private lateinit var repoEventQuestionAnswerDetail: EventQuestionAnswerDetailRepository

    @Autowired
    private lateinit var repoEvent: EventRepository

    @Autowired
    private lateinit var repoEventQuestion: EventQuestionRepository

    @Autowired
    private lateinit var repoEventForm: EventFormRepository

    @Autowired
    private lateinit var repoEventFormExcelFileEmail: EventFormExcelFileEmailRepository

    fun datatable(req: Pagination2Request): ResponseEntity<ReturnData> {
        return try {
            responseSuccess(data = repoNative.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }

    fun saveAnswer(request: EventQuestionAnswerRequest): ResponseEntity<ReturnData> {
        try {
            val validate = validate(request)
            val listMessage = validate["listMessage"] as List<ErrorMessage>
            if (listMessage.isNotEmpty()) {
                return responseBadRequest(data = listMessage)
            }

            val email = validate["email"] as EventFormExcelFileEmail

            var data = EventQuestionAnswer()

            val checkQuestionAnswer = repo.findByEmailEventIdFilledByActive(
                request.email,
                email.eventFormFileExcel?.eventId,
                email.eventFormFileExcel?.createdFor
            )

            if (checkQuestionAnswer.isPresent) {
                data = checkQuestionAnswer.get()
            }

            data.username = request.username
            data.gender = request.gender
            data.institution = request.institution
            data.email = request.email
            data.phoneNumber = request.phoneNumber
            data.eventId = email.eventFormFileExcel?.eventId
            data.filledBy = email.eventFormFileExcel?.createdFor

            repo.save(data)

            val detail = validate["eventQuestionAnswerDetail"] as List<EventQuestionAnswerDetail>?

            detail?.forEach {
                var answerDetail = EventQuestionAnswerDetail()
                val checkAnswerDetail =
                    repoEventQuestionAnswerDetail.findByEventQuestionAnswerIdAndEventQuestionIdAndActive(
                        data.id,
                        it.eventQuestionId
                    )
                if (checkAnswerDetail.isPresent) {
                    answerDetail = checkAnswerDetail.get()
                }

                answerDetail.eventQuestionAnswerId = data.id
                answerDetail.eventQuestionId = it.eventQuestionId
                answerDetail.eventQuestion = it.eventQuestion
                answerDetail.yesOrNoAnswer = it.yesOrNoAnswer
                answerDetail.scaleAsnwer = it.scaleAsnwer
                answerDetail.explanationAnswer = it.explanationAnswer

                data.answers?.add(
                    repoEventQuestionAnswerDetail.save(answerDetail)
                )
            }

            email.questionnaireStatus = EVENT_FORM_EXCEL_FILE_QUESTIONNAIRE_STATUS_DONE
            email.updatedAt = Date()
            repoEventFormExcelFileEmail.save(email)

            return responseSuccess(data = data)
        } catch (e: Exception) {
            throw e
        }
    }

    fun result(eventId: String?, createdFor: Int): ResponseEntity<ReturnData> {
        try {
            val result = ParticipantAnswerResult()

            val event = repoEvent.findByIdAndActive(eventId)
            if (event.isPresent) {
                result.eventName = event.get().name

                val findEventForm = repoEventForm.getByEventTypeIdAndActive(event.get().eventTypeId)
                if (findEventForm.isNotEmpty()) {
                    val eventQuestionAnswers = repo.getByEventIdAndFilledByAndActiveOrderByCreatedAtDesc(
                        event.get().id, createdFor
                    )

                    eventQuestionAnswers.forEach { eqa ->
                        result.respondents.add(
                            RespondentAnswerResult(
                                username = eqa.username,
                                gender = gender(eqa.gender),
                                institution = eqa.institution,
                                email = eqa.email,
                                phoneNumber = eqa.phoneNumber
                            )
                        )
                    }

                    result.totalRespondent = result.respondents.size.toLong()

                    val eventAnswerDetail = mutableListOf<EventQuestionAnswerDetail>()
                    eventQuestionAnswers.forEach {
                        it.answers?.forEach { a ->
                            a.eventQuestion = null
                            eventAnswerDetail.add(a)
                        }
                    }

                    EVENT_QUESTION_CATEGORIES.forEach { qc ->
                        val eventQuestionCategory = EventQuestionCategoryResult()
                        eventQuestionCategory.category = qc

                        findEventForm[0].questions?.filter { qf ->
                            qf.createdFor == createdFor &&
                                    qf.questionCategory == qc
                        }?.forEach {
                            val eventQuestionAnswerResult = EventQuestionAnswerResult()
                            eventQuestionAnswerResult.question = it

                            try {
                                val answerDetail = eventAnswerDetail.filter { adf -> adf.eventQuestionId == it.id }
                                    .toList() as MutableList<EventQuestionAnswerDetail>

                                if (it.questionType == EVENT_QUESTION_TYPE_EXPLANATION) {
                                    val answers = mutableListOf<String>()
                                    answerDetail.forEach { ad ->
                                        answers.add(ad.explanationAnswer ?: "-")
                                    }

                                    eventQuestionAnswerResult.answers = answers
                                }

                                eventQuestionAnswerResult.respondentCount = answerDetail.size.toLong()


                                if (it.questionType == EVENT_QUESTION_TYPE_YES_OR_NO) {
                                    eventQuestionAnswerResult.answerYesCount =
                                        answerDetail.count { cy -> cy.yesOrNoAnswer == true }.toLong()
                                    eventQuestionAnswerResult.answerNoCount =
                                        answerDetail.count { cy -> cy.yesOrNoAnswer == false }.toLong()
                                }

                                if (it.questionType == EVENT_QUESTION_TYPE_SCALE) {
                                    eventQuestionAnswerResult.answerScale1Count =
                                        answerDetail.count { c1 -> c1.scaleAsnwer == 1 }.toLong()
                                    eventQuestionAnswerResult.answerScale2Count =
                                        answerDetail.count { c1 -> c1.scaleAsnwer == 2 }.toLong()
                                    eventQuestionAnswerResult.answerScale3Count =
                                        answerDetail.count { c1 -> c1.scaleAsnwer == 3 }.toLong()
                                    eventQuestionAnswerResult.answerScale4Count =
                                        answerDetail.count { c1 -> c1.scaleAsnwer == 4 }.toLong()
                                    eventQuestionAnswerResult.answerScale5Count =
                                        answerDetail.count { c1 -> c1.scaleAsnwer == 5 }.toLong()

                                    var score = 0.00

                                    eventQuestionAnswerResult.answerScale1Count?.toInt()
                                        ?.let { it1 -> repeat(it1) { score += 1 } }
                                    eventQuestionAnswerResult.answerScale2Count?.toInt()
                                        ?.let { it2 -> repeat(it2) { score += 2 } }
                                    eventQuestionAnswerResult.answerScale3Count?.toInt()
                                        ?.let { it3 -> repeat(it3) { score += 3 } }
                                    eventQuestionAnswerResult.answerScale4Count?.toInt()
                                        ?.let { it4 -> repeat(it4) { score += 4 } }
                                    eventQuestionAnswerResult.answerScale5Count?.toInt()
                                        ?.let { it5 -> repeat(it5) { score += 5 } }


                                    val rc = eventQuestionAnswerResult.respondentCount?.toDouble() ?: 0.0

                                    eventQuestionAnswerResult.answerScaleScore = round(score / rc, 2).toDouble()
                                }
                            } catch (_: Exception) {

                            }

                            eventQuestionCategory.questionAnswers.add(eventQuestionAnswerResult)
                        }

                        result.questionCategories.add(eventQuestionCategory)
                    }
                }
            }

            return responseSuccess(data = result)
        } catch (e: Exception) {
            throw e
        }
    }

    private fun validate(request: EventQuestionAnswerRequest): Map<String, Any?> {
        val rData = mutableMapOf<String, Any?>()
        try {
            val listMessage: MutableList<ErrorMessage> = emptyList<ErrorMessage>().toMutableList()

            val emails = repoEventFormExcelFileEmail.getAllByActive().filter { uf ->
                val dataId =
                    "${uf.eventFormFileExcelId}${uf.email}"

                BCryptPasswordEncoder().matches(
                    dataId,
                    request.id,
                ) && BCryptPasswordEncoder().matches(
                    uf.eventFormFileExcel?.createdFor.toString(),
                    request.identityId,
                )
            }

            var email: EventFormExcelFileEmail? = null
            if (emails.isNotEmpty()) {
                email = emails[0]
            }

            if (email == null) {
                listMessage.add(
                    ErrorMessage(
                        "identityId", "Identity Id ${request.identityId} $VALIDATOR_MSG_NOT_FOUND"
                    )
                )
            } else {
                if (email.questionnaireStatus == EVENT_FORM_EXCEL_FILE_QUESTIONNAIRE_STATUS_DONE) {
                    listMessage.add(
                        ErrorMessage(
                            "identityId", "Tanggapan Formulir $VALIDATOR_MSG_HAS_ADDED"
                        )
                    )
                } else {
                    rData["email"] = email

                    val event = repoEvent.findByIdAndActive(email.eventFormFileExcel?.eventId)
                    if (!event.isPresent) {
                        listMessage.add(
                            ErrorMessage(
                                "identityId", "Event $VALIDATOR_MSG_NOT_FOUND"
                            )
                        )
                    } else {
                        if (request.email != email.email) {
                            listMessage.add(
                                ErrorMessage(
                                    "email", "Email $VALIDATOR_MSG_NOT_VALID"
                                )
                            )
                        } else {
                            val eventQuestionAnswerDetail = mutableListOf<EventQuestionAnswerDetail>()

                            request.answers?.forEach {
                                val question = repoEventQuestion.findByIdAndActive(it.questionId)
                                if (!question.isPresent) {
                                    listMessage.add(
                                        ErrorMessage(
                                            "questionId", "Question id ${it.questionId} $VALIDATOR_MSG_NOT_FOUND"
                                        )
                                    )
                                } else {
                                    val answerDetail = EventQuestionAnswerDetail()
                                    answerDetail.eventQuestionId = question.get().id
                                    answerDetail.eventQuestion = question.get()

                                    if (question.get().questionType == EVENT_QUESTION_TYPE_YES_OR_NO) {
                                        if (it.answer != null) {
                                            answerDetail.yesOrNoAnswer = it.answer as Boolean
                                        }
                                    }

                                    if (question.get().questionType == EVENT_QUESTION_TYPE_SCALE) {
                                        if (it.answer != null) {
                                            answerDetail.scaleAsnwer = it.answer as Int
                                        } else {
                                            answerDetail.scaleAsnwer = 0
                                        }
                                    }

                                    if (question.get().questionType == EVENT_QUESTION_TYPE_EXPLANATION) {
                                        if (it.answer != null) {
                                            answerDetail.explanationAnswer = it.answer as String
                                        }
                                    }

                                    eventQuestionAnswerDetail.add(answerDetail)
                                }
                            }

                            rData["eventQuestionAnswerDetail"] = eventQuestionAnswerDetail
                        }
                    }
                }
            }

            rData["listMessage"] = listMessage
        } catch (e: Exception) {
            throw e
        }

        return rData
    }
}

data class ParticipantAnswerResult(
    var eventName: String? = null,
    var totalRespondent: Long? = 0,
    var respondents: MutableList<RespondentAnswerResult> = mutableListOf(),
    var questionCategories: MutableList<EventQuestionCategoryResult> = mutableListOf()
)

data class RespondentAnswerResult(
    var username: String? = null,
    var gender: String? = null,
    var institution: String? = null,
    var email: String? = null,
    var phoneNumber: String? = null
)

data class EventQuestionCategoryResult(
    var category: Int? = null,
    var questionAnswers: MutableList<EventQuestionAnswerResult> = mutableListOf()
)

data class EventQuestionAnswerResult(
    var question: EventQuestion? = null,
    var respondentCount: Long? = 0,
    var answers: MutableList<String> = mutableListOf(),
    var answerYesCount: Long? = 0,
    var answerNoCount: Long? = 0,
    var answerScale1Count: Long? = 0,
    var answerScale2Count: Long? = 0,
    var answerScale3Count: Long? = 0,
    var answerScale4Count: Long? = 0,
    var answerScale5Count: Long? = 0,
    var answerScaleScore: Double? = 0.0
)


