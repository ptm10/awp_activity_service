package id.go.kemenag.madrasah.pmrms.awp.activity.service

import id.go.kemenag.madrasah.pmrms.awp.activity.constant.*
import id.go.kemenag.madrasah.pmrms.awp.activity.helpers.daysBetweenDates
import id.go.kemenag.madrasah.pmrms.awp.activity.helpers.responseSuccess
import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.ParamSearch
import id.go.kemenag.madrasah.pmrms.awp.activity.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.EventForm
import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.EventFormExcelFileEmail
import id.go.kemenag.madrasah.pmrms.awp.activity.repository.EventFormExcelFileEmailRepository
import id.go.kemenag.madrasah.pmrms.awp.activity.repository.EventFormRepository
import id.go.kemenag.madrasah.pmrms.awp.activity.repository.EventQuestionRepository
import id.go.kemenag.madrasah.pmrms.awp.activity.repository.EventRepository
import id.go.kemenag.madrasah.pmrms.awp.activity.repository.native.EventQuestionRepositoryNative
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

@Suppress("UNCHECKED_CAST")
@Service
class EventQuestionService {

    @Autowired
    private lateinit var repo: EventQuestionRepository

    @Autowired
    private lateinit var repoNative: EventQuestionRepositoryNative

    @Autowired
    private lateinit var repoEvent: EventRepository

    @Autowired
    private lateinit var repoEventFrom: EventFormRepository

    @Autowired
    private lateinit var repoEventFormExcelFileEmail: EventFormExcelFileEmailRepository

    fun datatable(req: Pagination2Request): ResponseEntity<ReturnData> {
        return try {
            responseSuccess(data = repoNative.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }

    fun existingQuestions(): ResponseEntity<ReturnData> {
        try {
            return responseSuccess(data = repo.getDistinctQuestions())
        } catch (e: Exception) {
            throw e
        }
    }

    fun eventQuestionByEventFilledInBy(
        id: String?,
        identityId: String?,
        req: Pagination2Request
    ): ResponseEntity<ReturnData> {
        try {
            val emails = repoEventFormExcelFileEmail.getAllByActive().filter { uf ->
                val dataId =
                    "${uf.eventFormFileExcelId}${uf.email}"

                BCryptPasswordEncoder().matches(
                    dataId,
                    id,
                ) && BCryptPasswordEncoder().matches(
                    uf.eventFormFileExcel?.createdFor.toString(),
                    identityId,
                )
            }

            var email: EventFormExcelFileEmail? = null
            if (emails.isNotEmpty()) {
                email = emails[0]
            }

            val rdata = mutableMapOf<String, Any?>()
            rdata["status"] = EVENT_QUESTION_MESSAGE_CONSTANT_EVENT_LINK_NOT_VALID
            rdata["eventName"] = null
            rdata["eventType"] = null
            rdata["createdFor"] = null

            if (email != null) {
                rdata["status"] = null
                rdata["createdFor"] = email.eventFormFileExcel?.createdFor

                if (email.questionnaireStatus == EVENT_FORM_EXCEL_FILE_QUESTIONNAIRE_STATUS_DONE) {
                    rdata["status"] = EVENT_QUESTION_MESSAGE_CONSTAN_PROCESSED
                }

                val event = repoEvent.findByIdAndActive(email.eventFormFileExcel?.eventId)
                if (event.isPresent) {
                    rdata["eventName"] = event.get().name
                    rdata["eventType"] = event.get().eventType?.name

                    if (event.get().status != EVENT_STATUS_IMPLEMENTATION && event.get().awpImplementation?.eventManagementTypeId != EVENT_MANAGEMENT_TYPE_CONTRACT) {
                        rdata["status"] = EVENT_QUESTION_MESSAGE_CONSTANT_EVENT_ONGOING
                    } else {
                        val dform: DateFormat = SimpleDateFormat("yyyy-MM-dd")
                        val now: Date = dform.parse(dform.format(Date()))

                        if (now < event.get().endDate && event.get().awpImplementation?.eventManagementTypeId != EVENT_MANAGEMENT_TYPE_CONTRACT) {
                            rdata["status"] = EVENT_QUESTION_MESSAGE_CONSTANT_EVENT_ONGOING
                        } else {
                            if ((event.get().endDate?.let { daysBetweenDates(now, it) } ?: 0) > 1) {
                                rdata["status"] = EVENT_QUESTION_MESSAGE_CONSTANT_EVENT_EXPIRED
                            }
                        }
                    }

                    if (rdata["status"] == null) {
                        var eventForm: EventForm? = null

                        val findEventForm = repoEventFrom.getByEventTypeIdAndActive(event.get().eventTypeId)
                        if (findEventForm.isNotEmpty()) {
                            eventForm = findEventForm[0]
                        }

                        if (eventForm != null) {
                            req.paramIs?.add(ParamSearch("eventFormId", "string", eventForm.id))
                        } else {
                            req.paramIs?.add(
                                ParamSearch(
                                    "eventFormId", "string", System.currentTimeMillis().toString()
                                )
                            )
                        }

                        req.paramIs?.add(
                            ParamSearch(
                                "createdFor", "int", email.eventFormFileExcel?.createdFor.toString()
                            )
                        )
                        rdata["question"] = repoNative.getPage(req)?.content
                    } else {
                        req.paramIs?.add(ParamSearch("id", "string", System.currentTimeMillis().toString()))
                        rdata["question"] = repoNative.getPage(req)?.content
                    }
                } else {
                    req.paramIs?.add(ParamSearch("id", "string", System.currentTimeMillis().toString()))
                    rdata["question"] = repoNative.getPage(req)?.content
                }
            }

            return responseSuccess(data = rdata)
        } catch (e: Exception) {
            throw e
        }
    }
}
