package id.go.kemenag.madrasah.pmrms.awp.activity.service

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import id.go.kemenag.madrasah.pmrms.awp.activity.constant.*
import id.go.kemenag.madrasah.pmrms.awp.activity.exception.UnautorizedException
import id.go.kemenag.madrasah.pmrms.awp.activity.helpers.*
import id.go.kemenag.madrasah.pmrms.awp.activity.model.ReturnDataFiles
import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.*
import id.go.kemenag.madrasah.pmrms.awp.activity.model.response.ErrorMessage
import id.go.kemenag.madrasah.pmrms.awp.activity.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.*
import id.go.kemenag.madrasah.pmrms.awp.activity.repository.*
import id.go.kemenag.madrasah.pmrms.awp.activity.repository.native.EventQuestionAnswerDetailRepositoryNative
import id.go.kemenag.madrasah.pmrms.awp.activity.repository.native.EventReportRepositoryNative
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import javax.servlet.http.HttpServletRequest


@Suppress("UNCHECKED_CAST")
@Service
class EventReportService {

    @Autowired
    private lateinit var repo: EventReportRepository

    @Autowired
    private lateinit var repoNative: EventReportRepositoryNative

    @Autowired
    private lateinit var repoEvent: EventRepository

    @Autowired
    private lateinit var serviceTask: TaskService

    @Autowired
    private lateinit var repoProvince: ProvinceRepository

    @Autowired
    private lateinit var repoRegency: RegencyRepository

    @Autowired
    private lateinit var httpServletRequest: HttpServletRequest

    @Autowired
    private lateinit var repoEventReportDocuments: EventReportDocumentsRepository

    @Autowired
    private lateinit var repoEventEvaluationPoint: EventEvaluationPointRepository

    @Autowired
    private lateinit var repoParticipanTargetPosition: ParticipanTargetPositionRepository

    @Autowired
    private lateinit var repoEventReportEvaluationPoints: EventReportEvaluationPointsRepository

    @Autowired
    private lateinit var repoEventReportParticipantTarget: EventReportParticipantTargetRepository

    @Autowired
    private lateinit var repoResources: ResourcesRepository

    @Autowired
    private lateinit var repoEventReportApprovalNote: EventReportApprovalNoteRepository

    @Autowired
    private lateinit var repoEventType: EventTypeRepository

    @Autowired
    private lateinit var repoUserRoleViewUserResources: UserRoleViewUserResourcesRepository

    @Autowired
    private lateinit var repoEventReportPok: EventReportPokRepository

    @Autowired
    private lateinit var repoPok: PokRepository

    @Autowired
    private lateinit var repoActivityMode: ActivityModeRepository

    @Autowired
    private lateinit var repoEventDocumentType: EventDocumentTypeRepository

    @Autowired
    private lateinit var repoEventForm: EventFormRepository

    @Autowired
    private lateinit var repoEventQuestion: EventQuestionRepository

    @Autowired
    private lateinit var repoEventQuestionAnswerDetail: EventQuestionAnswerDetailRepository

    @Autowired
    private lateinit var repoNativeEventQuestionAnswerDetail: EventQuestionAnswerDetailRepositoryNative

    @Autowired
    private lateinit var logActivityService: LogActivityService


    @Value("\${url.files}")
    private lateinit var filesUrl: String

    // detail
    fun getDetail(id: String): ResponseEntity<ReturnData> {
        try {
            val data = repo.findByIdAndActive(id)
            if (!data.isPresent) {
                return responseNotFound()
            }

            // creator allow edit
            if (data.get().createdBy == getUserLogin()?.id) {
                if (data.get().status == EVENT_REPORT_STATUS_DRAFT || data.get().status == EVENT_REPORT_STATUS_REVISION) {
                    data.get().creatorAllowedEdit = true
                }
            }

            // approve by consultan
            val consultanId = data.get().event?.awpImplementation?.projectOfficerResources?.userId
            if (getUserLogin()?.id == consultanId) {
                if (data.get().status == EVENT_REPORT_STATUS_DRAFT) {
                    data.get().consultanAllowedApprove = true
                }
            }

            return responseSuccess(data = data)
        } catch (e: Exception) {
            throw e
        }
    }

    fun datatable(req: Pagination2Request): ResponseEntity<ReturnData> {
        return try {
            responseSuccess(data = repoNative.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }

    fun updateData(id: String, request: EventReportRequest): ResponseEntity<ReturnData> {
        val update = repo.findByIdAndActive(id)
        return if (update.isPresent) {
            saveOrUpdate(request, update.get())
        } else {
            responseNotFound()
        }
    }

    fun saveData(req: EventReportRequest): ResponseEntity<ReturnData> {
        return saveOrUpdate(req)
    }

    fun saveOrUpdate(
        req: EventReportRequest, update: EventReport? = null
    ): ResponseEntity<ReturnData> {
        var oldData: EventReport? = null
        if (update != null) {
            val objectMapper = ObjectMapper()
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            oldData = objectMapper.readValue(
                objectMapper.writeValueAsString(update), EventReport::class.java
            ) as EventReport
        }

        try {
            if (!userHasRoles(
                    listOf(
                        ROLE_ID_STAFF,
                        ROLE_ID_ASSISTANT,
                        ROLE_ID_LSP,
                        ROLE_ID_PCU,
                        ROLE_ID_COORDINATOR,
                        ROLE_ID_CONSULTAN
                    )
                )
            ) {
                throw UnautorizedException(message = VALIDATOR_MSG_NOT_HAVE_ACCESS)
            }

            val validate = validate(req, update)
            val listMessage = validate["listMessage"] as List<ErrorMessage>
            if (listMessage.isNotEmpty()) {
                return responseBadRequest(data = listMessage)
            }


            val event = validate["event"] as Event
            if (!userHasRoles(listOf(ROLE_ID_PCU))) {
                if (getUserLogin()?.componentId != event.awpImplementation?.componentId) {
                    throw UnautorizedException(message = VALIDATOR_MSG_NOT_HAVE_ACCESS)
                }
            }

            val data: EventReport?
            data = update ?: EventReport()

            if (update != null) {
                if (getUserLogin()?.id != data.createdBy) {
                    throw UnautorizedException(message = VALIDATOR_MSG_NOT_HAVE_ACCESS)
                }
            }

            data.eventId = req.eventId
            data.event = event
            data.topic = req.topic
            data.eventCode = req.eventCode
            data.provinceId = req.provinceId
            data.regencyId = req.regencyId
            data.location = req.location
            data.startDate = validate["startDate"] as Date?
            data.endDate = validate["endDate"] as Date?
            data.location = req.location
            data.participantTarget = req.participantTarget
            data.locationOtherInformation = req.locationOtherInformation
            data.participantMale = req.participantMale
            data.participantFemale = req.participantFemale
            data.participantCount = req.participantCount
            data.participantOtherInformation = req.participantOtherInformation
            data.implementationSummary = req.implementationSummary
            data.eventEvaluationDescription = req.eventEvaluationDescription
            data.conclusionsAndRecommendations = req.conclusionsAndRecommendations
            data.eventOutput = req.eventOutput
            data.activityModeId = req.activityModeId
            data.attachmentFileId = req.attachmentFileId
            data.rraFileId = req.rraFileId
            data.budgetSource = req.budgetSource
            data.budgetYear = req.budgetYear
            data.makNumber = req.makNumber
            data.budgetCeiling = req.budgetCeiling
            data.rraBudget = req.rraBudget
            data.paragraph1 = req.paragraph1
            data.paragraph2 = req.paragraph2
            data.p3DurationDays = req.p3DurationDays
            data.p3DurationMonth = req.p3DurationMonth
            data.p3DurationYear = req.p3DurationYear
            data.p3EventTypeId = req.p3EventType
            data.p3StartDate = validate["p3StartDate"] as Date?
            data.p3EndDate = validate["p3EndDate"] as Date?
            data.paragraph4 = req.paragraph4
            data.paragraph5 = req.paragraph5
            data.technicalImplementer = req.technicalImplementer
            data.totalDuration = req.totalDuration
            data.schemaEvent = req.schemaEvent

            var taskTitle = "Membuat"
            if (update != null) {
                data.updatedBy = getUserLogin()?.id
                data.updatedAt = Date()

                if (data.status == EVENT_REPORT_STATUS_REVISION) {
                    taskTitle = "Merevisi"

                    serviceTask.approveRejectFromTask(
                        APPROVAL_STATUS_APPROVE,
                        data.id,
                        TASK_TYPE_EVENT_REPORT,
                        getUserLogin()?.id,
                        TASK_NUMBER_TASK_REVISION
                    )
                } else {
                    data.status = EVENT_REPORT_STATUS_DRAFT
                }

                update.documents?.forEach {
                    if (req.deletedDocumentIds?.contains(it.id) == true) {
                        it.active = false
                        it.updatedAt = Date()
                        repoEventReportDocuments.save(it)
                    }
                }

                update.evaluationPoints?.forEach {
                    if (req.deletedEventReportEvaluationPointsId?.contains(it.id) == true) {
                        it.active = false
                        it.updatedAt = Date()
                        repoEventReportEvaluationPoints.save(it)
                    }
                }

                update.participantTargets?.forEach {
                    if (req.deletedEventRerportParticipantTargetsId?.contains(it.id) == true) {
                        it.active = false
                        it.updatedAt = Date()
                        repoEventReportParticipantTarget.save(it)
                    }
                }

                update.pok?.forEach {
                    if (req.deletedPokIds?.contains(it.id) == true) {
                        it.active = false
                        it.updatedAt = Date()
                        repoEventReportPok.save(it)
                    }
                }
            } else {
                data.createdBy = getUserLogin()?.id
            }

            repo.save(data)

            val documents = validate["documents"] as List<EventReportDocuments>
            documents.forEach {
                data.documents?.add(
                    repoEventReportDocuments.save(
                        EventReportDocuments(
                            eventReportId = data.id,
                            eventDocumentTypeId = it.eventDocumentTypeId,
                            title = it.title,
                            date = it.date,
                            place = it.place,
                            description = it.description,
                            fileId = it.fileId
                        )
                    )
                )
            }

            val evaluationPoints = validate["evaluationPoints"] as List<EventReportEvaluationPointsReqeust>
            evaluationPoints.forEach {
                data.evaluationPoints?.add(
                    repoEventReportEvaluationPoints.save(
                        EventReportEvaluationPoints(
                            eventEvaluationPointId = it.evaluationPointId,
                            eventReportId = data.id,
                            value = it.value,
                            note = it.note
                        )
                    )
                )
            }

            val participantTargetPositions =
                validate["participantTargetPositions"] as List<EventReportParticipantTargetPositionReqeust>
            participantTargetPositions.forEach {
                data.participantTargets?.add(
                    repoEventReportParticipantTarget.save(
                        EventReportParticipantTarget(
                            eventReportId = data.id,
                            participantTargetPositionId = it.participantTargetId,
                            value = it.value
                        )
                    )
                )
            }

            req.newPokIds?.forEach {
                data.pok?.add(
                    repoEventReportPok.save(
                        EventReportPok(
                            eventReportId = data.id, pokId = it
                        )
                    )
                )
            }

            if (data.event?.createdBy != getUserLogin()?.id || userHasRoles(listOf(ROLE_ID_LSP))) {
                val taskDescription = "$taskTitle Laporan Event ${data.event?.name ?: "-"}"

                // create task for consultan event
                serviceTask.createOrUpdateFromTask(
                    createdBy = getUserLogin()?.id,
                    taskId = data.id,
                    taskType = TASK_TYPE_EVENT_REPORT,
                    description = taskDescription,
                    createdFor = data.event?.awpImplementation?.projectOfficerResources?.userId
                )

                if (data.status == EVENT_REPORT_STATUS_REVISION && oldData != null) {
                    if (compareDataChange(oldData, data)) {
                        data.status = EVENT_REPORT_STATUS_DRAFT
                        repo.save(data)
                    }
                }
            } else {
                approveByConsultan(ApproveRequest(data.id, APPROVAL_STATUS_APPROVE, "-"))
            }

            /*var eventQuestionAnswer: EventQuestionAnswer? = null

            if (req.questionAnswer?.isNotEmpty() == true) {
                val findEventQuestionAnswer = repoEventQuestionAnswer.findByEventIdAndFilledByAndActive(
                    data.eventId, EVENT_QUESTION_CREATED_FOR_EVALUATOR
                )

                eventQuestionAnswer = if (findEventQuestionAnswer.isPresent) {
                    findEventQuestionAnswer.get()
                } else {
                    repoEventQuestionAnswer.save(
                        EventQuestionAnswer(
                            eventId = data.eventId, filledBy = EVENT_QUESTION_CREATED_FOR_EVALUATOR
                        )
                    )
                }
            }

            if (eventQuestionAnswer != null) {
                req.questionAnswer?.forEach {
                    var eventQuestionAnswerDetail = EventQuestionAnswerDetail()

                    val findEventQuestionAnswerDetail =
                        repoEventQuestionAnswerDetail.findByEventQuestionAnswerIdAndEventQuestionIdAndActive(
                            eventQuestionAnswer.id,
                            it.eventQuestionId
                        )

                    if (findEventQuestionAnswerDetail.isPresent) {
                        eventQuestionAnswerDetail = findEventQuestionAnswerDetail.get()
                        eventQuestionAnswerDetail.updatedAt = Date()
                    } else {
                        eventQuestionAnswerDetail.eventQuestionAnswerId = eventQuestionAnswer.id
                        eventQuestionAnswerDetail.eventQuestionId = it.eventQuestionId
                        eventQuestionAnswerDetail.eventQuestion =
                            repoEventQuestion.findByIdAndActive(it.eventQuestionId).get()
                    }

                    if (eventQuestionAnswerDetail.eventQuestion?.questionType == EVENT_QUESTION_TYPE_YES_OR_NO) {
                        if (it.answer != null) {
                            eventQuestionAnswerDetail.yesOrNoAnswer = it.answer as Boolean
                        }
                    }

                    if (eventQuestionAnswerDetail.eventQuestion?.questionType == EVENT_QUESTION_TYPE_SCALE) {
                        if (it.answer != null) {
                            eventQuestionAnswerDetail.scaleAsnwer = it.answer as Int
                        } else {
                            eventQuestionAnswerDetail.scaleAsnwer = 0
                        }
                    }

                    if (eventQuestionAnswerDetail.eventQuestion?.questionType == EVENT_QUESTION_TYPE_EXPLANATION) {
                        if (it.answer != null) {
                            eventQuestionAnswerDetail.explanationAnswer = it.answer as String
                        }
                    }

                    eventQuestionAnswerDetail.comment = it.comment
                    repoEventQuestionAnswerDetail.save(eventQuestionAnswerDetail)
                }
            }*/

            return if (update == null) {
                responseCreated(data = data)
            } else {
                val ipAdress = httpServletRequest.getHeader("X-FORWARDED-FOR") ?: httpServletRequest.remoteAddr
                val userId = getUserLogin()?.id ?: ""
                Thread {
                    logActivityService.writeChangeData(
                        data,
                        oldData,
                        "event_report",
                        ipAdress,
                        userId
                    )
                }.start()

                responseSuccess(data = data)
            }
        } catch (e: Exception) {
            throw e
        }
    }

    fun approveByConsultan(request: ApproveRequest): ResponseEntity<ReturnData> {
        try {
            if (!userHasRoles(listOf(ROLE_ID_CONSULTAN, ROLE_ID_COORDINATOR))) {
                throw UnautorizedException(message = VALIDATOR_MSG_NOT_HAVE_ACCESS)
            }

            val find = repo.findByIdAndActive(request.id)
            if (!find.isPresent) {
                return responseNotFound()
            }

            val usersNotif = mutableListOf<String>()

            var status = EVENT_REPORT_STATUS_APPROVED
            if (request.approveStatus == APPROVAL_STATUS_REJECT) {
                status = EVENT_REPORT_STATUS_REVISION

                serviceTask.createRevisionTask(
                    taskId = find.get().id,
                    taskType = TASK_TYPE_EVENT_REPORT,
                    createdFor = getUserLogin()?.id,
                    description = "Revisi Laporan Event ${find.get().event?.name ?: " - "}"
                )
            } else {
                if (!userHasRoles(listOf(ROLE_ID_COORDINATOR))) {
                    val coordinatorId =
                        findCoordinator(find.get().event?.awpImplementation?.projectOfficerResources?.supervisiorId)
                    if (coordinatorId.isNullOrEmpty()) {
                        throw UnautorizedException(message = "Koordinataor $VALIDATOR_MSG_NOT_FOUND")
                    }
                    usersNotif.add(coordinatorId)
                }

                // user treasurer
                usersNotif.addAll(
                    repoUserRoleViewUserResources.getUserIdsByRoleIdsComponentIdPosition(
                        listOf(ROLE_ID_COORDINATOR, ROLE_ID_ADMINSTRATOR), 2, COMPONENT_ID_PMU, POSITION_TREASURER_PMU
                    )
                )
                // user pmu
                usersNotif.addAll(
                    repoUserRoleViewUserResources.getPmuHead(
                        listOf(ROLE_ID_COORDINATOR, ROLE_ID_ADMINSTRATOR), 2, COMPONENT_ID_PMU
                    )
                )
            }

            find.get().status = status
            if (status == EVENT_REPORT_STATUS_APPROVED) {
                // set event status to EVENT_STATUS_DONE
                find.get().event?.status = EVENT_STATUS_DONE
            }
            find.get().updatedBy = getUserLogin()?.id
            find.get().updatedAt = Date()
            repo.save(find.get())


            var createdFrom = EVENT_REPORT_APPROVAL_MESSAGE_FROM_CONSULTAN
            if (userHasRoles(listOf(ROLE_ID_COORDINATOR))) {
                createdFrom = EVENT_REPORT_APPROVAL_MESSAGE_FROM_COORDINATOR
            }

            repoEventReportApprovalNote.save(
                EventReportApprovalNote(
                    eventReportId = find.get().id,
                    userId = getUserLogin()?.id,
                    createdFrom = createdFrom,
                    note = request.note,
                    approvalStatus = request.approveStatus,
                )
            )

            serviceTask.approveRejectFromTask(
                request.approveStatus, find.get().id, TASK_TYPE_EVENT_REPORT, getUserLogin()?.id
            )

            usersNotif.forEach {
                serviceTask.createTaskNotification(
                    taskId = find.get().id,
                    taskType = TASK_TYPE_EVENT_REPORT,
                    createdBy = getUserLogin()?.id,
                    createdFor = it,
                    description = "Laporan Event ${find.get().event?.name ?: " - "}"
                )
            }

            return responseSuccess()
        } catch (e: Exception) {
            throw e
        }
    }

    private fun validate(
        request: EventReportRequest, update: EventReport? = null
    ): Map<String, Any?> {
        val rData = mutableMapOf<String, Any?>()
        try {
            val listMessage: MutableList<ErrorMessage> = emptyList<ErrorMessage>().toMutableList()

            val checkData = repo.findByEventIdAndActive(request.eventId)
            if (checkData.isPresent) {
                val checkMessage = ErrorMessage(
                    "eventId", "Event ${checkData.get().event?.name} $VALIDATOR_MSG_HAS_ADDED"
                )

                if (update != null) {
                    if (update.id != checkData.get().id) {
                        listMessage.add(checkMessage)
                    }
                } else {
                    listMessage.add(checkMessage)
                }
            }

            val event = repoEvent.findByIdAndActive(request.eventId)
            if (!event.isPresent) {
                listMessage.add(
                    ErrorMessage(
                        "eventId", "Event id ${request.eventId} $VALIDATOR_MSG_NOT_FOUND"
                    )
                )
            } else {
                rData["event"] = event.get()
            }

            val province = repoProvince.findByIdAndActive(request.provinceId)
            if (!province.isPresent) {
                listMessage.add(
                    ErrorMessage(
                        "provinceId", "Provinsi id ${request.provinceId} $VALIDATOR_MSG_NOT_FOUND"
                    )
                )
            }

            if (!request.regencyId.isNullOrEmpty()) {
                val regency = repoRegency.findByIdAndActive(request.regencyId)
                if (!regency.isPresent) {
                    listMessage.add(
                        ErrorMessage(
                            "regencyId", "Regency id ${request.regencyId} $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                }
            }

            val dform: DateFormat = SimpleDateFormat("yyyy-MM-dd")

            val documents = mutableListOf<EventReportDocuments>()
            request.newDocuments?.forEach {
                var date: Date? = null
                try {
                    date = dform.parse(it.date)
                } catch (_: Exception) {
                    listMessage.add(
                        ErrorMessage(
                            "newDocuments", "Tanggal $VALIDATOR_MSG_NOT_VALID"
                        )
                    )
                }

                val checkFile: ReturnDataFiles? = RequestHelpers.filesGetById(
                    it.fileId ?: "", filesUrl, httpServletRequest.getHeader(HEADER_STRING)
                )

                var checkedFile: Files? = null
                if (checkFile != null) {
                    if (checkFile.data != null) {
                        checkedFile = checkFile.data
                    }
                }

                if (checkedFile == null) {
                    listMessage.add(
                        ErrorMessage(
                            "newDocuments", "File id ${it.fileId} $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                }

                if (!it.eventDocumentTypeId.isNullOrEmpty()) {
                    val check = repoEventDocumentType.findByIdAndActive(it.eventDocumentTypeId)
                    if (!check.isPresent) {
                        listMessage.add(
                            ErrorMessage(
                                "newDocuments", "File id ${it.fileId} $VALIDATOR_MSG_NOT_FOUND"
                            )
                        )
                    }
                }

                documents.add(
                    EventReportDocuments(
                        title = it.title,
                        eventDocumentTypeId = it.eventDocumentTypeId,
                        date = date,
                        place = it.place,
                        description = it.description,
                        fileId = it.fileId
                    )
                )
            }
            rData["documents"] = documents

            val evaluationPoints = mutableListOf<EventReportEvaluationPointsReqeust>()
            request.newEventReportEvaluationPoints?.forEach {
                var evaluationPointId = it.evaluationPointId
                if (!it.newEvaluationPoint.isNullOrEmpty()) {
                    val evaluationPoint = repoEventEvaluationPoint.findByName(it.newEvaluationPoint)
                    evaluationPointId = if (evaluationPoint.isPresent) {
                        evaluationPoint.get().id
                    } else {
                        repoEventEvaluationPoint.save(
                            EventEvaluationPoint(
                                name = it.newEvaluationPoint?.trim()
                            )
                        ).id
                    }
                }

                if (!evaluationPointId.isNullOrEmpty()) {
                    evaluationPoints.add(
                        EventReportEvaluationPointsReqeust(
                            evaluationPointId = evaluationPointId, value = it.value, note = it.note
                        )
                    )
                }

            }
            rData["evaluationPoints"] = evaluationPoints


            val participantTargetPositions = mutableListOf<EventReportParticipantTargetPositionReqeust>()

            request.newEventReportParticipantTargets?.forEach {
                var participantTargetId = it.participantTargetId
                if (!it.newParticipantTarget.isNullOrEmpty()) {
                    val participantTarget = repoParticipanTargetPosition.findByName(it.newParticipantTarget)
                    participantTargetId = if (participantTarget.isPresent) {
                        participantTarget.get().id
                    } else {
                        repoParticipanTargetPosition.save(
                            ParticipanTargetPosition(
                                name = it.newParticipantTarget?.trim()
                            )
                        ).id
                    }
                }

                if (!participantTargetId.isNullOrEmpty()) {
                    participantTargetPositions.add(
                        EventReportParticipantTargetPositionReqeust(
                            participantTargetId = participantTargetId, value = it.value
                        )
                    )
                }

            }
            rData["participantTargetPositions"] = participantTargetPositions


            if (!request.attachmentFileId.isNullOrEmpty()) {
                val checkFile: ReturnDataFiles? = RequestHelpers.filesGetById(
                    request.attachmentFileId ?: "", filesUrl, httpServletRequest.getHeader(HEADER_STRING)
                )

                var checkedFile: Files? = null
                if (checkFile != null) {
                    if (checkFile.data != null) {
                        checkedFile = checkFile.data
                    }
                }

                if (checkedFile == null) {
                    listMessage.add(
                        ErrorMessage(
                            "attachmentFileId",
                            "Attachment File Id ${request.attachmentFileId} $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                }
            }

            if (!request.rraFileId.isNullOrEmpty()) {
                val checkFile: ReturnDataFiles? = RequestHelpers.filesGetById(
                    request.rraFileId ?: "", filesUrl, httpServletRequest.getHeader(HEADER_STRING)
                )

                var checkedFile: Files? = null
                if (checkFile != null) {
                    if (checkFile.data != null) {
                        checkedFile = checkFile.data
                    }
                }

                if (checkedFile == null) {
                    listMessage.add(
                        ErrorMessage(
                            "rraFileId", "Rincian Realisai Anggaran ${request.rraFileId} $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                }
            }

            rData["listMessage"] = listMessage


            if (!request.p3StartDate.isNullOrEmpty() && !request.p3EndDate.isNullOrEmpty()) {
                var p3StartDate: Date? = null
                var p3EndDate: Date? = null

                try {
                    p3StartDate = dform.parse(request.p3StartDate)
                } catch (_: Exception) {
                    listMessage.add(
                        ErrorMessage(
                            "p3StartDate", "Tanggal Mulai $VALIDATOR_MSG_NOT_VALID"
                        )
                    )
                }

                try {
                    p3EndDate = dform.parse(request.p3EndDate)
                } catch (_: Exception) {
                    listMessage.add(
                        ErrorMessage(
                            "p3EndDate", "Tanggal Selesai $VALIDATOR_MSG_NOT_VALID"
                        )
                    )
                }

                if (p3EndDate?.before(p3StartDate) == true) {
                    listMessage.add(
                        ErrorMessage(
                            "p3EndDate", "Tanggal Selesai $VALIDATOR_MSG_NOT_VALID"
                        )
                    )
                }


                rData["p3StartDate"] = p3StartDate as Date
                rData["p3EndDate"] = p3EndDate as Date
            }

            if (!request.startDate.isNullOrEmpty() && !request.endDate.isNullOrEmpty()) {
                var startDate: Date? = null
                var endDate: Date? = null

                try {
                    startDate = dform.parse(request.startDate)
                } catch (_: Exception) {
                    listMessage.add(
                        ErrorMessage(
                            "startDate", "Tanggal Mulai $VALIDATOR_MSG_NOT_VALID"
                        )
                    )
                }

                try {
                    endDate = dform.parse(request.endDate)
                } catch (_: Exception) {
                    listMessage.add(
                        ErrorMessage(
                            "endDate", "Tanggal Selesai $VALIDATOR_MSG_NOT_VALID"
                        )
                    )
                }

                if (endDate?.before(startDate) == true) {
                    listMessage.add(
                        ErrorMessage(
                            "endDate", "Tanggal Selesai $VALIDATOR_MSG_NOT_VALID"
                        )
                    )
                }

                rData["startDate"] = startDate as Date
                rData["endDate"] = endDate as Date
            }

            if (!request.technicalImplementer.isNullOrEmpty()) {
                val checkResources = repoResources.findByIdAndActive(request.technicalImplementer)
                if (!checkResources.isPresent) {
                    listMessage.add(
                        ErrorMessage(
                            "technicalImplementer",
                            "Pelaksana Teksis ${request.technicalImplementer} $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                }
            }

            if (!request.p3EventType.isNullOrEmpty()) {
                val checkEventType = repoEventType.findByIdAndActive(request.p3EventType)
                if (!checkEventType.isPresent) {
                    listMessage.add(
                        ErrorMessage(
                            "p3EventType", "p3EventType id ${request.p3EventType} $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                }
            }

            if (!request.activityModeId.isNullOrEmpty()) {
                val activityMode = repoActivityMode.findByIdAndActive(request.activityModeId)
                if (!activityMode.isPresent) {
                    listMessage.add(
                        ErrorMessage(
                            "activityModeId", "Activity Mode id ${request.regencyId} $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                }
            }

            request.newPokIds?.forEach {
                val checkPok = repoPok.findByIdAndActive(it)
                if (!checkPok.isPresent) {
                    listMessage.add(
                        ErrorMessage(
                            "newPokIds", "POK id $it $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                }
            }

            request.questionAnswer?.forEach {
                val eventQuestion = repoEventQuestion.findByIdAndActive(it.eventQuestionId)
                if (!eventQuestion.isPresent) {
                    listMessage.add(
                        ErrorMessage(
                            "questionAnswer", "Question id ${it.eventQuestionId} $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                }
            }

            rData["listMessage"] = listMessage
        } catch (e: Exception) {
            throw e
        }

        return rData
    }

    private fun findCoordinator(supervisiorId: String?): String? {
        try {
            val findResources = repoResources.findByIdAndActive(supervisiorId)
            if (findResources.isPresent) {
                val filterRole = findResources.get().user?.roles?.filter {
                    it.roleId == ROLE_ID_COORDINATOR
                }
                return if (!filterRole.isNullOrEmpty()) {
                    findResources.get().userId
                } else {
                    if (!findResources.get().supervisiorId.isNullOrEmpty()) {
                        findCoordinator(findResources.get().supervisiorId)
                    } else {
                        null
                    }
                }
            } else {
                return null
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return null
    }

    fun datatableQuestion(req: Pagination2Request): ResponseEntity<ReturnData> {
        try {
            val searchEventId: ParamSearch? = req.paramIs?.find { fp -> fp.field == "eventId" }

            if (searchEventId != null) {
                val eventQuestionAnswer = EventQuestionAnswer()

                val event = repoEvent.findByIdAndActive(searchEventId.value)
                if (event.isPresent) {
                    val eventForm = repoEventForm.getByEventTypeIdAndActive(event.get().eventTypeId)
                    if (eventForm.isNotEmpty()) {
                        eventForm[0].questions?.filter { fq -> fq.createdFor == EVENT_QUESTION_CREATED_FOR_EVALUATOR }
                            ?.forEach {
                                val checkAnswerDetail =
                                    repoEventQuestionAnswerDetail.findByEventQuestionAnswerIdAndEventQuestionIdAndActive(
                                        eventQuestionAnswer.id,
                                        it.id
                                    )
                                if (!checkAnswerDetail.isPresent) {
                                    repoEventQuestionAnswerDetail.save(
                                        EventQuestionAnswerDetail(
                                            eventQuestionAnswerId = eventQuestionAnswer.id,
                                            eventQuestionId = it.id,
                                            eventQuestion = it
                                        )
                                    )
                                }
                            }
                    }
                }

                req.paramIs?.add(ParamSearch("eventQuestionAnswerId", "string", eventQuestionAnswer.id))
                req.paramIs?.add(
                    ParamSearch(
                        "eventQuestion.createdFor",
                        "int",
                        EVENT_QUESTION_CREATED_FOR_EVALUATOR.toString()
                    )
                )
                req.paramIs?.remove(searchEventId)
            } else {
                req.paramIs?.add(ParamSearch("id", "string", System.currentTimeMillis().toString()))
            }

            return responseSuccess(data = repoNativeEventQuestionAnswerDetail.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }
}
