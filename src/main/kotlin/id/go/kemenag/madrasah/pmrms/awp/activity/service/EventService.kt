package id.go.kemenag.madrasah.pmrms.awp.activity.service

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import id.go.kemenag.madrasah.pmrms.awp.activity.constant.*
import id.go.kemenag.madrasah.pmrms.awp.activity.exception.UnautorizedException
import id.go.kemenag.madrasah.pmrms.awp.activity.helpers.*
import id.go.kemenag.madrasah.pmrms.awp.activity.model.ReturnDataFiles
import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.*
import id.go.kemenag.madrasah.pmrms.awp.activity.model.response.ErrorMessage
import id.go.kemenag.madrasah.pmrms.awp.activity.model.response.EventByStatus
import id.go.kemenag.madrasah.pmrms.awp.activity.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.*
import id.go.kemenag.madrasah.pmrms.awp.activity.repository.*
import id.go.kemenag.madrasah.pmrms.awp.activity.repository.native.EventRepositoryNative
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.time.format.DateTimeFormatter
import java.util.*
import javax.servlet.http.HttpServletRequest


@Suppress("UNCHECKED_CAST")
@Service
class EventService {

    @Autowired
    private lateinit var repoNative: EventRepositoryNative

    @Autowired
    private lateinit var repo: EventRepository

    @Autowired
    private lateinit var repoComponent: ComponentRepository

    @Autowired
    private lateinit var httpServletRequest: HttpServletRequest

    @Autowired
    private lateinit var repoResources: ResourcesRepository

    @Autowired
    private lateinit var repoProvince: ProvinceRepository

    @Autowired
    private lateinit var repoRegency: RegencyRepository

    @Autowired
    private lateinit var repoAwpImplementation: AwpImplementationRepository

    @Autowired
    private lateinit var repoEventStaff: EventStaffRepository

    @Autowired
    private lateinit var repoEventOtherStaff: EventOtherStaffRepository

    @Autowired
    private lateinit var repoEventAgenda: EventAgendaRepository

    @Autowired
    private lateinit var repoEventAgendaDetail: EventAgendaDetailRepository

    @Autowired
    private lateinit var repoEventType: EventTypeRepository

    @Autowired
    private lateinit var repoEventRab: EventRabRepository

    @Autowired
    private lateinit var repoEventRabDetail: EventRabDetailRepository

    @Autowired
    private lateinit var repoUserRoleViewUserResources: UserRoleViewUserResourcesRepository

    @Autowired
    private lateinit var repoEventApprovalNoteRepository: EventApprovalNoteRepository

    @Autowired
    private lateinit var repoEventReport: EventReportRepository

    @Autowired
    private lateinit var serviceTask: TaskService

    @Autowired
    private lateinit var repoPok: PokRepository

    @Autowired
    private lateinit var repoEventPok: EventPokRepository

    @Autowired
    private lateinit var repoActivityMode: ActivityModeRepository

    @Autowired
    private lateinit var logActivityService: LogActivityService

    @Autowired
    private lateinit var serviceResourcesSchedule: ResourcesScheduleService

    private val staffSupervisiorIdsTemp: MutableList<String> = mutableListOf()

    @Value("\${url.files}")
    private lateinit var filesUrl: String

    private var findCoordinatorCounter = 0

    fun datatableAll(req: Pagination2Request): ResponseEntity<ReturnData> {
        return try {
            responseSuccess(data = repoNative.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }

    fun datatableEventLsp(req: Pagination2Request): ResponseEntity<ReturnData> {
        return try {
            val findAwpImplementationId: ParamSearch? =
                req.paramIs?.find { pf -> pf.field == "awpImplementationId" }
            if (findAwpImplementationId != null) {
                val awpImplementation = repoAwpImplementation.findByIdAndActive(findAwpImplementationId.value)
                if (awpImplementation.isPresent) {
                    if (awpImplementation.get().eventManagementTypeId == EVENT_MANAGEMENT_TYPE_CONTRACT) {
                        val eventVolume = awpImplementation.get().eventVolume ?: 0
                        val repeat = eventVolume - repo.countByAwpImplementationIdAndActive(
                            findAwpImplementationId.value
                        )

                        if (repeat > 0) {
                            var num = 1
                            repeat(repeat.toInt()) {
                                val event = Event()
                                event.awpImplementationId = awpImplementation.get().id
                                event.name = "Event $num"
                                event.nameOtherInformation = awpImplementation.get().nameOtherInformation
                                event.locationOtherInformation = awpImplementation.get().locationOtherInformation
                                event.locationOtherInformation = awpImplementation.get().locationOtherInformation
                                event.startDate = awpImplementation.get().startDate
                                event.endDate = awpImplementation.get().endDate
                                event.eventOtherInformation = awpImplementation.get().eventOtherInformation
                                event.eventTypeId = awpImplementation.get().eventTypeId
                                event.activityModeId = awpImplementation.get().activityModeId
                                event.purpose = awpImplementation.get().purpose
                                event.eventOutput = awpImplementation.get().eventOutput
                                event.participantMale = awpImplementation.get().participantMale
                                event.participantFemale = awpImplementation.get().participantFemale
                                event.participantCount = awpImplementation.get().participantCount
                                event.participantOtherInformation =
                                    awpImplementation.get().participantOtherInformation
                                event.participantTarget = awpImplementation.get().participantTarget
                                event.status = EVENT_STATUS_IMPLEMENTATION
                                event.createdBy = getUserLogin()?.id
                                repo.save(event)

                                num++
                            }
                        }
                    }
                }
            }
            responseSuccess(data = repoNative.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }

    fun datatable(req: Pagination2Request): ResponseEntity<ReturnData> {
        return try {
            if (!userHasRoles(listOf(ROLE_ID_ADMINSTRATOR))) {
                if (!userHasRoles(listOf(ROLE_ID_PCU))) {
                    if (getUserLogin()?.component?.code != "4.4") {
                        var componentId: String? = getUserLogin()?.component?.id
                        if (getUserLogin()?.component?.code?.contains("4.") == true) {
                            val findComponent = repoComponent.findByCodeAndActive("4")
                            if (findComponent.isPresent) {
                                componentId = findComponent.get().id
                            }
                        }
                        if (componentId != null) {
                            req.paramIs?.add(ParamSearch("awpImplementation.componentId", "string", componentId))
                        } else {
                            req.paramIs?.add(
                                ParamSearch(
                                    "awpImplementation.componentId", "string", System.currentTimeMillis().toString()
                                )
                            )
                        }
                    }
                } else {
                    val ids = repoEventStaff.getEventIdsByResourcesId(getUserLogin()?.resourcesId)
                    if (ids.isNotEmpty()) {
                        req.paramIn?.add(ParamArray("id", "string", ids))
                    } else {
                        req.paramIs?.add(ParamSearch("id", "string", System.currentTimeMillis().toString()))
                    }
                }
            }

            repoNative.setOrderByStrLength(
                listOf("awpImplementation.componentCode.code")
            )

            req.paramIs?.add(
                ParamSearch(
                    "awpImplementation.eventManagementTypeId",
                    "string",
                    EVENT_MANAGEMENT_TYPE_SWAKELOLA
                )
            )

            responseSuccess(data = repoNative.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }

    fun schedule(req: EventScheduleRequest): ResponseEntity<ReturnData> {
        return try {
            val eventIds = repo.getAllIds()

            if ((req.self != false) || (req.staff != false) || !req.componentIds.isNullOrEmpty()) {
                eventIds.clear()
            }

            if (req.self == true) {
                val eventResources = repoEventStaff.getEventIdsByResourcesId(getUserLogin()?.resourcesId)
                if (eventResources.isNotEmpty()) {
                    eventIds.addAll(eventResources)
                }
            }

            if (req.staff == true) {
                val staffSupervisiorIds = getStaffSupervisiorId(getUserLogin()?.resourcesId ?: "", true)
                if (staffSupervisiorIds.isNotEmpty()) {
                    val staffSupervisiorNotSelf = staffSupervisiorIds.filter { f ->
                        f != getUserLogin()?.resourcesId
                    }

                    if (staffSupervisiorNotSelf.isNotEmpty()) {
                        val eventIdsStaffSupervisior = repoEventStaff.getEventIdsByResourcesIds(staffSupervisiorNotSelf)
                        if (eventIdsStaffSupervisior.isNotEmpty()) {
                            eventIds.addAll(eventIdsStaffSupervisior)
                        }
                    }

                    val eventIdsStaff = repoEventStaff.getEventIdsBySupervisiorIds(staffSupervisiorIds)
                    if (eventIdsStaff.isNotEmpty()) {
                        eventIds.addAll(eventIdsStaff)
                    }
                }
            }

            if (!req.componentIds.isNullOrEmpty()) {
                val componentsIds = mutableListOf<String>()
                val subComponentsIds = mutableListOf<String>()

                repoComponent.getByIdInAndActive(req.componentIds).forEach {
                    if (it.code?.contains(".") == false) {
                        componentsIds.add(it.id ?: "")
                    } else {
                        subComponentsIds.add(it.id ?: "")
                    }
                }

                if (componentsIds.isNotEmpty()) {
                    val eventIdsComponent = repo.getIdsByComponentIds(componentsIds)
                    if (eventIdsComponent.isNotEmpty()) {
                        eventIds.addAll(eventIdsComponent)
                    }
                }

                if (subComponentsIds.isNotEmpty()) {
                    val eventIdsSubComponent = repo.getIdsBySubComponentIds(subComponentsIds)
                    if (eventIdsSubComponent.isNotEmpty()) {
                        eventIds.addAll(eventIdsSubComponent)
                    }
                }
            }

            val pageRequest = Pagination2Request()
            pageRequest.enablePage = false

            if (eventIds.isNotEmpty()) {
                pageRequest.paramIn?.add(ParamArray("id", "string", eventIds.distinct()))
                pageRequest.sort?.add(Sort("startDate", "asc"))
            } else {
                pageRequest.paramIs?.add(ParamSearch("id", "string", System.currentTimeMillis().toString()))
            }

            responseSuccess(data = repoNative.getPage(pageRequest)?.content)
        } catch (e: Exception) {
            throw e
        }
    }

    fun selfEventByStatus(): ResponseEntity<ReturnData> {
        try {
            val data = EventByStatus()

            val eventResources = repoEventStaff.getEventIdsByResourcesId(getUserLogin()?.resourcesId)
            if (eventResources.isNotEmpty()) {

                val doneIds = mutableListOf<String>()
                val onGoingIds = mutableListOf<String>()
                val planningIds = mutableListOf<String>()

                eventResources.forEach {
                    val done = repo.findByIdAndEndDateBeforeAndStatusAndActive(it, Date(), EVENT_STATUS_IMPLEMENTATION)
                    if (done.isPresent) {
                        if (!doneIds.contains(done.get().id)) {
                            doneIds.add(done.get().id ?: "")
                        }
                    }
                }

                eventResources.forEach {
                    val onGoing = repo.findByIdAndDateBetWeenStatus(it, Date(), EVENT_STATUS_IMPLEMENTATION)
                    if (onGoing.isPresent) {
                        if (!onGoingIds.contains(onGoing.get().id) && !doneIds.contains(onGoing.get().id)) {
                            onGoingIds.add(onGoing.get().id ?: "")
                        }
                    }
                }

                eventResources.forEach {
                    val planning = repo.findByIdAndStatusAndActive(it, EVENT_STATUS_IMPLEMENTATION)
                    if (planning.isPresent) {
                        if (!planningIds.contains(planning.get().id) && !onGoingIds.contains(planning.get().id) && !doneIds.contains(
                                planning.get().id
                            )
                        ) {
                            planningIds.add(planning.get().id ?: "")
                        }
                    }
                }

                if (doneIds.isNotEmpty()) {
                    data.done = doneIds.size.toString()
                    if (doneIds.size in 1..9) {
                        data.done = "0${data.done}"
                    }
                }

                if (onGoingIds.isNotEmpty()) {
                    data.onGoing = onGoingIds.size.toString()
                    if (onGoingIds.size in 1..9) {
                        data.onGoing = "0${data.onGoing}"
                    }
                }

                if (planningIds.isNotEmpty()) {
                    data.planning = planningIds.size.toString()
                    if (planningIds.size in 1..9) {
                        data.planning = "0${data.planning}"
                    }
                }
            }

            return responseSuccess(data = data)
        } catch (e: Exception) {
            throw e
        }
    }

    fun selfEventByStatusDatatable(req: Pagination2Request): ResponseEntity<ReturnData> {
        return try {
            val eventResources = repoEventStaff.getEventIdsByResourcesId(getUserLogin()?.resourcesId)
            if (eventResources.isNotEmpty()) {
                var filter = listOf<Event>()
                val events = repo.getByIds(eventResources)

                val searchParamStatus = req.paramIs?.find {
                    it.field == EVENT_SELF_SEARCH_FIELD
                }

                var now = Date();
                val sdf = SimpleDateFormat("yyyy-MM-dd")
                now = sdf.parse(sdf.format(now))

                if (searchParamStatus != null) {
                    if (searchParamStatus.value?.toInt() == EVENT_SELF_PLANNING) {
                        filter = events.filter { f ->
                            f.status == EVENT_STATUS_IMPLEMENTATION
//                            (
//                                    f.status == EVENT_STATUS_PLANNING
//                                    || f.status == EVENT_STATUS_PLANNING_REVISION
//                                    || f.status == EVENT_STATUS_EVALUATED_BY_COORDINATOR
//                                    || f.status == EVENT_STATUS_EVELUATION_COORDINATOR_REVISION
//                                    || f.status == EVENT_STATUS_EVALUATED_BY_TEARSURER
//                                    || f.status == EVENT_STATUS_EVALUATION_TREASURER_REVISION)
                                    && f.startDate?.after(now) == true
                                    && f.active == true
                        }
                    }


                    if (searchParamStatus.value?.toInt() == EVENT_SELF_ON_GOING) {
                        filter = events.filter { f ->
                            f.status == EVENT_STATUS_IMPLEMENTATION
                                    && f.startDate?.compareTo(now)!! <= 0
                                    && f.endDate?.compareTo(now)!! >= 0
                                    && f.active == true
                        }
                    }

                    if (searchParamStatus.value?.toInt() == EVENT_SELF_STATUS_DONE) {

                        filter = events.filter { f ->
                            f.status == EVENT_STATUS_IMPLEMENTATION
                                    && f.endDate?.before(now) == true
                                    && f.active == true
                        }
                    }
                }

                val filterIds = mutableListOf<String>()
                filter.forEach {
                    filterIds.add(it.id ?: "")
                }

                if (filterIds.isNotEmpty()) {
                    req.paramIn?.add(ParamArray("id", "string", filterIds.distinct()))
                } else {
                    req.paramIs?.add(ParamSearch("id", "string", System.currentTimeMillis().toString()))
                }
            } else {
                req.paramIs?.add(ParamSearch("id", "string", System.currentTimeMillis().toString()))
            }

            req.paramIs?.removeIf {
                it.field == EVENT_SELF_SEARCH_FIELD
            }

            responseSuccess(data = repoNative.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }

    fun updateData(id: String, request: EventRequest): ResponseEntity<ReturnData> {
        val update = repo.findByIdAndActive(id)
        return if (update.isPresent) {
            saveOrUpdate(request, update.get())
        } else {
            responseNotFound()
        }
    }

    fun saveData(req: EventRequest): ResponseEntity<ReturnData> {
        return saveOrUpdate(req)
    }

    fun saveOrUpdate(req: EventRequest, update: Event? = null): ResponseEntity<ReturnData> {
        var oldData: Event? = null
        if (update != null) {
            val objectMapper = ObjectMapper()
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            oldData = objectMapper.readValue(
                objectMapper.writeValueAsString(update), Event::class.java
            ) as Event
        }

        try {
            if (!userHasRoles(listOf(ROLE_ID_CONSULTAN, ROLE_ID_COORDINATOR, ROLE_ID_LSP))) {
                throw UnautorizedException(message = VALIDATOR_MSG_NOT_HAVE_ACCESS)
            }

            val validate = validate(req, update)
            val listMessage = validate["listMessage"] as List<ErrorMessage>
            if (listMessage.isNotEmpty()) {
                return responseBadRequest(data = listMessage)
            }

            val data: Event?
            data = update ?: Event()

            data.awpImplementationId = req.awpImplementationId
            data.name = req.name
            data.nameOtherInformation = req.nameOtherInformation
            data.locationOtherInformation = req.locationOtherInformation
            data.startDate = validate["startDate"] as Date
            data.endDate = validate["endDate"] as Date
            data.eventOtherInformation = req.eventOtherInformation
            data.eventTypeId = req.eventTypeId
            data.background = req.background
            data.description = req.description
            data.purpose = req.purpose
            data.eventOutput = req.eventOutput
            data.participantMale = req.participantMale
            data.participantFemale = req.participantFemale
            data.participantCount = req.participantCount
            data.participantOtherInformation = req.participantOtherInformation
            data.participantTarget = req.participantTarget
            data.contactPerson = req.contactPerson
            data.provinceId = req.provinceId
            data.regencyId = req.regencyId
            data.location = req.location
            data.agendaFileId = req.agendaFileId
            data.rabFileId = req.rabFileId
            data.pokEvent = req.pokEvent
            data.budgetPok = req.budgetPok
            data.activityModeId = req.activityModeId

            var taskTitle = "Membuat"
            if (update != null) {
                data.updatedBy = getUserLogin()?.id
                data.updatedAt = Date()

                if (data.status == EVENT_STATUS_PLANNING_REVISION) {
                    taskTitle = "Merevisi"

                    serviceTask.approveRejectFromTask(
                        APPROVAL_STATUS_APPROVE, data.id, TASK_TYPE_EVENT, getUserLogin()?.id, TASK_NUMBER_TASK_REVISION
                    )
                } else {
                    data.status = EVENT_STATUS_PLANNING
                }

                update.staff?.forEach {
                    if (req.staffIds?.contains(it.id) == false) {
                        it.active = false
                        it.updatedAt = Date()
                        repoEventStaff.save(it)
                        serviceResourcesSchedule.deleteFromTask(it.resourcesId, data.id, TASK_TYPE_EVENT)
                    }
                }

                update.otherStaff?.forEach {
                    if (req.otherStaffIds?.contains(it.id) == false) {
                        it.active = false
                        it.updatedAt = Date()
                        repoEventOtherStaff.save(it)
                    }
                }

                update.agendas?.forEach {
                    val filter: List<EventAgendaRequest>? = req.eventAgendas?.filter { f -> f.eventAgendaId == it.id }

                    if (filter.isNullOrEmpty()) {
                        it.active = false
                        it.updatedAt = Date()
                        repoEventAgenda.save(it)
                    }

                    it.details?.forEach { d ->
                        val filterDetail: List<EventAgendaRequest>? =
                            req.eventAgendas?.filter { f -> f.eventAgendaDetailIds?.contains(d.id) == true }

                        if (filterDetail.isNullOrEmpty()) {
                            d.active = false
                            d.updatedAt = Date()
                            repoEventAgendaDetail.save(d)
                        }
                    }
                }

                update.rab?.forEach {
                    val filter: List<EventRabRequest>? = req.eventRab?.filter { f -> f.eventRabId == it.id }

                    if (filter.isNullOrEmpty()) {
                        it.active = false
                        it.updatedAt = Date()
                        repoEventRab.save(it)
                    }

                    it.details?.forEach { d ->
                        val filterDetail: List<EventRabRequest>? =
                            req.eventRab?.filter { f -> f.eventRabDetailIds?.contains(d.id) == true }

                        if (filterDetail.isNullOrEmpty()) {
                            d.active = false
                            d.updatedAt = Date()
                            repoEventRabDetail.save(d)
                        }
                    }
                }

                update.pok?.forEach {
                    if (req.pokIds?.contains(it.id) == false) {
                        it.active = false
                        it.updatedAt = Date()
                        repoEventPok.save(it)
                    }
                }
            } else {
                data.createdBy = getUserLogin()?.id
            }

            repo.save(data)

            val resourcesList = validate["resourcesList"] as List<Resources>

            val dformNotif: DateFormat = SimpleDateFormat("dd MMMM yyyy")
            val dform: DateFormat = SimpleDateFormat("yyyy-MM-dd")

            val scheDuleDesciption = "Event ${data.name} mulai tanggal ${dformNotif.format(data.startDate)} sampai ${
                dformNotif.format(data.endDate)
            }"

            req.newStaff?.forEach {
                data.staff?.add(
                    repoEventStaff.save(
                        EventStaff(eventId = data.id, resourcesId = it, resources = resourcesList.find { fr ->
                            fr.id == it
                        })
                    )
                )

                serviceResourcesSchedule.createFromTask(
                    ResourcesSchedule(
                        resourcesId = it,
                        taskId = data.id,
                        taskType = TASK_TYPE_EVENT,
                        approveStatus = APPROVAL_STATUS_WAITING,
                        startDate = data.startDate,
                        endDate = data.endDate,
                        daysLength = getDaysBetween(data.startDate ?: Date(), data.endDate ?: Date(), dform),
                        title = TASK_TITLE_EVENT,
                        description = scheDuleDesciption
                    )
                )
            }

            req.newOtherStaff?.forEach {
                data.otherStaff?.add(
                    repoEventOtherStaff.save(
                        EventOtherStaff(
                            eventId = data.id, name = it.name, position = it.position, institution = it.institution
                        )
                    )
                )
            }

            val eventAgendas = validate["eventAgendas"] as List<EventAgenda>
            eventAgendas.forEach {
                val eventAgenda = repoEventAgenda.save(
                    EventAgenda(
                        eventId = data.id, date = it.date
                    )
                )

                data.agendas?.add(eventAgenda)

                it.details?.forEach { d ->
                    eventAgenda.details?.add(
                        repoEventAgendaDetail.save(
                            EventAgendaDetail(
                                eventAgendaId = eventAgenda.id,
                                startTime = d.startTime,
                                endTime = d.endTime,
                                activity = d.activity,
                                pic = d.pic
                            )
                        )
                    )
                }
            }

            val eventRabList = validate["eventRabList"] as List<EventRab>
            eventRabList.forEach {
                val eventRab = EventRab()
                eventRab.eventId = data.id
                eventRab.code = it.code
                eventRab.name = it.name
                eventRab.subtotal = it.subtotal
                repoEventRab.save(eventRab)

                data.rab?.add(eventRab)

                it.details?.forEach { d ->
                    eventRab.details?.add(
                        repoEventRabDetail.save(
                            EventRabDetail(
                                eventRabId = eventRab.id,
                                name = d.name,
                                quantity = d.quantity,
                                price = d.price,
                                subtotal = d.subtotal
                            )
                        )
                    )
                }
            }

            req.newPokIds?.forEach {
                data.pok?.add(
                    repoEventPok.save(
                        EventPok(
                            eventId = data.id, pokId = it
                        )
                    )
                )
            }

            val taskDescription = "$taskTitle Rencana Event ${data.name}"

            val approvalUser = validate["approvalUser"] as String
            serviceTask.createOrUpdateFromTask(
                createdBy = getUserLogin()?.id,
                taskId = data.id,
                taskType = TASK_TYPE_EVENT,
                description = taskDescription,
                createdFor = approvalUser
            )

            if (data.status == EVENT_STATUS_PLANNING_REVISION && oldData != null) {
                if (compareDataChange(oldData, data)) {
                    data.status = EVENT_STATUS_PLANNING
                    repo.save(data)
                }
            }

            return if (update == null) {
                responseCreated(data = data)
            } else {
                val ipAdress = httpServletRequest.getHeader("X-FORWARDED-FOR") ?: httpServletRequest.remoteAddr
                val userId = getUserLogin()?.id ?: ""
                Thread {
                    logActivityService.writeChangeData(
                        data,
                        oldData,
                        "event",
                        ipAdress,
                        userId
                    )
                }.start()

                responseSuccess(data = data)
            }
        } catch (e: Exception) {
            throw e
        }
    }

    fun getDetail(id: String): ResponseEntity<ReturnData> {
        try {
            val data = repo.findByIdAndActive(id)
            if (!data.isPresent) {
                return responseNotFound()
            }

            // creator allow edit
            if (data.get().createdBy == getUserLogin()?.id) {
                if (data.get().status == EVENT_STATUS_PLANNING || data.get().status == EVENT_STATUS_PLANNING_REVISION) {
                    data.get(). creatorAllowedEdit = true
                }

                // creator allow create report
                if (userHasRoles(listOf(ROLE_ID_CONSULTAN, ROLE_ID_COORDINATOR, ROLE_ID_PCU))) {
                    if (data.get().status == EVENT_STATUS_IMPLEMENTATION) {
                        val dform: DateFormat = SimpleDateFormat("yyyy-MM-dd")
                        val now: Date = dform.parse(dform.format(Date()))

                        data.get().creatorAllowCreateReport = true
                        val checkReport = repoEventReport.findByEventIdAndActive(data.get().id)
                        if (checkReport.isPresent) {
                            data.get().creatorAllowCreateReport = false
                        }

                        if (now <= data.get().endDate) {
                            data.get().creatorAllowCreateReport = false
                        }
                    }
                }
            }

            if (resourcesHasAllRoles(
                    listOf(ROLE_ID_CONSULTAN),
                    data.get().awpImplementation?.projectOfficerResources
                )
            ) {
                // approve by coordinator
                val coordinatorId =
                    findCoordinator(data.get().awpImplementation?.projectOfficerResources?.supervisiorId)
                if (getUserLogin()?.id == coordinatorId) {
                    if (data.get().status == EVENT_STATUS_PLANNING || data.get().status == EVENT_STATUS_EVELUATION_COORDINATOR_REVISION) {
                        data.get().coordinatorAllowedApprove = true
                    }
                }
            }


            // approve by pmu treasurer
            val usersPmuTreasurer = repoUserRoleViewUserResources.getUserIdsByRoleIdsComponentIdPosition(
                listOf(ROLE_ID_COORDINATOR, ROLE_ID_ADMINSTRATOR), 2, COMPONENT_ID_PMU, POSITION_TREASURER_PMU
            )
            if (usersPmuTreasurer.contains(getUserLogin()?.id)) {
                if (resourcesHasAllRoles(
                        listOf(ROLE_ID_CONSULTAN),
                        data.get().awpImplementation?.projectOfficerResources
                    )
                ) {
                    if (data.get().status == EVENT_STATUS_EVALUATED_BY_COORDINATOR) {
                        data.get().pmuTreasurerAllowedApprove = true
                    }
                }

                if (resourcesHasAllRoles(
                        listOf(ROLE_ID_COORDINATOR),
                        data.get().awpImplementation?.projectOfficerResources
                    )
                ) {
                    if (data.get().status == EVENT_STATUS_PLANNING) {
                        data.get().pmuTreasurerAllowedApprove = true
                    }
                }
            }

            // approve by pmu head
            val usersPmuHead = repoUserRoleViewUserResources.getPmuHead(
                listOf(ROLE_ID_COORDINATOR, ROLE_ID_ADMINSTRATOR),
                2,
                COMPONENT_ID_PMU,
            )
            if (usersPmuHead.contains(getUserLogin()?.id)) {
                if (data.get().status == EVENT_STATUS_EVALUATED_BY_TEARSURER) {
                    data.get().pmuHeadAllowedApprove = true
                }
            }

            // allow create report
            if (userHasRoles(listOf(ROLE_ID_ASSISTANT, ROLE_ID_STAFF, ROLE_ID_PCU))) {
                val dform: DateFormat = SimpleDateFormat("yyyy-MM-dd")
                val now: Date = dform.parse(dform.format(Date()))

                // progress
                data.get().staffOrAssistantAllowCreateProgressReport =
                        now >= data.get().startDate && now <= data.get().endDate
                                && data.get().status == EVENT_STATUS_IMPLEMENTATION

                if (data.get().staffOrAssistantAllowCreateProgressReport == true) {
                    if (userHasRoles(listOf(ROLE_ID_PCU))) {
                        val filterStaff: List<EventStaff>? = data.get().staff?.filter { fs ->
                            fs.resourcesId == getUserLogin()?.resourcesId
                        }
                        if (filterStaff.isNullOrEmpty()) {
                            data.get().staffOrAssistantAllowCreateProgressReport = false
                        }
                    }
                }

                // report
                val checkReport = repoEventReport.findByEventIdAndActive(data.get().id)
                data.get().staffOrAssistantAllowCreateReport = !checkReport.isPresent
                        && now > data.get().endDate && data.get().status == EVENT_STATUS_IMPLEMENTATION

                if (data.get().staffOrAssistantAllowCreateReport == true) {
                    data.get().staffOrAssistantAllowCreateProgressReport = false
                }

                if (data.get().staffOrAssistantAllowCreateReport == true) {
                    if (userHasRoles(listOf(ROLE_ID_PCU))) {
                        val filterStaff: List<EventStaff>? = data.get().staff?.filter { fs ->
                            fs.resourcesId == getUserLogin()?.resourcesId
                        }
                        if (filterStaff.isNullOrEmpty()) {
                            data.get().staffOrAssistantAllowCreateReport = false
                        }
                    }
                }
            }


            return responseSuccess(data = data)
        } catch (e: Exception) {
            throw e
        }
    }

    fun approveByCoordinator(request: EventEvaluationRequest): ResponseEntity<ReturnData> {
        try {
            if (!userHasRoles(listOf(ROLE_ID_COORDINATOR))) {
                throw UnautorizedException(message = VALIDATOR_MSG_NOT_HAVE_ACCESS)
            }

            val find = repo.findByIdAndActive(request.id)
            if (!find.isPresent) {
                return responseNotFound()
            }

            val validate = validateApproveCoordinator()
            val listMessage = validate["listMessage"] as List<ErrorMessage>
            if (listMessage.isNotEmpty()) {
                return responseBadRequest(data = listMessage)
            }

            var status = EVENT_STATUS_EVALUATED_BY_COORDINATOR
            if (request.approveStatus == APPROVAL_STATUS_REJECT) {
                status = EVENT_STATUS_PLANNING_REVISION

                serviceTask.createRevisionTask(
                    taskId = find.get().id,
                    taskType = TASK_TYPE_EVENT,
                    createdFor = getUserLogin()?.id,
                    description = "Revisi Rencana Event ${find.get().name}"
                )
            } else {
                // create task for pmu
                var title = ""
                if (find.get().status == EVENT_STATUS_EVELUATION_COORDINATOR_REVISION) {
                    title = " Revisi"
                }

                val taskDescription = "Persetujuan$title Evaluasi Event ${find.get().name}"
                val usersPmu = validate["usersPmu"] as List<String>

                usersPmu.forEach {
                    serviceTask.createOrUpdateFromTask(
                        createdBy = getUserLogin()?.id,
                        taskId = find.get().id,
                        taskType = TASK_TYPE_EVENT,
                        description = taskDescription,
                        createdFor = it
                    )
                }
            }

            find.get().status = status
            find.get().updatedBy = getUserLogin()?.id
            find.get().updatedAt = Date()
            repo.save(find.get())

            repoEventApprovalNoteRepository.save(
                EventApprovalNote(
                    eventId = find.get().id,
                    userId = getUserLogin()?.id,
                    createdFrom = EVENT_APPROVAL_MESSAGE_FROM_COORDINATOR,
                    note = request.note,
                    approvalStatus = request.approveStatus,
                )
            )

            serviceTask.approveRejectFromTask(
                APPROVAL_STATUS_APPROVE, find.get().id, TASK_TYPE_EVENT, getUserLogin()?.id, TASK_NUMBER_TASK_REVISION
            )

            serviceTask.approveRejectFromTask(
                request.approveStatus, find.get().id, TASK_TYPE_EVENT, getUserLogin()?.id, TASK_NUMBER_TASK
            )

            return responseSuccess()
        } catch (e: Exception) {
            throw e
        }
    }

    fun approveByTreasurerPmu(request: EventEvaluationRequest): ResponseEntity<ReturnData> {
        try {
            if (!userHasAllRoles(listOf(ROLE_ID_COORDINATOR, ROLE_ID_ADMINSTRATOR))) {
                throw UnautorizedException(message = VALIDATOR_MSG_NOT_HAVE_ACCESS)
            }

            val find = repo.findByIdAndActive(request.id)
            if (!find.isPresent) {
                return responseNotFound()
            }

            val validate = validateApproveTreasurerPmu(request)
            val listMessage = validate["listMessage"] as List<ErrorMessage>
            if (listMessage.isNotEmpty()) {
                return responseBadRequest(data = listMessage)
            }

            var status = EVENT_STATUS_EVALUATED_BY_TEARSURER
            if (request.approveStatus == APPROVAL_STATUS_REJECT) {
                status = EVENT_STATUS_EVELUATION_COORDINATOR_REVISION

                if (resourcesHasAllRoles(
                        listOf(ROLE_ID_COORDINATOR),
                        find.get().awpImplementation?.projectOfficerResources
                    )
                ) {
                    status = EVENT_STATUS_PLANNING_REVISION
                }

                serviceTask.createRevisionTask(
                    taskId = find.get().id,
                    taskType = TASK_TYPE_EVENT,
                    createdFor = getUserLogin()?.id,
                    description = "Revisi Evaluasi Rencana Event ${find.get().name}"
                )
            } else {
                // create task for pmu
                var title = ""
                if (find.get().status == EVENT_STATUS_EVALUATION_TREASURER_REVISION) {
                    title = " Revisi"
                }

                val taskDescription = "Persetujuan$title Evaluasi Event ${find.get().name}"
                val usersPmu = validate["usersPmu"] as List<String>

                usersPmu.forEach {
                    serviceTask.createOrUpdateFromTask(
                        createdBy = getUserLogin()?.id,
                        taskId = find.get().id,
                        taskType = TASK_TYPE_EVENT,
                        description = taskDescription,
                        createdFor = it
                    )
                }
            }

            find.get().budgetAvailable = request.budgetAvailable
            find.get().status = status
            find.get().updatedBy = getUserLogin()?.id
            find.get().updatedAt = Date()
            repo.save(find.get())

            repoEventApprovalNoteRepository.save(
                EventApprovalNote(
                    eventId = find.get().id,
                    userId = getUserLogin()?.id,
                    createdFrom = EVENT_APPROVAL_MESSAGE_FROM_PMU_TREASURER,
                    note = request.note,
                    approvalStatus = request.approveStatus
                )
            )

            serviceTask.approveRejectFromTask(
                request.approveStatus, find.get().id, TASK_TYPE_EVENT, getUserLogin()?.id
            )

            return responseSuccess()
        } catch (e: Exception) {
            throw e
        }
    }

    fun approveByHeadPmu(request: EventEvaluationRequest): ResponseEntity<ReturnData> {
        try {
            if (!userHasAllRoles(listOf(ROLE_ID_COORDINATOR, ROLE_ID_ADMINSTRATOR))) {
                throw UnautorizedException(message = VALIDATOR_MSG_NOT_HAVE_ACCESS)
            }

            val find = repo.findByIdAndActive(request.id)
            if (!find.isPresent) {
                return responseNotFound()
            }

            val validate = validateApproveHeadPmu(request, find.get())
            val listMessage = validate["listMessage"] as List<ErrorMessage>
            if (listMessage.isNotEmpty()) {
                return responseBadRequest(data = listMessage)
            }

            var status = EVENT_STATUS_IMPLEMENTATION
            if (request.approveStatus == APPROVAL_STATUS_REJECT) {
                status = EVENT_STATUS_EVELUATION_COORDINATOR_REVISION

                if (resourcesHasAllRoles(
                        listOf(ROLE_ID_COORDINATOR),
                        find.get().awpImplementation?.projectOfficerResources
                    )
                ) {
                    status = EVENT_STATUS_PLANNING_REVISION
                }
            }

            find.get().status = status
            find.get().updatedBy = getUserLogin()?.id
            find.get().updatedAt = Date()
            repo.save(find.get())

            repoEventApprovalNoteRepository.save(
                EventApprovalNote(
                    eventId = find.get().id,
                    userId = getUserLogin()?.id,
                    createdFrom = EVENT_APPROVAL_MESSAGE_FROM_PMU_HEAD,
                    note = request.note,
                    approvalStatus = request.approveStatus
                )
            )

            if (request.approveStatus == APPROVAL_STATUS_REJECT) {
                serviceTask.createRevisionTask(
                    taskId = find.get().id,
                    taskType = TASK_TYPE_EVENT,
                    createdFor = getUserLogin()?.id,
                    description = "Revisi Evaluasi Event ${find.get().name}",
                    step = 2
                )
            }

            serviceTask.approveRejectFromTask(
                request.approveStatus, find.get().id, TASK_TYPE_EVENT, getUserLogin()?.id
            )

            find.get().staff?.forEach {
                serviceResourcesSchedule.approveFromTask(
                    request.approveStatus, it.resourcesId, find.get().id, TASK_TYPE_EVENT
                )
            }

            return responseSuccess()
        } catch (e: Exception) {
            throw e
        }
    }

    private fun findCoordinator(supervisiorId: String?): String? {
        findCoordinatorCounter++
        try {
            val findResources = repoResources.findByIdAndActive(supervisiorId)
            if (findResources.isPresent) {
                val filterRole = findResources.get().user?.roles?.filter {
                    it.roleId == ROLE_ID_COORDINATOR
                }
                return if (!filterRole.isNullOrEmpty()) {
                    findResources.get().userId
                } else {
                    if (!findResources.get().supervisiorId.isNullOrEmpty()) {
                        findCoordinator(findResources.get().supervisiorId)
                    } else {
                        null
                    }
                }
            } else {
                return null
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return null
    }

    fun getStaffSupervisiorId(supervisiorId: String, first: Boolean = false): List<String> {
        try {
            if (first) {
                staffSupervisiorIdsTemp.clear()
            }
            val findStaffSupervior: List<String> = repoResources.getBySupervisiorIdAndActive(supervisiorId)
            if (findStaffSupervior.isNotEmpty()) {
                staffSupervisiorIdsTemp.add(supervisiorId)
                findStaffSupervior.forEach {
                    if (repoResources.countBySupervisiorIdAndActive(it) > 0) {
                        getStaffSupervisiorId(it)
                    }
                }
            }
        } catch (e: Exception) {
            throw e
        }

        return staffSupervisiorIdsTemp.toList()
    }

    private fun validate(request: EventRequest, update: Event? = null): Map<String, Any?> {
        val rData = mutableMapOf<String, Any?>()
        try {
            val listMessage: MutableList<ErrorMessage> = emptyList<ErrorMessage>().toMutableList()

            val awpImplementation = repoAwpImplementation.findByIdAndActive(request.awpImplementationId)
            if (!awpImplementation.isPresent) {
                listMessage.add(
                    ErrorMessage(
                        "awpImplementation",
                        "Awp Impementation id ${request.awpImplementationId} $VALIDATOR_MSG_NOT_FOUND"
                    )
                )
            } else {
                rData["awpImplementation"] = awpImplementation.get()
            }

            var dateStart: Date? = null
            var dateEnd: Date? = null
            val dform: DateFormat = SimpleDateFormat("yyyy-MM-dd")

            try {
                dateStart = dform.parse(request.startDate)
            } catch (_: Exception) {
                listMessage.add(
                    ErrorMessage(
                        "startDate", "Tanggal Mulai $VALIDATOR_MSG_NOT_VALID"
                    )
                )
            }

            try {
                dateEnd = dform.parse(request.endDate)
            } catch (_: Exception) {
                listMessage.add(
                    ErrorMessage(
                        "endDate", "Tanggal Selesai $VALIDATOR_MSG_NOT_VALID"
                    )
                )
            }

            if (dateEnd?.before(dateStart) == true) {
                listMessage.add(
                    ErrorMessage(
                        "endDate", "Tanggal Selesai $VALIDATOR_MSG_NOT_VALID"
                    )
                )
            }

            rData["startDate"] = dateStart as Date
            rData["endDate"] = dateEnd as Date

            val eventType = repoEventType.findByIdAndActive(request.eventTypeId)
            if (!eventType.isPresent) {
                listMessage.add(
                    ErrorMessage(
                        "eventTypeId", "Event Type id ${request.eventTypeId} $VALIDATOR_MSG_NOT_FOUND"
                    )
                )
            }

            val province = repoProvince.findByIdAndActive(request.provinceId)
            if (!province.isPresent) {
                listMessage.add(
                    ErrorMessage(
                        "provinceId", "Provinsi id ${request.provinceId} $VALIDATOR_MSG_NOT_FOUND"
                    )
                )
            }

            if (!request.regencyId.isNullOrEmpty()) {
                val regency = repoRegency.findByIdAndActive(request.regencyId)
                if (!regency.isPresent) {
                    listMessage.add(
                        ErrorMessage(
                            "regencyId", "Regency id ${request.regencyId} $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                }
            }

            if (!request.activityModeId.isNullOrEmpty()) {
                val activityMode = repoActivityMode.findByIdAndActive(request.activityModeId)
                if (!activityMode.isPresent) {
                    listMessage.add(
                        ErrorMessage(
                            "activityModeId", "Activity Mode id ${request.activityModeId} $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                }
            }

            val eventAgendas = mutableListOf<EventAgenda>()
            val dtform: DateFormat = SimpleDateFormat("HH:mm")

            request.newEventAgendas?.forEach {
                var date: Date? = null
                try {
                    date = dform.parse(it.date)
                } catch (_: Exception) {
                    listMessage.add(
                        ErrorMessage(
                            "newEventAgendas", "Tanggal Agenda $VALIDATOR_MSG_NOT_VALID"
                        )
                    )
                }

                val eventAgenda = EventAgenda()
                eventAgenda.date = date

                it.details.forEach { d ->
                    var startTime: Date? = null
                    try {
                        startTime = dtform.parse(d.startTime)
                    } catch (_: Exception) {
                        listMessage.add(
                            ErrorMessage(
                                "newEventAgendas", "Waktu Mulai Agenda $VALIDATOR_MSG_NOT_VALID"
                            )
                        )
                    }

                    var endTime: Date? = null
                    try {
                        endTime = dtform.parse(d.endTime)
                    } catch (_: Exception) {
                        listMessage.add(
                            ErrorMessage(
                                "newEventAgendas", "Waktu Selesai Agenda $VALIDATOR_MSG_NOT_VALID"
                            )
                        )
                    }

                    eventAgenda.details?.add(
                        EventAgendaDetail(
                            startTime = startTime, endTime = endTime, activity = d.activity, pic = d.pic
                        )
                    )
                }

                eventAgendas.add(eventAgenda)
            }

            rData["eventAgendas"] = eventAgendas

            val eventRabList = mutableListOf<EventRab>()
            request.newEventRab?.forEach {
                val eventRab = EventRab()
                eventRab.code = it.code
                eventRab.name = it.name

                var subtotal: Long = 0
                it.details?.forEach { d ->
                    val eventRabDetail = EventRabDetail()
                    eventRabDetail.name = d.name
                    eventRabDetail.quantity = d.quantity
                    eventRabDetail.price = d.price

                    if (eventRabDetail.quantity != null && eventRabDetail.price != null) {
                        eventRabDetail.subtotal = eventRabDetail.quantity!! * eventRabDetail.price!!
                        subtotal += eventRabDetail.subtotal!!
                    }

                    eventRab.details?.add(eventRabDetail)
                }

                eventRab.subtotal = subtotal

                eventRabList.add(eventRab)
            }

            rData["eventRabList"] = eventRabList


            val resourcesList = mutableListOf<Resources>()
            request.newStaff?.forEach {
                val resources = repoResources.findByIdAndActive(it)
                if (!resources.isPresent) {
                    listMessage.add(
                        ErrorMessage(
                            "newStaff", "Resources id $it $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                } else {
                    val checkEventStaff = repoEventStaff.findByResourcesIdStartDateEndDate(
                        it, dateStart, dateEnd, EVENT_STATUS_IMPLEMENTATION
                    )
                    if (checkEventStaff.isNotEmpty()) {
                        var resourcesName = resources.get().user?.firstName
                        if (!resources.get().user?.lastName.isNullOrEmpty()) {
                            resourcesName += " ${resources.get().user?.lastName}"
                        }

                        val errorMessage = ErrorMessage(
                            "newStaff",
                            "Staff $resourcesName sudah terdaftar pada Event ${checkEventStaff[0].event?.name} tanggal ${
                                dform.format(
                                    checkEventStaff[0].event?.startDate
                                )
                            } sampai ${dform.format(checkEventStaff[0].event?.endDate)}"
                        )

                        if (update != null) {
                            if (update.id != checkEventStaff[0].eventId) {
                                listMessage.add(errorMessage)
                            }
                        } else {
                            listMessage.add(errorMessage)
                        }
                    } else {
                        resourcesList.add(resources.get())
                    }
                }
            }

            rData["resourcesList"] = resourcesList


            if (!request.rabFileId.isNullOrEmpty()) {
                val checkFile: ReturnDataFiles? = RequestHelpers.filesGetById(
                    request.rabFileId ?: "", filesUrl, httpServletRequest.getHeader(HEADER_STRING)
                )

                var checkedFile: Files? = null
                if (checkFile != null) {
                    if (checkFile.data != null) {
                        checkedFile = checkFile.data
                    }
                }

                if (checkedFile == null) {
                    listMessage.add(
                        ErrorMessage(
                            "rabFileId", "Rab File Id ${request.rabFileId} $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                }
            }

            if (!request.agendaFileId.isNullOrEmpty()) {
                val checkFile: ReturnDataFiles? = RequestHelpers.filesGetById(
                    request.agendaFileId ?: "", filesUrl, httpServletRequest.getHeader(HEADER_STRING)
                )

                var checkedFile: Files? = null
                if (checkFile != null) {
                    if (checkFile.data != null) {
                        checkedFile = checkFile.data
                    }
                }

                if (checkedFile == null) {
                    listMessage.add(
                        ErrorMessage(
                            "agendaFileId", "Agenda File Id ${request.rabFileId} $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                }
            }

            if (awpImplementation.isPresent) {
                var approvalUser: String? = null
                if (resourcesHasAllRoles(listOf(ROLE_ID_CONSULTAN), awpImplementation.get().projectOfficerResources)) {
                    findCoordinatorCounter = 0
                    val coordinatorId = findCoordinator(awpImplementation.get().projectOfficer)
                    if (coordinatorId.isNullOrEmpty()) {
                        listMessage.add(
                            ErrorMessage(
                                "projectOfficer", "Koordinator Project Officer $VALIDATOR_MSG_NOT_FOUND"
                            )
                        )
                    }

                    approvalUser = coordinatorId
                } else if (resourcesHasAllRoles(
                        listOf(ROLE_ID_COORDINATOR),
                        awpImplementation.get().projectOfficerResources
                    )
                ) {
                    val usersPmuTreasurer = repoUserRoleViewUserResources.getUserIdsByRoleIdsComponentIdPosition(
                        listOf(ROLE_ID_COORDINATOR, ROLE_ID_ADMINSTRATOR), 2, COMPONENT_ID_PMU, POSITION_TREASURER_PMU
                    )
                    if (usersPmuTreasurer.isEmpty()) {
                        listMessage.add(
                            ErrorMessage(
                                "projectOfficer", "User $POSITION_TREASURER_PMU $VALIDATOR_MSG_NOT_FOUND"
                            )
                        )
                    } else {
                        approvalUser = usersPmuTreasurer[0]
                    }
                }

                rData["approvalUser"] = approvalUser
            }


            request.newPokIds?.forEach {
                val checkPok = repoPok.findByIdAndActive(it)
                if (!checkPok.isPresent) {
                    listMessage.add(
                        ErrorMessage(
                            "newPokIds", "POK id $it $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                }
            }

            if (!request.contactPerson.isNullOrEmpty()) {
                val checkResources = repoResources.findByIdAndActive(request.contactPerson)
                if (!checkResources.isPresent) {
                    listMessage.add(
                        ErrorMessage(
                            "contactPerson", "Contact Person Project Officer $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                }
            }

            rData["listMessage"] = listMessage
        } catch (e: Exception) {
            throw e
        }

        return rData
    }

    private fun validateApproveCoordinator(): Map<String, Any> {
        val rData = mutableMapOf<String, Any>()
        try {
            val listMessage: MutableList<ErrorMessage> = emptyList<ErrorMessage>().toMutableList()

            val usersPmu = repoUserRoleViewUserResources.getUserIdsByRoleIdsComponentIdPosition(
                listOf(ROLE_ID_COORDINATOR, ROLE_ID_ADMINSTRATOR), 2, COMPONENT_ID_PMU, POSITION_TREASURER_PMU
            )

            if (usersPmu.isEmpty()) {
                listMessage.add(
                    ErrorMessage(
                        "approveStatus", "User $POSITION_TREASURER_PMU $VALIDATOR_MSG_NOT_FOUND"
                    )
                )
            } else {
                rData["usersPmu"] = usersPmu
            }

            rData["listMessage"] = listMessage
        } catch (e: Exception) {
            throw e
        }

        return rData
    }

    private fun validateApproveTreasurerPmu(request: EventEvaluationRequest): Map<String, Any> {
        val rData = mutableMapOf<String, Any>()
        try {
            val listMessage: MutableList<ErrorMessage> = emptyList<ErrorMessage>().toMutableList()

            val usersPmu = repoUserRoleViewUserResources.getPmuHead(
                listOf(ROLE_ID_COORDINATOR, ROLE_ID_ADMINSTRATOR), 2, COMPONENT_ID_PMU
            )

            if (usersPmu.isEmpty()) {
                listMessage.add(
                    ErrorMessage(
                        "approveStatus", "User $POSITION_TREASURER_PMU $VALIDATOR_MSG_NOT_FOUND"
                    )
                )
            } else {
                rData["usersPmu"] = usersPmu
            }


            if (request.budgetAvailable == null) {
                listMessage.add(
                    ErrorMessage(
                        "budgetAvailable", "Anggaran Tersedia $VALIDATOR_MSG_REQUIRED"
                    )
                )
            }


            rData["listMessage"] = listMessage
        } catch (e: Exception) {
            throw e
        }

        return rData
    }

    private fun validateApproveHeadPmu(request: EventEvaluationRequest, event: Event): Map<String, Any> {
        val rData = mutableMapOf<String, Any>()
        try {
            val listMessage: MutableList<ErrorMessage> = emptyList<ErrorMessage>().toMutableList()

            if (request.approveStatus == APPROVAL_STATUS_APPROVE) {
                val dform: DateFormat = SimpleDateFormat("yyyy-MM-dd")
                event.staff?.forEach {
                    val checkEventStaff = repoEventStaff.findByResourcesIdStartDateEndDate(
                        it.resourcesId, event.startDate, event.endDate, EVENT_STATUS_IMPLEMENTATION
                    )

                    if (checkEventStaff.isNotEmpty()) {
                        var resourcesName = it.resources?.user?.firstName
                        if (!it.resources?.user?.lastName.isNullOrEmpty()) {
                            resourcesName += " ${it.resources?.user?.lastName}"
                        }

                        val errorMessage = ErrorMessage(
                            "approveStatus",
                            "Staff $resourcesName sudah terdaftar pada Event ${checkEventStaff[0].event?.name} tanggal ${
                                dform.format(
                                    checkEventStaff[0].event?.startDate
                                )
                            } sampai ${dform.format(checkEventStaff[0].event?.endDate)}"
                        )

                        if (event.id != checkEventStaff[0].eventId) {
                            listMessage.add(errorMessage)
                        }
                    }
                }
            }

            rData["listMessage"] = listMessage

        } catch (e: Exception) {
            throw e
        }

        return rData
    }
}

