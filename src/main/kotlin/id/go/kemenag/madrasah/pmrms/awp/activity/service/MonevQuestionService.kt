package id.go.kemenag.madrasah.pmrms.awp.activity.service

import id.go.kemenag.madrasah.pmrms.awp.activity.constant.MONEV_QUESTION_MAPPING_BY_TYPE_TO_QUESTION
import id.go.kemenag.madrasah.pmrms.awp.activity.helpers.responseSuccess
import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.ParamArray
import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.ParamSearch
import id.go.kemenag.madrasah.pmrms.awp.activity.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.awp.activity.repository.native.MonevQuestionRepositoryNative
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service

@Suppress("UNCHECKED_CAST")
@Service
class MonevQuestionService {

    @Autowired
    private lateinit var repoNative: MonevQuestionRepositoryNative

    fun datatable(req: Pagination2Request): ResponseEntity<ReturnData> {
        return try {
            val requestResultChainType: ParamSearch? = req.paramIs?.find { f -> f.field == "monevTypeId" }
            if (requestResultChainType != null) {
                val ids = MONEV_QUESTION_MAPPING_BY_TYPE_TO_QUESTION[requestResultChainType.value]
                if (!ids.isNullOrEmpty()) {
                    req.paramIn?.add(ParamArray("id", "string", ids))
                } else {
                    req.paramIn?.add(ParamArray("id", "string", listOf(System.currentTimeMillis().toString())))
                }

                req.paramIs?.remove(requestResultChainType)
            }
            responseSuccess(data = repoNative.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }
}
