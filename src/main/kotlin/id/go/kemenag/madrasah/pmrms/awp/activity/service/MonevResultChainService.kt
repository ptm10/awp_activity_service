package id.go.kemenag.madrasah.pmrms.awp.activity.service

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import id.go.kemenag.madrasah.pmrms.awp.activity.constant.VALIDATOR_MSG_NOT_FOUND
import id.go.kemenag.madrasah.pmrms.awp.activity.helpers.*
import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.MonevResultChainRequest
import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.MonevResultChainUpdateResultRequest
import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.awp.activity.model.response.ErrorMessage
import id.go.kemenag.madrasah.pmrms.awp.activity.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.*
import id.go.kemenag.madrasah.pmrms.awp.activity.repository.*
import id.go.kemenag.madrasah.pmrms.awp.activity.repository.native.MonevResultChainRepositoryNative
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import javax.servlet.http.HttpServletRequest


@Suppress("UNCHECKED_CAST")
@Service
class MonevResultChainService {

    @Autowired
    private lateinit var repo: MonevResultChainRepository

    @Autowired
    private lateinit var repoComponent: ComponentRepository

    @Autowired
    private lateinit var repoPdo: PdoRepository

    @Autowired
    private lateinit var repoIri: IriRepository

    @Autowired
    private lateinit var repoMonevType: MonevTypeRepository

    @Autowired
    private lateinit var repoMonevQuestion: MonevQuestionRepository

    @Autowired
    private lateinit var repoMonevResultChainQuestion: MonevResultChainQuestionRepository

    @Autowired
    private lateinit var repoNative: MonevResultChainRepositoryNative

    @Autowired
    private lateinit var repoMonevSuccessIndicator: MonevSuccessIndicatorRepository

    @Autowired
    private lateinit var repoMonevSuccessIndicatorFiles: MonevSuccessIndicatorFIlesRepository

    @Autowired
    private lateinit var logActivityService: LogActivityService

    @Autowired
    private lateinit var httpServletRequest: HttpServletRequest

    fun datatable(req: Pagination2Request): ResponseEntity<ReturnData> {
        return try {
            responseSuccess(data = repoNative.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }

    fun saveData(request: MonevResultChainRequest): ResponseEntity<ReturnData> {
        try {
            return saveOrUpdate(request)
        } catch (e: Exception) {
            throw e
        }
    }

    fun updateData(id: String, request: MonevResultChainRequest): ResponseEntity<ReturnData> {
        val update = repo.findByIdAndActive(id)
        return if (update.isPresent) {
            saveOrUpdate(request, update.get())
        } else {
            responseNotFound()
        }
    }

    fun getDetail(id: String): ResponseEntity<ReturnData> {
        try {
            val data = repo.findByIdAndActive(id)
            if (data.isPresent) {
                return responseSuccess(data = data.get())
            }
            return responseNotFound()
        } catch (e: Exception) {
            throw e
        }
    }

    fun saveOrUpdate(req: MonevResultChainRequest, update: MonevResultChain? = null): ResponseEntity<ReturnData> {
        var oldData: MonevResultChain? = null
        if (update != null) {
            val objectMapper = ObjectMapper()
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            oldData = objectMapper.readValue(
                objectMapper.writeValueAsString(update), MonevResultChain::class.java
            ) as MonevResultChain
        }
        try {
            val validate = validate(req, update)
            val listMessage = validate["listMessage"] as List<ErrorMessage>
            if (listMessage.isNotEmpty()) {
                return responseBadRequest(data = listMessage)
            }

            val data: MonevResultChain?
            data = update ?: validate["monevResultChain"] as MonevResultChain

            data.year = req.year
            data.monevTypeId = req.monevTypeId
            data.componentId = req.componentId
            data.subComponentId = req.subComponentId
            data.parrentId = req.parrentId
            data.code = req.code
            data.name = req.name

            if (update == null) {
                data.createdBy = getUserLogin()?.id
            } else {
                data.updatedBy = getUserLogin()?.id
                data.updatedAt = Date()
            }

            repo.save(data)

            req.newSuccessIndicators?.forEach {
                try {
                    var successIndicator = MonevSuccessIndicator()

                    val check = repoMonevSuccessIndicator.findByMonevResultChainIdAndCodeAndActive(data.id, it.code)
                    if (check.isPresent) {
                        successIndicator = check.get()
                        successIndicator.updatedAt = Date()
                    }

                    successIndicator.monevResultChainId = data.id
                    successIndicator.code = it.code
                    successIndicator.name = it.name
                    successIndicator.pdoId = it.pdoId
                    successIndicator.iriId = it.iriId
                    successIndicator.baselineValue = it.baselineValue
                    successIndicator.targetTotal = it.targetTotal

                    successIndicator.cumulativeTarget2020 = it.cumulativeTarget2020
                    successIndicator.cumulativeTargetDummy2020 = it.cumulativeTargetDummy2020
                    successIndicator.notApplicable2020 = it.notApplicable2020

                    successIndicator.cumulativeTarget2021 = it.cumulativeTarget2021
                    successIndicator.cumulativeTargetDummy2021 = it.cumulativeTargetDummy2021
                    successIndicator.notApplicable2021 = it.notApplicable2021

                    successIndicator.cumulativeTarget2022 = it.cumulativeTarget2022
                    successIndicator.cumulativeTargetDummy2022 = it.cumulativeTargetDummy2022
                    successIndicator.notApplicable2022 = it.notApplicable2022

                    successIndicator.cumulativeTarget2023 = it.cumulativeTarget2023
                    successIndicator.cumulativeTargetDummy2023 = it.cumulativeTargetDummy2023
                    successIndicator.notApplicable2023 = it.notApplicable2023

                    successIndicator.cumulativeTarget2024 = it.cumulativeTarget2024
                    successIndicator.cumulativeTargetDummy2024 = it.cumulativeTargetDummy2024
                    successIndicator.notApplicable2024 = it.notApplicable2024

                    repoMonevSuccessIndicator.save(successIndicator)
                } catch (e: Exception) {
                    throw e
                }
            }


            val newQuestions = validate["newQuestions"] as List<MonevQuestion>?

            newQuestions?.forEachIndexed { index, monevQuestion ->
                for (i in 2020..2024) {
                    if (!repoMonevResultChainQuestion.findByYearAndMonevResultChainIdAndMonevQuestionIdAndActive(
                            i,
                            data.id,
                            monevQuestion.id
                        ).isPresent
                    ) {
                        repoMonevResultChainQuestion.save(
                            MonevResultChainQuestion(
                                monevResultChainId = data.id,
                                monevQuestionId = monevQuestion.id,
                                number = index + 1,
                                year = i
                            )
                        )
                    }
                }
            }

            req.deletedSuccessIndicatorsIds?.forEach {
                val deleted = repoMonevSuccessIndicator.findByIdAndActive(it)
                if (deleted.isPresent) {
                    deleted.get().active = false
                    deleted.get().deletedAt = Date()
                    deleted.get().deletedBy = getUserLogin()?.id
                    repoMonevSuccessIndicator.save(deleted.get())
                }
            }

            req.deletedQuestionIds?.forEach {
                val deleted = repoMonevResultChainQuestion.findByIdAndActive(it)
                if (deleted.isPresent) {
                    deleted.get().active = false
                    deleted.get().updatedAt = Date()
                    repoMonevResultChainQuestion.save(deleted.get())

                    for (i in 1..4) {
                        val deletedOther =
                            repoMonevResultChainQuestion.findByYearAndMonevResultChainIdAndMonevQuestionIdAndActive(
                                "202$i".toInt(),
                                data.id,
                                deleted.get().monevQuestionId
                            )

                        if (deletedOther.isPresent) {
                            deletedOther.get().active = false
                            deletedOther.get().updatedAt = Date()
                            repoMonevResultChainQuestion.save(deletedOther.get())
                        }
                    }
                }
            }

            for (i in 2020..2024) {
                repoMonevResultChainQuestion.findByYearAndMonevResultChainIdAndActiveOrderByNumber(
                    i, data.id
                ).forEachIndexed { index, monevResultChainQuestion ->
                    monevResultChainQuestion.number = index + 1
                    repoMonevResultChainQuestion.save(monevResultChainQuestion)
                }
            }

            return if (update == null) {
                responseCreated(data = data)
            } else {
                val ipAdress = httpServletRequest.getHeader("X-FORWARDED-FOR") ?: httpServletRequest.remoteAddr
                val userId = getUserLogin()?.id ?: ""
                Thread {
                    logActivityService.writeChangeData(
                        data,
                        oldData,
                        "monev_result_chain",
                        ipAdress,
                        userId
                    )
                }.start()

                responseSuccess(data = data)
            }
        } catch (e: Exception) {
            throw e
        }
    }

    fun updateResult(id: String, req: MonevResultChainUpdateResultRequest): ResponseEntity<ReturnData> {
        try {
            val data = repo.findByIdAndActive(id)
            if (!data.isPresent) {
                return responseNotFound()
            }

            when (req.year) {
                2020 -> {
                    data.get().status2020 = req.status
                }

                2021 -> {
                    data.get().status2021 = req.status
                }

                2022 -> {
                    data.get().status2022 = req.status
                }

                2023 -> {
                    data.get().status2023 = req.status
                }

                2024 -> {
                    data.get().status2024 = req.status
                }
            }

            repo.save(data.get())

            req.successIndicatorResult?.forEach {
                data.get().successIndicator?.filter { f ->
                    f.id == it.id
                }?.forEach { msi ->
                    when (req.year) {
                        2020 -> {
                            msi.result2020 = it.result
                        }

                        2021 -> {
                            msi.result2021 = it.result
                        }

                        2022 -> {
                            msi.result2022 = it.result
                        }

                        2023 -> {
                            msi.result2023 = it.result
                        }

                        2024 -> {
                            msi.result2024 = it.result
                        }
                    }

                    repoMonevSuccessIndicator.save(msi)
                    setFilesSuccessIndicator(req.year, msi, it.newFilesIds, it.deletedFilesIds)
                }
            }

            req.answers?.forEach {
                data.get().questions?.filter { f ->
                    f.id == it.id && f.year == req.year
                }?.forEach { q ->
                    q.yesOrNoAnswer = it.yesOrNoAnswer
                    q.answer = it.answer
                    repoMonevResultChainQuestion.save(q)
                }
            }

            return responseSuccess(data = data.get())
        } catch (e: Exception) {
            throw e
        }
    }

    private fun setFilesSuccessIndicator(
        year: Int?,
        msi: MonevSuccessIndicator,
        newFilesIds: List<String>?,
        deletedFilesIds: List<String>?
    ) {
        try {
            newFilesIds?.forEach {
                repoMonevSuccessIndicatorFiles.save(
                    MonevSuccessIndicatorFiles(
                        successIndicatorId = msi.id,
                        year = year,
                        fileId = it
                    )
                )
            }

            deletedFilesIds?.forEach {
                val delete = repoMonevSuccessIndicatorFiles.findByIdAndActive(it)
                if (delete.isPresent) {
                    delete.get().updatedAt = Date()
                    delete.get().active = false
                    repoMonevSuccessIndicatorFiles.save(delete.get())
                }
            }

        } catch (e: Exception) {
            throw e
        }
    }

    fun groupList(yearParam: Int?): ResponseEntity<ReturnData> {
        try {
            val datas: MutableList<ResultChainParrentChild> = emptyList<ResultChainParrentChild>().toMutableList()

            val dform: DateFormat = SimpleDateFormat("yyyy")
            val year = yearParam ?: dform.format(Date()).toInt()
            val all: List<MonevResultChain> = repo.getAllByYearAndActiveOrderByCode(year)

            all.filter { af -> af.parrentId == null }.forEachIndexed { index, monevResultChain ->
                if (index == 0) {
                    val resultChain = ResultChainParrentChild()
                    resultChain.id = monevResultChain.id
                    resultChain.resultChainCode = monevResultChain.code
                    resultChain.result = monevResultChain.name
                    resultChain.label = "${resultChain.resultChainCode} - ${resultChain.result}"
                    resultChain.resultStatus2020 = monevResultChain.status2020
                    resultChain.resultStatus2021 = monevResultChain.status2021
                    resultChain.resultStatus2022 = monevResultChain.status2022
                    resultChain.resultStatus2023 = monevResultChain.status2023
                    resultChain.resultStatus2024 = monevResultChain.status2024
                    resultChain.children = findChild(monevResultChain, all)
                    datas.add(resultChain)
                }
            }

            return responseSuccess(data = datas)
        } catch (e: Exception) {
            throw e
        }
    }

    private fun findChild(
        resultChain: MonevResultChain, all: List<MonevResultChain>
    ): List<ResultChainParrentChild> {
        try {
            val datas: MutableList<ResultChainParrentChild> = emptyList<ResultChainParrentChild>().toMutableList()

            all.filter { af -> af.parrentId == resultChain.id }.forEach {
                val parrentChild = ResultChainParrentChild()
                parrentChild.id = it.id
                parrentChild.resultChainCode = it.code
                parrentChild.result = it.name
                parrentChild.label = "${parrentChild.resultChainCode} - ${parrentChild.result}"
                parrentChild.resultStatus2020 = it.status2020
                parrentChild.resultStatus2021 = it.status2021
                parrentChild.resultStatus2022 = it.status2022
                parrentChild.resultStatus2023 = it.status2023
                parrentChild.resultStatus2024 = it.status2024

                if (all.any { ac -> ac.parrentId == it.id }) {
                    parrentChild.children = findChild(it, all)
                }

                datas.add(parrentChild)
            }

            return datas
        } catch (e: Exception) {
            throw e
        }
    }

    private fun validate(request: MonevResultChainRequest, update: MonevResultChain? = null): Map<String, Any?> {
        val rData = mutableMapOf<String, Any?>()
        try {
            val listMessage: MutableList<ErrorMessage> = emptyList<ErrorMessage>().toMutableList()

            val checkData = repo.findByYearCodeActive(request.year, request.code)
            if (checkData.isPresent) {
                rData["monevResultChain"] = checkData.get()
            } else {
                rData["monevResultChain"] = repo.save(MonevResultChain(year = request.year, code = request.code))
            }

            val monevType = repoMonevType.findByIdAndActive(request.monevTypeId)
            if (!monevType.isPresent) {
                listMessage.add(
                    ErrorMessage(
                        "monevTypeId", "Monev Type id ${request.monevTypeId} $VALIDATOR_MSG_NOT_FOUND"
                    )
                )
            }

            if (!request.componentId.isNullOrEmpty()) {
                val checkComponent = repoComponent.findByIdAndActive(request.componentId)
                if (!checkComponent.isPresent) {
                    listMessage.add(
                        ErrorMessage(
                            "componentId", "Component id ${request.componentId} $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                }
            }

            if (!request.subComponentId.isNullOrEmpty()) {
                val checkComponent = repoComponent.findByIdAndActive(request.subComponentId)
                if (!checkComponent.isPresent) {
                    listMessage.add(
                        ErrorMessage(
                            "subComponentId", "Sub Component id ${request.subComponentId} $VALIDATOR_MSG_NOT_FOUND"
                        )
                    )
                }
            }

            request.newSuccessIndicators?.forEach {
                if (!it.pdoId.isNullOrEmpty()) {
                    val checkPdo = repoPdo.findByIdAndActive(it.pdoId)
                    if (!checkPdo.isPresent) {
                        listMessage.add(
                            ErrorMessage(
                                "pdoId", "Pdo id ${it.pdoId} $VALIDATOR_MSG_NOT_FOUND"
                            )
                        )
                    }
                }

                if (!it.iriId.isNullOrEmpty()) {
                    val checkIri = repoIri.findByIdAndActive(it.iriId)
                    if (!checkIri.isPresent) {
                        listMessage.add(
                            ErrorMessage(
                                "iriId", "Iri id ${it.iriId} $VALIDATOR_MSG_NOT_FOUND"
                            )
                        )
                    }
                }
            }

            val newQuestions =
                mutableListOf<MonevQuestion>()

            request.newQuestions?.forEach {
                val check = repoMonevQuestion.findByQuestionYesOrNoActive(it.question, it.yesOrNo)
                if (check.isPresent) {
                    newQuestions.add(check.get())
                } else {
                    newQuestions.add(
                        repoMonevQuestion.save(
                            MonevQuestion(question = it.question?.trim(), yesOrNo = it.yesOrNo)
                        )
                    )
                }
            }

            rData["newQuestions"] = newQuestions
            rData["listMessage"] = listMessage
        } catch (e: Exception) {
            throw e
        }

        return rData
    }

    fun delete(id: String): ResponseEntity<ReturnData> {
        try {
            val data = repo.findByIdAndActive(id)
            if (data.isPresent) {
                data.get().active = false
                data.get().deletedBy = getUserLogin()?.id
                data.get().deletedAt = Date()
                repo.save(data.get())

                data.get().successIndicator?.forEach {
                    it.active = false
                    it.updatedAt = Date()
                    repoMonevSuccessIndicator.save(it)
                }

                data.get().questions?.forEach {
                    it.active = false
                    it.updatedAt = Date()
                    repoMonevResultChainQuestion.save(it)
                }

                return responseSuccess()
            }
            return responseNotFound()
        } catch (e: Exception) {
            throw e
        }
    }
}

data class ResultChainParrentChild(

    var id: String? = null,

    var resultChainCode: String? = null,

    var result: String? = null,

    var label: String? = null,

    var resultStatus2020: Int? = null,

    var resultStatus2021: Int? = null,

    var resultStatus2022: Int? = null,

    var resultStatus2023: Int? = null,

    var resultStatus2024: Int? = null,

    var children: List<ResultChainParrentChild> = emptyList()
)
