package id.go.kemenag.madrasah.pmrms.awp.activity.service

import id.go.kemenag.madrasah.pmrms.awp.activity.helpers.responseSuccess
import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.awp.activity.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.awp.activity.repository.native.VMonevContributionRepositoryNative
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service

@Suppress("UNCHECKED_CAST")
@Service
class MonevService {

    @Autowired
    private lateinit var repoNativeVMonevContribution: VMonevContributionRepositoryNative

    fun datatableContribution(req: Pagination2Request): ResponseEntity<ReturnData> {
        try {
            return responseSuccess(data = repoNativeVMonevContribution.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }
}

