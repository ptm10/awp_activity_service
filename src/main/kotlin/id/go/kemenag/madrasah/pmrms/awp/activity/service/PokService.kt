package id.go.kemenag.madrasah.pmrms.awp.activity.service

import id.go.kemenag.madrasah.pmrms.awp.activity.helpers.responseSuccess
import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.awp.activity.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.awp.activity.repository.native.PokRepositoryNative
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service

@Suppress("UNCHECKED_CAST", "NAME_SHADOWING")
@Service
class PokService {
    @Autowired
    private lateinit var repoNative: PokRepositoryNative

    fun datatable(req: Pagination2Request): ResponseEntity<ReturnData> {
        return try {
            responseSuccess(data = repoNative.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }
}
