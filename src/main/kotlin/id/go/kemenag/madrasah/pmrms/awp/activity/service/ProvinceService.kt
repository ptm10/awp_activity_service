package id.go.kemenag.madrasah.pmrms.awp.activity.service

import id.go.kemenag.madrasah.pmrms.awp.activity.helpers.responseSuccess
import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.awp.activity.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.awp.activity.repository.native.ProvinceRepositoryNative
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service

@Service
class ProvinceService {

    @Autowired
    private lateinit var repoNative: ProvinceRepositoryNative

    fun datatable(req: Pagination2Request): ResponseEntity<ReturnData> {
        try {
            return responseSuccess(data = repoNative.getPage(req))
        } catch (e: Exception) {
            throw e
        }
    }
}
