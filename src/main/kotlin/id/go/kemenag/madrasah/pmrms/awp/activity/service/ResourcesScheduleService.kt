package id.go.kemenag.madrasah.pmrms.awp.activity.service

import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.ResourcesSchedule
import id.go.kemenag.madrasah.pmrms.awp.activity.repository.ResourcesScheduleRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

@Service
class ResourcesScheduleService {

    @Autowired
    private lateinit var repo: ResourcesScheduleRepository

    fun createFromTask(
        req: ResourcesSchedule
    ) {
        try {
            var schedule: ResourcesSchedule? = ResourcesSchedule()
            val findSchedule =
                repo.findByResourcesIdAndTaskIdAndTaskTypeAndActive(req.resourcesId, req.taskId, req.taskType)
            if (findSchedule.isPresent) {
                schedule = findSchedule.get()
                schedule.updatedAt = Date()
            }

            if (schedule != null) {
                repo.save(schedule.apply {
                    this.resourcesId = req.resourcesId
                    this.taskType = req.taskType
                    this.taskId = req.taskId
                    this.approveStatus = req.approveStatus
                    this.startDate = req.startDate
                    this.endDate = req.endDate
                    this.daysLength = req.daysLength
                    this.title = req.title
                    this.description = req.description
                })
            }
        } catch (e: Exception) {
            throw e
        }
    }

    fun approveFromTask(approveStatus: Int?, resourcesId: String?, taskId: String?, taskType: Int?) {
        try {
            val findSchedule =
                repo.findByResourcesIdAndTaskIdAndTaskTypeAndActive(resourcesId, taskId, taskType)
            if (findSchedule.isPresent) {
                findSchedule.get().approveStatus = approveStatus
                findSchedule.get().updatedAt = Date()
                repo.save(findSchedule.get())
            }
        } catch (e: Exception) {
            throw e
        }
    }

    fun deleteFromTask(resourcesId: String?, taskId: String?, taskType: Int?) {
        try {
            val findSchedule =
                repo.findByResourcesIdAndTaskIdAndTaskTypeAndActive(resourcesId, taskId, taskType)
            if (findSchedule.isPresent) {
                findSchedule.get().active = false
                findSchedule.get().updatedAt = Date()
                repo.save(findSchedule.get())
            }
        } catch (e: Exception) {
            throw e
        }
    }

}
