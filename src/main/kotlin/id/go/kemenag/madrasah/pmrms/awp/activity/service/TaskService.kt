package id.go.kemenag.madrasah.pmrms.awp.activity.service

import id.go.kemenag.madrasah.pmrms.awp.activity.constant.*
import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.Task
import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.TaskNotification
import id.go.kemenag.madrasah.pmrms.awp.activity.repository.TaskNotificationRepository
import id.go.kemenag.madrasah.pmrms.awp.activity.repository.TaskRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

@Service
class TaskService {

    @Autowired
    private lateinit var repo: TaskRepository

    @Autowired
    private lateinit var repoTaskNotification: TaskNotificationRepository

    fun createOrUpdateFromTask(
        createdBy: String?,
        taskId: String?,
        taskType: Int,
        description: String,
        createdFor: String? = null,
        taskNumber: Int? = TASK_NUMBER_TASK
    ) {
        try {
            val findTask = repo.findByTaskIdAndTaskTypeAndCreatedForAndTaskNumberAndActive(
                taskId,
                taskType,
                createdFor,
                taskNumber
            )
            if (findTask.isPresent) {
                findTask.get().updatedAt = Date()
                findTask.get().status = TASK_STATUS_NEW
            } else {
                repo.save(Task().apply {
                    this.createdBy = createdBy
                    this.createdFor = createdFor
                    this.taskType = taskType
                    this.taskId = taskId
                    this.description = description
                    this.taskNumber = taskNumber
                })
            }

            var updateNotif = false
            repoTaskNotification.getByTaskIdAndTaskTypeAndUserIdAndActive(taskId, taskType, createdFor).forEach { tn ->
                tn.readed = false
                tn.message = description
                tn.updatedAt = Date()
                repoTaskNotification.save(tn)
                updateNotif = true
            }

            if (!updateNotif) {
                repoTaskNotification.save(TaskNotification().apply {
                    this.userId = createdFor
                    this.taskType = taskType
                    this.taskId = taskId
                    this.message = description
                    this.createdBy = createdBy
                })
            }
        } catch (e: Exception) {
            throw e
        }
    }

    fun approveRejectFromTask(
        approveStatus: Int?,
        taskId: String?,
        taskType: Int,
        createdFor: String?,
        taskNumber: Int? = TASK_NUMBER_TASK
    ) {
        try {
            if (approveStatus == APPROVAL_STATUS_APPROVE || approveStatus == APPROVAL_STATUS_REJECT) {
                var taskStatus = TASK_STATUS_APPROVED
                if (approveStatus == APPROVAL_STATUS_REJECT) {
                    taskStatus = TASK_STATUS_REJECT
                }

                val task = repo.findByTaskIdAndTaskTypeAndCreatedForAndTaskNumberAndActive(
                    taskId,
                    taskType,
                    createdFor,
                    taskNumber
                )
                if (task.isPresent) {
                    task.get().updatedBy = createdFor
                    task.get().done = true
                    task.get().updatedAt = Date()
                    task.get().status = taskStatus
                    repo.save(task.get())
                }

                repoTaskNotification.getByTaskIdAndTaskTypeAndUserIdAndActive(taskId, taskType, createdFor)
                    .forEach { tn ->
                        tn.updatedAt = Date()
                        tn.active = false
                        repoTaskNotification.save(tn)
                    }
            }
        } catch (e: Exception) {
            throw e
        }
    }

    fun createRevisionTask(taskId: String?, taskType: Int, createdFor: String?, description: String, step: Int = 1) {
        try {
            val task = repo.findByTaskIdAndTaskTypeAndCreatedForAndTaskNumberAndActive(taskId, taskType, createdFor)
            if (task.isPresent) {
                if (step == 1) {
                    createOrUpdateFromTask(
                        createdFor,
                        taskId,
                        taskType,
                        description,
                        task.get().createdBy,
                        TASK_NUMBER_TASK_REVISION
                    )
                }

                if (step == 2) {
                    val task2 = repo.findByTaskIdAndTaskTypeAndCreatedForAndTaskNumberAndActive(
                        taskId,
                        taskType,
                        task.get().createdBy
                    )

                    if (task2.isPresent) {
                        createOrUpdateFromTask(
                            createdFor,
                            taskId,
                            taskType,
                            description,
                            task2.get().createdBy,
                            TASK_NUMBER_TASK_REVISION
                        )
                    }
                }
            }
        } catch (e: Exception) {
            throw e
        }
    }

    fun createTaskNotification(
        taskId: String?,
        taskType: Int,
        createdBy: String?,
        createdFor: String?,
        description: String?
    ) {
        try {
            var updateNotif = false
            repoTaskNotification.getByTaskIdAndTaskTypeAndUserIdAndActive(taskId, taskType, createdFor).forEach { tn ->
                tn.readed = false
                tn.message = description
                tn.updatedAt = Date()
                repoTaskNotification.save(tn)
                updateNotif = true
            }

            if (!updateNotif) {
                repoTaskNotification.save(TaskNotification().apply {
                    this.userId = createdFor
                    this.taskType = taskType
                    this.taskId = taskId
                    this.message = description
                    this.createdBy = createdBy
                })
            }
        } catch (e: Exception) {
            throw e
        }
    }

}
