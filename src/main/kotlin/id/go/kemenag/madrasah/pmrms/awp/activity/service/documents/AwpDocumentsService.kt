package id.go.kemenag.madrasah.pmrms.awp.activity.service.documents

import com.lowagie.text.DocumentException
import id.go.kemenag.madrasah.pmrms.awp.activity.constant.COMPONENT_ID_PMU
import id.go.kemenag.madrasah.pmrms.awp.activity.constant.ROLE_ID_ADMINSTRATOR
import id.go.kemenag.madrasah.pmrms.awp.activity.constant.ROLE_ID_COORDINATOR
import id.go.kemenag.madrasah.pmrms.awp.activity.helpers.*
import id.go.kemenag.madrasah.pmrms.awp.activity.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.*
import id.go.kemenag.madrasah.pmrms.awp.activity.repository.*
import id.go.kemenag.madrasah.pmrms.awp.activity.service.ComponentService
import id.go.kemenag.madrasah.pmrms.awp.activity.utility.ExcelUtility
import id.go.kemenag.madrasah.pmrms.awp.activity.utility.HtmlRecursiveParameters
import id.go.kemenag.madrasah.pmrms.awp.activity.utility.HtmlToDoc
import org.apache.commons.lang3.StringUtils
import org.apache.poi.util.Units
import org.apache.poi.xwpf.usermodel.*
import org.apache.xmlbeans.XmlCursor
import org.jsoup.Jsoup
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.io.ClassPathResource
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import org.thymeleaf.context.Context
import org.thymeleaf.spring5.SpringTemplateEngine
import org.xhtmlrenderer.pdf.ITextRenderer
import java.io.*
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.time.Year
import javax.imageio.ImageIO
import javax.servlet.http.HttpServletResponse


@Suppress("UNCHECKED_CAST", "NAME_SHADOWING")
@Service
class AwpDocumentsService {
    @Autowired
    private lateinit var repo: AwpRepository

    @Autowired
    private lateinit var response: HttpServletResponse

    @Autowired
    private lateinit var repoUserRoleViewUserResources: UserRoleViewUserResourcesRepository

    @Autowired
    private lateinit var repoUser: UserRepository

    @Autowired
    private lateinit var repoComponent: ComponentRepository

    @Autowired
    private lateinit var repoPdo: PdoRepository

    @Autowired
    private lateinit var repoIri: IriRepository

    @Autowired
    private lateinit var repoAwpData: AwpDataRepository

    @Autowired
    private lateinit var serviceComponent: ComponentService

    @Value("\${directories.pdf}")
    private lateinit var dirPdf: String

    @Value("\${directories.word}")
    private lateinit var dirWord: String

    @Value("\${directories.export}")
    private lateinit var dirExport: String

    @Value("\${directories.upload}")
    private lateinit var dirUpload: String

    private val pdfResources = "/static/"

    private val wordTemplate = "files/AWP-template.docx"

    @Autowired
    private lateinit var templateEngine: SpringTemplateEngine

    fun downloadExcel(ids: List<String>): ResponseEntity<ReturnData> {
        try {
            val limitStr = 32767
            val datas = repo.getByIdInAndActive(ids)

            val dir = File(dirExport + File.separator + "awp" + File.separator + "excel")
            if (!dir.exists()) {
                dir.mkdirs()
            }

            val fileName = "awp-${System.currentTimeMillis()}.xlsx"
            val excelFile = dir.absolutePath + File.separator + fileName

            val excel = ExcelUtility(excelFile, 0)

            val row = excel.createRow(0)

            val heads = listOf(
                "Tahun",
                "Kode Komponen",
                "Nama Komponen",
                "Kode Sub Komponen",
                "Nama Sub Komponen",
                "Kode Sub Sub Komponen",
                "Nama Sub Sub Komponen",
                "Kode Kegiatan",
                "Nama Kegiatan",
                "Moda Kegiatan",
                "Keterangan Kegiatan",
                "Budget Awp",
                "Upload RAB Kegiatan",
                "Tujuan Kegaitan",
                "Deskripsi Kegaitan",
                "Cara Pengelolaan Kegiatan",
                "Volume Event",
                "Waktu Pelaksanaan Kegiatan",
                "Informasi Lain Waktu Pelaksanaan Kegiatan",
                "Tempat Pelaksanaan",
                "Informasi Lain Tempat Pelaksanaan",
                "Jenis Kegiatan",
                "Jumlah Narasumber",
                "Asal Lembaga Narasumber",
                "Informasi Lain Narasumber",
                "Jumlah Peserta",
                "Informasi Lain Peserta",
                "Sasaran Peserta",
                "Output Kegiatan",
                "Kontribusi Hasil Terhadap PDOI",
                "Kontribusi Hasil Terhadap IRI",
                "Potensi Resiko",
                "Mitigasi Resiko",
                "Keterangan Resiko",
                "Kemungkinan Terjadi",
                "Dampak",
                "Gejala Risiko"
            )

            heads.forEachIndexed { index, s ->
                val cellHeadYear = row?.createCell(index)
                cellHeadYear?.setCellValue(s)
                cellHeadYear?.cellStyle = excel.headStyle()

                row?.let { excel.setAutoSize(it) }
            }


            datas.forEachIndexed { index, awp ->
                val rowCntn = excel.createRow(index + 1)

                val year = rowCntn?.createCell(0)
                year?.setCellValue(awp.year?.toString() ?: "")

                val component = rowCntn?.createCell(1)
                component?.setCellValue(awp.component?.code ?: "")

                val nameComponent = rowCntn?.createCell(2)
                nameComponent?.setCellValue(awp.component?.description ?: "")

                val subComponent = rowCntn?.createCell(3)
                subComponent?.setCellValue(awp.subComponent?.code ?: "")

                val nameSubComponent = rowCntn?.createCell(4)
                nameSubComponent?.setCellValue(awp.subComponent?.description ?: "")

                var strSubSubComponent = ""
                var strNameSubSubComponent = ""
                if (!awp.subSubComponent?.code.isNullOrEmpty()) {
                    strSubSubComponent = awp.subSubComponent?.code ?: ""
                }
                if (!awp.subSubComponentName.isNullOrEmpty()) {
                    strNameSubSubComponent = awp.subSubComponentName ?: ""
                }

                val subSubComponent = rowCntn?.createCell(5)
                subSubComponent?.setCellValue(strSubSubComponent)

                val nameSubSubComponent = rowCntn?.createCell(6)
                nameSubSubComponent?.setCellValue(strNameSubSubComponent)

                val componentCode = rowCntn?.createCell(7)
                componentCode?.setCellValue(awp.componentCode?.code ?: "")

                val name = rowCntn?.createCell(8)
                name?.setCellValue(awp.name ?: "")

                val moda = rowCntn?.createCell(9)
                moda?.setCellValue(awp.activityMode?.name ?: "")

                val nameOtherInformation = rowCntn?.createCell(10)
                nameOtherInformation?.setCellValue(
                    Jsoup.parse(
                        StringUtils.substring(
                            awp.nameOtherInformation ?: "", 0, limitStr
                        )
                    ).text()
                )

                val budget = rowCntn?.createCell(11)
                budget?.setCellValue(awp.budgetAwp?.toDouble() ?: 0.toDouble())

                val rab = rowCntn?.createCell(12)
                rab?.setCellValue(awp.rabFile?.filename ?: "")

                val purpose = rowCntn?.createCell(13)
                purpose?.setCellValue(Jsoup.parse(StringUtils.substring(awp.purpose ?: "", 0, limitStr)).text())

                val description = rowCntn?.createCell(14)
                description?.setCellValue(Jsoup.parse(StringUtils.substring(awp.description ?: "", 0, limitStr)).text())

                val eventManagement = rowCntn?.createCell(15)
                eventManagement?.setCellValue(awp.eventManagementType?.name ?: "")

                val volume = rowCntn?.createCell(16)
                volume?.setCellValue(awp.eventVolume?.toDouble() ?: 0.toDouble())

                val eventDate = rowCntn?.createCell(17)
                eventDate?.setCellValue("${dateFormatIndonesia(awp.startDate)} - ${dateFormatIndonesia(awp.endDate)}")

                val eventOtherInfo = rowCntn?.createCell(18)
                eventOtherInfo?.setCellValue(
                    Jsoup.parse(
                        StringUtils.substring(
                            awp.eventOtherInformation ?: "", 0, limitStr
                        )
                    ).text()
                )

                var strPlace = ""
                val size = awp.provincesRegencies?.size ?: 0
                val sizeIdx = size - 1
                if (size > 0) {
                    strPlace = ""
                }

                awp.provincesRegencies?.forEachIndexed { idxAp, awpProvinces ->
                    if (idxAp in 1 until sizeIdx) {
                        strPlace += ", "
                    }

                    strPlace += "${awpProvinces.province?.name}("


                    val sizePr = awpProvinces.regencies?.size ?: 0
                    val isizePr = sizePr - 1
                    awpProvinces.regencies?.forEachIndexed { idxApr, awpProvincesRegencies ->
                        if (idxApr in 1 until isizePr) {
                            strPlace += ", "
                        }
                        strPlace += awpProvincesRegencies.regency?.name
                    }

                    if (sizePr == 0) {
                        strPlace += ""
                    }

                    strPlace += ")"
                }

                val place = rowCntn?.createCell(19)
                place?.setCellValue(strPlace)

                val placeOtherInfo = rowCntn?.createCell(20)
                placeOtherInfo?.setCellValue(
                    Jsoup.parse(
                        StringUtils.substring(
                            awp.locationOtherInformation ?: "", 0, limitStr
                        )
                    ).text()
                )

                val eventType = rowCntn?.createCell(21)
                eventType?.setCellValue(awp.eventType?.name ?: "")

                val nsCount = rowCntn?.createCell(22)
                nsCount?.setCellValue(awp.nsCount?.toDouble() ?: 0.toDouble())

                val nsInstitution = rowCntn?.createCell(23)
                nsInstitution?.setCellValue(Jsoup.parse(awp.nsInstitution ?: "").text())

                val nsOtherInformation = rowCntn?.createCell(24)
                nsOtherInformation?.setCellValue(
                    Jsoup.parse(
                        StringUtils.substring(
                            awp.nsOtherInformation ?: "", 0, limitStr
                        )
                    ).text()
                )

                val participantCount = rowCntn?.createCell(25)
                participantCount?.setCellValue(awp.participantCount?.toDouble() ?: 0.toDouble())

                val participantOtherInformation = rowCntn?.createCell(26)
                participantOtherInformation?.setCellValue(
                    Jsoup.parse(
                        StringUtils.substring(
                            awp.participantOtherInformation ?: "", 0, limitStr
                        )
                    ).text()
                )

                val participantTarget = rowCntn?.createCell(27)
                participantTarget?.setCellValue(
                    Jsoup.parse(
                        StringUtils.substring(
                            awp.participantTarget ?: "", 0, limitStr
                        )
                    ).text()
                )

                val eventOutput = rowCntn?.createCell(28)
                eventOutput?.setCellValue(Jsoup.parse(StringUtils.substring(awp.eventOutput ?: "", 0, limitStr)).text())

                val pdoSize = awp.pdo?.size ?: 0
                val ipdoSize = pdoSize - 1
                var pdoStr = ""
                if (pdoSize > 0) {
                    pdoStr = ""
                }
                awp.pdo?.forEachIndexed { idxPdo, awpPdo ->
                    if (idxPdo in 1 until ipdoSize) {
                        pdoStr += ", "
                    }
                    pdoStr += awpPdo.pdo?.name
                }

                val pdo = rowCntn?.createCell(29)
                pdo?.setCellValue(pdoStr)

                val iriSize = awp.iri?.size ?: 0
                val iiriSize = iriSize - 1
                var iriStr = ""
                if (iiriSize > 0) {
                    iriStr = ""
                }
                awp.iri?.forEachIndexed { idxIri, awpIri ->
                    if (idxIri in 1 until iiriSize) {
                        iriStr += ", "
                    }
                    iriStr += awpIri.iri?.name
                }

                val iri = rowCntn?.createCell(30)
                iri?.setCellValue(iriStr)

                val descriptionRisk = rowCntn?.createCell(31)
                descriptionRisk?.setCellValue(
                    Jsoup.parse(StringUtils.substring(awp.descriptionRisk ?: "", 0, limitStr)).text()
                )

                val mitigationRisk = rowCntn?.createCell(32)
                mitigationRisk?.setCellValue(
                    Jsoup.parse(StringUtils.substring(awp.mitigationRisk ?: "", 0, limitStr)).text()
                )

                val risk = rowCntn?.createCell(33)
                risk?.setCellValue(Jsoup.parse(StringUtils.substring(awp.risk ?: "", 0, limitStr)).text())

                val potentialRisk = rowCntn?.createCell(34)
                potentialRisk?.setCellValue(awp.potentialRisk?.toDouble() ?: 0.toDouble())

                val impactRisk = rowCntn?.createCell(35)
                impactRisk?.setCellValue(awp.impactRisk?.toDouble() ?: 0.toDouble())

                val symptomsRisk = rowCntn?.createCell(36)
                symptomsRisk?.setCellValue(
                    Jsoup.parse(StringUtils.substring(awp.symptomsRisk ?: "", 0, limitStr)).text()
                )
            }

            excel.writeToFile(0)

            val file = File(excelFile)
            if (Files.exists(file.toPath())) {
                response.contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                response.addHeader(
                    "Content-Disposition", "attachment; filename=$fileName"
                )
                Files.copy(file.toPath(), response.outputStream)
                response.outputStream.flush()
            } else {
                response.contentType = "application/json"
                response.status = HttpServletResponse.SC_UNAUTHORIZED
            }

            return responseSuccess(data = datas)
        } catch (e: Exception) {
            throw e
        }
    }

    fun downloadPdf(id: String?) {
        try {
            val findData = repo.findByIdAndActive(id)
            if (!findData.isPresent) {
                response.sendError(HttpServletResponse.SC_NOT_FOUND)
            } else {
                val file: Path = Paths.get(generatePdf(findData.get())!!.absolutePath)
                if (Files.exists(file)) {
                    response.contentType = "application/pdf"
                    response.addHeader(
                        "Content-Disposition", "attachment; filename=" + file.fileName
                    )
                    Files.copy(file, response.outputStream)
                    response.outputStream.flush()
                } else {
                    response.contentType = "application/json"
                    response.status = HttpServletResponse.SC_UNAUTHORIZED
                }
            }
        } catch (ex: DocumentException) {
            ex.printStackTrace()
        } catch (ex: IOException) {
            ex.printStackTrace()
        }
    }

    fun generatePdf(data: Awp?): File? {
        try {
            val ctx = Context()
            ctx.setVariable("componentCode", data?.component?.code ?: "")
            ctx.setVariable("componentDescription", data?.component?.description ?: "")
            ctx.setVariable("subComponentDescription", data?.subComponent?.description ?: "")
            ctx.setVariable("budgetAwp", formatRupiah(data?.budgetAwp?.toDouble() ?: 0.toDouble()))
            ctx.setVariable("executor", data?.eventManagementType?.name ?: "")
            ctx.setVariable("purpose", replaceUnusedSpaceCkEditor(data?.purpose ?: ""))
            ctx.setVariable("description", replaceUnusedSpaceCkEditor(data?.description ?: ""))
            ctx.setVariable("startTime", dateFormatIndonesia(data?.startDate, false))
            ctx.setVariable("endTime", dateFormatIndonesia(data?.endDate, false))
            ctx.setVariable("volume", data?.eventVolume ?: "")
            ctx.setVariable("nsCount", data?.nsCount ?: 0)
            ctx.setVariable("institution", data?.nsInstitution ?: "")
            ctx.setVariable("result", replaceUnusedSpaceCkEditor(data?.eventOutput ?: ""))

            var pdo = ""
            data?.pdo?.forEach {
                pdo += "- ${it.pdo?.name ?: "- "} - ${it.pdo?.description ?: "- "}<br/>"
            }

            var iri = ""
            data?.iri?.forEach {
                iri += "- ${it.iri?.name ?: "- "} - ${it.iri?.description ?: "- "}<br/>"
            }

            ctx.setVariable("pdo", pdo)
            ctx.setVariable("iri", iri)
            ctx.setVariable("participantCount", data?.participantCount ?: 0)
            ctx.setVariable("participantOtherInformation", data?.participantOtherInformation ?: "")

            val nameSpace = if (data?.createdByUser?.lastName.isNullOrEmpty()) "" else " "
            val createdBy = "${data?.createdByUser?.firstName}${nameSpace}${data?.createdByUser?.lastName}, ${
                dateFormatIndonesia(
                    data?.createdAt
                )
            }"
            ctx.setVariable("createdBy", createdBy)

            var updatedBy = "-"
            if (data?.updatedByUser != null) {
                val nameSpace = if (data.updatedByUser?.lastName.isNullOrEmpty()) "" else " "
                updatedBy = "${data.updatedByUser?.firstName}${nameSpace}${data.updatedByUser?.lastName}, ${
                    dateFormatIndonesia(
                        data.updatedAt
                    )
                }"
            }
            ctx.setVariable("updatedBy", updatedBy)

            var location = ""
            data?.provincesRegencies?.forEachIndexed { ipr, pr ->
                var provinceName = pr.province?.name
                if (provinceName == "Lainnya") {
                    provinceName = "tempat yang telah ditentukan"
                }

                if (ipr > 0 && ipr < ((data.provincesRegencies?.size ?: 0) - 1)) {
                    location += ", "
                }

                location += provinceName

                pr.regencies?.forEachIndexed { ireq, reg ->
                    var regencyName = reg.regency?.name
                    if (regencyName == "Lainnya") {
                        regencyName = "tempat yang telah ditentukan"
                    }

                    if (ireq > 0 && ireq < ((pr.regencies?.size ?: 0) - 1)) {
                        location += ", "
                    }

                    if (ireq == 0) {
                        location += "("
                    }

                    location += regencyName

                    if (ireq == ((pr.regencies?.size ?: 0) - 1)) {
                        location += ")"
                    }
                }
            }

            ctx.setVariable("location", location)

            val html: String = loadAndFillTemplate(ctx)
            return renderPdf(html)
        } catch (e: Exception) {
            throw e
        }
    }

    private fun renderPdf(html: String): File? {
        try {
            val path = Paths.get(dirPdf)
            if (!Files.exists(path)) {
                Files.createDirectories(path)
            }

            val file = File.createTempFile("awp-", ".pdf", path.toFile())

            val outputStream: OutputStream = FileOutputStream(file)
            val renderer = ITextRenderer()
            renderer.setDocumentFromString(
                html, ClassPathResource(pdfResources).url.toExternalForm()
            )
            renderer.layout()
            renderer.createPDF(outputStream)
            outputStream.close()
            file.deleteOnExit()
            return file
        } catch (e: Exception) {
            throw e
        }
    }

    private fun loadAndFillTemplate(context: Context): String {
        return templateEngine.process("awp-pdf", context)
    }

    fun downloadWord(year: Int?) {
        try {
            val path = Paths.get(dirWord)
            if (!Files.exists(path)) {
                Files.createDirectories(path)
            }

            val file = File.createTempFile("awp-", ".docx", path.toFile())

            var paramYear = year
            if (paramYear == null) {
                paramYear = Year.now().value
            }

            var awpData: AwpData? = null
            val findAwpData = repoAwpData.findByYearAndActive(paramYear - 1)
            if (findAwpData.isPresent) {
                awpData = findAwpData.get()
            }

            writeWord(file.path, paramYear, repo.getByYearAndActive(paramYear), awpData)

            if (Files.exists(file.toPath())) {
                response.contentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                response.addHeader(
                    "Content-Disposition", "attachment; filename=" + file.name
                )
                Files.copy(file.toPath(), response.outputStream)
                response.outputStream.flush()
            }
        } catch (ex: DocumentException) {
            ex.printStackTrace()
        } catch (ex: IOException) {
            ex.printStackTrace()
        }
    }

    @Throws(IOException::class)
    private fun writeWord(output: String, year: Int, datas: List<Awp>, awpData: AwpData?) {
        try {
//            val path = File("C:\\Users\\ghith\\Desktop\\template\\AWP-template.docx").toPath()
//            Files.newInputStream(path)
            XWPFDocument(
                ClassPathResource(wordTemplate).inputStream
            ).use { doc ->

                var findPdoTable = false
                val listAppendWordTable = mutableListOf<AppendWordTable>()

                var paragraphComponentAwp: XWPFParagraph? = null
                var paragraphTableOfContent: XWPFParagraph? = null
                var paragraphKeteranganKontribusiKomponent: XWPFParagraph? = null
                var paragraphKeteranganKemajuanProgram: XWPFParagraph? = null
                var paragraphKeteranganKendala: XWPFParagraph? = null
                var paragraphKeteranganPembelajaran: XWPFParagraph? = null
                var paragraphKeteranganRencanaKerja: XWPFParagraph? = null
                var paragraphKeteranganUsulanKegiatan: XWPFParagraph? = null

                // membaca text yang ada di paragraf template
                val mapReplace: MutableMap<String, String> = getstrReplaceWord(year, awpData);
                for (p in doc.paragraphs.toMutableList()) {
                    val runs = p.runs.toMutableList()
                    if (runs != null) {
                        for (r in runs) {
                            var docText = r.text()
//                            println("doc.paragraphs=> $docText")

                            if (docText != null) {
                                if (docText.contains("replaceTableOfContents")) {
                                    paragraphTableOfContent = p
                                }
                                if (docText.contains("replaceComponentAwp")) {
                                    paragraphComponentAwp = p
                                    r.setText("", 0)
                                }

                                if (docText.contains("#rkkk")) {
                                    paragraphKeteranganKontribusiKomponent = p
                                }
                                if (docText.contains("replaceKeteranganKemajuanProgram")) {
                                    paragraphKeteranganKemajuanProgram = p
                                }
                                if (docText.contains("replaceKeteranganKendala")) {
                                    paragraphKeteranganKendala = p
                                }
                                if (docText.contains("replaceKeteranganPembelajaran")) {
                                    paragraphKeteranganPembelajaran = p
                                }
                                if (docText.contains("replaceKeteranganRencanaKerja")) {
                                    paragraphKeteranganRencanaKerja = p
                                }
                                if (docText.contains("replaceKeteranganUsulanKegiatan")) {
                                    paragraphKeteranganUsulanKegiatan = p
                                }

//                                getstrReplaceWord(year, docText, awpData)?.let {
//                                    docText = docText.replace(docText, it)
//                                    r.setText(docText, 0)
//
//                                }
                                mapReplace.forEach found@{
                                    if (docText.contains(it.key)) {
                                        docText = docText.replace(it.key, "")
                                        r.setText(docText, 0)

                                        handleHtml(null, p, r, it.value)
                                        return@found
                                    }
                                }
                            }
                        }
                    }
                }

                // membaca text yang ada di tabel template
                for (tbl in doc.tables.toMutableList()) {
                    for (row in tbl.rows.toMutableList()) {
                        for (cell in row.tableCells.toMutableList()) {
                            for (p in cell.paragraphs.toMutableList()) {
                                for (r in p.runs.toMutableList()) {
                                    var textTable = r.text()

                                    if (!findPdoTable) {
                                        if (textTable.contains("Perkembangan")) {
                                            listAppendWordTable.add(AppendWordTable("pdo", tbl, tbl.rows.size))
                                            findPdoTable = true
                                        }
                                    }

                                    if (textTable != null) {
                                        mapReplace.forEach found@{
                                            if (textTable.contains(it.key)) {
                                                textTable = textTable.replace(it.key, "")
                                                r.setText(textTable, 0)

                                                handleHtml(cell, p, r, it.value)
                                                return@found
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                listAppendWordTable.forEach {
                    if (it.name == "pdo") {
                        appendPdoTable(it, year, awpData)
                    }
                }

                paragraphComponentAwp?.let {
                    writeComponentAwp(doc, it, year, datas, awpData)
                }

                paragraphTableOfContent?.let {
                    excelPoiCreateToc(doc, it)
                }

                writeOtherFiles(
                    awpData,
                    paragraphKeteranganKontribusiKomponent,
                    paragraphKeteranganKemajuanProgram,
                    paragraphKeteranganKendala,
                    paragraphKeteranganPembelajaran,
                    paragraphKeteranganRencanaKerja,
                    paragraphKeteranganUsulanKegiatan
                )

                doc.enforceUpdateFields()
                FileOutputStream(output).use { out ->
                    doc.write(out)
                }
            }
        } catch (e: Exception) {
            throw e
        }
    }

    private fun writeOtherFiles(
        awpData: AwpData?,
        paragraphKeteranganKontribusiKomponent: XWPFParagraph?,
        paragraphKeteranganKemajuanProgram: XWPFParagraph?,
        paragraphKeteranganKendala: XWPFParagraph?,
        paragraphKeteranganPembelajaran: XWPFParagraph?,
        paragraphKeteranganRencanaKerja: XWPFParagraph?,
        paragraphKeteranganUsulanKegiatan: XWPFParagraph?
    ) {
        try {
            if (awpData != null) {
                if (paragraphKeteranganKontribusiKomponent != null) {
                    val datas: List<AwpDataOtherFiles>? = awpData.otherFiles?.filter {
                        it.key == "keteranganKontribusiKomponent"
                    }

                    if (datas != null) {
                        whriteOtherFilesImageToParagraph(paragraphKeteranganKontribusiKomponent, datas)
                    }
                }

                if (paragraphKeteranganKemajuanProgram != null) {
                    val datas: List<AwpDataOtherFiles>? = awpData.otherFiles?.filter {
                        it.key == "keteranganKemajuanProgram"
                    }

                    if (datas != null) {
                        whriteOtherFilesImageToParagraph(paragraphKeteranganKemajuanProgram, datas)
                    }
                }

                if (paragraphKeteranganKendala != null) {
                    val datas: List<AwpDataOtherFiles>? = awpData.otherFiles?.filter {
                        it.key == "keteranganKemajuanProgram"
                    }

                    if (datas != null) {
                        whriteOtherFilesImageToParagraph(paragraphKeteranganKendala, datas)
                    }
                }

                if (paragraphKeteranganPembelajaran != null) {
                    val datas: List<AwpDataOtherFiles>? = awpData.otherFiles?.filter {
                        it.key == "keteranganPembelajaran"
                    }
                    if (datas != null) {
                        whriteOtherFilesImageToParagraph(paragraphKeteranganPembelajaran, datas)
                    }
                }

                if (paragraphKeteranganRencanaKerja != null) {
                    val datas: List<AwpDataOtherFiles>? = awpData.otherFiles?.filter {
                        it.key == "keteranganRencanaKerja"
                    }
                    if (datas != null) {
                        whriteOtherFilesImageToParagraph(paragraphKeteranganRencanaKerja, datas)
                    }
                }

                if (paragraphKeteranganUsulanKegiatan != null) {
                    val datas: List<AwpDataOtherFiles>? = awpData.otherFiles?.filter {
                        it.key == "keteranganUsulanKegiatan"
                    }
                    if (datas != null) {
                        whriteOtherFilesImageToParagraph(paragraphKeteranganUsulanKegiatan, datas)
                    }
                }
            }
        } catch (e: Exception) {
            throw e
        }
    }

    private fun whriteOtherFilesImageToParagraph(p: XWPFParagraph, datas: List<AwpDataOtherFiles>) {
        try {
            val content = p.createRun()

            datas.forEach { fad ->
                val fileDir =
                    "$dirUpload${File.separator}${fad.file?.filepath ?: ""}${File.separator}${fad.file?.filename ?: ""}"
                val fileFocus = File(fileDir)
                if (fileFocus.exists()) {
                    excelPoiAddBreak(content, 2)
                    val fis = FileInputStream(fileFocus)

                    var pictureType = XWPFDocument.PICTURE_TYPE_JPEG
                    if (fileFocus.extension == "png") {
                        pictureType = XWPFDocument.PICTURE_TYPE_PNG
                    }

                    var height = 0
                    var width = 0
                    try {
                        val readImage = ImageIO.read(fileFocus)
                        height = readImage.width
                        width = readImage.height
                    } catch (_: Exception) {
                    }

                    content.addPicture(
                        fis, pictureType, fileFocus.name, Units.toEMU(height.toDouble()), Units.toEMU(width.toDouble())
                    )
                    fis.close()
                } else {
                    println("file not found : $fileDir")
                }
            }

            excelPoiAddBreak(content, 2)
        } catch (e: Exception) {
            throw e
        }
    }

    private fun writeComponentAwp(doc: XWPFDocument, p: XWPFParagraph, year: Int, datas: List<Awp>, awpData: AwpData?) {
        try {
            val components = serviceComponent.groupList(year).body?.data as List<ComponentService.ComponentSubComponent>
            var nowP = p
            components.forEachIndexed { index, componentSubComponent ->
                var cursor = nowP.ctp?.newCursor()
                cursor?.toEndToken()
                while (cursor?.toNextToken() != XmlCursor.TokenType.START);
                val paragraphHeader: XWPFParagraph = p.document.insertNewParagraph(cursor)
                nowP = paragraphHeader
//                doc.setParagraph(paragraphHeader, doc.getPosOfParagraph(p))
//                doc.removeBodyElement(doc.getPosOfParagraph(p))
                paragraphHeader.style = "Heading1"
                paragraphHeader.isPageBreak = true

                val r = paragraphHeader.createRun()
                r.setText("KOMPONEN ${componentSubComponent.code}")
                r.addBreak()
                r.setText(componentSubComponent.description?.uppercase())
                r.addBreak()

                cursor = nowP.ctp?.newCursor()
                cursor?.toEndToken()
                while (cursor?.toNextToken() != XmlCursor.TokenType.START);
                val content1: XWPFParagraph = p.document.insertNewParagraph(cursor)
                nowP = content1
                val c1 = content1.createRun()

                c1.setText("Komponen ${componentSubComponent.code} terdiri dari ${componentSubComponent.subComponent.size} sub-komponen yaitu:")

                componentSubComponent.subComponent.forEachIndexed { sci, sc ->
                    cursor = nowP.ctp?.newCursor()
                    cursor?.toEndToken()
                    while (cursor?.toNextToken() != XmlCursor.TokenType.START);
                    val content2: XWPFParagraph = p.document.insertNewParagraph(cursor)
                    nowP = content2
                    content2.style = "ListParagraph"
                    content2.numID = excelPoiBulletNumId(doc)
                    val c2 = content2.createRun()
                    c2.setText("Sub-Komponen ${sc.code} ${sc.description}")
                    if (sci == (componentSubComponent.subComponent.size - 1)) {
                        excelPoiAddBreak(c2, 2)
                    }
                }

                cursor = nowP.ctp?.newCursor()
                cursor?.toEndToken()
                while (cursor?.toNextToken() != XmlCursor.TokenType.START);
                val contentRingkasan: XWPFParagraph = p.document.insertNewParagraph(cursor)
                nowP = contentRingkasan
                val cr = contentRingkasan.createRun()
                cr.isBold = true
                cr.setText("Fokus dan Kegiatan TA $year")

                var fokus = ""
                if (awpData != null) {
                    when (componentSubComponent.code) {
                        "1" -> {
                            fokus = awpData.fokusKomponen1 ?: ""
                        }

                        "2" -> {
                            fokus = awpData.fokusKomponen2 ?: ""
                        }

                        "3" -> {
                            fokus = awpData.fokusKomponen3 ?: ""
                        }

                        "4" -> {
                            fokus = awpData.fokusKomponen4 ?: ""
                        }
                    }
                }

                if (fokus != "") {
                    cursor = nowP.ctp?.newCursor()
                    cursor?.toEndToken()
                    while (cursor?.toNextToken() != XmlCursor.TokenType.START);
                    val paragraphFokus: XWPFParagraph = p.document.insertNewParagraph(cursor)
                    nowP = paragraphFokus
                    val fokusContent = paragraphFokus.createRun()
                    fokusContent.setText(Jsoup.parse(fokus).text())

                    val files = awpData?.files?.filter { ff ->
                        ff.component == componentSubComponent.code
                    }
                    if (!files.isNullOrEmpty()) {
                        files.forEach { fad ->
                            val fileDir =
                                "$dirUpload${File.separator}${fad.file?.filepath ?: ""}${File.separator}${fad.file?.filename ?: ""}"
                            val fileFocus = File(fileDir)
                            if (fileFocus.exists()) {
                                excelPoiAddBreak(fokusContent, 2)
                                val fis = FileInputStream(fileFocus)

                                var pictureType = XWPFDocument.PICTURE_TYPE_JPEG
                                if (fileFocus.extension == "png") {
                                    pictureType = XWPFDocument.PICTURE_TYPE_PNG
                                }

                                var height = 0
                                var width = 0
                                try {
                                    val readImage = ImageIO.read(fileFocus)
                                    height = readImage.width
                                    width = readImage.height
                                } catch (_: Exception) {
                                }

                                fokusContent.addPicture(
                                    fis,
                                    pictureType,
                                    fileFocus.name,
                                    Units.toEMU(height.toDouble()),
                                    Units.toEMU(width.toDouble())
                                )
                                fis.close()
                            } else {
                                println("file not found : $fileDir")
                            }
                        }
                    }

                    excelPoiAddBreak(fokusContent, 2)
                }

                componentSubComponent.subComponent.forEachIndexed { sci2, sc2 ->
                    cursor = nowP.ctp?.newCursor()
                    cursor?.toEndToken()
                    while (cursor?.toNextToken() != XmlCursor.TokenType.START);
                    val paragraphHeaderSsc: XWPFParagraph = p.document.insertNewParagraph(cursor)
                    nowP = paragraphHeaderSsc
                    paragraphHeaderSsc.style = "Heading2"

                    val rsc = paragraphHeaderSsc.createRun()
                    rsc.setText("${sc2.code} - ${sc2.description?.uppercase()}")
                    rsc.addBreak()

                    sc2.subComponent.forEachIndexed { ssci2, ssc2 ->
                        cursor = nowP.ctp?.newCursor()
                        cursor?.toEndToken()
                        while (cursor?.toNextToken() != XmlCursor.TokenType.START);
                        val paragraphHeaderSsc: XWPFParagraph = p.document.insertNewParagraph(cursor)
                        nowP = paragraphHeaderSsc
                        paragraphHeaderSsc.style = "Heading3"
                        val rssc = paragraphHeaderSsc.createRun()
                        rssc.setText("${ssc2.code} - ${ssc2.description?.uppercase()}")
                        rssc.addBreak()

                        nowP = writeAwpFromComponent(datas, ssc2, nowP)

                        ssc2.subComponent.forEachIndexed { kci, kc ->
                            cursor = nowP.ctp?.newCursor()
                            cursor?.toEndToken()
                            while (cursor?.toNextToken() != XmlCursor.TokenType.START);
                            val paragraphHeaderk: XWPFParagraph = p.document.insertNewParagraph(cursor)
                            nowP = paragraphHeaderk
                            paragraphHeaderk.style = "Heading4"
                            val k = paragraphHeaderk.createRun()
                            k.setText("Kegiatan ${kc.code} : ")
                            k.addBreak()
                            k.setText(kc.description?.uppercase())
                            k.addBreak()

                            nowP = writeAwpFromComponent(datas, kc, nowP)
                        }
                    }
                }

            }
        } catch (e: Exception) {
            throw e
        }
    }

    private fun writeAwpFromComponent(
        datas: List<Awp>, kc: ComponentService.ComponentSubComponent, p: XWPFParagraph
    ): XWPFParagraph {
        var contentAwp = p
        datas.filter { fa ->
            fa.componentCodeId == kc.id
        }.forEach { awp ->

            if (!awp.activityMode?.description.isNullOrEmpty()) {
                var cursor = contentAwp.ctp?.newCursor()
                cursor?.toEndToken()
                while (cursor?.toNextToken() != XmlCursor.TokenType.START);
                contentAwp = p.document.insertNewParagraph(cursor)
                var ca = contentAwp.createRun()
                ca.setText(awp.activityMode?.description)
            }

            if (!awp.description.isNullOrEmpty()) {
                var cursor = contentAwp.ctp?.newCursor()
                cursor?.toEndToken()
                while (cursor?.toNextToken() != XmlCursor.TokenType.START);
                contentAwp = p.document.insertNewParagraph(cursor)
                var ca = contentAwp.createRun()
                contentAwp = handleHtml(null, contentAwp, ca, awp.description ?: "")
//                ca.setText(Jsoup.parse(awp.description ?: "").text())
            }

            var cursor = contentAwp.ctp?.newCursor()
            cursor?.toEndToken()
            while (cursor?.toNextToken() != XmlCursor.TokenType.START);
            contentAwp = p.document.insertNewParagraph(cursor)
            var ca = contentAwp.createRun()
            ca.setText("Anggaran Yang Diperlukan : ${formatRupiah(awp.budgetAwp?.toDouble() ?: 0.toDouble())}")

            excelPoiAddBreak(ca, 2)
            ca.setText("1) Tujuan Kegiatan")

            if (!awp.purpose.isNullOrEmpty()) {
                contentAwp = handleHtml(null, contentAwp, ca, awp.purpose ?: "")
//                ca.setText(Jsoup.parse(awp.purpose ?: "").text())
            }

            cursor = contentAwp.ctp?.newCursor()
            cursor?.toEndToken()
            while (cursor?.toNextToken() != XmlCursor.TokenType.START);
            contentAwp = p.document.insertNewParagraph(cursor)
            ca = contentAwp.createRun()
            excelPoiAddBreak(ca, 1)
            ca.setText("2) Deskripsi Kegiatan")

            if (!awp.description.isNullOrEmpty()) {
                contentAwp = handleHtml(null, contentAwp, ca, awp.description ?: "")
//                ca.setText(Jsoup.parse(awp.description ?: "").text())
            }

            cursor = contentAwp.ctp?.newCursor()
            cursor?.toEndToken()
            while (cursor?.toNextToken() != XmlCursor.TokenType.START);
            contentAwp = p.document.insertNewParagraph(cursor)
            ca = contentAwp.createRun()
            excelPoiAddBreak(ca, 1)
            ca.setText("3) Waktu Pelaksanaan")
            cursor = contentAwp.ctp?.newCursor()
            cursor?.toEndToken()
            while (cursor?.toNextToken() != XmlCursor.TokenType.START);
            contentAwp = p.document.insertNewParagraph(cursor)
            ca = contentAwp.createRun()
            ca.setText(
                "Kegiatan ini akan dilaksanakan pada ${dateFormatIndonesia(awp.startDate)} - ${
                    dateFormatIndonesia(
                        awp.endDate
                    )
                }"
            )

            if (!awp.eventOtherInformation.isNullOrEmpty()) {
                cursor = contentAwp.ctp?.newCursor()
                cursor?.toEndToken()
                while (cursor?.toNextToken() != XmlCursor.TokenType.START);
                contentAwp = p.document.insertNewParagraph(cursor)
                ca = contentAwp.createRun()
                contentAwp = handleHtml(null, contentAwp, ca, awp.eventOtherInformation ?: "")
//                ca.setText(Jsoup.parse(awp.eventOtherInformation ?: "").text())
            }

            cursor = contentAwp.ctp?.newCursor()
            cursor?.toEndToken()
            while (cursor?.toNextToken() != XmlCursor.TokenType.START);
            contentAwp = p.document.insertNewParagraph(cursor)
            ca = contentAwp.createRun()
            excelPoiAddBreak(ca, 1)

            ca.setText("4) Tempat Pelaksanaan")
            cursor = contentAwp.ctp?.newCursor()
            cursor?.toEndToken()
            while (cursor?.toNextToken() != XmlCursor.TokenType.START);
            contentAwp = p.document.insertNewParagraph(cursor)
            ca = contentAwp.createRun()
            ca.setText("Kegiatan ini akan dilaksanakan di")

            awp.provincesRegencies?.forEach { pr ->
                cursor = contentAwp.ctp?.newCursor()
                cursor?.toEndToken()
                while (cursor?.toNextToken() != XmlCursor.TokenType.START);
                contentAwp = p.document.insertNewParagraph(cursor)
                ca = contentAwp.createRun()
                var provinceName = pr.province?.name
                if (provinceName == "Lainnya") {
                    provinceName = "tempat yang telah ditentukan"
                }

                ca.setText("- $provinceName")
                pr.regencies?.forEachIndexed { iprr, prr ->
                    val size = (pr.regencies?.size ?: 0) - 1

                    if (iprr == 0) {
                        ca.setText("(")
                    }

                    if (iprr > 0 && iprr < size + 1) {
                        ca.setText(", ")
                    }

                    var regencyName = prr.regency?.name
                    if (regencyName == "Lainnya") {
                        regencyName = "tempat yang telah ditentukan"
                    }

                    ca.setText(regencyName)
                    if (iprr == size) {
                        ca.setText(")")
                    }
                }
//                ca.addBreak()
            }

            if (!awp.locationOtherInformation.isNullOrEmpty()) {
                cursor = contentAwp.ctp?.newCursor()
                cursor?.toEndToken()
                while (cursor?.toNextToken() != XmlCursor.TokenType.START);
                contentAwp = p.document.insertNewParagraph(cursor)
                ca = contentAwp.createRun()
                contentAwp = handleHtml(null, contentAwp, ca, awp.locationOtherInformation ?: "")
//                ca.setText(Jsoup.parse(awp.locationOtherInformation ?: "").text())
            }

            cursor = contentAwp.ctp?.newCursor()
            cursor?.toEndToken()
            while (cursor?.toNextToken() != XmlCursor.TokenType.START);
            contentAwp = p.document.insertNewParagraph(cursor)
            ca = contentAwp.createRun()
            excelPoiAddBreak(ca, 1)
            ca.setText("5) Narasumber")

            cursor = contentAwp.ctp?.newCursor()
            cursor?.toEndToken()
            while (cursor?.toNextToken() != XmlCursor.TokenType.START);
            contentAwp = p.document.insertNewParagraph(cursor)
            ca = contentAwp.createRun()

            var narasumber = "Narasumber yang dibutuhkan pada kegiatan ini sebanyak ${awp.nsCount} orang"

            if (awp.nsCount == null || awp.nsCount.toString() == "0") {
                narasumber = "Kegiatan ini tidak memerlukan narasumber"
            }
            ca.setText(narasumber)

            if (!awp.nsInstitution.isNullOrEmpty()) {
                cursor = contentAwp.ctp?.newCursor()
                cursor?.toEndToken()
                while (cursor?.toNextToken() != XmlCursor.TokenType.START);
                contentAwp = p.document.insertNewParagraph(cursor)
                ca = contentAwp.createRun()
                contentAwp = handleHtml(null, contentAwp, ca, awp.nsInstitution ?: "")
//                ca.setText(Jsoup.parse(awp.nsInstitution ?: "").text())
            }

            if (!awp.nsOtherInformation.isNullOrEmpty()) {
                cursor = contentAwp.ctp?.newCursor()
                cursor?.toEndToken()
                while (cursor?.toNextToken() != XmlCursor.TokenType.START);
                contentAwp = p.document.insertNewParagraph(cursor)
                ca = contentAwp.createRun()
                contentAwp = handleHtml(null, contentAwp, ca, awp.nsOtherInformation ?: "")
//                ca.setText(Jsoup.parse(awp.nsOtherInformation ?: "").text())

            }

            cursor = contentAwp.ctp?.newCursor()
            cursor?.toEndToken()
            while (cursor?.toNextToken() != XmlCursor.TokenType.START);
            contentAwp = p.document.insertNewParagraph(cursor)
            ca = contentAwp.createRun()
            excelPoiAddBreak(ca, 1)
            ca.setText("6) Peserta Kegiatan")

            cursor = contentAwp.ctp?.newCursor()
            cursor?.toEndToken()
            while (cursor?.toNextToken() != XmlCursor.TokenType.START);
            contentAwp = p.document.insertNewParagraph(cursor)
            ca = contentAwp.createRun()
            ca.setText("Peserta kegiatan ini berjumlah ${awp.participantCount} orang. Adapun sasaran peserta kegiatan ini adalah : ")

            if (!awp.participantOtherInformation.isNullOrEmpty()) {
                cursor = contentAwp.ctp?.newCursor()
                cursor?.toEndToken()
                while (cursor?.toNextToken() != XmlCursor.TokenType.START);
                contentAwp = p.document.insertNewParagraph(cursor)
                ca = contentAwp.createRun()
                contentAwp = handleHtml(null, contentAwp, ca, awp.participantOtherInformation ?: "")
//                ca.setText(Jsoup.parse(awp.participantOtherInformation ?: "").text())
            }


            if (!awp.participantTarget.isNullOrEmpty()) {
                cursor = contentAwp.ctp?.newCursor()
                cursor?.toEndToken()
                while (cursor?.toNextToken() != XmlCursor.TokenType.START);
                contentAwp = p.document.insertNewParagraph(cursor)
                ca = contentAwp.createRun()
                contentAwp = handleHtml(null, contentAwp, ca, awp.participantTarget ?: "")
//                ca.setText(Jsoup.parse(awp.participantTarget ?: "").text())

            }
            cursor = contentAwp.ctp?.newCursor()
            cursor?.toEndToken()
            while (cursor?.toNextToken() != XmlCursor.TokenType.START);
            contentAwp = p.document.insertNewParagraph(cursor)
            ca = contentAwp.createRun()
            excelPoiAddBreak(ca, 1)
            ca.setText("7) Cara Pengelolaan Kegiatan")

            cursor = contentAwp.ctp?.newCursor()
            cursor?.toEndToken()
            while (cursor?.toNextToken() != XmlCursor.TokenType.START);
            contentAwp = p.document.insertNewParagraph(cursor)
            ca = contentAwp.createRun()
            ca.setText("${awp.eventManagementType?.description}")

            cursor = contentAwp.ctp?.newCursor()
            cursor?.toEndToken()
            while (cursor?.toNextToken() != XmlCursor.TokenType.START);
            contentAwp = p.document.insertNewParagraph(cursor)
            ca = contentAwp.createRun()
            ca.addBreak()
            ca.setText("8) Hasil Kegiatan dan Kontribusi Hasil terhadap Pencapaian Target PDOI dan IRI")

            cursor = contentAwp.ctp?.newCursor()
            cursor?.toEndToken()
            while (cursor?.toNextToken() != XmlCursor.TokenType.START);
            contentAwp = p.document.insertNewParagraph(cursor)
            ca = contentAwp.createRun()
            ca.setText("Kegiatan ini merupakan salah satu dukungan terhadap kegiatan penting yang berkontribusi terhadap pencapaian target indikator PDO dan IRI yang ditetapkan pada tahun 2023 yaitu")

            if (!awp.eventOutput.isNullOrEmpty()) {
                cursor = contentAwp.ctp?.newCursor()
                cursor?.toEndToken()
                while (cursor?.toNextToken() != XmlCursor.TokenType.START);
                contentAwp = p.document.insertNewParagraph(cursor)
                ca = contentAwp.createRun()

                contentAwp = handleHtml(null, contentAwp, ca, awp.eventOutput ?: "")
//                ca.setText(Jsoup.parse(awp.eventOutput ?: "").text())
            }

            awp.pdo?.forEach { awpdo ->
                cursor = contentAwp.ctp?.newCursor()
                cursor?.toEndToken()
                while (cursor?.toNextToken() != XmlCursor.TokenType.START);
                contentAwp = p.document.insertNewParagraph(cursor)
                ca = contentAwp.createRun()
                ca.setText("- ${awpdo.pdo?.name} - ${awpdo.pdo?.description}")

            }

            awp.iri?.forEach { awpiri ->
                cursor = contentAwp.ctp?.newCursor()
                cursor?.toEndToken()
                while (cursor?.toNextToken() != XmlCursor.TokenType.START);
                contentAwp = p.document.insertNewParagraph(cursor)
                ca = contentAwp.createRun()
                ca.setText("- ${awpiri.iri?.name} - ${awpiri.iri?.description}")
            }
            cursor = contentAwp.ctp?.newCursor()
            cursor?.toEndToken()
            while (cursor?.toNextToken() != XmlCursor.TokenType.START);
            contentAwp = p.document.insertNewParagraph(cursor)
            ca = contentAwp.createRun()
            excelPoiAddBreak(ca, 1)
            ca.setText("9) Potensi Risiko dan Mitigasi risiko")

            if (!awp.descriptionRisk.isNullOrEmpty()) {
//                handleHtml(null, p, ca,awp.descriptionRisk ?: "")
//                ca.setText("Potensi : ${Jsoup.parse(awp.descriptionRisk ?: "").text()}")
                cursor = contentAwp.ctp?.newCursor()
                cursor?.toEndToken()
                while (cursor?.toNextToken() != XmlCursor.TokenType.START);
                contentAwp = p.document.insertNewParagraph(cursor)
                ca = contentAwp.createRun()
                ca.setText("Potensi : ")
                cursor = contentAwp.ctp?.newCursor()
                cursor?.toEndToken()
                while (cursor?.toNextToken() != XmlCursor.TokenType.START);
                contentAwp = p.document.insertNewParagraph(cursor)
                ca = contentAwp.createRun()
                contentAwp = handleHtml(null, contentAwp, ca, awp.descriptionRisk ?: "")
                cursor = contentAwp.ctp?.newCursor()
                cursor?.toEndToken()
                while (cursor?.toNextToken() != XmlCursor.TokenType.START);
                contentAwp = p.document.insertNewParagraph(cursor)
                ca = contentAwp.createRun()
            } else {
                ca.addBreak()
                ca.setText("Potensi :")
            }

            if (!awp.mitigationRisk.isNullOrEmpty()) {
                cursor = contentAwp.ctp?.newCursor()
                cursor?.toEndToken()
                while (cursor?.toNextToken() != XmlCursor.TokenType.START);
                contentAwp = p.document.insertNewParagraph(cursor)
                ca = contentAwp.createRun()
                ca.setText("Resiko : ")
                cursor = contentAwp.ctp?.newCursor()
                cursor?.toEndToken()
                while (cursor?.toNextToken() != XmlCursor.TokenType.START);
                contentAwp = p.document.insertNewParagraph(cursor)
                ca = contentAwp.createRun()
                contentAwp = handleHtml(null, contentAwp, ca, awp.mitigationRisk ?: "")
//                ca.setText("Resiko : ${Jsoup.parse(awp.mitigationRisk ?: "").text()}")
                cursor = contentAwp.ctp?.newCursor()
                cursor?.toEndToken()
                while (cursor?.toNextToken() != XmlCursor.TokenType.START);
                contentAwp = p.document.insertNewParagraph(cursor)
                ca = contentAwp.createRun()
            } else {
                ca.addBreak()
                ca.setText("Resiko :")
            }

            excelPoiAddBreak(ca, 2)
        }
        return contentAwp;
    }

    private fun appendPdoTable(table: AppendWordTable, year: Int, awpData: AwpData?) {
        try {
            val yearBefore = year - 1
            val diffYear = (yearBefore - 2020) + 1
            val xwpTable: XWPFTable? = table.table
            var idx = table.index ?: 0
            repoPdo.getByActiveOrderByCode().forEach {
                val trPdo = xwpTable?.insertNewTableRow(idx)
                val tcp1 = trPdo?.createCell()?.paragraphs
                if (tcp1 != null) {
                    for (p in tcp1) {
                        val pRun = p.createRun()
                        pRun.setText("PDO #${it.code}:")
                        pRun.addBreak()
                        pRun.setText(it.description)
                        pRun.addBreak()
                        pRun.addBreak()

                        val yearValue = mutableMapOf<Int, String?>()
                        yearValue[2020] = it.y_2020
                        yearValue[2021] = it.y_2021
                        yearValue[2022] = it.y_2022
                        yearValue[2023] = it.y_2023
                        yearValue[2024] = it.y_2024

                        val strTarget = "Target PDO ${it.code} $yearBefore Y($diffYear):"
                        pRun.setText(strTarget)
                        pRun.addBreak()
                        pRun.setText(yearValue[yearBefore])

                    }
                }

                var pdoTarget = ""
                val pdo: AwpDataPdo? = awpData?.pdo?.find { fp ->
                    fp.pdoId == it.id
                }

                if (awpData != null) {
                    if (pdo != null) {
                        pdoTarget = pdo.target ?: ""
                    }
                }


                trPdo?.createCell()?.text = Jsoup.parse(pdoTarget).text()
                idx++

                repoIri.getByCodeAndActive(it.code).forEach { ir ->
                    val trIri = xwpTable?.insertNewTableRow(idx)
                    val tcp1 = trIri?.createCell()?.paragraphs
                    if (tcp1 != null) {
                        for (p in tcp1) {
                            val pRun = p.createRun()
                            pRun.setText("IRI #${ir.name?.replace("IRI-", "")}:")
                            pRun.addBreak()
                            pRun.setText(ir.description)
                            pRun.addBreak()
                            pRun.addBreak()

                            val yearValue = mutableMapOf<Int, String?>()
                            yearValue[2020] = ir.y_2020
                            yearValue[2021] = ir.y_2021
                            yearValue[2022] = ir.y_2022
                            yearValue[2023] = ir.y_2023
                            yearValue[2024] = ir.y_2024

                            val strTarget = "Target ${ir.name} $yearBefore Y($diffYear):"
                            pRun.setText(strTarget)
                            pRun.addBreak()
                            pRun.setText(yearValue[yearBefore])
                        }
                    }

                    var iriTarget = ""
                    if (pdo != null) {
                        val iri: AwpDataPdoIri? = pdo.iri?.find { fi ->
                            fi.iriId == ir.id
                        }

                        if (iri != null) {
                            iriTarget = iri.target ?: ""
                        }
                    }

                    trIri?.createCell()?.text = Jsoup.parse(iriTarget).text()
                    idx++
                }
            }
        } catch (e: Exception) {
            throw e
        }
    }

    // replace data string pada template sesuai dengan string yang akan di replace
    private fun getstrReplaceWord(year: Int, awpData: AwpData?): MutableMap<String, String> {
        try {/*if (replace?.contains("#rc") == true) println("getstrReplaceWord => $replace")*/

            val mapReplace = mutableMapOf<String, String>()
            mapReplace["replaceComponentAwp"] = ""
            mapReplace["replaceTableOfContents"] = ""

            mapReplace["replaceYearBefore"] = (year - 1).toString()
            mapReplace["replaceYear"] = year.toString()

            var replacePmuHead = ""
            val usersPmuHead = repoUserRoleViewUserResources.getPmuHead(
                listOf(ROLE_ID_COORDINATOR, ROLE_ID_ADMINSTRATOR),
                2,
                COMPONENT_ID_PMU,
            )

            if (usersPmuHead.isNotEmpty()) {
                val user = repoUser.findByIdAndActive(usersPmuHead[0])
                if (user.isPresent) {
                    replacePmuHead = ""
                    replacePmuHead += user.get().firstName
                    if (!user.get().lastName.isNullOrEmpty()) {
                        replacePmuHead += " ${user.get().lastName}"
                    }
                }
            }


            mapReplace["replacePmuHead"] = replacePmuHead
            mapReplace["replaceCountRefromYear"] = "ke${indonesianNumber((Year.now().value - 2021) + 1)}"

            var replaceComponent1Description = ""
            val component1 = repoComponent.findByCodeAndActive("1")
            if (component1.isPresent) {
                replaceComponent1Description = component1.get().description ?: ""
            }
            mapReplace["#rc1"] = replaceComponent1Description

            var replaceComponent2Description = ""
            val component2 = repoComponent.findByCodeAndActive("2")
            if (component2.isPresent) {
                replaceComponent2Description = component2.get().description ?: ""
            }
            mapReplace["#rc2"] = replaceComponent2Description

            var replaceComponent3Description = ""
            val component3 = repoComponent.findByCodeAndActive("3")
            if (component3.isPresent) {
                replaceComponent3Description = component3.get().description ?: ""
            }
            mapReplace["#rc3"] = replaceComponent3Description

            var replaceComponent4Description = ""
            val component4 = repoComponent.findByCodeAndActive("3")
            if (component4.isPresent) {
                replaceComponent4Description = component4.get().description ?: ""
            }
            mapReplace["#rc4"] = replaceComponent4Description

            val mapDataAwp = mapAwpData(awpData)

            mapReplace["replaceDIPAAwalKomponen1"] = formatRupiah(mapDataAwp["dipaAwalKomponen1"], false)

            mapReplace["replaceDIPAAwalKomponen2"] = formatRupiah(mapDataAwp["dipaAwalKomponen2"], false)

            mapReplace["replaceDIPAAwalKomponen3"] = formatRupiah(mapDataAwp["dipaAwalKomponen3"], false)

            mapReplace["replaceDIPAAwalKomponen4"] = formatRupiah(mapDataAwp["dipaAwalKomponen4"], false)


            mapReplace["replaceDIPARevisiKomponen1"] = formatRupiah(mapDataAwp["dipaRevisiKomponen1"], false)

            mapReplace["replaceDIPARevisiKomponen2"] = formatRupiah(mapDataAwp["dipaRevisiKomponen2"], false)

            mapReplace["replaceDIPARevisiKomponen3"] = formatRupiah(mapDataAwp["dipaRevisiKomponen3"], false)

            mapReplace["replaceDIPARevisiKomponen4"] = formatRupiah(mapDataAwp["dipaRevisiKomponen4"], false)


            mapReplace["replaceRealisasiKomponen1"] = formatRupiah(mapDataAwp["dipaRealisasiKomponen1"], false)

            mapReplace["replaceRealisasiKomponen2"] = formatRupiah(mapDataAwp["dipaRealisasiKomponen2"], false)

            mapReplace["replaceRealisasiKomponen3"] = formatRupiah(mapDataAwp["dipaRealisasiKomponen3"], false)

            mapReplace["replaceRealisasiKomponen4"] = formatRupiah(mapDataAwp["dipaRealisasiKomponen4"], false)


            mapReplace["replaceRealisasiPersenKomponen1"] = round(mapDataAwp["realisasiPercentKomponent1"], 2)

            mapReplace["replaceRealisasiPersenKomponen2"] = round(mapDataAwp["realisasiPercentKomponent2"], 2)

            mapReplace["replaceRealisasiPersenKomponen3"] = round(mapDataAwp["realisasiPercentKomponent3"], 3)

            mapReplace["replaceRealisasiPersenKomponen4"] = round(mapDataAwp["realisasiPercentKomponent4"], 4)


            mapReplace["replaceDIPAAwalRupiahMurni"] = formatRupiah(mapDataAwp["dipaAwalRupiahMurni"], false)

            mapReplace["replaceDIPARevisiRupiahMurni"] = formatRupiah(mapDataAwp["dipaRevisiRupiahMurni"], false)

            mapReplace["replaceRealisasiRupiahMurni"] = formatRupiah(mapDataAwp["dipaRealisasiRupiahMurni"], false)

            mapReplace["replaceRealisasiPersenRupiahMurni"] = round(mapDataAwp["dipaRealisasiPercent"], 2)

            mapReplace["replaceDIPAAwal"] = formatRupiah(mapDataAwp["totalDipaAwal"], false)

            mapReplace["replaceDIPARevisi"] = formatRupiah(mapDataAwp["totalDipaRevisi"], false)

            mapReplace["replaceRealisasi"] = formatRupiah(mapDataAwp["totalDipaRealisasi"], false)

            mapReplace["replaceRealisasiPersen"] = round(mapDataAwp["totalDipaPercent"], 2)


            mapReplace["#rtdpr"] = formatRupiah(mapDataAwp["totalDipaRevisi"])
            mapReplace["#rtr"] = formatRupiah(mapDataAwp["totalDipaRealisasi"])
            mapReplace["#rtrp"] = round(mapDataAwp["totalDipaPercent"], 2) + "%"

            mapReplace["#rkkk"] = awpData?.keteranganKontribusiKomponent ?: ""

            mapReplace["replaceKeteranganKendala"] = awpData?.keteranganKendala ?: ""
            mapReplace["replaceKeteranganPembelajaran"] = awpData?.keteranganPembelajaran ?: ""
            mapReplace["replaceKeteranganRencanaKerja"] = awpData?.keteranganRencanaKerja ?: ""
            mapReplace["replaceKeteranganUsulanKegiatan"] = awpData?.keteranganUsulanKegiatan ?: ""

            mapReplace["replaceKeteranganKemajuanProgram"] = awpData?.keteranganKemajuanProgram ?: ""

            mapReplace["replaceKeteranganDanHambatanKomponen1"] = awpData?.hambatanKomponen1 ?: ""
            mapReplace["replaceKeteranganDanHambatanKomponen2"] = awpData?.hambatanKomponen2 ?: ""
            mapReplace["replaceKeteranganDanHambatanKomponen3"] = awpData?.hambatanKomponen3 ?: ""
            mapReplace["replaceKeteranganDanHambatanKomponen4"] = awpData?.hambatanKomponen4 ?: ""

//            if (mapReplace.contains(replace)) {
//                return mapReplace[replace]
//            }

            return mapReplace
        } catch (e: Exception) {
            throw e
        }
    }

    private fun mapAwpData(awpData: AwpData?): Map<String, Double> {
        val mapData = mutableMapOf<String, Double>()
        try {

            val dipaAwalKomponen1 = awpData?.dipaAwalKomponen1?.toDouble() ?: 0.toDouble()
            mapData["dipaAwalKomponen1"] = dipaAwalKomponen1

            val dipaAwalKomponen2 = awpData?.dipaAwalKomponen2?.toDouble() ?: 0.toDouble()
            mapData["dipaAwalKomponen2"] = dipaAwalKomponen2

            val dipaAwalKomponen3 = awpData?.dipaAwalKomponen3?.toDouble() ?: 0.toDouble()
            mapData["dipaAwalKomponen3"] = dipaAwalKomponen3

            val dipaAwalKomponen4 = awpData?.dipaAwalKomponen4?.toDouble() ?: 0.toDouble()
            mapData["dipaAwalKomponen4"] = dipaAwalKomponen4

            val totalDipaAwal = dipaAwalKomponen1 + dipaAwalKomponen2 + dipaAwalKomponen3 + dipaAwalKomponen4
            mapData["totalDipaAwal"] = totalDipaAwal

            val dipaRevisiKomponen1 = awpData?.dipaRevisiKomponen1?.toDouble() ?: 0.toDouble()
            mapData["dipaRevisiKomponen1"] = dipaRevisiKomponen1

            val dipaRevisiKomponen2 = awpData?.dipaRevisiKomponen2?.toDouble() ?: 0.toDouble()
            mapData["dipaRevisiKomponen2"] = dipaRevisiKomponen2

            val dipaRevisiKomponen3 = awpData?.dipaRevisiKomponen3?.toDouble() ?: 0.toDouble()
            mapData["dipaRevisiKomponen3"] = dipaRevisiKomponen3

            val dipaRevisiKomponen4 = awpData?.dipaRevisiKomponen4?.toDouble() ?: 0.toDouble()
            mapData["dipaRevisiKomponen4"] = dipaRevisiKomponen4

            val totalDipaRevisi = dipaRevisiKomponen1 + dipaAwalKomponen2 + dipaAwalKomponen3 + dipaAwalKomponen4
            mapData["totalDipaRevisi"] = totalDipaRevisi

            val dipaRealisasiKomponen1 = awpData?.dipaRealisasiKomponen1?.toDouble() ?: 0.toDouble()
            mapData["dipaRealisasiKomponen1"] = dipaRealisasiKomponen1

            val dipaRealisasiKomponen2 = awpData?.dipaRealisasiKomponen2?.toDouble() ?: 0.toDouble()
            mapData["dipaRealisasiKomponen2"] = dipaRealisasiKomponen2

            val dipaRealisasiKomponen3 = awpData?.dipaRealisasiKomponen3?.toDouble() ?: 0.toDouble()
            mapData["dipaRealisasiKomponen3"] = dipaRealisasiKomponen3

            val dipaRealisasiKomponen4 = awpData?.dipaRealisasiKomponen4?.toDouble() ?: 0.toDouble()
            mapData["dipaRealisasiKomponen4"] = dipaRealisasiKomponen4

            val totalDipaRealisasi =
                dipaRealisasiKomponen1 + dipaRealisasiKomponen2 + dipaRealisasiKomponen3 + dipaRealisasiKomponen4
            mapData["totalDipaRealisasi"] = totalDipaRealisasi

            val realisasiPercentKomponent1 = (dipaRealisasiKomponen1 / dipaRevisiKomponen1) * 100
            mapData["realisasiPercentKomponent1"] = realisasiPercentKomponent1

            val realisasiPercentKomponent2 = (dipaRealisasiKomponen2 / dipaRevisiKomponen2) * 100
            mapData["realisasiPercentKomponent2"] = realisasiPercentKomponent2

            val realisasiPercentKomponent3 = (dipaRealisasiKomponen3 / dipaRevisiKomponen3) * 100
            mapData["realisasiPercentKomponent3"] = realisasiPercentKomponent3

            val realisasiPercentKomponent4 = (dipaRealisasiKomponen4 / dipaRevisiKomponen4) * 100
            mapData["realisasiPercentKomponent4"] = realisasiPercentKomponent4

            val dipaAwalRupiahMurni = awpData?.dipaAwalRupiahMurni?.toDouble() ?: 0.toDouble()
            mapData["dipaAwalRupiahMurni"] = dipaAwalRupiahMurni

            val dipaRevisiRupiahMurni = awpData?.dipaRevisiRupiahMurni?.toDouble() ?: 0.toDouble()
            mapData["dipaRevisiRupiahMurni"] = dipaRevisiRupiahMurni

            val dipaRealisasiRupiahMurni = awpData?.dipaRealisasiRupiahMurni?.toDouble() ?: 0.toDouble()
            mapData["dipaRealisasiRupiahMurni"] = dipaRealisasiRupiahMurni

            val dipaRealisasiPercent = (dipaRealisasiRupiahMurni / dipaRevisiRupiahMurni) * 100
            mapData["dipaRealisasiPercent"] = dipaRealisasiPercent

            val totalDipaPercent =
                (realisasiPercentKomponent1 + realisasiPercentKomponent2 + realisasiPercentKomponent3 + realisasiPercentKomponent4 + dipaRealisasiPercent) / 5
            mapData["totalDipaPercent"] = totalDipaPercent

        } catch (e: Exception) {
            throw e
        }

        return mapData
    }

    private fun handleHtml(
        cell: XWPFTableCell?,
        paragraph: XWPFParagraph,
        run: XWPFRun,
        html: String,
    ): XWPFParagraph {
        val doc = Jsoup.parse(replaceUnusedSpaceCkEditor(html))
        val elements = doc.select("body")
        return HtmlToDoc.handleHtmlRecursive(elements, cell, paragraph, run, HtmlRecursiveParameters())
    }

    data class AppendWordTable(
        var name: String? = null,
        var table: XWPFTable? = null,
        var index: Int? = 0,
    )
}
