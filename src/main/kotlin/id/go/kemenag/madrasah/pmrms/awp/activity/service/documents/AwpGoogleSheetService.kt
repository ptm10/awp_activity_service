package id.go.kemenag.madrasah.pmrms.awp.activity.service.documents

import com.google.api.client.auth.oauth2.Credential
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport
import com.google.api.client.http.javanet.NetHttpTransport
import com.google.api.client.json.gson.GsonFactory
import com.google.api.client.util.store.FileDataStoreFactory
import com.google.api.services.sheets.v4.Sheets
import com.google.api.services.sheets.v4.SheetsScopes
import com.google.api.services.sheets.v4.model.ValueRange
import id.go.kemenag.madrasah.pmrms.awp.activity.helpers.responseSuccess
import id.go.kemenag.madrasah.pmrms.awp.activity.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.awp.activity.service.AwpService
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import java.io.File
import java.io.FileNotFoundException
import java.io.InputStream
import java.io.InputStreamReader
import java.util.*

@Service
class AwpGoogleSheetService {
    val applicationName = "Google Sheets API Java Quickstart"
    val httpTransport = GoogleNetHttpTransport.newTrustedTransport()
    val spreadsheetId = "1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms"
    val jsonFactory = GsonFactory.getDefaultInstance()
    val tokensDirectotyPath = "tokens"
    val filePath = "/credentials.json"
    val scopes = Collections.singletonList(SheetsScopes.SPREADSHEETS_READONLY)

    fun testGoogleSheet(): ResponseEntity<ReturnData> {
        try {
            val range = "Class Data!A2:F"
            val service = Sheets.Builder(httpTransport, jsonFactory, getCredentials(httpTransport))
                .setApplicationName(applicationName)
                .build()

            val response: ValueRange = service.spreadsheets().values()[spreadsheetId, range]
                .execute()
            val values: List<List<Any>> = response.getValues()
            if (values.isEmpty()) {
                println("No data found.")
            } else {
                println("Name, Major")
                for (row in values) {
                    // Print columns A and E, which correspond to indices 0 and 4.
                    System.out.printf("%s, %s\n", row[0], row[4])
                }
            }

            return responseSuccess(data = response)
        } catch (e: Exception) {
            throw e
        }
    }

    private fun getCredentials(httpTransport: NetHttpTransport): Credential? {
        try {
            // Load client secrets.
            val inputStream: InputStream = AwpService::class.java.getResourceAsStream(filePath)
                ?: throw FileNotFoundException("Resource not found: $filePath")
            val clientSecrets = GoogleClientSecrets.load(jsonFactory, InputStreamReader(inputStream))

            // Build flow and trigger user authorization request.
            val flow = GoogleAuthorizationCodeFlow.Builder(
                httpTransport, jsonFactory, clientSecrets, scopes
            )
                .setDataStoreFactory(FileDataStoreFactory(File(tokensDirectotyPath)))
                .setAccessType("offline")
                .build()
            val receiver = LocalServerReceiver.Builder().setPort(8888).build()
            return AuthorizationCodeInstalledApp(flow, receiver).authorize("user")
        } catch (e: Exception) {
            throw e
        }
    }
}
