package id.go.kemenag.madrasah.pmrms.awp.activity.service.documents

import com.lowagie.text.DocumentException
import id.go.kemenag.madrasah.pmrms.awp.activity.constant.*
import id.go.kemenag.madrasah.pmrms.awp.activity.helpers.dateFormatIndonesia
import id.go.kemenag.madrasah.pmrms.awp.activity.helpers.formatRupiah
import id.go.kemenag.madrasah.pmrms.awp.activity.helpers.responseNotFound
import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.AwpImplementation
import id.go.kemenag.madrasah.pmrms.awp.activity.repository.*
import org.apache.poi.xwpf.usermodel.IRunBody
import org.apache.poi.xwpf.usermodel.XWPFDocument
import org.apache.poi.xwpf.usermodel.XWPFParagraph
import org.apache.poi.xwpf.usermodel.XWPFRun
import org.apache.xmlbeans.XmlCursor
import org.apache.xmlbeans.XmlObject
import org.jsoup.Jsoup
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTR
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.io.ClassPathResource
import org.springframework.stereotype.Service
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*
import javax.servlet.http.HttpServletResponse

@Suppress("UNREACHABLE_CODE")
@Service
class AwpImplementationDocumentService {

    @Autowired
    private lateinit var repo: AwpImplementationRepository

    @Autowired
    private lateinit var repoUserRoleViewUserResources: UserRoleViewUserResourcesRepository

    @Autowired
    private lateinit var repoUser: UserRepository

    @Autowired
    private lateinit var repoEvent: EventRepository

    @Autowired
    private lateinit var repoEventReport: EventReportRepository

    private val wordTemplate = "files/Awp-implementation-template.docx"

    @Value("\${directories.word}")
    private lateinit var dirWord: String

    @Autowired
    private lateinit var response: HttpServletResponse

    @Autowired
    private lateinit var serviceProjectReportDocument: ProjectReportDocumentService

    fun downloadWord(id: String?) {
        try {
            val data = repo.findByIdAndActive(id)
            if (!data.isPresent) {
                responseNotFound(response)
            } else {
                if (data.get().status != AWP_IMPLEMENTATION_STATUS_IMPLEMENTATION) {
                    responseNotFound(response)
                }
            }

            val path = Paths.get(dirWord)
            if (!Files.exists(path)) {
                Files.createDirectories(path)
            }

            val file = File.createTempFile("awp-implementation-report-", ".docx", path.toFile())

            writeWord(file.path, data.get())

            if (Files.exists(file.toPath())) {
                response.contentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                response.addHeader(
                    "Content-Disposition", "attachment; filename=" + file.name
                )
                Files.copy(file.toPath(), response.outputStream)
                response.outputStream.flush()
            }
        } catch (ex: DocumentException) {
            ex.printStackTrace()
        } catch (ex: IOException) {
            ex.printStackTrace()
        }
    }

    @Throws(IOException::class)
    private fun writeWord(output: String, data: AwpImplementation) {
        try {

            var paragraphParticipant: XWPFParagraph? = null
            var paragraphNs: XWPFParagraph? = null
            var paragraphRisk: XWPFParagraph? = null
            var paragraphAnggaran: XWPFParagraph? = null
            var paragraphProjectOfficer: XWPFParagraph? = null
            var paragraphEventReport: XWPFParagraph? = null

            XWPFDocument(
                ClassPathResource(wordTemplate).inputStream
            ).use { doc ->
                // membaca text yang ada di paragraf template
                for (p in doc.paragraphs) {
                    val cursor: XmlCursor = p.ctp.newCursor()
                    cursor.selectPath("declare namespace w='http://schemas.openxmlformats.org/wordprocessingml/2006/main' .//*/w:txbxContent/w:p/w:r")

                    val ctrsintxtbx: MutableList<XmlObject> = ArrayList()
                    while (cursor.hasNextSelection()) {
                        cursor.toNextSelection()
                        val obj = cursor.getObject()
                        if (!ctrsintxtbx.contains(obj)) {
                            ctrsintxtbx.add(obj)
                        }
                    }

                    for (obj in ctrsintxtbx) {
                        val ctr: CTR = CTR.Factory.parse(obj.xmlText())
                        val bufferrun = XWPFRun(ctr, p as IRunBody?)
                        var docText = bufferrun.getText(0)

//                        println("doc.textBox=> $docText")

                        if (docText != null) {
                            getstrReplaceWord(docText, data)?.let {
                                docText = docText.replace(docText, it)
                                bufferrun.setText(docText, 0)
                            }
                        }

                        obj.set(bufferrun.ctr)
                    }


                    val runs = p.runs
                    if (runs != null) {
                        for (r in runs) {
                            var docText = r.getText(0)
//                            println("doc.paragraphs=> $docText")

                            if (docText != null) {
                                if (docText == "cevent") {
                                    paragraphEventReport = p
                                }

                                getstrReplaceWord(docText, data)?.let {
                                    docText = docText.replace(docText, it)
                                    r.setText(docText, 0)
                                }
                            }
                        }
                    }
                }

                // membaca text yang ada di tabel template
                for (tbl in doc.tables) {
                    for (row in tbl.rows) {
                        for (cell in row.tableCells) {
                            for (p in cell.paragraphs) {
                                for (r in p.runs) {
                                    var textTable = r.text()

//                                    println("doc.tableCells=> $textTable")

                                    if (textTable != null) {
                                        if (textTable == "rparticipant") {
                                            paragraphParticipant = p
                                        }

                                        if (textTable == "rns") {
                                            paragraphNs = p
                                        }

                                        if (textTable == "rrisk") {
                                            paragraphRisk = p
                                        }

                                        if (textTable == "rab") {
                                            paragraphAnggaran = p
                                        }

                                        if (textTable == "t7") {
                                            paragraphProjectOfficer = p
                                        }

                                        getstrReplaceWord(textTable, data)?.let {
                                            textTable = textTable.replace(textTable, it)
                                            r.setText(textTable, 0)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                paragraphParticipant?.let {
                    writeParticipant(it, data)
                }

                paragraphNs?.let {
                    writeNs(it, data)
                }

                paragraphRisk?.let {
                    writeRisk(it, data)
                }

                paragraphAnggaran?.let {
                    writeAnggaran(it, data)
                }

                paragraphProjectOfficer?.let {
                    writeProjectOfficer(it, data)
                }

                paragraphEventReport?.let {
                    serviceProjectReportDocument.writeEventReportFromAwpImplementationReport(doc, it, data)
                }

                FileOutputStream(output).use { out ->
                    doc.write(out)
                }
            }
        } catch (e: Exception) {
            throw e
        }
    }

    private fun writeParticipant(p: XWPFParagraph, data: AwpImplementation) {
        try {
            val content: XWPFParagraph = p
            val ct = content.createRun()
            ct.fontSize = 11
            ct.setText("Jumlah Peserta : ${(data.participantMale ?: 0) + (data.participantFemale ?: 0)}")
            ct.addBreak()
            ct.setText("Peserta Laki-laki : ${data.participantMale ?: 0}")
            ct.addBreak()
            ct.setText("Peserta Laki-laki : ${data.participantFemale ?: 0}")
        } catch (e: Exception) {
            throw e
        }
    }

    private fun writeNs(p: XWPFParagraph, data: AwpImplementation) {
        try {
            val content: XWPFParagraph = p
            val ct = content.createRun()
            ct.fontSize = 11
            ct.setText("Jumlah Narasumber : ${(data.nsMale ?: 0) + (data.nsFemale ?: 0)}")
            ct.addBreak()
            ct.setText("Narasumber Laki-laki : ${data.nsMale ?: 0}")
            ct.addBreak()
            ct.setText("Narasumber Perempuan : ${data.nsFemale ?: 0}")
        } catch (e: Exception) {
            throw e
        }
    }

    private fun writeProjectOfficer(p: XWPFParagraph, data: AwpImplementation) {
        try {
            val content: XWPFParagraph = p
            val ct = content.createRun()
            ct.fontSize = 11

            if (data.eventManagementTypeId == EVENT_MANAGEMENT_TYPE_SWAKELOLA) {
                var projectOfficer = ""
                if (!data.projectOfficerResources?.user?.firstName.isNullOrEmpty()) {
                    projectOfficer += data.projectOfficerResources?.user?.firstName
                }
                if (!data.projectOfficerResources?.user?.lastName.isNullOrEmpty()) {
                    projectOfficer += " ${data.projectOfficerResources?.user?.lastName}"
                }
                ct.setText("Project Officer : $projectOfficer")
            } else {
                ct.setText("LSP : ${data.lspContractData ?: ""}")
                ct.addBreak()
                ct.setText("Nomor Kontrak : ${data.lspContractNumber ?: ""}")
                ct.addBreak()

                var picLsp = ""
                if (!data.lspPicUser?.firstName.isNullOrEmpty()) {
                    picLsp += data.projectOfficerResources?.user?.firstName
                }
                if (!data.lspPicUser?.lastName.isNullOrEmpty()) {
                    picLsp += " ${data.projectOfficerResources?.user?.lastName}"
                }
                ct.setText("PIC LSP : $picLsp")
            }

        } catch (e: Exception) {
            throw e
        }
    }

    private fun writeRisk(p: XWPFParagraph, data: AwpImplementation) {
        try {
            val content: XWPFParagraph = p
            val ct = content.createRun()
            ct.fontSize = 11
            ct.setText("Keterangan Risiko : ${Jsoup.parse(data.descriptionRisk ?: "").text()}")
            ct.addBreak()
            ct.setText("Dampak Risiko : ${data.impactRisk ?: 0}")
            ct.addBreak()
            ct.setText("Kemungkinan Terjadi : ${data.potentialRisk ?: 0}")
            ct.addBreak()
            ct.setText("Mitigasi : ${Jsoup.parse(data.mitigationRisk ?: "").text()}")
            ct.addBreak()
            ct.setText("Gejala Risiko :${Jsoup.parse(data.symptomsRisk ?: "").text()}")
        } catch (e: Exception) {
            throw e
        }
    }

    private fun writeAnggaran(p: XWPFParagraph, data: AwpImplementation) {
        try {
            val content: XWPFParagraph = p
            val ct = content.createRun()
            ct.fontSize = 11
            ct.setText("Rencana Anggaran AWP : ${formatRupiah(data.budgetAwp?.toDouble())}")
            ct.addBreak()
            ct.setText("Rencana Anggaran POK : ${formatRupiah(data.budgetPok?.toDouble())}")
            ct.addBreak()

            var realitation = 0.toLong()
            repoEvent.getByAwpImplementationIdAndStatusAndActive(data.id, EVENT_STATUS_IMPLEMENTATION).forEach { e ->
                repoEventReport.getByEventIdAndStatusAndActive(e.id, EVENT_REPORT_STATUS_APPROVED).forEach { er ->
                    er.rraBudget?.let { errr ->
                        realitation += errr
                    }
                }
            }


            ct.setText("Realisasi Anggaran : ${formatRupiah(realitation.toDouble())}")
        } catch (e: Exception) {
            throw e
        }
    }

    // replace data string pada template sesuai dengan string yang akan di replace
    private fun getstrReplaceWord(docText: String, data: AwpImplementation): String? {
        try {
            val mapReplace = mutableMapOf<String, String>()

            mapReplace["rparticipant"] = ""
            mapReplace["rns"] = ""
            mapReplace["rrisk"] = ""
            mapReplace["rab"] = ""
            mapReplace["rpic"] = ""
            mapReplace["t7"] = ""
            mapReplace["cevent"] = ""

            mapReplace["rname"] = data.name ?: ""
            mapReplace["rdate"] = "${dateFormatIndonesia(data.startDate)} - ${dateFormatIndonesia(data.endDate)}"
            mapReplace["rcode"] = data.componentCode?.code ?: ""
            mapReplace["rnow"] = dateFormatIndonesia(Date())
            mapReplace["rc1"] = "Jakarta, ${dateFormatIndonesia(Date())}"
            mapReplace["ryear"] = data.awp?.year?.toString() ?: ""
            mapReplace["rpurpose"] = Jsoup.parse(data.purpose ?: "").text()
            mapReplace["rstart"] = dateFormatIndonesia(data.startDate)
            mapReplace["rend"] = dateFormatIndonesia(data.endDate)
            mapReplace["rvolume"] = data.eventVolume?.toString() ?: "0"
            mapReplace["rmtype"] = data.eventManagementType?.name ?: ""

            mapReplace["rcomponentname"] = "${data.component?.code ?: ""} - ${data.component?.description ?: ""}"
            mapReplace["rsubcomponentname"] =
                "${data.subComponent?.code ?: ""} - ${data.subComponent?.description ?: ""}"
            mapReplace["rrcn"] = "${data.subSubComponent?.code ?: ""} - ${data.subSubComponent?.description ?: ""}"
            mapReplace["rcodename"] = "${data.componentCode?.code ?: ""} - ${data.componentCode?.description ?: ""}"

            mapReplace["rtype"] = data.eventType?.name ?: ""
            mapReplace["rvolume"] = data.eventVolume?.toString() ?: "0"
            mapReplace["rpurpose"] = Jsoup.parse(data.purpose ?: "").text()
            mapReplace["routput"] = Jsoup.parse(data.eventOutput ?: "").text()

            var place = ""
            data.provincesRegencies?.forEach { pr ->
                if (place != "") {
                    place += ", "
                }

                var provinceName = pr.province?.name
                if (provinceName == "Lainnya") {
                    provinceName = "tempat yang telah ditentukan"
                }

                place += provinceName

                pr.regencies?.forEachIndexed { iprr, prr ->
                    val size = (pr.regencies?.size ?: 0) - 1

                    if (iprr == 0) {
                        place += "("
                    }

                    if (iprr > 0 && iprr < size + 1) {
                        place += ", "
                    }

                    var regencyName = prr.regency?.name
                    if (regencyName == "Lainnya") {
                        regencyName = "tempat yang telah ditentukan"
                    }

                    place += regencyName
                    if (iprr == size) {
                        place += ")"
                    }
                }
            }

            mapReplace["rplace"] = place

            var rcreatedby = ""
            if (!data.createdByUser?.firstName.isNullOrEmpty()) {
                rcreatedby += data.createdByUser?.firstName
            }
            if (!data.createdByUser?.lastName.isNullOrEmpty()) {
                rcreatedby += " ${data.createdByUser?.lastName}"
            }

            mapReplace["rcreatedby"] = data.componentCode?.code ?: ""

            var pic = ""
            if (!data.projectOfficerResources?.user?.firstName.isNullOrEmpty()) {
                pic += data.projectOfficerResources?.user?.firstName
            }

            if (!data.projectOfficerResources?.user?.lastName.isNullOrEmpty()) {
                pic += " ${data.projectOfficerResources?.user?.lastName}"
            }

            if (data.eventManagementTypeId == EVENT_MANAGEMENT_TYPE_CONTRACT) {
                if (!data.lspPicUser?.firstName.isNullOrEmpty()) {
                    pic += data.lspPicUser?.firstName
                }
                if (!data.lspPicUser?.lastName.isNullOrEmpty()) {
                    pic += " ${data.lspPicUser?.lastName}"
                }
            }

            mapReplace["rpic"] = pic

            var secretary = ""
            val usersSecretary = repoUserRoleViewUserResources.getUserIdsByRoleIdsComponentIdPosition(
                listOf(ROLE_ID_COORDINATOR, ROLE_ID_ADMINSTRATOR), 2, COMPONENT_ID_PMU, POSITION_SECRETARY_PMU
            )

            if (usersSecretary.isNotEmpty()) {
                val user = repoUser.findByIdAndActive(usersSecretary[0])
                if (user.isPresent) {
                    if (!user.get().firstName.isNullOrEmpty()) {
                        secretary += user.get().firstName
                    }
                    if (!user.get().lastName.isNullOrEmpty()) {
                        secretary += " ${user.get().lastName}"
                    }
                }
            }

            mapReplace["rsec"] = secretary


            var pmu = ""
            val usersPmuHead = repoUserRoleViewUserResources.getPmuHead(
                listOf(ROLE_ID_COORDINATOR, ROLE_ID_ADMINSTRATOR),
                2,
                COMPONENT_ID_PMU,
            )

            if (usersPmuHead.isNotEmpty()) {
                val user = repoUser.findByIdAndActive(usersPmuHead[0])
                if (user.isPresent) {
                    pmu += user.get().firstName
                    if (!user.get().lastName.isNullOrEmpty()) {
                        pmu += " ${user.get().lastName}"
                    }
                }
            }

            mapReplace["rpmu"] = pmu

            var coordinator = ""
            val usersCoordinator = repoUserRoleViewUserResources.getUserIdsByRoleIdsComponentIdPosition(
                listOf(ROLE_ID_COORDINATOR, ROLE_ID_ADMINSTRATOR), 2, COMPONENT_ID_PMU, POSITION_SECRETARY_PMU
            )

            if (usersCoordinator.isNotEmpty()) {
                val user = repoUser.findByIdAndActive(usersCoordinator[0])
                if (user.isPresent) {
                    if (!user.get().firstName.isNullOrEmpty()) {
                        coordinator += user.get().firstName
                    }
                    if (!user.get().lastName.isNullOrEmpty()) {
                        coordinator += " ${user.get().lastName}"
                    }
                }
            }

            mapReplace["rcoordinator"] = coordinator
            mapReplace["rapprovedby"] = pmu

            return mapReplace[docText]
        } catch (e: Exception) {
            throw e
        }
    }
}
