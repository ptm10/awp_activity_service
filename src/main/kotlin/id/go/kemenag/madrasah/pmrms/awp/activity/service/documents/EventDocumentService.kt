package id.go.kemenag.madrasah.pmrms.awp.activity.service.documents

import com.lowagie.text.DocumentException
import id.go.kemenag.madrasah.pmrms.awp.activity.constant.*
import id.go.kemenag.madrasah.pmrms.awp.activity.helpers.*
import id.go.kemenag.madrasah.pmrms.awp.activity.model.ReturnDataFiles
import id.go.kemenag.madrasah.pmrms.awp.activity.model.notif.EmailRequest
import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.UploadFormUserRequest
import id.go.kemenag.madrasah.pmrms.awp.activity.model.response.ErrorMessage
import id.go.kemenag.madrasah.pmrms.awp.activity.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.*
import id.go.kemenag.madrasah.pmrms.awp.activity.repository.EventFormExcelFileEmailRepository
import id.go.kemenag.madrasah.pmrms.awp.activity.repository.EventFormExcelFileRepository
import id.go.kemenag.madrasah.pmrms.awp.activity.repository.EventRepository
import id.go.kemenag.madrasah.pmrms.awp.activity.utility.ExcelUtility
import org.apache.commons.io.IOUtils
import org.apache.commons.validator.routines.EmailValidator
import org.apache.poi.xwpf.usermodel.XWPFDocument
import org.apache.poi.xwpf.usermodel.XWPFParagraph
import org.jsoup.Jsoup
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.io.ClassPathResource
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import org.thymeleaf.context.Context
import org.thymeleaf.spring5.SpringTemplateEngine
import org.xhtmlrenderer.pdf.ITextRenderer
import java.io.*
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.StandardCopyOption
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


@Suppress("UNCHECKED_CAST")
@Service
class EventDocumentService {

    @Autowired
    private lateinit var repo: EventRepository

    @Autowired
    private lateinit var response: HttpServletResponse

    @Autowired
    private lateinit var templateEngine: SpringTemplateEngine

    @Autowired
    private lateinit var repoEventFormExcelFile: EventFormExcelFileRepository

    @Autowired
    private lateinit var repoEventFormExcelFileEmail: EventFormExcelFileEmailRepository

    @Value("\${directories.pdf}")
    private lateinit var dirPdf: String

    @Value("\${directories.temp}")
    private lateinit var dirTemp: String

    @Value("\${url.files}")
    private lateinit var filesUrl: String

    private val wordTemplate = "files/Event-template.docx"

    private val formUserTemplates = "files/Event-form-user-template.xlsx"

    private val pdfResources = "/static/"

    @Value("\${url.notif}")
    private lateinit var notifUrl: String

    @Autowired
    private lateinit var httpServletRequest: HttpServletRequest

    @Value("\${directories.word}")
    private lateinit var dirWord: String

    @Value("\${directories.upload}")
    private lateinit var dirUpload: String

    @Value("\${questionnaire.url}")
    private lateinit var questionnaireUrl: String

    fun downloadWord(id: String?) {
        try {
            val data = repo.findByIdAndActive(id)
            if (!data.isPresent) {
                responseNotFound(response)
            }

            val path = Paths.get(dirWord)
            if (!Files.exists(path)) {
                Files.createDirectories(path)
            }

            val file = File.createTempFile("event-", ".docx", path.toFile())

            writeWord(file.path, data.get())

            if (Files.exists(file.toPath())) {
                response.contentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                response.addHeader(
                    "Content-Disposition", "attachment; filename=" + file.name
                )
                Files.copy(file.toPath(), response.outputStream)
                response.outputStream.flush()
            }
        } catch (ex: DocumentException) {
            ex.printStackTrace()
        } catch (ex: IOException) {
            ex.printStackTrace()
        }
    }

    fun downloadTemplateFormUser(): ByteArray? {
        var fis: FileInputStream? = null
        return try {
            val resources = ClassPathResource(formUserTemplates)

            val path = Paths.get(dirTemp)
            if (!Files.exists(path)) {
                Files.createDirectories(path)
            }

            val file = File.createTempFile("event-form-user-template-", ".xlsx", path.toFile())
            Files.copy(resources.inputStream, file.toPath(), StandardCopyOption.REPLACE_EXISTING)

            response.setHeader("Content-Disposition", "attachment; filename=${file.name}")
            fis = FileInputStream(file)
            IOUtils.toByteArray(fis)
        } catch (e: Exception) {
            e.printStackTrace()
            e.message!!.toByteArray()
        } finally {
            if (fis != null) {
                try {
                    fis.close()
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        }
    }


    @Throws(IOException::class)
    private fun writeWord(output: String, data: Event) {
        try {
            XWPFDocument(
                ClassPathResource(wordTemplate).inputStream
            ).use { doc ->
                var paragraphInvitation: XWPFParagraph? = null

                // membaca text yang ada di paragraf template
                for (p in doc.paragraphs) {
                    val runs = p.runs
                    if (runs != null) {
                        for (r in runs) {
                            var docText = r.getText(0)
//                            println("doc.paragraphs=> $docText")

                            if (docText != null) {
                                getstrReplaceWord(docText, data)?.let {
                                    docText = docText.replace(docText, it)
                                    r.setText(docText, 0)
                                }

                                if (docText.contains("pInvitation")) {
                                    paragraphInvitation = p
                                    docText = docText.replace(docText, "")
                                    r.setText(docText, 0)
                                }
                            }
                        }
                    }
                }

                if (paragraphInvitation != null) {
                    writeInvitation(data, paragraphInvitation, doc)
                }

                FileOutputStream(output).use { out ->
                    doc.write(out)
                }
            }
        } catch (e: Exception) {
            throw e
        }
    }

    private fun writeInvitation(data: Event, p: XWPFParagraph, doc: XWPFDocument) {
        try {
            val institution = mutableMapOf<String, List<EventOtherStaff>>()
            data.otherStaff?.forEach {
                if (!institution.containsKey(it.institution?.trim()?.lowercase())) {
                    val dataInstitution = data.otherStaff?.filter { fo ->
                        fo.institution?.lowercase()?.trim() == it.institution?.lowercase()?.trim()
                    }

                    if (dataInstitution?.isNotEmpty() == true) {
                        institution[it.institution ?: ""] = dataInstitution
                    }
                }
            }

            institution.forEach { (t, u) ->
                val content: XWPFParagraph = p.document.insertNewParagraph(p.ctp?.newCursor())
                val ca = content.createRun()
                ca.isBold = true
                ca.setText(t)

                u.forEachIndexed { index, eventOtherStaff ->
                    val cList: XWPFParagraph = p.document.insertNewParagraph(p.ctp?.newCursor())
                    val cl = cList.createRun()
                    cl.setText("${index + 1}. ${eventOtherStaff.name}")
                }
            }

            val component = mutableMapOf<String, List<EventStaff>>()
            data.staff?.forEach {
                val componentName = it.resources?.user?.component?.description?.trim()?.lowercase()
                if (!component.containsKey(componentName)) {
                    val dataStaff = data.staff?.filter { fo ->
                        val userComponent = fo.resources?.user?.component?.description?.trim()?.lowercase()
                        userComponent == componentName
                    }
                    if (dataStaff?.isNotEmpty() == true) {
                        component[componentName ?: ""] = dataStaff
                    }
                }
            }

            component.forEach { (t, u) ->
                val content: XWPFParagraph = p.document.insertNewParagraph(p.ctp?.newCursor())
                val ca = content.createRun()
                ca.isBold = true
                ca.setText(t)

                u.forEachIndexed { index, eventOtherStaff ->
                    val cList: XWPFParagraph = p.document.insertNewParagraph(p.ctp?.newCursor())
                    val cl = cList.createRun()

                    var name = ""

                    if (!eventOtherStaff.resources?.user?.firstName.isNullOrEmpty()) {
                        name += eventOtherStaff.resources?.user?.firstName
                    }

                    if (!eventOtherStaff.resources?.user?.lastName.isNullOrEmpty()) {
                        name += eventOtherStaff.resources?.user?.lastName
                    }

                    cl.setText("${index + 1}. $name")
                }
            }

            val pcu = data.staff?.filter { fs ->
                val fr = fs.resources?.user?.roles?.filter { frr ->
                    frr.roleId == ROLE_ID_PCU
                }
                fr?.isNotEmpty() == true
            }

            if (!pcu.isNullOrEmpty()) {
                val content: XWPFParagraph = p.document.insertNewParagraph(p.ctp?.newCursor())
                val ca = content.createRun()
                ca.isBold = true
                ca.setText("PCU")

                pcu.forEachIndexed { index, pcuData ->
                    val cList: XWPFParagraph = p.document.insertNewParagraph(p.ctp?.newCursor())
                    val cl = cList.createRun()

                    var name = ""

                    if (!pcuData.resources?.user?.firstName.isNullOrEmpty()) {
                        name += pcuData.resources?.user?.firstName
                    }

                    if (!pcuData.resources?.user?.lastName.isNullOrEmpty()) {
                        name += pcuData.resources?.user?.lastName
                    }

                    cl.setText("${index + 1}. $name")
                }
            }
        } catch (e: Exception) {
            throw e
        }
    }

    // replace data string pada template sesuai dengan string yang akan di replace
    private fun getstrReplaceWord(text: String, data: Event): String? {
        try {
            val mapReplace = mutableMapOf<String, String>()
            mapReplace["replaceDateDaysYears"] = dateFormatIndonesia(Date(), true)
            mapReplace["replaceEventName"] = data.name ?: ""

            var location = ""
            if (!data.province?.name.isNullOrEmpty()) {
                location += data.province?.name
            }
            if (!data.province?.name.isNullOrEmpty()) {
                location += " (${data.province?.name})"
            }

            mapReplace["replaceLocation"] = location
            mapReplace["replaceTime"] = "${dateFormatIndonesia(data.startDate)} - ${dateFormatIndonesia(data.endDate)}"
            mapReplace["replaceBackground"] = Jsoup.parse(data.background ?: "").text()
            mapReplace["replaceDescription"] = Jsoup.parse(data.description ?: "").text()
            mapReplace["replacePurpose"] = Jsoup.parse(data.purpose ?: "").text()
            mapReplace["replaceOutput"] = Jsoup.parse(data.eventOutput ?: "").text()
            mapReplace["replaceTimeDays"] =
                "${dateFormatIndonesia(data.startDate, true)} - ${dateFormatIndonesia(data.endDate, true)}"
            mapReplace["replaceTIme"] = "${dateFormatIndonesia(data.startDate)} - ${dateFormatIndonesia(data.endDate)}"

            mapReplace["replaceEventAgenda"] = data.agendaFile?.filename ?: ""

            val totalParticipant = (data.staff?.size ?: 0) + (data.otherStaff?.size ?: 0)
            var replaceTotalParticipant =
                "Peserta dalam kegiatan ini berjumlah $totalParticipant Orang, terdiri dari : "
            if (totalParticipant == 0) {
                replaceTotalParticipant = "Tidak ada peserta kegiatan"
            }
            mapReplace["replaceTotalParticipant"] = replaceTotalParticipant

            var replaceParticipant = ""

            if (data.staff?.isNotEmpty() == true) {
                val staff = data.staff?.filter {
                    val filterRole = it.resources?.user?.roles?.filter { fr ->
                        fr.roleId != ROLE_ID_PCU
                    }
                    filterRole?.isNotEmpty() == true
                }
                if (staff?.isNotEmpty() == true) {
                    replaceParticipant += "Peserta Staff PMU berjumlah ${staff.size} orang"
                }
            }

            if (data.staff?.isNotEmpty() == true) {
                val staff = data.staff?.filter {
                    val filterRole = it.resources?.user?.roles?.filter { fr ->
                        fr.roleId == ROLE_ID_PCU
                    }
                    filterRole?.isNotEmpty() == true
                }
                if (staff?.isNotEmpty() == true) {
                    if (replaceParticipant != "") {
                        replaceParticipant += ", "
                    }
                    replaceParticipant += "Peserta Staff PCU berjumlah ${staff.size} orang"
                }
            }

            if (data.otherStaff?.isNotEmpty() == true) {
                if (replaceParticipant != "") {
                    replaceParticipant += ", "
                }
                replaceParticipant += "Peserta Diluar Staff berjumlah ${data.otherStaff?.size} orang"
            }

            mapReplace["replaceParticipant"] = replaceParticipant


            var contactPerson = ""
            if (!data.contactPersonResources?.user?.firstName.isNullOrEmpty()) {
                contactPerson += data.contactPersonResources?.user?.firstName
            }
            if (!data.contactPersonResources?.user?.lastName.isNullOrEmpty()) {
                contactPerson += " ${data.contactPersonResources?.user?.lastName}"
            }
            mapReplace["replaceContacPerson"] = contactPerson

            return mapReplace[text]
        } catch (e: Exception) {
            throw e
        }
    }

    fun downloadPdf(id: String?) {
        try {
            val findData = repo.findByIdAndActive(id)
            if (!findData.isPresent) {
                response.sendError(HttpServletResponse.SC_NOT_FOUND)
            } else {
                if (findData.get().status != EVENT_STATUS_IMPLEMENTATION) {
                    response.sendError(HttpServletResponse.SC_NOT_FOUND)
                }

                val file: Path = Paths.get(generatePdf(findData.get())!!.absolutePath)
                if (Files.exists(file)) {
                    response.contentType = "application/pdf"
                    response.addHeader(
                        "Content-Disposition", "attachment; filename=" + file.fileName
                    )
                    Files.copy(file, response.outputStream)
                    response.outputStream.flush()
                }
            }
        } catch (ex: DocumentException) {
            ex.printStackTrace()
        } catch (ex: IOException) {
            ex.printStackTrace()
        }
    }

    fun generatePdf(data: Event?): File? {
        val ctx = Context()
        ctx.setVariable("id", data?.id)
        ctx.setVariable("backgorund", replaceUnusedSpaceCkEditor(data?.background ?: ""))
        ctx.setVariable("description", replaceUnusedSpaceCkEditor(data?.description ?: ""))

        val html: String = loadAndFillTemplate(ctx)
        return renderPdf(html)
    }

    private fun renderPdf(html: String): File? {
        val path = Paths.get(dirPdf)
        if (!Files.exists(path)) {
            Files.createDirectories(path)
        }

        val file = File.createTempFile("event-", ".pdf", path.toFile())

        val outputStream: OutputStream = FileOutputStream(file)
        val renderer = ITextRenderer()
        renderer.setDocumentFromString(
            html, ClassPathResource(pdfResources).url.toExternalForm()
        )
        renderer.layout()
        renderer.createPDF(outputStream)
        outputStream.close()
        file.deleteOnExit()
        return file
    }

    private fun loadAndFillTemplate(context: Context): String {
        return templateEngine.process("event-pdf", context)
    }

    fun uploadFormUser(req: UploadFormUserRequest): ResponseEntity<ReturnData> {
        try {
            val validate = validateUploadFormUser(req)
            val listMessage = validate["listMessage"] as List<ErrorMessage>
            if (listMessage.isNotEmpty()) {
                return responseBadRequest(data = listMessage)
            }

            var eventFormExcel = EventFormExcelFile()
            val findEventFormExcel =
                repoEventFormExcelFile.findByEventIdAndCreatedForAndActive(req.eventId, req.createdFor)
            if (findEventFormExcel.isPresent) {
                eventFormExcel = findEventFormExcel.get()
                eventFormExcel.emails?.filter { fe ->
                    fe.questionnaireStatus == EVENT_FORM_EXCEL_FILE_QUESTIONNAIRE_STATUS_NOT_DONE
                            && fe.emailSentStatus != EVENT_FORM_EXCEL_FILE_EMAIL_STATUS_SUCCESS
                }?.forEach { em ->
                    em.active = false
                    em.updatedAt = Date()
                    repoEventFormExcelFileEmail.save(em)
                }
            }

            eventFormExcel.eventId = req.eventId
            eventFormExcel.fileId = req.fileId
            eventFormExcel.createdFor = req.createdFor

            repoEventFormExcelFile.save(eventFormExcel)

            val file = validate["file"] as id.go.kemenag.madrasah.pmrms.awp.activity.pojo.Files

            val fileDir = "$dirUpload${File.separator}${file.filepath ?: ""}${File.separator}${file.filename ?: ""}"
            val excelFile = File(fileDir)

            val excel = ExcelUtility(excelFile.absolutePath, 0)

            val maxRow = excel.getMaxRow()
            val maxColumn = excel.getMaxColumn()
            if (maxRow != null && maxColumn != null) {
                if (maxRow > 0) {
                    for (r in 1..maxRow) {
                        var eventFormExcelFileEmail = EventFormExcelFileEmail()
                        eventFormExcelFileEmail.eventFormFileExcelId = eventFormExcel.id

                        var valid = true
                        for (c in 1..maxColumn) {
                            val index = r + 1
                            val name = excel.toName(c)
                            val cell = name + index
                            val obj = excel.getCellValue(cell)

                            if (c == 2) {
                                if (obj == null) {
                                    valid = false
                                } else {
                                    if (obj.toString() == "") {
                                        valid = false
                                    } else {
                                        if (obj.toString().trim()
                                                .lowercase() == "Contoh Nama Penerima Email".lowercase()
                                        ) {
                                            valid = false
                                        }
                                    }
                                }
                            }

                            if (valid) {
                                if (c == 2) {
                                    eventFormExcelFileEmail.name = obj.toString()
                                }
                                if (c == 3) {
                                    eventFormExcelFileEmail.email = obj.toString()
                                }
                            }
                        }

                        if (valid) {
                            val chcekEventFormExcelFileEmail =
                                repoEventFormExcelFileEmail.findByEventFormFileExcelIdAndEmailAndActive(
                                    eventFormExcel.id,
                                    eventFormExcelFileEmail.email
                                )

                            if (chcekEventFormExcelFileEmail.isPresent) {
                                eventFormExcelFileEmail = chcekEventFormExcelFileEmail.get()
                                eventFormExcelFileEmail.updatedAt = Date()
                            }

                            repoEventFormExcelFileEmail.save(eventFormExcelFileEmail)
                        }
                    }
                }
            }

            excel.close()

            Thread {
                sendUserEmail(eventFormExcel.id)
            }.start()

            return responseSuccess()
        } catch (e: Exception) {
            throw e
        }
    }

    private fun sendUserEmail(id: String?) {
        try {
            val data = repoEventFormExcelFile.findByIdAndActive(id)
            if (data.isPresent) {
                data.get().emails?.filter { fe ->
                    fe.active == true
                            && fe.questionnaireStatus == EVENT_FORM_EXCEL_FILE_QUESTIONNAIRE_STATUS_NOT_DONE && fe.emailSentStatus != EVENT_FORM_EXCEL_FILE_EMAIL_STATUS_SUCCESS
                }?.forEach {
                    it.emailSentStatus = EVENT_FORM_EXCEL_FILE_EMAIL_STATUS_FAILED
                    try {
                        if (EmailValidator.getInstance().isValid(it.email)) {
                            val dataIdentity =
                                "${data.get().id}${it.email}"

                            val identity = encryptWithoutStr(dataIdentity, listOf("/", "."))

                            val identityId =
                                encryptWithoutStr(it.eventFormFileExcel?.createdFor.toString(), listOf("/", "."))

                            val link = "$questionnaireUrl$identity/$identityId"
                            val linkClick =
                                "<a href='$questionnaireUrl$identity/$identityId' target='_blank'>klik link</a>"

                            var message = "<p>Kepada, ${it.name} di tempat.</p>"
                            message += "<p>Silahkan $linkClick di bawah ini untuk mengisi formulir tanggapan. Link ini bersifat rahasia, mohon untuk tidak di sebar luaskan.</p><br/>"
                            message += link

                            val rdata = RequestHelpers.notifSendEmail(
                                notifUrl,
                                EmailRequest(
                                    it.email,
                                    "Formulir Tanggapan - ${data.get().event?.name}",
                                    message
                                ),
                                ""
                            )
                            if (rdata?.success == true) {
                                it.emailSentStatus = EVENT_FORM_EXCEL_FILE_EMAIL_STATUS_SUCCESS
                            }
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    it.updatedAt = Date()
                    repoEventFormExcelFileEmail.save(it)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun validateUploadFormUser(request: UploadFormUserRequest, update: Event? = null): Map<String, Any?> {
        val rData = mutableMapOf<String, Any?>()
        try {
            val listMessage: MutableList<ErrorMessage> = emptyList<ErrorMessage>().toMutableList()

            val event = repo.findByIdAndActive(request.eventId)
            if (!event.isPresent) {
                listMessage.add(
                    ErrorMessage(
                        "eventId", "Event Id ${request.eventId} $VALIDATOR_MSG_NOT_FOUND"
                    )
                )
            }

            val checkFile: ReturnDataFiles? = RequestHelpers.filesGetById(
                request.fileId ?: "", filesUrl, httpServletRequest.getHeader(HEADER_STRING)
            )

            var checkedFile: id.go.kemenag.madrasah.pmrms.awp.activity.pojo.Files? = null
            if (checkFile != null) {
                if (checkFile.data != null) {
                    checkedFile = checkFile.data
                }
            }

            if (checkedFile == null) {
                listMessage.add(
                    ErrorMessage(
                        "fileId", "File Id ${request.fileId} $VALIDATOR_MSG_NOT_FOUND"
                    )
                )
            } else {
                rData["file"] = checkFile?.data
            }

            rData["listMessage"] = listMessage
            return rData
        } catch (e: Exception) {
            throw e
        }
    }

}
