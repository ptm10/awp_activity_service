package id.go.kemenag.madrasah.pmrms.awp.activity.service.documents

import com.lowagie.text.DocumentException
import id.go.kemenag.madrasah.pmrms.awp.activity.constant.*
import id.go.kemenag.madrasah.pmrms.awp.activity.helpers.*
import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.EventReport
import id.go.kemenag.madrasah.pmrms.awp.activity.repository.EventReportRepository
import id.go.kemenag.madrasah.pmrms.awp.activity.repository.UserRepository
import id.go.kemenag.madrasah.pmrms.awp.activity.repository.UserRoleViewUserResourcesRepository
import org.apache.poi.util.Units
import org.apache.poi.xwpf.usermodel.IRunBody
import org.apache.poi.xwpf.usermodel.XWPFDocument
import org.apache.poi.xwpf.usermodel.XWPFParagraph
import org.apache.poi.xwpf.usermodel.XWPFRun
import org.apache.xmlbeans.XmlCursor
import org.apache.xmlbeans.XmlObject
import org.jsoup.Jsoup
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTR
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.io.ClassPathResource
import org.springframework.stereotype.Service
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*
import javax.imageio.ImageIO
import javax.servlet.http.HttpServletResponse


@Suppress("UNREACHABLE_CODE")
@Service
class EventReportDocumentService {

    @Autowired
    private lateinit var repo: EventReportRepository

    @Autowired
    private lateinit var response: HttpServletResponse

    @Autowired
    private lateinit var repoUserRoleViewUserResources: UserRoleViewUserResourcesRepository

    @Autowired
    private lateinit var repoUser: UserRepository

    @Value("\${directories.upload}")
    private lateinit var dirUpload: String

    private val wordTemplate = "files/Event-report-template.docx"

    @Value("\${directories.word}")
    private lateinit var dirWord: String

    @Value("\${download.file.url}")
    private lateinit var downloadFileUrl: String

    fun downloadWord(id: String?) {
        try {
            val data = repo.findByIdAndActive(id)
            if (!data.isPresent) {
                responseNotFound(response)
            }

            val path = Paths.get(dirWord)
            if (!Files.exists(path)) {
                Files.createDirectories(path)
            }

            val file = File.createTempFile("event-report-", ".docx", path.toFile())

            writeWord(file.path, data.get())

            if (Files.exists(file.toPath())) {
                response.contentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                response.addHeader(
                    "Content-Disposition", "attachment; filename=" + file.name
                )
                Files.copy(file.toPath(), response.outputStream)
                response.outputStream.flush()
            }
        } catch (ex: DocumentException) {
            ex.printStackTrace()
        } catch (ex: IOException) {
            ex.printStackTrace()
        }
    }

    @Throws(IOException::class)
    private fun writeWord(output: String, data: EventReport) {
        try {
            XWPFDocument(
                ClassPathResource(wordTemplate).inputStream
            ).use { doc ->
                var paragraphAgenda: XWPFParagraph? = null
                var paragraphSchedule: XWPFParagraph? = null
                var paragraphProfileNarasumber: XWPFParagraph? = null
                var paragraphNotulensi: XWPFParagraph? = null
                var paragraphFoto: XWPFParagraph? = null
                var paragraphBahanNarasumber: XWPFParagraph? = null

                // membaca text yang ada di paragraf template
                for (p in doc.paragraphs) {
                    val cursor: XmlCursor = p.ctp.newCursor()
                    cursor.selectPath("declare namespace w='http://schemas.openxmlformats.org/wordprocessingml/2006/main' .//*/w:txbxContent/w:p/w:r")

                    val ctrsintxtbx: MutableList<XmlObject> = ArrayList()
                    while (cursor.hasNextSelection()) {
                        cursor.toNextSelection()
                        val obj = cursor.getObject()
                        if (!ctrsintxtbx.contains(obj)) {
                            ctrsintxtbx.add(obj)
                        }
                    }

                    for (obj in ctrsintxtbx) {
                        val ctr: CTR = CTR.Factory.parse(obj.xmlText())
                        val bufferrun = XWPFRun(ctr, p as IRunBody?)
                        var docText = bufferrun.getText(0)

//                        println("doc.textBox=> $docText")

                        if (docText != null) {
                            getstrReplaceWord(docText, data)?.let {
                                docText = docText.replace(docText, it)
                                bufferrun.setText(docText, 0)
                            }
                        }

                        obj.set(bufferrun.ctr)
                    }


                    val runs = p.runs
                    if (runs != null) {
                        for (r in runs) {
                            var docText = r.getText(0)
//                            println("doc.paragraphs=> $docText")

                            if (docText != null) {
                                if (docText.contains("rf1")) {
                                    paragraphAgenda = p
                                }

                                if (docText.contains("rf2")) {
                                    paragraphSchedule = p
                                }

                                if (docText.contains("rf3")) {
                                    paragraphProfileNarasumber = p
                                }

                                if (docText.contains("rf4")) {
                                    paragraphNotulensi = p
                                }

                                if (docText.contains("rf5")) {
                                    paragraphFoto = p
                                }

                                if (docText.contains("rf6")) {
                                    paragraphBahanNarasumber = p
                                }

                                getstrReplaceWord(docText, data)?.let {
                                    docText = docText.replace(docText, it)
                                    r.setText(docText, 0)
                                }
                            }
                        }
                    }
                }

                // membaca text yang ada di tabel template
                for (tbl in doc.tables) {
                    for (row in tbl.rows) {
                        for (cell in row.tableCells) {
                            for (p in cell.paragraphs) {
                                for (r in p.runs) {
                                    var textTable = r.text()

//                                    println("doc.tableCells=> $textTable")

                                    if (textTable != null) {
                                        getstrReplaceWord(textTable, data)?.let {
                                            textTable = textTable.replace(textTable, it)
                                            r.setText(textTable, 0)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                paragraphAgenda?.let {
                    writeDocuments(EVENT_DOCUMENT_TYPE_ID_AGENDA, it, data)
                }

                paragraphSchedule?.let {
                    writeDocuments(EVENT_DOCUMENT_TYPE_JADWAL_ACARA, it, data)
                }

                paragraphProfileNarasumber?.let {
                    writeDocuments(EVENT_DOCUMENT_PROFILE_NARASUMBER, it, data)
                }

                paragraphNotulensi?.let {
                    writeDocuments(EVENT_DOCUMENT_TYPE_NOTULENSI, it, data)
                }

                paragraphFoto?.let {
                    writeDocuments(EVENT_DOCUMENT_TYPE_FOTO, it, data)
                }

                paragraphBahanNarasumber?.let {
                    writeDocuments(EVENT_DOCUMENT_BAHAN_NARASUMBER, it, data)
                }

                FileOutputStream(output).use { out ->
                    doc.write(out)
                }
            }
        } catch (e: Exception) {
            throw e
        }
    }

    private fun writeDocuments(doctypeId: String?, p: XWPFParagraph, data: EventReport) {
        try {
            p.removeRun(0)

            data.documents?.filter { df -> df.eventDocumentTypeId.equals(doctypeId) }?.forEach {
                val contentTitle: XWPFParagraph = p.document.insertNewParagraph(p.ctp?.newCursor())
                val ct = contentTitle.createRun()
                ct.isBold = true
                ct.setText(it.title)

                val contentP: XWPFParagraph = p.document.insertNewParagraph(p.ctp?.newCursor())
                val content = contentP.createRun()

                content.setText(it.description)
                content.addBreak()
                content.setText(it.place)

                val fileDir =
                    "$dirUpload${File.separator}${it.file?.filepath ?: ""}${File.separator}${it.file?.filename ?: ""}"
                val fileFocus = File(fileDir)
                if (fileFocus.exists()) {

                    content.addBreak()

                    if (it.file?.fileExt == "png" || it.file?.fileExt == "jpg" || it.file?.fileExt == "jpeg") {

                        val fis = FileInputStream(fileFocus)
                        var pictureType = XWPFDocument.PICTURE_TYPE_JPEG
                        if (fileFocus.extension == "png") {
                            pictureType = XWPFDocument.PICTURE_TYPE_PNG
                        }

                        var height = 0
                        var width = 0
                        try {
                            val readImage = ImageIO.read(fileFocus)
                            height = readImage.width
                            width = readImage.height
                        } catch (_: Exception) {
                        }

                        content.addPicture(
                            fis,
                            pictureType,
                            it.file?.filename,
                            Units.toEMU(height.toDouble()),
                            Units.toEMU(width.toDouble())
                        )
                        fis.close()
                    } else {
                        val url = "$downloadFileUrl${it.fileId}"
                        content.setText(url)
                    }
                } else {
                    println("file not found : $fileDir")
                }

                excelPoiAddBreak(content, 2)
            }
        } catch (e: Exception) {
            throw e
        }
    }

    // replace data string pada template sesuai dengan string yang akan di replace
    private fun getstrReplaceWord(text: String, data: EventReport): String? {
        try {
            val mapReplace = mutableMapOf<String, String>()

            mapReplace["rname"] = data.event?.name ?: ""
            mapReplace["rName"] = data.event?.name ?: ""
            mapReplace["replacetema"] = data.event?.nameOtherInformation ?: "-"
            mapReplace["rlocation"] = Jsoup.parse(data.location ?: "").text()
            mapReplace["rdate"] = "${dateFormatIndonesia(data.startDate)} - ${dateFormatIndonesia(data.endDate)}"
            mapReplace["rcreatedat"] = dateFormatIndonesia(data.createdAt)
            mapReplace["rdatesignature"] = "Jakarta, ${dateFormatIndonesia(Date())}"
            mapReplace["rapproveddate"] = dateFormatIndonesia(data.updatedAt)
            mapReplace["rreference"] = data.event?.name ?: ""
            mapReplace["rapprovedby"] = "approved by"

            var coordinator = data.event?.awpImplementation?.projectOfficerResources?.user?.firstName ?: ""
            val coorDinatorLastName = data.event?.awpImplementation?.projectOfficerResources?.user?.lastName ?: ""

            if (coorDinatorLastName != "") {
                coordinator += " $coorDinatorLastName"
            }

            mapReplace["rcoordinator"] = coordinator
            mapReplace["rcoordinatorsignature"] = "<$coordinator>"


            var treasurer = ""
            val usersPmuTreasurer = repoUserRoleViewUserResources.getUserIdsByRoleIdsComponentIdPosition(
                listOf(ROLE_ID_COORDINATOR, ROLE_ID_ADMINSTRATOR), 2, COMPONENT_ID_PMU, POSITION_TREASURER_PMU
            )

            if (usersPmuTreasurer.isNotEmpty()) {
                val user = repoUser.findByIdAndActive(usersPmuTreasurer[0])
                if (user.isPresent) {
                    if (!user.get().firstName.isNullOrEmpty()) {
                        treasurer += user.get().firstName
                    }
                    if (!user.get().lastName.isNullOrEmpty()) {
                        treasurer += " ${user.get().lastName}"
                    }
                }
            }

            mapReplace["rtreasurer"] = treasurer

            var pmu = ""
            val usersPmuHead = repoUserRoleViewUserResources.getPmuHead(
                listOf(ROLE_ID_COORDINATOR, ROLE_ID_ADMINSTRATOR),
                2,
                COMPONENT_ID_PMU,
            )

            if (usersPmuHead.isNotEmpty()) {
                val user = repoUser.findByIdAndActive(usersPmuHead[0])
                if (user.isPresent) {
                    pmu += user.get().firstName
                    if (!user.get().lastName.isNullOrEmpty()) {
                        pmu += " ${user.get().lastName}"
                    }
                }
            }

            mapReplace["rpmu"] = pmu

            var diffFdays = 0.toLong()
            if (data.startDate != null && data.endDate != null) {
                diffFdays = daysBetweenDates(data.endDate!!, data.startDate!!) + 1
            }


            val eventInfo =
                "selama $diffFdays hari dengan skema ${data.event?.awpImplementation?.eventManagementType?.name} pada tanggal ${
                    startEndDateIndonesia(
                        data.startDate,
                        data.endDate
                    )
                } di ${data.location ?: "-"} ${data.locationOtherInformation ?: "-"}"


            mapReplace["reventInfo"] = eventInfo

            mapReplace["rcomponentname"] = data.event?.awpImplementation?.component?.description ?: ""
            mapReplace["rsubcomponentname"] = data.event?.awpImplementation?.subComponent?.description ?: ""
            mapReplace["rrcn"] = data.event?.awpImplementation?.subSubComponent?.description ?: ""
            mapReplace["reventtype"] = data.event?.eventType?.name ?: ""
            mapReplace["rplceventtype"] = data.event?.eventType?.name ?: ""
            mapReplace["rtime"] = "${dateFormatIndonesia(data.startDate)} - ${dateFormatIndonesia(data.endDate)}"
            mapReplace["rprovincename"] = data.province?.name ?: ""
            mapReplace["rregencyname"] = data.regency?.name ?: ""
            mapReplace["rloin"] = data.location ?: ""
            mapReplace["rlocation"] = data.locationOtherInformation ?: ""
            mapReplace["rpurpose"] = Jsoup.parse(data.event?.purpose ?: "").text()
            mapReplace["routput"] = Jsoup.parse(data.eventOutput ?: "").text()
            mapReplace["rmanagementype"] = data.event?.eventType?.name ?: ""
            mapReplace["rcomponentcode"] = data.event?.awpImplementation?.component?.code ?: ""
            mapReplace["rdescription"] = Jsoup.parse(data.event?.description ?: "").text()
            mapReplace["ryear"] = data.event?.awpImplementation?.awp?.year?.toString() ?: ""
            mapReplace["ryeardesc"] = "Sumber anggaran event ini dibebankan pada DIPA Ditjen Pendidikan Islam Tahun Anggaran ${data.event?.awpImplementation?.awp?.year?.toString() ?: ""} yang bersumber dari IBRD 8992-ID dengan MAK Nomor ${data.makNumber ?: ""}"
            mapReplace["rpaguanggaran"] = formatRupiah(data.budgetCeiling?.toDouble() ?: 0.toDouble())
            mapReplace["rrealitation"] = formatRupiah(data.rraBudget?.toDouble() ?: 0.toDouble())
            mapReplace["rparticipantmale"] = data.participantMale?.toString() ?: "0"
            mapReplace["rparticipantfemale"] = data.participantFemale?.toString() ?: "0"
            mapReplace["rtotalparticipant"] = ((data.participantMale ?: 0) + (data.participantFemale ?: 0)).toString()
            mapReplace["rsummary"] = Jsoup.parse(data.implementationSummary ?: "").text()
            mapReplace["rconclutionandrecomendation"] = Jsoup.parse(data.conclusionsAndRecommendations ?: "").text()
            mapReplace["rparticipant"] = "Peserta"
            mapReplace["reventmanagementtype"] = "Pelaksana Teknis dalam pelaksanaan event ini adalah ${data.event?.awpImplementation?.eventManagementType?.name ?: ""}"


            var pic = ""
            if (!data.event?.awpImplementation?.projectOfficerResources?.user?.firstName.isNullOrEmpty()) {
                pic += data.event?.awpImplementation?.projectOfficerResources?.user?.firstName
            }

            if (!data.event?.awpImplementation?.projectOfficerResources?.user?.lastName.isNullOrEmpty()) {
                pic += " ${data.event?.awpImplementation?.projectOfficerResources?.user?.lastName}"
            }

            if (data.event?.awpImplementation?.eventManagementTypeId == EVENT_MANAGEMENT_TYPE_CONTRACT) {
                if (!data.event?.awpImplementation?.lspPicUser?.firstName.isNullOrEmpty()) {
                    pic += data.event?.awpImplementation?.lspPicUser?.firstName
                }
                if (!data.event?.awpImplementation?.lspPicUser?.lastName.isNullOrEmpty()) {
                    pic += " ${data.event?.awpImplementation?.lspPicUser?.lastName}"
                }
            }

            mapReplace["rpic"] = pic
            mapReplace["rpicnote"] = "Note: PIC Event : $pic"

            var rcreatedby = ""
            if (!data.createdByUser?.firstName.isNullOrEmpty()) {
                rcreatedby += data.createdByUser?.firstName
            }
            if (!data.createdByUser?.lastName.isNullOrEmpty()) {
                rcreatedby += " ${data.createdByUser?.lastName}"
            }

            mapReplace["rcreatedby"] = rcreatedby

            return mapReplace[text]
        } catch (e: Exception) {
            throw e
        }
    }

}
