package id.go.kemenag.madrasah.pmrms.awp.activity.service.documents

import com.lowagie.text.DocumentException
import id.go.kemenag.madrasah.pmrms.awp.activity.constant.*
import id.go.kemenag.madrasah.pmrms.awp.activity.helpers.*
import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.ProjectReportDocumentRequest
import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.AwpData
import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.AwpImplementation
import id.go.kemenag.madrasah.pmrms.awp.activity.pojo.EventReport
import id.go.kemenag.madrasah.pmrms.awp.activity.repository.*
import id.go.kemenag.madrasah.pmrms.awp.activity.service.ComponentService
import org.apache.poi.util.Units
import org.apache.poi.xwpf.usermodel.*
import org.apache.xmlbeans.XmlCursor
import org.apache.xmlbeans.XmlObject
import org.jsoup.Jsoup
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTR
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.io.ClassPathResource
import org.springframework.stereotype.Service
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException
import java.nio.file.Files
import java.nio.file.Paths
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.time.Year
import java.util.*
import javax.imageio.ImageIO
import javax.servlet.http.HttpServletResponse


@Suppress("UNCHECKED_CAST", "NAME_SHADOWING", "UNREACHABLE_CODE")
@Service
class ProjectReportDocumentService {

    @Autowired
    private lateinit var response: HttpServletResponse

    @Autowired
    private lateinit var repoEventReport: EventReportRepository

    @Autowired
    private lateinit var repoEventQuestionAnswer: EventQuestionAnswerRepository

    @Autowired
    private lateinit var repoAwpData: AwpDataRepository

    @Autowired
    private lateinit var eventFormRepository: EventFormRepository

    @Autowired
    private lateinit var repoUserRoleViewUserResources: UserRoleViewUserResourcesRepository

    @Autowired
    private lateinit var repoUser: UserRepository

    @Autowired
    private lateinit var repoEvent: EventRepository

    @Autowired
    private lateinit var serviceComponent: ComponentService

    @Value("\${directories.upload}")
    private lateinit var dirUpload: String

    private val wordTemplate = "files/Project-report-template.docx"

    @Value("\${directories.word}")
    private lateinit var dirWord: String

    @Value("\${download.file.url}")
    private lateinit var downloadFileUrl: String

    fun downloadWord(request: ProjectReportDocumentRequest) {
        try {
            val dform: DateFormat = SimpleDateFormat("yyyy-MM-dd")

            val reports = repoEventReport.getByDateBetwwenStatus(
                dform.parse(request.startDate),
                dform.parse(request.endDate),
                EVENT_REPORT_STATUS_APPROVED
            )

            val uniqueAwpImplementation = mutableListOf<String?>()
            val awpImplementations = mutableListOf<AwpImplementation>()

            reports.forEach {
                if (!uniqueAwpImplementation.contains(it.event?.awpImplementationId)) {
                    it.event?.awpImplementation?.let { it1 ->
                        awpImplementations.add(it1)
                        uniqueAwpImplementation.add(it.event?.awpImplementationId)
                    }
                }
            }

            val path = Paths.get(dirWord)
            if (!Files.exists(path)) {
                Files.createDirectories(path)
            }

            val file = File.createTempFile("project-report-", ".docx", path.toFile())

            var awpData: AwpData? = null
            val findAwpData = repoAwpData.findByYearAndActive(Year.now().value - 1)
            if (findAwpData.isPresent) {
                awpData = findAwpData.get()
            }

            writeWord(
                file.path,
                reports,
                awpImplementations,
                dform.parse(request.startDate),
                dform.parse(request.endDate),
                awpData,
                request.componentIds
            )

            if (Files.exists(file.toPath())) {
                response.contentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                response.addHeader(
                    "Content-Disposition", "attachment; filename=" + file.name
                )
                Files.copy(file.toPath(), response.outputStream)
                response.outputStream.flush()
            }
        } catch (ex: DocumentException) {
            ex.printStackTrace()
        } catch (ex: IOException) {
            ex.printStackTrace()
        }
    }

    private fun writeWord(
        output: String,
        reports: List<EventReport>,
        awpImplementations: List<AwpImplementation>,
        startDate: Date,
        endDate: Date,
        awpData: AwpData?,
        componentIds: List<String>?
    ) {
        try {
            XWPFDocument(
                ClassPathResource(wordTemplate).inputStream
            ).use { doc ->

                var paragraphComponent: XWPFParagraph? = null

                // membaca text yang ada di paragraf template
                for (p in doc.paragraphs) {
                    val cursor: XmlCursor = p.ctp.newCursor()
                    cursor.selectPath("declare namespace w='http://schemas.openxmlformats.org/wordprocessingml/2006/main' .//*/w:txbxContent/w:p/w:r")

                    val ctrsintxtbx: MutableList<XmlObject> = ArrayList()
                    while (cursor.hasNextSelection()) {
                        cursor.toNextSelection()
                        val obj = cursor.getObject()
                        if (!ctrsintxtbx.contains(obj)) {
                            ctrsintxtbx.add(obj)
                        }
                    }

                    for (obj in ctrsintxtbx) {
                        val ctr: CTR = CTR.Factory.parse(obj.xmlText())
                        val bufferrun = XWPFRun(ctr, p as IRunBody?)
                        var docText = bufferrun.getText(0)

//                        println("doc.textBox=> $docText")

                        if (docText != null) {
                            getstrReplaceWord(docText, reports, startDate, endDate)?.let {
                                docText = docText.replace(docText, it)
                                bufferrun.setText(docText, 0)
                            }
                        }

                        obj.set(bufferrun.ctr)
                    }

                    val runs = p.runs
                    if (runs != null) {
                        for (r in runs) {
                            var docText = r.getText(0)

//                            println("doc.paragraphs=> $docText")

                            if (docText != null) {
                                if (docText.contains("rcomponent")) {
                                    paragraphComponent = p
                                }

                                getstrReplaceWord(docText, reports, startDate, endDate)?.let {
                                    docText = docText.replace(docText, it)
                                    r.setText(docText, 0)
                                }
                            }
                        }
                    }

                    // membaca text yang ada di tabel template
                    for (tbl in doc.tables) {
                        for (row in tbl.rows) {
                            for (cell in row.tableCells) {
                                for (p in cell.paragraphs) {
                                    for (r in p.runs) {
                                        var textTable = r.text()

//                                        println("doc.tableCells=> $textTable")

                                        if (textTable != null) {
                                            getstrReplaceWord(textTable, reports, startDate, endDate)?.let {
                                                textTable = textTable.replace(textTable, it)
                                                r.setText(textTable, 0)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                paragraphComponent?.let {
                    writeComponent(doc, it, reports, awpImplementations, awpData, componentIds)
                }

                FileOutputStream(output).use { out ->
                    doc.write(out)
                }
            }
        } catch (e: Exception) {
            throw e
        }
    }

    private fun writeComponent(
        doc: XWPFDocument,
        p: XWPFParagraph,
        reports: List<EventReport>,
        awpImplementations: List<AwpImplementation>,
        awpData: AwpData?,
        componentIds: List<String>?
    ) {
        try {
            var components =
                serviceComponent.groupList(Year.now().value).body?.data as List<ComponentService.ComponentSubComponent>

            components = if (!componentIds.isNullOrEmpty()) {
                components.filter { cf ->
                    componentIds.contains(cf.id)
                }
            } else {
                emptyList()
            }

            components.forEachIndexed { _, componentSubComponent ->
                val paragraphHeader: XWPFParagraph = p.document.insertNewParagraph(p.ctp?.newCursor())
                paragraphHeader.style = "Heading1"
                paragraphHeader.isPageBreak = true

                val r = paragraphHeader.createRun()
                r.setText("KOMPONEN ${componentSubComponent.code}")
                r.addBreak()
                r.setText(componentSubComponent.description?.uppercase())
                r.addBreak()

                val content1: XWPFParagraph = p.document.insertNewParagraph(p.ctp?.newCursor())
                val c1 = content1.createRun()

                c1.setText("Komponen ${componentSubComponent.code} terdiri dari ${componentSubComponent.subComponent.size} sub-komponen yaitu:")

                componentSubComponent.subComponent.forEachIndexed { sci, sc ->
                    val content2: XWPFParagraph = p.document.insertNewParagraph(p.ctp?.newCursor())
                    content2.style = "ListParagraph"
                    content2.numID = excelPoiBulletNumId(doc)
                    val c2 = content2.createRun()
                    c2.setText("Sub-Komponen ${sc.code} ${sc.description}")
                    if (sci == (componentSubComponent.subComponent.size - 1)) {
                        excelPoiAddBreak(c2, 2)
                    }
                }

                val contentRingkasan: XWPFParagraph = p.document.insertNewParagraph(p.ctp?.newCursor())
                val cr = contentRingkasan.createRun()
                cr.isBold = true
                cr.setText("Fokus dan Kegiatan TA ${Year.now().value - 1}")

                var fokus = ""
                if (awpData != null) {
                    when (componentSubComponent.code) {
                        "1" -> {
                            fokus = awpData.fokusKomponen1 ?: ""
                        }
                        "2" -> {
                            fokus = awpData.fokusKomponen2 ?: ""
                        }
                        "3" -> {
                            fokus = awpData.fokusKomponen3 ?: ""
                        }
                        "4" -> {
                            fokus = awpData.fokusKomponen4 ?: ""
                        }
                    }
                }

                if (fokus != "") {
                    val paragraphFokus: XWPFParagraph = p.document.insertNewParagraph(p.ctp?.newCursor())
                    val fokusContent = paragraphFokus.createRun()
                    fokusContent.setText(Jsoup.parse(fokus).text())

                    val files = awpData?.files?.filter { ff ->
                        ff.component == componentSubComponent.code
                    }
                    if (!files.isNullOrEmpty()) {
                        files.forEach { fad ->
                            val fileDir =
                                "$dirUpload${File.separator}${fad.file?.filepath ?: ""}${File.separator}${fad.file?.filename ?: ""}"
                            val fileFocus = File(fileDir)
                            if (fileFocus.exists()) {
                                excelPoiAddBreak(fokusContent, 2)
                                val fis = FileInputStream(fileFocus)

                                var pictureType = XWPFDocument.PICTURE_TYPE_JPEG
                                if (fileFocus.extension == "png") {
                                    pictureType = XWPFDocument.PICTURE_TYPE_PNG
                                }

                                var height = 0
                                var width = 0
                                try {
                                    val readImage = ImageIO.read(fileFocus)
                                    height = readImage.width
                                    width = readImage.height
                                } catch (_: Exception) {
                                }

                                fokusContent.addPicture(
                                    fis,
                                    pictureType,
                                    fileFocus.name,
                                    Units.toEMU(height.toDouble()),
                                    Units.toEMU(width.toDouble())
                                )
                                fis.close()
                            } else {
                                println("file not found : $fileDir")
                            }
                        }
                    }

                    excelPoiAddBreak(fokusContent, 2)
                }

                componentSubComponent.subComponent.forEachIndexed { sci2, sc2 ->
                    val paragraphHeaderSsc: XWPFParagraph = p.document.insertNewParagraph(p.ctp?.newCursor())
                    paragraphHeaderSsc.style = "Heading2"

                    val rsc = paragraphHeaderSsc.createRun()
                    rsc.setText("${sc2.code} - ${sc2.description?.uppercase()}")
                    rsc.addBreak()

                    sc2.subComponent.forEachIndexed { ssci2, ssc2 ->
                        val paragraphHeaderSsc: XWPFParagraph = p.document.insertNewParagraph(p.ctp?.newCursor())
                        paragraphHeaderSsc.style = "Heading3"
                        val rssc = paragraphHeaderSsc.createRun()
                        rssc.setText("${ssc2.code} - ${ssc2.description?.uppercase()}")
                        rssc.addBreak()


                        ssc2.subComponent.forEachIndexed { kci, kc ->
                            val paragraphHeaderk: XWPFParagraph = p.document.insertNewParagraph(p.ctp?.newCursor())
                            paragraphHeaderk.style = "Heading4"
                            val k = paragraphHeaderk.createRun()
                            k.setText("${kc.code} ${kc.description}")
                            k.addBreak()

                            writeAwpImplementationFromComponent(doc, reports, awpImplementations, kc, p)
                        }
                    }
                }
            }
        } catch (e: Exception) {
            throw e
        }
    }

    private fun writeAwpImplementationFromComponent(
        doc: XWPFDocument,
        reports: List<EventReport>,
        awpImplementations: List<AwpImplementation>,
        kc: ComponentService.ComponentSubComponent,
        p: XWPFParagraph
    ) {
        try {
            awpImplementations.filter { aif -> aif.componentCodeId == kc.id }.forEach { awpi ->
                val contentAwp: XWPFParagraph = p.document.insertNewParagraph(p.ctp?.newCursor())
                val ca = contentAwp.createRun()
                ca.setText(awpi.name)

                val table: XWPFTable = doc.insertNewTbl(p.ctp?.newCursor())

                val tableRow1 = table.getRow(0)
                tableRow1.getCell(0).text = "1. Nama Proyek"
                tableRow1.addNewTableCell().text = "REP-MEQR"

                val tableRow2 = table.createRow()
                tableRow2.getCell(0).text = "2. Komponen"
                tableRow2.getCell(1).text = "${awpi.component?.code} ${awpi.component?.description}"

                val tableRow3 = table.createRow()
                tableRow3.getCell(0).text = "3. Sub-Komponen"
                tableRow3.getCell(1).text = "${awpi.subComponent?.code} ${awpi.subComponent?.description}"

                val tableRow4 = table.createRow()
                tableRow4.getCell(0).text = "4. Sub-Sub-Komponen"
                tableRow4.getCell(1).text = "${awpi.subSubComponent?.code} ${awpi.subSubComponent?.description}"

                val tableRow5 = table.createRow()
                tableRow5.getCell(0).text = "5. Nama Kegiatan"
                tableRow5.getCell(1).text = "${awpi.componentCode?.code} ${awpi.componentCode?.description}"

                val tableRow6 = table.createRow()
                tableRow6.getCell(0).text = "6. Waktu Pelaksanaan"
                tableRow6.getCell(1).text =
                    "${dateFormatIndonesia(awpi.startDate)} ${dateFormatIndonesia(awpi.endDate)}"


                var place = ""
                awpi.provincesRegencies?.forEach { pr ->
                    if (place != "") {
                        place += ", "
                    }

                    var provinceName = pr.province?.name
                    if (provinceName == "Lainnya") {
                        provinceName = "tempat yang telah ditentukan"
                    }

                    place += provinceName

                    pr.regencies?.forEachIndexed { iprr, prr ->
                        val size = (pr.regencies?.size ?: 0) - 1

                        if (iprr == 0) {
                            place += "("
                        }

                        if (iprr > 0 && iprr < size + 1) {
                            place += ", "
                        }

                        var regencyName = prr.regency?.name
                        if (regencyName == "Lainnya") {
                            regencyName = "tempat yang telah ditentukan"
                        }

                        place += regencyName
                        if (iprr == size) {
                            place += ")"
                        }
                    }
                }

                val tableRow7 = table.createRow()
                tableRow7.getCell(0).text = "7. Tempat Pelaksanaan"
                tableRow7.getCell(1).text = place

                val tableRow8 = table.createRow()
                tableRow8.getCell(0).text = "8. Jenis Kegiatan"
                tableRow8.getCell(1).text = awpi.activityMode?.name

                val tableRow9 = table.createRow()
                tableRow9.getCell(0).text = "9. Volume Event"
                tableRow9.getCell(1).text = awpi.eventVolume.toString()

                val tableRow10 = table.createRow()
                tableRow10.getCell(0).text = "10. Cara Pengelolaan"
                tableRow10.getCell(1).text = awpi.eventManagementType?.name

                val tablePurpose: XWPFTable =
                    p.document.insertNewParagraph(p.ctp?.newCursor()).document.insertNewTbl(p.ctp?.newCursor())
                tablePurpose.width = 8500
                val tablePurpose1 = tablePurpose.getRow(0)
                tablePurpose1.getCell(0).text = "11. Tujuan Kegiatan"
                val tablePurpose2 = tablePurpose.createRow()
                tablePurpose2.getCell(0).text = Jsoup.parse(awpi.purpose ?: "-").text()

                val tableOutput: XWPFTable =
                    p.document.insertNewParagraph(p.ctp?.newCursor()).document.insertNewTbl(p.ctp?.newCursor())
                tableOutput.width = 8500
                val tableOutput1 = tableOutput.getRow(0)
                tableOutput1.getCell(0).text = "12. Output Kegiatan"
                val tableOutput2 = tableOutput.createRow()
                tableOutput2.getCell(0).text = Jsoup.parse(awpi.eventOutput ?: "-").text()

                val tableRealitation: XWPFTable =
                    p.document.insertNewParagraph(p.ctp?.newCursor()).document.insertNewTbl(p.ctp?.newCursor())
                tableRealitation.width = 8500
                val tableRealitation1 = tableRealitation.getRow(0)
                tableRealitation1.getCell(0).text = "13. Realisasi Peserta"
                val tableRealitation2 = tableRealitation.createRow()

                val runRealiataion: XWPFRun = tableRealitation2.getCell(0).getParagraphArray(0).createRun()
                runRealiataion.setText("Jumlah Peserta : ${((awpi.participantMale ?: 0) + (awpi.participantFemale ?: 0))}")
                runRealiataion.addBreak()
                runRealiataion.setText("Jumlah Peserta Laki-laki : ${awpi.participantMale ?: 0}")
                runRealiataion.addBreak()
                runRealiataion.setText("Jumlah Peserta Perempuan : ${awpi.participantFemale ?: 0}")


                val tableRealitationNs: XWPFTable =
                    p.document.insertNewParagraph(p.ctp?.newCursor()).document.insertNewTbl(p.ctp?.newCursor())
                tableRealitationNs.width = 8500
                val tableRealitationNs1 = tableRealitationNs.getRow(0)
                tableRealitationNs1.getCell(0).text = "14. Realisasi Narasumber"
                val tableRealitationNs2 = tableRealitationNs.createRow()

                val runNs: XWPFRun = tableRealitationNs2.getCell(0).getParagraphArray(0).createRun()
                runNs.setText("Jumlah Narasumber : ${((awpi.nsMale ?: 0) + (awpi.nsFemale ?: 0))}")
                runNs.addBreak()
                runNs.setText("Jumlah Peserta Laki-laki : ${awpi.nsMale ?: 0}")
                runNs.addBreak()
                runNs.setText("Jumlah Peserta Perempuan : ${awpi.nsFemale ?: 0}")


                val tableRisk: XWPFTable =
                    p.document.insertNewParagraph(p.ctp?.newCursor()).document.insertNewTbl(p.ctp?.newCursor())
                tableRisk.width = 8500
                val tableRisk1 = tableRisk.getRow(0)
                tableRisk1.getCell(0).text = "15. Risiko"
                val tableRisk2 = tableRisk.createRow()

                val runRisk: XWPFRun = tableRisk2.getCell(0).getParagraphArray(0).createRun()
                runRisk.setText("Keterangan Risiko : ${Jsoup.parse(awpi.descriptionRisk ?: "-").text()}")
                runRisk.addBreak()
                runRisk.setText("Dampak Risiko : ${awpi.impactRisk ?: 0}")
                runRisk.addBreak()
                runRisk.setText("Kemungkinan Terjadi : ${awpi.potentialRisk ?: 0}")
                runRisk.addBreak()
                runRisk.setText("Migitasi : ${Jsoup.parse(awpi.mitigationRisk ?: "-").text()}")
                runRisk.addBreak()
                runRisk.setText("Gejala Risiko : ${awpi.symptomsRisk ?: 0}")

                val tableAnggaran: XWPFTable =
                    p.document.insertNewParagraph(p.ctp?.newCursor()).document.insertNewTbl(p.ctp?.newCursor())
                tableAnggaran.width = 8500
                val tableAnggaran1 = tableAnggaran.getRow(0)
                tableAnggaran1.getCell(0).text = "16. Anggaran"
                val tableAnggaran2 = tableAnggaran.createRow()

                val runAnggaran: XWPFRun = tableAnggaran2.getCell(0).getParagraphArray(0).createRun()
                runAnggaran.setText("Rencana Anggaran AWP : ${formatRupiah(awpi.budgetAwp?.toDouble())}")
                runAnggaran.addBreak()
                runAnggaran.setText("Rencana Anggaran POK : ${formatRupiah(awpi.budgetPok?.toDouble())}")
                runAnggaran.addBreak()

                var realitation = 0.toLong()
                reports.filter { df -> df.event?.awpImplementationId == awpi.id }.forEach { dts ->
                    realitation += dts.rraBudget ?: 0
                }

                runAnggaran.setText("Realisasi Anggaran : ${formatRupiah(realitation.toDouble())}")

                val tablePelaksana: XWPFTable =
                    p.document.insertNewParagraph(p.ctp?.newCursor()).document.insertNewTbl(p.ctp?.newCursor())
                tablePelaksana.width = 8500
                val tablePelaksana1 = tablePelaksana.getRow(0)
                tablePelaksana1.getCell(0).text = "17. Pelaksana Kegiatan"
                val tablePelaksana2 = tablePelaksana.createRow()


                val runPelaksana: XWPFRun = tablePelaksana2.getCell(0).getParagraphArray(0).createRun()

                if (awpi.eventManagementTypeId == EVENT_MANAGEMENT_TYPE_SWAKELOLA) {
                    var projectOfficer = ""
                    if (!awpi.projectOfficerResources?.user?.firstName.isNullOrEmpty()) {
                        projectOfficer += awpi.projectOfficerResources?.user?.firstName
                    }
                    if (!awpi.projectOfficerResources?.user?.lastName.isNullOrEmpty()) {
                        projectOfficer += " ${awpi.projectOfficerResources?.user?.lastName}"
                    }
                    runPelaksana.setText("Project Officer : $projectOfficer")
                } else {
                    runAnggaran.addBreak()
                    runAnggaran.setText("LSP : ${awpi.lspContractData}")
                    runAnggaran.addBreak()
                    runAnggaran.setText("Nomor Kontrak : ${awpi.lspContractNumber}")
                    runAnggaran.addBreak()

                    var picLsp = ""
                    if (!awpi.lspPicUser?.firstName.isNullOrEmpty()) {
                        picLsp += awpi.projectOfficerResources?.user?.firstName
                    }
                    if (!awpi.lspPicUser?.lastName.isNullOrEmpty()) {
                        picLsp += " ${awpi.projectOfficerResources?.user?.lastName}"
                    }
                    runAnggaran.setText("PIC LSP : $picLsp")
                }

                writeEventReport(doc, p, reports, awpi)
            }
        } catch (e: Exception) {
            throw e
        }
    }

    private fun writeEventReport(
        doc: XWPFDocument,
        p: XWPFParagraph,
        reports: List<EventReport>,
        awpi: AwpImplementation
    ) {
        try {
            reports.filter { fd -> fd.event?.awpImplementationId == awpi.id }.forEach { e ->
                val content: XWPFParagraph = p.document.insertNewParagraph(p.ctp?.newCursor())
                content.style = "Heading5"
                val ca = content.createRun()
                ca.addBreak()
                ca.setText(e.event?.name)
                ca.addBreak()

                val tableEvent: XWPFTable = doc.insertNewTbl(p.ctp?.newCursor())
                tableEvent.width = 8500

                val tableRow1 = tableEvent.getRow(0)
                tableRow1.getCell(0).text = "1. Komponen Proyek"
                tableRow1.addNewTableCell().text = e.event?.awpImplementation?.componentCode?.code ?: ""

                val tableRow2 = tableEvent.createRow()
                tableRow2.getCell(0).text = "2. Nama Event"
                tableRow2.getCell(1).text = e.event?.name ?: ""

                val tableRow3 = tableEvent.createRow()
                tableRow3.getCell(0).text = "3. Waktu Pelaksanaan"
                tableRow3.getCell(1).text = "${dateFormatIndonesia(e.startDate)} - ${dateFormatIndonesia(e.endDate)}"

                val tableRow4 = tableEvent.createRow()
                tableRow4.getCell(0).text = "4. Tempat Pelaksanaan"
                tableRow4.getCell(1).text = "${dateFormatIndonesia(e.startDate)} - ${dateFormatIndonesia(e.endDate)}"


                val contentOverView: XWPFParagraph = p.document.insertNewParagraph(p.ctp?.newCursor())
                val cov = contentOverView.createRun()
                cov.isBold = true
                excelPoiAddBreak(cov, 2)
                cov.setText("A. Overview Proyek")
                writeOverViewProject(p)

                writeEventDescription("B. Deskripsi Event", Jsoup.parse(e.event?.description ?: "-").text(), p)
                writeEventDescription("C. Tujuan", Jsoup.parse(e.event?.purpose ?: "-").text(), p)
                writeEventDescription(
                    "D. Pelaksana Teknis Event",
                    "Pelaksana Teknis dalam pelaksanaan event ini adalah ${e.event?.awpImplementation?.eventManagementType?.name}",
                    p
                )
                writeEventDescription(
                    "E. Event Output", "Keluaran (output) yang dihasilkan dari event ini adalah: ${
                        Jsoup.parse(e.eventOutput ?: "").text()
                    }", p
                )
                writeEventDescription(
                    "F. Sumber Anggaran",
                    "Sumber anggaran event ini dibebankan pada DIPA Ditjen Pendidikan Islam Tahun Anggaran 2022 yang bersumber dari IBRD 8992-ID dengan MAK Nomor 738931",
                    p
                )

                val contentAnggaran: XWPFParagraph = p.document.insertNewParagraph(p.ctp?.newCursor())
                val cna = contentAnggaran.createRun()
                cna.isBold = true
                cna.setText("G. Realisasi Anggaran")
                writeRealisasiAngaran(e, p, doc)

                val contentEvaluation: XWPFParagraph = p.document.insertNewParagraph(p.ctp?.newCursor())
                val ct = contentEvaluation.createRun()
                ct.isBold = true
                ct.addBreak()
                ct.setText("H. Evaluasi Event")
                writeEventReportEvaluationAnswer(e, p, doc, EVENT_QUESTION_CREATED_FOR_EVALUATOR)

                val contentEvaluationParticipant: XWPFParagraph = p.document.insertNewParagraph(p.ctp?.newCursor())
                val ctp = contentEvaluationParticipant.createRun()
                ctp.isBold = true
                ct.addBreak()
                ctp.setText("I. Evaluasi Pelaksanaan Event")
                writeEventReportEvaluationAnswer(e, p, doc, EVENT_QUESTION_CREATED_FOR_PARTICIPANT)

                val realitationParticipant: XWPFParagraph = p.document.insertNewParagraph(p.ctp?.newCursor())
                val rp = realitationParticipant.createRun()
                rp.isBold = true
                rp.setText("J. Realisasi Peserta Event ")
                writeParticipantRealitation(e, p)

                writeEventDescription(
                    "K. Ringkasan Pelaksanaan Event", Jsoup.parse(e.implementationSummary ?: "").text(), p)
                writeEventDescription("L. Kesimpulan", Jsoup.parse(e.conclusionsAndRecommendations ?: "").text(), p)

                EVENT_DOCUMENT_TYPE_PROJECT_REPORT_NAMES.forEach { (k, _) ->
                    writeDocuments(k, p, e)
                }
            }
        } catch (e: Exception) {
            throw e
        }
    }

    fun writeEventReportFromAwpImplementationReport(doc: XWPFDocument, p: XWPFParagraph, data: AwpImplementation) {
        try {
            val reports = mutableListOf<EventReport>()
            repoEvent.getByAwpImplementationIdAndStatusAndActive(data.id, EVENT_STATUS_IMPLEMENTATION).forEach { e ->
                val fReports = repoEventReport.getByEventIdAndStatusAndActive(e.id, EVENT_REPORT_STATUS_APPROVED)
                if (fReports.isNotEmpty()) {
                    reports.addAll(fReports)
                }
            }

            writeEventReport(doc, p, reports, data)
        } catch (e: Exception) {
            throw e
        }
    }

    private fun writeOverViewProject(p: XWPFParagraph) {
        val content: XWPFParagraph = p.document.insertNewParagraph(p.ctp?.newCursor())
        content.alignment = ParagraphAlignment.BOTH
        val ct = content.createRun()
        ct.setText("Proyek Realizing Education’s Promise: Support to Indonesia’s Ministry of Religious Affairs for Improved Quality of Education (Madrasah Education Quality Reform) –selanjutnya disebut Realizing Education’s Promise- Madrasah Education Quality Reform [REP-MEQR] (IBRD Loan 8992-ID) bertujuan untuk meningkatkan mutu pengelolaan dan layanan pendidikan madrasah dalam binaan Kementerian Agama. Proyek ini dilaksanakan dalam waktu lima tahun, dimulai pada awal tahun 2020 dan berakhir pada tahun 2024 dengan pembiayaan dari Bank Dunia (IBRD 8992-ID). Proyek ini akan dilaksanakan di 34 provinsi dan 514 kabupaten/kota di seluruh Indonesia")
        ct.addBreak()
        ct.addBreak()
        ct.setText("Proyek ini terdiri atas empat komponen proyek yang dapat meningkatkan hasil belajar siswa dan sistem pengelolaan pendidikan di Kementerian Agama. Keempat komponen tersebut adalah:")
        ct.addBreak()
        ct.setText("1. Penerapan Sistem e-RKAM (Rencana Kerja dan Anggaran Madrasah berbasis Elektronik) secara Nasional dan Pemberian Dana Bantuan untuk Madrasah. Sistem e-RKAM ini memungkinkan terjadinya peningkatan efektivitas pembelanjaan melalui sistem perencanaan dan penganggaran berbasis kinerja di madrasah dan sekolah penerima BOS di bawah Kemenag yang memungkinkan madrasah dan satuan pendidikan keagamaan lainnya untuk merencanakan, menganggarkan, dan memonitor penggunaan dana dengan lebih efektif. Pemberian dana bantuan dimaksudkan untuk mendukung percepatan pencapaian SNP berdasarkan hasil Evaluasi Diri Madrasah (EDM) dan penerapan e-RKAM.")
        ct.addBreak()
        ct.setText("2. Penerapan Sistem Penilaian Hasil Belajar di tingkat Madrasah Ibtidaiyah (MI) untuk Seluruh Peserta Didik Kelas 4 Secara Nasional. Asesmen ini diharapkan dapat mengukur dampak dari pendanaan terhadap hasil belajar siswa dan mengidentifikasi aspek-aspek apa saja yang perlu ditingkatkan.")
        ct.addBreak()
        ct.setText("3. Kebijakan dan Pengembangan Keprofesian Berkelanjutan untuk Guru, Kepala Madrasah, dan Tenaga Kependidikan Madrasah. Peningkatan akses terhadap pelatihan yang bermutu memungkinkan terjadinya peningkatan kompetensi guru dan tenaga kependidikan.")
        ct.addBreak()
        ct.setText("4. Penguatan Sistem untuk Mendukung Peningkatan Mutu Pendidikan. Sistem pendataan yang baik diharapkan dapat mendorong perumusan kebijakan berbasis data yang valid dan akurat. Penguatan sistem pengelolaan madrasah dan tata kelola di semua jenjang kantor Kemenag diharapkan dapat meningkatkan sistem penyelenggaran pendidikan yang bermutu di Kemenag.")
    }

    private fun writeRealisasiAngaran(e: EventReport, p: XWPFParagraph, doc: XWPFDocument) {
        try {
            val content: XWPFParagraph = p.document.insertNewParagraph(p.ctp?.newCursor())
            val ct = content.createRun()
            ct.setText("Realisasi anggaran yang digunakan dalam Event ini adalah sebagai berikut:")

            val table: XWPFTable = doc.insertNewTbl(p.ctp?.newCursor())
            table.width = 8500

            val tableRow1 = table.getRow(0)
            tableRow1.getCell(0).text = "No"
            tableRow1.createCell().text = "Uraian"
            tableRow1.createCell().text = "Jumlah"

            val tableRow2 = table.createRow()
            tableRow2.getCell(0).text = "1"
            tableRow2.getCell(1).text = "Jumlah pagu anggaran"
            tableRow2.getCell(2).text = formatRupiah(e.budgetCeiling?.toDouble())

            val tableRow3 = table.createRow()
            tableRow3.getCell(0).text = "2"
            tableRow3.getCell(1).text = "Jumlah realisasi anggaran"
            tableRow3.getCell(2).text = formatRupiah(e.rraBudget?.toDouble())
        } catch (e: Exception) {
            throw e
        }
    }

    private fun writeParticipantRealitation(e: EventReport, p: XWPFParagraph) {
        try {
            val content: XWPFParagraph = p.document.insertNewParagraph(p.ctp?.newCursor())
            val ct = content.createRun()
            ct.setText("Jumlah Peserta Laki-Laki    :  ${e.participantMale ?: 0}")
            ct.addBreak()
            ct.setText("Jumlah Peserta Perempuan    :  ${e.participantFemale ?: 0}")
            ct.addBreak()
            ct.setText("Total Peserta   :  ${(e.participantMale ?: 0) + (e.participantFemale ?: 0)}")
        } catch (e: Exception) {
            throw e
        }
    }

    /*private fun writeEventReportEvaluation(eventReport: EventReport, p: XWPFParagraph, doc: XWPFDocument) {
        try {
            val content: XWPFParagraph = p.document.insertNewParagraph(p.ctp?.newCursor())
            val ct = content.createRun()
            ct.setText("Berikut ini adalah point evaluasi event:")

            val form = eventFormRepository.getByEventTypeIdAndActive(eventReport.event?.eventTypeId)
            if (form.isNotEmpty()) {
                val table: XWPFTable = doc.insertNewTbl(p.ctp?.newCursor())
                table.width = 8500

                val tableRow1 = table.getRow(0)
                tableRow1.getCell(0).text = "Item dinilai"
                tableRow1.createCell().text = "Penilaian"
                tableRow1.createCell().text = "Catatan"

                var answersEvaluator: MutableList<EventQuestionAnswerDetail>? = null

                val answerEvaluator = repoEventQuestionAnswer.findByEventIdAndFilledByAndActive(
                    eventReport.eventId, EVENT_QUESTION_CREATED_FOR_EVALUATOR
                )

                if (answerEvaluator.isPresent) {
                    answersEvaluator = answerEvaluator.get().answers
                }

                val mergeCell = mutableListOf<Int>()
                var cellMergePos = 1

                val categories = mutableListOf<Int>()
                form[0].questions?.filter { fqf ->
                    fqf.createdFor == EVENT_QUESTION_CREATED_FOR_EVALUATOR
                }?.forEach { c ->
                    if (!categories.contains(c.questionCategory)) {
                        c.questionCategory?.let { it1 -> categories.add(it1) }
                    }
                }

                categories.sortedBy { cs -> cs }.forEachIndexed { index, qc ->
                    val tableRow1 = table.createRow()
                    tableRow1.getCell(0).text = "${index + 1}. ${EVENT_QUESTION_CATEGORY_NAMES[qc] ?: "-"}"
                    mergeCell.add(cellMergePos)


                    form[0].questions?.sortedBy { stq ->
                        stq.questionType
                    }
                        ?.filter { an -> an.questionCategory == qc && an.createdFor == EVENT_QUESTION_CREATED_FOR_EVALUATOR }
                        ?.forEach { qst ->
                            val tableQuestion = table.createRow()
                            tableQuestion.getCell(0).text = qst.question

                            val answerData = answersEvaluator?.filter { aef ->
                                aef.eventQuestionId == qst.id
                            }

                            when (qst.questionType) {
                                EVENT_QUESTION_TYPE_SCALE -> {
                                    if (answerData?.isNotEmpty() == true) {
                                        var scale = ""
                                        if (answerData[0].scaleAsnwer != null) {
                                            scale = strStarAnswer(answerData[0].scaleAsnwer)
                                        }
                                        tableQuestion.getCell(1).text = scale
                                    } else {
                                        tableQuestion.getCell(1).text = ""
                                    }
                                }

                                EVENT_QUESTION_TYPE_YES_OR_NO -> {
                                    if (answerData?.isNotEmpty() == true) {
                                        tableQuestion.getCell(1).text = strYesOrnoAnswer(answerData[0].yesOrNoAnswer)
                                    } else {
                                        tableQuestion.getCell(1).text = ""
                                    }
                                }
                                EVENT_QUESTION_TYPE_EXPLANATION -> {
                                    if (answerData?.isNotEmpty() == true) {
                                        var explanation = ""
                                        if (answerData[0].explanationAnswer != null) {
                                            explanation = answerData[0].explanationAnswer.toString()
                                        }

                                        tableQuestion.getCell(1).text = explanation
                                    } else {
                                        tableQuestion.getCell(1).text = ""
                                    }
                                }
                                else -> {
                                    tableQuestion.getCell(1).text = ""
                                }
                            }

                            if (answerData?.isNotEmpty() == true) {
                                var comment = ""
                                if (answerData[0].comment != null) {
                                    comment = answerData[0].comment.toString()
                                }

                                tableQuestion.getCell(2).text = comment
                            } else {
                                tableQuestion.getCell(2).text = ""
                            }
                    }

                    cellMergePos += (form[0].questions?.filter { an -> an.questionCategory == qc && an.createdFor == EVENT_QUESTION_CREATED_FOR_EVALUATOR }?.size
                        ?: 0) + 1
                }

                mergeCell.forEach {
                    mergeTableWordCellHorizontally(table, it, 0, 2)
                }

                writeStartDescription(p)
            }
        } catch (e: Exception) {
            throw e
        }
    }*/

    private fun writeEventReportEvaluationAnswer(
        eventReport: EventReport, p: XWPFParagraph, doc: XWPFDocument, createdFor: Int
    ) {
        try {
            val content: XWPFParagraph = p.document.insertNewParagraph(p.ctp?.newCursor())
            val ct = content.createRun()

            if (createdFor == EVENT_QUESTION_CREATED_FOR_PARTICIPANT) {
                ct.setText("Berikut ini adalah point evaluasi pelaksanaan event:")
            } else {
                ct.setText("Berikut ini adalah point evaluasi event:")
            }

            val form = eventFormRepository.getByEventTypeIdAndActive(eventReport.event?.eventTypeId)
            if (form.isNotEmpty()) {
                val answerParticipant = repoEventQuestionAnswer.getByEventIdAndFilledByAndActiveOrderByCreatedAtDesc(
                    eventReport.eventId, createdFor
                )

                val categories = mutableListOf<Int>()
                form[0].questions?.filter { fqf ->
                    fqf.createdFor == createdFor
                }?.forEach { c ->
                    if (!categories.contains(c.questionCategory)) {
                        c.questionCategory?.let { it1 -> categories.add(it1) }
                    }
                }

                answerParticipant.forEach { ap ->
                    val table: XWPFTable = doc.insertNewTbl(p.ctp?.newCursor())
                    table.width = 8500

                    val tableRow1 = table.getRow(0)
                    tableRow1.getCell(0).text = "Item dinilai"
                    tableRow1.createCell().text = "Penilaian"

                    if (createdFor == EVENT_QUESTION_CREATED_FOR_EVALUATOR) {
                        tableRow1.createCell().text = "Catatan"
                    }

                    val answerDatas = ap.answers

                    val mergeCell = mutableListOf<Int>()
                    var cellMergePos = 1

                    categories.sortedBy { cs -> cs }.forEachIndexed { index, qt ->

                        val tableRowContent = table.createRow()
                        tableRowContent.getCell(0).text = "${index + 1}. ${EVENT_QUESTION_CATEGORY_NAMES[qt] ?: "-"}"
                        mergeCell.add(cellMergePos)

                        form[0].questions?.sortedBy { stq ->
                            stq.questionType
                        }?.filter { an -> an.questionCategory == qt && an.createdFor == createdFor }?.forEach { qst ->
                            val tableQuestion = table.createRow()
                            tableQuestion.getCell(0).text = qst.question

                            when (qst.questionType) {
                                EVENT_QUESTION_TYPE_SCALE -> {
                                    if (answerDatas?.isNotEmpty() == true) {
                                        val answerData = answerDatas.filter { adf -> adf.eventQuestionId == qst.id }
                                        if (answerData.isNotEmpty()) {
                                            var scale = ""
                                            if (answerData[0].scaleAsnwer != null) {
                                                scale = strStarAnswer(answerData[0].scaleAsnwer)
                                            }
                                            tableQuestion.getCell(1).text = scale
                                        } else {
                                            tableQuestion.getCell(1).text = ""
                                        }
                                    } else {
                                        tableQuestion.getCell(1).text = ""
                                    }
                                }
                                EVENT_QUESTION_TYPE_YES_OR_NO -> {
                                    if (answerDatas?.isNotEmpty() == true) {
                                        val answerData = answerDatas.filter { adf -> adf.eventQuestionId == qst.id }
                                        if (answerData.isNotEmpty()) {
                                            tableQuestion.getCell(1).text =
                                                strYesOrnoAnswer(answerData[0].yesOrNoAnswer)
                                        } else {
                                            tableQuestion.getCell(1).text = ""
                                        }
                                    } else {
                                        tableQuestion.getCell(1).text = ""
                                    }
                                }
                                EVENT_QUESTION_TYPE_EXPLANATION -> {
                                    if (answerDatas?.isNotEmpty() == true) {
                                        val answerData = answerDatas.filter { adf -> adf.eventQuestionId == qst.id }
                                        if (answerData.isNotEmpty()) {
                                            var explanation = ""
                                            if (answerData[0].explanationAnswer != null) {
                                                explanation = answerData[0].explanationAnswer.toString()
                                            }

                                            tableQuestion.getCell(1).text = explanation
                                        }
                                    } else {
                                        tableQuestion.getCell(1).text = ""
                                    }
                                }
                                else -> {
                                    tableQuestion.getCell(1).text = ""
                                }
                            }

                            if (createdFor == EVENT_QUESTION_CREATED_FOR_EVALUATOR) {
                                if (answerDatas?.isNotEmpty() == true) {
                                    var comment = ""
                                    val answerData = answerDatas.filter { adf -> adf.eventQuestionId == qst.id }
                                    if (answerData.isNotEmpty()) {
                                        if (answerData[0].comment != null) {
                                            comment = answerData[0].comment.toString()
                                        }
                                    }
                                    tableQuestion.getCell(2).text = comment
                                } else {
                                    tableQuestion.getCell(2).text = ""
                                }
                            }
                        }

                        cellMergePos += (form[0].questions?.filter { an -> an.questionCategory == qt && an.createdFor == createdFor }?.size
                            ?: 0) + 1
                    }

                    mergeCell.forEach {
                        var tocol = 1
                        if (createdFor == EVENT_QUESTION_CREATED_FOR_EVALUATOR) {
                            tocol = 2
                        }

                        mergeTableWordCellHorizontally(table, it, 0, tocol)
                    }

                    writeStartDescription(p, true)
                }
            }
        } catch (e: Exception) {
            throw e
        }
    }

    fun writeEventDescription(title: String, value: String, p: XWPFParagraph, addBreak: Boolean = false) {
        try {
            val contentTitle: XWPFParagraph = p.document.insertNewParagraph(p.ctp?.newCursor())
            val ct = contentTitle.createRun()
            ct.isBold = true
            if (addBreak) {
                ct.addBreak()
            }

            ct.setText(title)

            val contentValue: XWPFParagraph = p.document.insertNewParagraph(p.ctp?.newCursor())
            val av = contentValue.createRun()
            av.setText(value)
        } catch (e: Exception) {
            throw e
        }
    }

    fun writeStartDescription(p: XWPFParagraph, addBreak: Boolean = false) {
        try {
            val contentTableDesc: XWPFParagraph = p.document.insertNewParagraph(p.ctp?.newCursor())
            val ctD = contentTableDesc.createRun()
            ctD.addBreak()
            ctD.isItalic = true
            ctD.fontSize = 9
            ctD.setText("Keterangan")
            ctD.addBreak()
            ctD.setText("*****")
            ctD.setText(" : Sangat Baik")
            ctD.addBreak()
            ctD.setText("****")
            ctD.setText(" ")
            ctD.setText(" ")
            ctD.setText(" : Baik")
            ctD.addBreak()
            ctD.setText("***")
            ctD.setText(" ")
            ctD.setText(" ")
            ctD.setText(" ")
            ctD.setText(" ")
            ctD.setText(" : Cukup")
            ctD.addBreak()
            ctD.setText("**")
            ctD.setText(" ")
            ctD.setText(" ")
            ctD.setText(" ")
            ctD.setText(" ")
            ctD.setText(" ")
            ctD.setText(" ")
            ctD.setText(" : Kurang")
            ctD.addBreak()
            ctD.setText("*")
            ctD.setText(" ")
            ctD.setText(" ")
            ctD.setText(" ")
            ctD.setText(" ")
            ctD.setText(" ")
            ctD.setText(" ")
            ctD.setText(" ")
            ctD.setText(" ")
            ctD.setText(" : Sangat Kurang")

            if (addBreak) {
                excelPoiAddBreak(ctD, 2)
            }
        } catch (e: Exception) {
            throw e
        }
    }

    // replace data string pada template sesuai dengan string yang akan di replace
    private fun getstrReplaceWord(text: String, reports: List<EventReport>, startDate: Date, endDate: Date): String? {
        try {
            val mapReplace = mutableMapOf<String, String>()
            mapReplace["rcomponent"] = ""
            mapReplace["rtime"] = "${dateFormatIndonesia(startDate)} - ${dateFormatIndonesia(endDate)}"
            mapReplace["rdate"] = dateFormatIndonesia(Date())

            var consultan = ""
            val usersPmuConsultan = repoUserRoleViewUserResources.getUserIdsByRoleIdsComponentIdPosition(
                listOf(ROLE_ID_CONSULTAN, ROLE_ID_ADMINSTRATOR),
                2,
                COMPONENT_ID_PMU,
                POSITION_PROJECT_MANAGEMENT_SPECIALIST
            )

            if (usersPmuConsultan.isNotEmpty()) {
                val user = repoUser.findByIdAndActive(usersPmuConsultan[0])
                if (user.isPresent) {
                    if (!user.get().firstName.isNullOrEmpty()) {
                        consultan += user.get().firstName
                    }
                    if (!user.get().lastName.isNullOrEmpty()) {
                        consultan += " ${user.get().lastName}"
                    }
                }
            }
            mapReplace["consultan"] = consultan

            var coordinator = ""
            val usersCoordinator = repoUserRoleViewUserResources.getUserIdsByRoleIdsComponentIdPosition(
                listOf(ROLE_ID_COORDINATOR, ROLE_ID_ADMINSTRATOR),
                2,
                COMPONENT_ID_PMU,
                POSITION_SECRETARY_PMU
            )

            if (usersCoordinator.isNotEmpty()) {
                val user = repoUser.findByIdAndActive(usersCoordinator[0])
                if (user.isPresent) {
                    if (!user.get().firstName.isNullOrEmpty()) {
                        coordinator += user.get().firstName
                    }
                    if (!user.get().lastName.isNullOrEmpty()) {
                        coordinator += " ${user.get().lastName}"
                    }
                }
            }
            mapReplace["coordinator"] = coordinator

            var rsecretary = ""
            val usersPmuTreasurer = repoUserRoleViewUserResources.getUserIdsByRoleIdsComponentIdPosition(
                listOf(ROLE_ID_COORDINATOR, ROLE_ID_ADMINSTRATOR), 2, COMPONENT_ID_PMU, POSITION_TREASURER_PMU
            )

            if (usersPmuTreasurer.isNotEmpty()) {
                val user = repoUser.findByIdAndActive(usersPmuTreasurer[0])
                if (user.isPresent) {
                    if (!user.get().firstName.isNullOrEmpty()) {
                        rsecretary += user.get().firstName
                    }
                    if (!user.get().lastName.isNullOrEmpty()) {
                        rsecretary += " ${user.get().lastName}"
                    }
                }
            }
            mapReplace["secretary"] = rsecretary


            var pmu = ""
            val usersPmuHead = repoUserRoleViewUserResources.getPmuHead(
                listOf(ROLE_ID_COORDINATOR, ROLE_ID_ADMINSTRATOR),
                2,
                COMPONENT_ID_PMU,
            )

            if (usersPmuHead.isNotEmpty()) {
                val user = repoUser.findByIdAndActive(usersPmuHead[0])
                if (user.isPresent) {
                    pmu += user.get().firstName
                    if (!user.get().lastName.isNullOrEmpty()) {
                        pmu += " ${user.get().lastName}"
                    }
                }
            }
            mapReplace["pmu"] = pmu

            mapReplace["ryear"] = ""
            mapReplace["rcount"] = ""


            var volume = 0
            reports.forEach {
                it.event?.awpImplementation?.eventVolume?.let { v ->
                    volume += v.toInt()
                }
            }

            mapReplace["rtotalvolume"] = volume.toString()
            mapReplace["rdatesignature"] = "Jakarta ${dateFormatIndonesia(Date())}"
            mapReplace["rstart"] = dateFormatIndonesia(startDate)
            mapReplace["rend"] = dateFormatIndonesia(endDate)

            return mapReplace[text]
        } catch (e: Exception) {
            throw e
        }
    }

    private fun writeDocuments(doctypeId: String?, p: XWPFParagraph, data: EventReport) {
        try {
            val contentPTitle: XWPFParagraph = p.document.insertNewParagraph(p.ctp?.newCursor())
            contentPTitle.isPageBreak = true
            contentPTitle.style = "Heading6"
            val ct = contentPTitle.createRun()
            ct.setText(EVENT_DOCUMENT_TYPE_PROJECT_REPORT_NAMES[doctypeId])

            data.documents?.filter { df -> df.eventDocumentTypeId.equals(doctypeId) }?.forEach {
                val contentTitle: XWPFParagraph = p.document.insertNewParagraph(p.ctp?.newCursor())
                val ct = contentTitle.createRun()
                ct.isBold = true
                ct.setText(it.title)

                val contentP: XWPFParagraph = p.document.insertNewParagraph(p.ctp?.newCursor())
                val content = contentP.createRun()

                content.setText(it.description)
                content.addBreak()
                content.setText(it.place)

                val fileDir =
                    "$dirUpload${File.separator}${it.file?.filepath ?: ""}${File.separator}${it.file?.filename ?: ""}"
                val fileFocus = File(fileDir)
                if (fileFocus.exists()) {

                    content.addBreak()

                    if (it.file?.fileExt == "png" || it.file?.fileExt == "jpg" || it.file?.fileExt == "jpeg") {

                        val fis = FileInputStream(fileFocus)
                        var pictureType = XWPFDocument.PICTURE_TYPE_JPEG
                        if (fileFocus.extension == "png") {
                            pictureType = XWPFDocument.PICTURE_TYPE_PNG
                        }

                        var height = 0
                        var width = 0
                        try {
                            val readImage = ImageIO.read(fileFocus)
                            height = readImage.width
                            width = readImage.height
                        } catch (_: Exception) {
                        }

                        content.addPicture(
                            fis,
                            pictureType,
                            it.file?.filename,
                            Units.toEMU(height.toDouble()),
                            Units.toEMU(width.toDouble())
                        )
                        fis.close()
                    } else {
                        val url = "$downloadFileUrl${it.fileId}"
                        content.setText(url)
                    }
                } else {
                    println("file not found : $fileDir")
                }

                excelPoiAddBreak(content, 2)
            }
        } catch (e: Exception) {
            throw e
        }
    }
}
