package id.go.kemenag.madrasah.pmrms.awp.activity.utility

import org.apache.poi.ss.usermodel.*
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException
import kotlin.math.max

class ExcelUtility(val path: String, val sheet: Int) {

    private val HSLF = 1f
    private val XSLF = 2f
    private var _excelVersion = XSLF
    private var _excel: FileInputStream? = null
    private var _workbook: XSSFWorkbook? = null
    private var _filePath: String? = null
    private var _sheet: Sheet? = null
    private var _rowAlreadySet = false
    private var _columnAlreadySet = false
    private var _row: Row? = null
    private var _currentRow = 0
    private var _currentColumn = 0
    private var _cell: Cell? = null

    init {
        initExcel()
    }

    private fun initExcel() {
        var initWb: XSSFWorkbook? = null

        if (!File(path).exists()) {
            initWb = XSSFWorkbook()
            initWb.write(FileOutputStream(path))
        }

        setVersionByPath(path)
        _excel = FileInputStream(path)

        if (_excelVersion == XSLF) {
            _workbook = if (initWb == null) {
                XSSFWorkbook(_excel)
            } else {
                initWb.createSheet("Sheet1")
                val fos = FileOutputStream(path)
                initWb.write(fos)
                fos.flush()
                fos.close()
                XSSFWorkbook(_excel)
            }

            _sheet = _workbook?.getSheetAt(sheet)
            _filePath = path
        }
    }

    private fun setVersionByPath(path: String) {
        val ext = this.getExtensionFromPath(path)
        _excelVersion = if (ext == "xlsx") {
            XSLF
        } else {
            HSLF
        }
    }

    private fun getExtensionFromPath(path: String): String? {
        var ext: String? = null
        val i = path.lastIndexOf(".")
        val p = max(path.lastIndexOf('/'), path.lastIndexOf('\\'))
        if (i > p) {
            ext = path.substring(i + 1)
        }
        return ext
    }

    fun getMaxRow(): Int? {
        return _sheet?.lastRowNum
    }

    fun getMaxColumn(): Int? {
        return if (_rowAlreadySet) {
            _row?.lastCellNum?.toInt()
        } else {
            _row = _sheet?.getRow(0)
            _row?.lastCellNum?.toInt()
        }
    }

    fun toName(num: Int): String {
        var number = num
        val sb = StringBuilder()
        while (number-- > 0) {
            sb.append(('A'.toInt() + number % 26).toChar())
            number /= 26
        }
        return sb.reverse().toString()
    }

    fun getCellValue(cell: String): Any? {
        var value: Any? = null
        if (this._excelVersion == XSLF) {
            val rc = getRowAndColumn(cell)
            value = getCellValue(rc)
        }
        return value
    }

    private fun getCellValue(rc: RowAndColumn): Any? {
        var value: Any? = null
        if (this._excelVersion == XSLF) {
            if (_rowAlreadySet && _columnAlreadySet && rc.row == _currentRow && rc.column != _currentColumn) {
                _cell = _row?.getCell(rc.column)
                _currentColumn = rc.column
                _columnAlreadySet = true
            } else {
                _row = _sheet?.getRow(rc.row)
                _currentRow = rc.row
                _rowAlreadySet = true
                _cell = _row?.getCell(rc.column)
                _currentColumn = rc.column
                _columnAlreadySet = true
            }
            try {
                try {
                    value = _cell?.stringCellValue
                } catch (e: Exception) {
                    try {
                        value = _cell?.numericCellValue
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }

            } catch (e: NullPointerException) {
                value = null
            }

        }
        return value
    }

    private fun getRowAndColumn(cell: String): RowAndColumn {
        val row = Integer.parseInt(cell.replace("\\D+".toRegex(), ""))
        val column = stringToNumber(cell.replace("[^A-Za-z]+".toRegex(), ""))
        return RowAndColumn(row - 1, column)
    }

    fun getRow(rowNum: Int): Row? {
        return _sheet?.getRow(rowNum)
    }

    fun createRow(rowNum: Int): Row? {
        return _sheet?.createRow(rowNum)
    }

    private fun stringToNumber(str: String): Int {
        val ls = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray()
        val m = HashMap<Char, Int>()
        var j = 1
        for (c in ls) {
            m[c] = j++
        }
        var i = 0
        var mul = 1
        for (c in StringBuffer(str).reverse().toString().toCharArray()) {
            i += m[c]!! * mul
            mul *= ls.size
        }
        return i - 1
    }

    fun close() {
        _excel?.close()
    }

    @Throws(IOException::class)
    fun writeToFile(activeSheet: Int) {
        _workbook?.setActiveSheet(activeSheet)
        _workbook?.forceFormulaRecalculation = true
        val fos = _filePath?.let { FileOutputStream(it) }
        _workbook?.write(fos)
        fos?.flush()
        fos?.close()
        close()
    }

    fun headStyle(): CellStyle? {
        val style = _workbook?.createCellStyle()
        style?.fillForegroundColor = IndexedColors.GREY_25_PERCENT.getIndex()
        style?.fillPattern = FillPatternType.SOLID_FOREGROUND
        style?.borderBottom = BorderStyle.THIN
        style?.borderLeft = BorderStyle.THIN
        style?.borderRight = BorderStyle.THIN
        style?.borderTop = BorderStyle.THIN
        style?.alignment = HorizontalAlignment.CENTER

        val font = _workbook?.createFont()
        font?.bold = true
        style?.setFont(font)

        return style
    }

    fun setAutoSize(row: Row) {
        for (colNum in 0 until row.lastCellNum) {
            row.sheet.autoSizeColumn(colNum)
        }
    }

    data class RowAndColumn(var row: Int, var column: Int)

}
