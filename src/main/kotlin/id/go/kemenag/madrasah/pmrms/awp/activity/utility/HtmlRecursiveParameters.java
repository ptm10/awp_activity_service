package id.go.kemenag.madrasah.pmrms.awp.activity.utility;

import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STNumberFormat;

import java.math.BigInteger;


public class HtmlRecursiveParameters {
    private BigInteger bulletNumberID;
    private STNumberFormat.Enum numberFormat;
    private int bulletFontSize;
    private String bulletNumberSymbol;
    private XWPFTable table;
    private XWPFTableRow row;
    private XWPFTableCell cell;
    private int rowIndex;
    private int rowCellIndex;
    private XWPFParagraph lastParagraph;

    public HtmlRecursiveParameters setBulletNumberID(BigInteger bulletNumberID) {
        this.bulletNumberID = bulletNumberID;
        return this;
    }

    public HtmlRecursiveParameters setBulletFontSize(int bulletFontSize) {
        this.bulletFontSize = bulletFontSize;
        return this;
    }

    public HtmlRecursiveParameters setTable(XWPFTable table) {
        this.table = table;
        return this;
    }

    public  HtmlRecursiveParameters setRow(XWPFTableRow row) {
        this.row = row;
        return this;
    }

    public  HtmlRecursiveParameters setNumberFormat(STNumberFormat.Enum numberFormat) {
        this.numberFormat = numberFormat;
        return this;
    }

    public  HtmlRecursiveParameters setBulletNumberSymbol(String bulletNumberSymbol) {
        this.bulletNumberSymbol = bulletNumberSymbol;
        return this;
    }

    public BigInteger getBulletNumberID() {
        return bulletNumberID;
    }

    public int getBulletFontSize() {
        return bulletFontSize;
    }

    public XWPFTable getTable() {
        return table;
    }

    public XWPFTableRow getRow() {
        return row;
    }

    public STNumberFormat.Enum getNumberFormat() {
        return numberFormat;
    }

    public String getBulletNumberSymbol() {
        return bulletNumberSymbol;
    }

    public XWPFTableCell getCell() {
        return cell;
    }

    public HtmlRecursiveParameters setCell(XWPFTableCell cell) {
        this.cell = cell;
        return this;
    }

    public int getRowIndex() {
        return rowIndex;
    }

    public HtmlRecursiveParameters setRowIndex(int rowIndex) {
        this.rowIndex = rowIndex;
        return this;
    }

    public int getRowCellIndex() {
        return rowCellIndex;
    }

    public HtmlRecursiveParameters setRowCellIndex(int rowCellIndex) {
        this.rowCellIndex = rowCellIndex;
        return this;
    }

    public XWPFParagraph getLastParagraph() {
        return lastParagraph;
    }

    public HtmlRecursiveParameters setLastParagraph(XWPFParagraph lastParagraph) {
        this.lastParagraph = lastParagraph;
        return this;
    }
}
