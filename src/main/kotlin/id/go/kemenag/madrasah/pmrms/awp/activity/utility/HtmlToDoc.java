package id.go.kemenag.madrasah.pmrms.awp.activity.utility;

import org.apache.poi.xwpf.usermodel.*;
import org.apache.xmlbeans.XmlCursor;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTAbstractNum;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTLvl;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STNumberFormat;

import java.math.BigInteger;
import java.util.List;

public class HtmlToDoc {
    public static XWPFParagraph handleHtmlRecursive(Elements elements, XWPFTableCell cell, XWPFParagraph paragraph, XWPFRun run, HtmlRecursiveParameters parameters) {
        HtmlRecursiveParameters params = parameters;
        for (Element e : elements) {
            processElement(e, cell, paragraph, run, params);
        }
        if (params != null && params.getLastParagraph() != null) return params.getLastParagraph();
        else return paragraph;
    }

    private static BigInteger getNumId(XWPFDocument document, HtmlRecursiveParameters param) {
        CTAbstractNum cTAbstractNum = CTAbstractNum.Factory.newInstance();

        XWPFNumbering numbering = document.createNumbering();

        BigInteger id = BigInteger.valueOf(0);
        boolean found = false;
        while (!found) {
            Object o = numbering.getAbstractNum(id);
            found = (o == null);
            if (!found) id = id.add(BigInteger.ONE);
        }
        cTAbstractNum.setAbstractNumId(id);

        //Bullet list
        CTLvl cTLvl = cTAbstractNum.addNewLvl();
        cTLvl.setIlvl(BigInteger.valueOf(0)); // set indent level 0
        STNumberFormat.Enum numberFormat = param.getNumberFormat();
        cTLvl.addNewNumFmt().setVal(numberFormat);
        cTLvl.addNewLvlText().setVal(param.getBulletNumberSymbol());
        if (numberFormat == STNumberFormat.DECIMAL) cTLvl.addNewStart().setVal(BigInteger.valueOf(1));

        XWPFAbstractNum abstractNum = new XWPFAbstractNum(cTAbstractNum);
        BigInteger abstractNumID = numbering.addAbstractNum(abstractNum);
        BigInteger numID = numbering.addNum(abstractNumID);
        return numID;
    }

    private static void handleHtmlList(XWPFParagraph paragraph, HtmlRecursiveParameters param) {
        paragraph.setNumID(param.getBulletNumberID());
        paragraph.getCTP().getPPr().addNewRPr().addNewSz().setVal(BigInteger.valueOf(param.getBulletFontSize()));
        // set indents in Twips (twentieth of an inch point, 1440 Twips = 1 inch
        paragraph.setIndentFromLeft(1440 / 4); // indent from left 360 Twips = 1/4 inch
        paragraph.setIndentationHanging(1440 / 4); // indentation hanging 360 Twips = 1/4 inch
    }

    private static XWPFParagraph processElement(Element e, XWPFTableCell cell, XWPFParagraph paragraph, XWPFRun run, HtmlRecursiveParameters params) {

        XWPFRun pRun = run;
        XWPFParagraph pParagraph = paragraph;
        Elements children = e.children();
        String tagName = e.tagName();
        XWPFDocument doc = paragraph.getDocument();
        if ("p".equals(tagName)) {
            if (cell == null) {
                XmlCursor cursor = paragraph.getCTP().newCursor();
                cursor.toEndToken();
                while (cursor.toNextToken() != XmlCursor.TokenType.START) ;
                pParagraph = doc.insertNewParagraph(cursor);
                params.setLastParagraph(pParagraph);
//                int paragraphPos = doc.getPosOfParagraph(paragraph);
//                if( paragraphPos < doc.getParagraphs().size())
//                    doc.setParagraph(pParagraph, paragraphPos);
            } else pParagraph = cell.addParagraph();
            pRun = pParagraph.createRun();
        } else if ("b".equals(tagName) || "strong".equals(tagName)) {
//            pRun = pParagraph.createRun();
            pRun.setBold(true);
        } else if ("u".equals(tagName)) {
//            pRun = pParagraph.createRun();
            pRun.setUnderline(UnderlinePatterns.SINGLE);

        } else if ("i".equals(tagName)) {
//            pRun = pParagraph.createRun();
            pRun.setItalic(true);
        } else if ("ul".equals(tagName)) {
            XWPFTableCell tCell = cell;
            if (isInTable(e)) tCell = params.getCell();
            params = new HtmlRecursiveParameters().setNumberFormat(STNumberFormat.BULLET).setBulletNumberSymbol("•").setBulletFontSize(24).setCell(tCell);

            BigInteger numId = getNumId(pParagraph.getDocument(), params);
            params.setBulletNumberID(numId);

        } else if ("ol".equals(tagName)) {
            XWPFTableCell tCell = cell;
            if (isInTable(e)) tCell = params.getCell();
            params = new HtmlRecursiveParameters().setNumberFormat(STNumberFormat.DECIMAL).setBulletNumberSymbol("%1. ").setBulletFontSize(18).setCell(tCell);

            BigInteger numId = getNumId(pParagraph.getDocument(), params);
            params.setBulletNumberID(numId);

        } else if ("li".equals(tagName)) {
            if (params.getCell() != null) pParagraph = params.getCell().addParagraph();
            else {
                XmlCursor cursor = pParagraph.getCTP().newCursor();
                cursor.toEndToken();
                while (cursor.toNextToken() != XmlCursor.TokenType.START) ;
                pParagraph = paragraph.getDocument().insertNewParagraph(cursor);
                params.setLastParagraph(paragraph);
            }
            handleHtmlList(pParagraph, params);
            pRun = pParagraph.createRun();
        } else if ("center".equals(tagName)) {
            pParagraph.setAlignment(ParagraphAlignment.CENTER);
        } else if ("table".equals(tagName)) {
            XWPFTable table;
            if (cell == null) table = pParagraph.getDocument().insertNewTbl(pParagraph.getCTP().newCursor());
            else {
                pParagraph = cell.addParagraph();
                pRun = pParagraph.createRun();
                table = cell.insertNewTbl(pParagraph.getCTP().newCursor());

                table.setBottomBorder(XWPFTable.XWPFBorderType.SINGLE, 1, 0, "000000");
                table.setRightBorder(XWPFTable.XWPFBorderType.SINGLE, 1, 0, "000000");
                table.setTopBorder(XWPFTable.XWPFBorderType.SINGLE, 1, 0, "000000");
                table.setLeftBorder(XWPFTable.XWPFBorderType.SINGLE, 1, 0, "000000");
                table.setInsideHBorder(XWPFTable.XWPFBorderType.SINGLE, 1, 0, "000000");
                table.setInsideVBorder(XWPFTable.XWPFBorderType.SINGLE, 1, 0, "000000");
            }


            table.setCellMargins(50, 100, 50, 100);
            params = new HtmlRecursiveParameters().setTable(table).setRowIndex(-1);
        } else if ("tr".equals(tagName)) {
            int rowIndex = params.getRowIndex();
            rowIndex++;
            XWPFTableRow row = null;
            if (rowIndex == 0) {
                row = params.getTable().getRow(0);

            }
            if (row == null) row = params.getTable().createRow();
            params.setRow(row).setRowIndex(rowIndex).setRowCellIndex(0);
        } else if ("td".equals(tagName)) {
            int rowCellIndex = params.getRowCellIndex();
            XWPFTableCell tCell = null;
            if (params.getRowIndex() == 0) {
                if (rowCellIndex == 0) {
                    tCell = params.getRow().getCell(0);
                }

                if (tCell == null) {
                    tCell = params.getRow().addNewTableCell();
                }

            } else {
                tCell = params.getRow().getCell(rowCellIndex);
            }

            rowCellIndex++;
            params.setRowCellIndex(rowCellIndex).setCell(tCell);

//            tCell.removeParagraph(0);
            try {
                pParagraph = tCell.getParagraphArray(0);
                pRun = pParagraph.createRun();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
//
        if (children.isEmpty()) pRun.setText(e.text());
        else {
            List<TextNode> textNodes = e.textNodes();
            int textNodesSize = textNodes.size();
            if (textNodesSize == 0) {
                handleHtmlRecursive(children, cell, pParagraph, pRun, params);
            } else {
                int index = 0;
                int childIndex = 0;
                int textNodeIndex = 0;
                boolean done;
                TextNode textNode = textNodes.get(textNodeIndex);
                Element child = children.get(childIndex);
                do {
                    if (textNode.siblingIndex() == index) {
                        pRun.setText(textNode.text());
                        textNodeIndex++;
                        if (textNodeIndex < textNodesSize) {
                            textNode = textNodes.get(textNodeIndex);
                        }
                    } else {
                        processElement(child, cell, pParagraph, pRun, params);
                        childIndex++;
                        if (childIndex < children.size()) child = children.get(childIndex);
                    }
                    index++;
                    pRun = pParagraph.createRun();
                    done = childIndex == children.size() && textNodeIndex == textNodesSize;
                } while (!done);
            }
        }
        if (params != null && params.getLastParagraph() != null) return params.getLastParagraph();
        else return paragraph;
    }

    private static boolean isInTable(Element e) {
        Elements parents = e.parents();
        for (Element parent : parents) {
            if (parent.tagName().equals("table")) return true;
        }
        return false;
    }
}
