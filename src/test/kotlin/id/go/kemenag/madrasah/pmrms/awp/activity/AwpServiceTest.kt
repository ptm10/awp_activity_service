package id.go.kemenag.madrasah.pmrms.awp.activity

import id.go.kemenag.madrasah.pmrms.awp.activity.model.request.Pagination2Request
import id.go.kemenag.madrasah.pmrms.awp.activity.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.awp.activity.service.AwpService
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.MethodOrderer
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestMethodOrder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity

@Suppress("UNCHECKED_CAST")
@TestMethodOrder(MethodOrderer.MethodName::class)
@SpringBootTest
class AwpServiceTest {

    @Autowired
    private lateinit var service: AwpService

    @Test
    @DisplayName("Test Datatable")
    fun test1() {
        println("Test Datatable")
        val test: ResponseEntity<ReturnData> =
            service.datatable(Pagination2Request())
        Assertions.assertThat(test.statusCode).isNotEqualTo(HttpStatus.INTERNAL_SERVER_ERROR)
    }

    @Test
    @DisplayName("Test List Year")
    fun test2() {
        println("Test List Year")
        val test: ResponseEntity<ReturnData> =
            service.listYear()
        Assertions.assertThat(test.statusCode).isNotEqualTo(HttpStatus.INTERNAL_SERVER_ERROR)
    }

    @Test
    @DisplayName("Test Get Detail")
    fun test3() {
        println("Test Get Detail")
        val test: ResponseEntity<ReturnData> =
            service.getDetail("861f5e16-7456-11ec-bc82-0242ac110010")
        Assertions.assertThat(test.statusCode).isNotEqualTo(HttpStatus.INTERNAL_SERVER_ERROR)
    }
}
