package id.go.kemenag.madrasah.pmrms.awp.activity

import id.go.kemenag.madrasah.pmrms.awp.activity.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.awp.activity.service.ComponentService
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.MethodOrderer
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestMethodOrder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

@Suppress("UNCHECKED_CAST")
@TestMethodOrder(MethodOrderer.MethodName::class)
@SpringBootTest
class ComponentServiceTest {

    @Autowired
    private lateinit var service: ComponentService

    @Test
    @DisplayName("Test Group List")
    fun test1() {
        println("Test Group List")
        val dform: DateFormat = SimpleDateFormat("yyyy")
        val test: ResponseEntity<ReturnData> = service.groupList(dform.format(Date()).toInt())
        Assertions.assertThat(test.statusCode).isNotEqualTo(HttpStatus.INTERNAL_SERVER_ERROR)
    }

}
